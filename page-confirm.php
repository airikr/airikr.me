<?php

	if(isset($_GET['del'])) {

		require_once 'site-settings.php';

		$get_filename = safetag($_GET['nam']);

		unlink($dir_files.'/uploads/'.$get_filename.'.webp');
		unlink($dir_files.'/uploads/'.$get_filename.'-thumb.webp');
		unlink($dir_files.'/uploads/'.$get_filename.'-thumb-list.webp');

		header("Location: ".url('uploads'));
		exit;



	} else {

		require_once 'site-header.php';



		$get_filename = safetag($_GET['nam']);
		$get_type = safetag($_GET['typ']);

		if($get_type == 'photo') {
			$file = $dir_files.'/uploads/'.$get_filename.'.webp';

		} elseif($get_type == 'cover') {
			$file = $dir_files.'/uploads/cover/'.$get_filename.'.webp';

		} elseif($get_type == 'track') {
			$file = $dir_files.'/tracks/'.$get_filename.'.gpx';
		}







		if($is_loggedin == false) {
			header("Location: ".url('admin?prev=uploaded'));
			exit;


		} else {
			echo '<section id="confirm">';
				echo '<h1>'.$lang['pages']['uploaded']['title'][$get_type].'</h1>';

				if($get_type != 'track') {
					echo '<div class="image" style="background-image: url('.url('photo-thumb:'.$get_filename).');"></div>';
				}

				echo '<div class="item side-by-side">';
					echo '<div class="value">';
						echo $lang['pages']['uploaded']['filename'];
					echo '</div>';

					echo '<div class="label">';
						echo $get_filename;
					echo '</div>';
				echo '</div>';

				echo '<div class="item side-by-side">';
					echo '<div class="value">';
						echo $lang['pages']['uploaded']['filesize'];
					echo '</div>';

					echo '<div class="label">';
						echo format_filesize(filesize($file));
					echo '</div>';
				echo '</div>';

				if($get_type == 'photo') {
					echo '<div class="item side-by-side">';
						echo '<div class="value">';
							echo $lang['pages']['uploaded']['tag'];
						echo '</div>';

						echo '<div class="label">';
							echo '[photo='.$get_filename.']';
						echo '</div>';
					echo '</div>';
				}



				echo '<div class="options">';
					echo link_($lang['pages']['uploaded']['options']['json'], url('api/track:'.$get_filename));

					echo '<a href="'.url('uploaded:'.$get_filename.'/'.$get_type.'/confirm-deletion').'" class="delete">';
						echo $lang['pages']['uploaded']['options']['delete'];
					echo '</a>';
				echo '</div>';
			echo '</section>';
		}







		require_once 'site-footer.php';

	}

?>
