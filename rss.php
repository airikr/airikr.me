<?php

	require_once 'site-settings.php';

	header('Content-Type: application/rss+xml;charset=utf-8');

	$get_page = (!isset($_GET['pag']) ? null : safetag($_GET['pag']));

	$languagetag = ((empty($get_lang) OR $get_lang == 'se') ? 'sv-se' : 'en-gb');

	$arr_categories_blog = [
		'Blog', 'Personal', 'Biking', 'Adventure', 'Journal'
	];

	$arr_categories_gallery = [
		'Personal', 'Photography', 'Sharing', 'Gallery'
	];

	$arr_categories_biking = [
		'Personal', 'Biking', 'Adventure'
	];

	$arr_categories_guestbook = [
		'Guestbook', 'People'
	];

	if($get_page == 'blog') {
		$title = $lang['rss']['titles']['blog'];
	} elseif($get_page == 'comments') {
		$title = $lang['rss']['titles']['comments'];
	} elseif($get_page == 'reactions') {
		$title = $lang['rss']['titles']['reactions'];
	} elseif($get_page == 'biking') {
		$title = $lang['rss']['titles']['biking'];
	} elseif($get_page == 'gallery') {
		$title = $lang['rss']['titles']['gallery'];
	} elseif($get_page == 'guestbook') {
		$title = $lang['rss']['titles']['guestbook'];
	}

?><rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<atom:link href="<?php echo $site_url.'/rss'; ?>" rel="self" type="application/rss+xml" />
		<title><![CDATA[<?php echo $site_title.' - '.$title; ?>]]></title>
		<link><?php echo $site_url; ?></link>
		<description><![CDATA[<?php echo $lang['metadata']['descriptions']['rss'][$get_page]; ?>]]></description>
		<language><?php echo $languagetag; ?></language>
		<webMaster><?php echo $config_email.' ('.$config_webmaster.')'; ?></webMaster>
		<copyright><?php echo date('Y').' '.$config_webmaster; ?></copyright><?php
			if($get_page == 'blog') {
				foreach($arr_categories_blog AS $category) {
					echo '<category>'.$category.'</category>';
				}

			} elseif($get_page == 'gallery') {
				foreach($arr_categories_gallery AS $category) {
					echo '<category>'.$category.'</category>';
				}

			} elseif($get_page == 'biking') {
				foreach($arr_categories_biking AS $category) {
					echo '<category>'.$category.'</category>';
				}

			} elseif($get_page == 'guestbook') {
				foreach($arr_categories_guestbook AS $category) {
					echo '<category>'.$category.'</category>';
				}
			}

			if($get_page == 'blog') {
				$posts = glob($dir_files.'/posts/published/*.json');
				natsort($posts);
				foreach(array_reverse($posts) AS $post) {
					$fileinfo = pathinfo($post);
					$post = json_decode(file_get_contents($post), false);
					$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);
					$content = ($get_lang == 'se' ? $post->content->se : $post->content->en);
					$cover = (!file_exists($dir_files.'/uploads/covers/'.$fileinfo['filename'].'.webp') ? null : url('cover:'.$fileinfo['filename']));
		?><item>
			<guid isPermaLink="true"><?php echo $site_url.'/blog/'.$fileinfo['filename']; ?></guid>
			<author><?php echo $config_email.' ('.$config_webmaster.')'; ?></author>
			<title><![CDATA[<?php echo $subject; ?>]]></title>
			<link><?php echo $site_url.'/blog/'.$fileinfo['filename']; ?></link>
			<description><![CDATA[<?php echo ((isset($post->hundreddaystooffload->enabled) AND $post->hundreddaystooffload->enabled == true) ? '<img src="'.url($dir_images.'/100DaysToOffload.webp', true).'">' : (empty($cover) ? '' : '<img src="'.$cover.'">')) . tags($Parsedown->text($content), true); ?>]]></description>
			<pubDate><?php echo date('D, d M Y H:i:s O', strtotime($fileinfo['filename'])); ?></pubDate>
			<source url="<?php echo $site_url.'/rss/blog'; ?>"><?php echo $site_title; ?></source>
			<comments><?php echo $site_url.'/blog/'.$fileinfo['filename'].'#comments'; ?></comments>
		</item><?php
				}



			} elseif($get_page == 'comments') {
				$comments = glob($dir_files.'/posts/comments/*.json');
				natsort($comments);
				foreach(array_reverse($comments) AS $comment) {
					$fileinfo = pathinfo($comment);
					list($dt, $td, $hash) = explode('-', $fileinfo['filename']);

					$data = json_decode(file_get_contents($comment), true);
					$id_post = $dt;
					$id_comment = $data['id'];
					$name = $data['about']['name'];
					$comment = $data['comment'];
					$published = $data['published'];
		?><item>
			<guid isPermaLink="true"><?php echo $site_url.'/blog/'.$id_post.'?lang='.$get_lang.'#comment-'.$id_comment; ?></guid>
			<author><?php echo $name; ?></author>
			<title><![CDATA[<?php echo date('Y-m-d, H:i', $published).': '.$name; ?>]]></title>
			<link><?php echo $site_url.'/blog/'.$id_post.'?lang='.$get_lang.'#comment-'.$id_comment; ?></link>
			<description><![CDATA[<?php echo $Parsedown->text($comment); ?>]]></description>
			<pubDate><?php echo $published; ?></pubDate>
			<source url="<?php echo $site_url.'/rss/comments'; ?>"><?php echo $site_title; ?></source>
		</item><?php
				}



			} elseif($get_page == 'gallery') {
				$photos = glob($dir_files.'/gallery/*.json');
				natsort($photos);
				foreach(array_reverse($photos) AS $photo) {
					$fileinfo = pathinfo($photo);
					$data = json_decode(file_get_contents($photo), true);
					$description = (($get_lang == 'se' ? (empty($data['description']['se']) ? '' : $data['description']['se']) : (empty($data['description']['en']) ? '' : $data['description']['en'])));
					$camera = $data['exif']['camera']['manufacturer'].' '.$data['exif']['camera']['model'];
					$taken = date('Y-m-d, H:i', $data['exif']['taken']);
					$iso = $data['exif']['iso'];
					$aperture = $data['exif']['aperture'];
					$exposure = $data['exif']['exposure'];
					$image = (!file_exists($dir_files.'/gallery/'.$fileinfo['filename'].'.webp') ? null : url('gallery:'.$fileinfo['filename'], true));
					$is_sensitive = $data['sensitive'];
		?><item>
			<guid isPermaLink="true"><?php echo $site_url.'/gallery/photo:'.$fileinfo['filename']; ?></guid>
			<author><?php echo $config_email.' ('.$config_webmaster.')'; ?></author>
			<title><![CDATA[<?php echo $fileinfo['filename'] . ($is_sensitive == false ? '' : ' ('.($get_lang == 'se' ? 'varning: känslig bild' : 'warning: sensitive image').')'); ?>]]></title>
			<link><?php echo $site_url.'/gallery/photo:'.$fileinfo['filename']; ?></link>
			<description><![CDATA[<?php echo '<img src="'.$image.'">'.$description.'<br><br>'.$lang['rss']['camera'].': '.$camera.'<br>'.$lang['rss']['taken'].': '.$taken.'<br>'.$lang['rss']['iso'].': '.$iso.'<br>'.$lang['rss']['aperture'].': '.$aperture.'<br>'.$lang['rss']['exposure'].': '.$exposure; ?>]]></description>
			<pubDate><?php echo date('D, d M Y H:i:s O', strtotime($taken)); ?></pubDate>
			<source url="<?php echo $site_url.'/rss/gallery'; ?>"><?php echo $site_title; ?></source>
		</item><?php
				}



			} elseif($get_page == 'biking') {
				$tracks = glob($dir_files.'/tracks/json/*.json');
				natsort($tracks);
				foreach(array_reverse($tracks) AS $track) {
					$fileinfo = pathinfo($track);
					$data = json_decode(file_get_contents($track), true);
					$occurred = $data['occurred']['date'].', '.$data['occurred']['time'];
					$distance = format_number($data['distance']['km']);
					$movingtime = $data['movingtime']['hours'] . $lang['pages']['biking']['details']['hour'].' '.$data['movingtime']['minutes'] . $lang['pages']['biking']['details']['minute'];
					$speed_top = format_number($data['speed']['top']['kmh']);
					$speed_average = format_number($data['speed']['average']['kmh']);
					$cover = (!file_exists($dir_files.'/tracks/images/'.$data['occurred']['filename'].'.webp') ? null : url('track:'.$data['occurred']['filename'], true));
		?><item>
			<guid isPermaLink="true"><?php echo $site_url.'/biking-track:'.$fileinfo['filename']; ?></guid>
			<author><?php echo $config_email.' ('.$config_webmaster.')'; ?></author>
			<title><![CDATA[<?php echo $occurred; ?>]]></title>
			<link><?php echo $site_url.'/biking-track:'.$fileinfo['filename']; ?></link>
			<description><![CDATA[<?php echo $lang['rss']['occurred'].': '.$occurred.'<br>'.$lang['rss']['distance'].': '.$distance.' km<br>'.$lang['rss']['movingtime'].': '.$movingtime.'<br>'.$lang['rss']['speed-top'].': '.$speed_top.' km/h<br>'.$lang['rss']['speed-average'].': '.$speed_average.' km/h'; ?>]]></description>
			<pubDate><?php echo date('D, d M Y H:i:s O', $fileinfo['filename']); ?></pubDate>
			<source url="<?php echo $site_url.'/rss/biking'; ?>"><?php echo $site_title; ?></source>
		</item><?php
				}



			} elseif($get_page == 'guestbook') {
				$posts = glob($dir_files.'/guestbook/*.json');
				natsort($posts);
				foreach(array_reverse($posts) AS $post) {
					$fileinfo = pathinfo($post);

					$data = json_decode(file_get_contents($post), true);
					$name = $data['name'];
					$message = $data['message'];
					$published = $fileinfo['filename'];
		?><item>
			<guid isPermaLink="true"><?php echo $site_url.'/guestbook?lang='.$get_lang; ?></guid>
			<author><?php echo $name; ?></author>
			<title><![CDATA[<?php echo date('Y-m-d, H:i', $published).': '.$name; ?>]]></title>
			<link><?php echo $site_url.'/guestbook?lang='.$get_lang; ?></link>
			<description><![CDATA[<?php echo $Parsedown->text($message); ?>]]></description>
			<pubDate><?php echo $published; ?></pubDate>
			<source url="<?php echo $site_url.'/rss/guestbooks'; ?>"><?php echo $site_title; ?></source>
		</item><?php
			}
		}
		?>
	</channel>
</rss>