<?php

	require_once 'site-header.php';







	echo '<section id="copyright">';
		echo '<div class="pagelogo">'.svgicon('license-cc').'</div>';
		echo '<h1>'.$lang['pages']['copyright']['title'].'</h1>';

		foreach($lang['pages']['copyright']['content'] AS $content) {
			echo $Parsedown->text($content);
		}


		echo '<div class="creative-commons">';
			foreach($lang['pages']['copyright']['creativecommons'] AS $key => $value) {
				echo '<div class="item side-by-side">';
					echo '<div class="icon">';
						echo svgicon('license-'.$key);
					echo '</div>';

					echo '<div class="text">';
						foreach($value AS $content) {
							echo $Parsedown->text($content);
						}
					echo '</div>';
				echo '</div>';
			}
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
