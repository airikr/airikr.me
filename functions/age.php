<?php

	function age($year, $month, $day, $time = null) {
		if(!is_numeric($year) OR !is_numeric($month) OR !is_numeric($day)) {
			return 'XX';

		} else {
			$diff = date_diff(date_create($year.'-'.$month.'-'.$day . (empty($time) ? null : ' '.$time)), date_create(date('Y-m-d'.(empty($time) ? null : ' Hi'))));
			return $diff->y;
		}
	}

?>
