<?php

    function renderMonth($displayM, $displayY, $arr_blogdays = []) {
        $daysOfWeek = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];

        $dateUtil = new DateTime($displayY.'/'.$displayM.'/01');
        $year = $dateUtil->format('Y');
        $week = $dateUtil->format('W');

        $week_start = new DateTime();
        $week_start->setISODate($year, $week);
        $nextDay = clone $week_start;

        $i = 1;
        $weekdays = 7; // how many days do we display per row
        $currday = 1; // current week day
        $daysno = 36; // number of display dates

        $calendar = '<div>';
            $calendar .= '<div class="month">';
                $calendar .= date_($displayM, 'month');
            $calendar .= '</div>';

            $calendar .= '<table class="event-calendar" cellpadding="0" cellspacing="2">';
                $calendar .= '<tbody>';
                    while($i < $daysno) {
                        if($i == 1) {
                            $calendar .= '<tr>';
                            $calendar .= '<td></td>';
                        }

                        $currday++;
                        if($currday > $weekdays) {
                            $calendar .= '</tr>';
                            $calendar .= '<tr>';
                            $currday = 1;
                        }

                        $nextDay->add(new DateInterval('P1D'));
                        if($nextDay->format('m') == $displayM) {
                            $calendar .= '<td class="day';
                            $calendar .= ($nextDay->format('ymj') == date('ymj') ? ' today' : '');
                            $calendar .= ((in_array($nextDay->format('Y-m-d'), $arr_blogdays) AND $displayM == $nextDay->format('m')) ? ' selected' : '');
                            $calendar .= '"></td>';
                        } else {
                            $calendar .= '<td></td>';
                        }
                        $i++;

                        if($i == $daysno) {
                            $calendar .= '</tr>';
                        }
                    }
                $calendar .= '</tbody>';
            $calendar .= '</table>';
        $calendar .= '</div>';

        return $calendar;
    }

?>
