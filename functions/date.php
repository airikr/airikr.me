<?php

	function date_($timestamp, $format) {
		global $get_dateformat, $get_daysuffix, $lang;

		$months = [
			1 => $lang['months'][1],
			2 => $lang['months'][2],
			3 => $lang['months'][3],
			4 => $lang['months'][4],
			5 => $lang['months'][5],
			6 => $lang['months'][6],
			7 => $lang['months'][7],
			8 => $lang['months'][8],
			9 => $lang['months'][9],
			10 => $lang['months'][10],
			11 => $lang['months'][11],
			12 => $lang['months'][12]
		];

		$days = [
			1 => $lang['days'][1],
			2 => $lang['days'][2],
			3 => $lang['days'][3],
			4 => $lang['days'][4],
			5 => $lang['days'][5],
			6 => $lang['days'][6],
			7 => $lang['days'][7]
		];


		if($format == 'datetime') {
			return date('Y-m-d, H:i', $timestamp);

		} elseif($format == 'date') {
			return date('Y-m-d', $timestamp);

		} elseif($format == 'time') {
			return date('H:i', $timestamp);

		} elseif($format == 'year') {
			return date('Y', $timestamp);

		} elseif($format == 'month') {
			return $months[((is_numeric($timestamp) AND strlen($timestamp) < 3) ? $timestamp : date('n', $timestamp))];

		} elseif($format == 'day-month') {
			return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]);

		} elseif($format == 'month-year') {
			return $months[date('n', $timestamp)].' '.date('Y', $timestamp);

		} elseif($format == 'day-month-year') {
			return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).', '.date('Y', $timestamp);

		} elseif($format == 'day-month-year-time') {
			if($lang['metadata']['language'] == 'se') {
				return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).' '.date('Y', $timestamp).', kl. '.date('H:i', $timestamp);
			} else {
				return $months[date('n', $timestamp)].' '.date('jS', $timestamp).' '.date('Y', $timestamp).' at '.date('g:i a', $timestamp);
			}

		} elseif($format == 'fully-detailed') {
			return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).' '.date('Y', $timestamp).' ('.mb_strtolower($days[date('N', $timestamp)]).'), '.date('H:i (O T)', $timestamp).' ('.mb_strtolower((date('I', $timestamp) == 1 ? $lang['summertime'] : $lang['wintertime'])).')';
		}
	}

?>
