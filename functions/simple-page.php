<?php

	function simplepage($string, $is_email = true, $error = false) {
		global $site_title, $site_favicon;

		$content = '<!DOCTYPE html>';
		$content .= '<html lang="sv">';
			$content .= '<head>';
				$content .= '<title>'.$site_title.'</title>';

				$content .= '<meta charset="UTF-8">';
				$content .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, maximum-scale=3">';

				$content .= '<link rel="icon" href="'.$site_favicon.'">';

				$content .= '<style>';
					$content .= 'html{background-color:#1a1b26;color:#c0caf5;font-family:"Arial",sans-serif;font-size:.98rem;margin:0;padding:0;}';
					$content .= 'body{max-width:700px;line-height:150%;padding:20px 40px;}';
					$content .= 'a{color:#e0af68;text-decoration:underline;}';
					$content .= 'p{hyphens:auto;line-height:1.5rem;margin:0;padding:10px 0;text-align:left;word-wrap:break-word;-moz-hyphens:auto;-webkit-hyphens:auto;-o-hyphens:auto;}';
					$content .= 'h1{font-size:190%;font-weight:700;line-height:160%;margin:0 0 5px 0;padding:0;}';
					$content .= 'code{font-family:"Victor Mono";font-size:90%;line-height:100%;}';
					$content .= 'textarea.base64{background-color:#171721;border:1px solid #414868;border-radius:3px;color:#c0caf5;font-family:"Victor Mono",monospace;font-size:.98rem;height:400px;outline:none;padding:5px 10px;width:calc(100% - (6px * 2));}';
					$content .= '.color-red{color:#e67e80;}';
					$content .= '.error > h1{font-size:124%;margin:0;}';
					$content .= '.error > ul{padding:5px 0;margin:0 0 0 30px;}';
					$content .= '.go-back{margin-top:15px;margin-left:-10px;}';
					$content .= '.go-back > a{padding:10px;text-decoration:none;}';
					$content .= '.message{border-left:1px solid #414868;margin:15px 0;padding:10px 15px;';
					$content .= 'code {background-color:#414868;border-radius:3px;color:#ff9e64;font-family:"Victor Mono",monospace;font-size:.98rem;line-height:var(--lineheight);padding:3px;}';
				$content .= '</style>';
			$content .= '</head>';

			$content .= '<body'.($error == false ? '' : ' class="color-red"').'>';
				$content .= $string;
			$content .= '</body>';
		$content .= '</html>';



		return $content;
	}

?>
