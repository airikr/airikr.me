<?php

	use geertw\IpAnonymizer\IpAnonymizer;

	require_once 'site-settings.php';



	$get_name = (!isset($_GET['nam']) ? null : safetag($_GET['nam']));
	$get_type = (!isset($_GET['typ']) ? null : safetag($_GET['typ']));
	$get_thumb = (!isset($_GET['thb']) ? false : true);
	$get_thumblist = (!isset($_GET['thl']) ? null : true);
	$browser_cache = 2419200;

	if($get_type == 'cover') {
		$image = $dir_files.'/uploads/covers/'.$get_name.'.webp';

	} elseif($get_type == 'photo') {
		$image = $dir_files.'/uploads/'.$get_name . (!empty($get_thumb) ? '-thumb' : (!empty($get_thumblist) ? '-thumb-list' : '')).'.webp';

	} elseif($get_type == 'track') {
		$image = $dir_files.'/tracks/images/'.$get_name.'.webp';

	} elseif($get_type == 'trackthumb') {
		$image = $dir_files.'/tracks/images/'.$get_name.'-thumb.webp';

	} elseif($get_type == 'metadata') {
		$image = 'images/metadata-'.$get_name.'.webp';

	} elseif($get_type == 'gallery' AND $get_thumb == true) {
		$image = $dir_files.'/gallery/photos/'.$get_name.'/256/'.$get_name.'.webp';

	} elseif($get_type == 'gallery' AND $get_thumb == false) {
		$image = $dir_files.'/gallery/photos/'.$get_name.'/800/'.$get_name.'.webp';

	} elseif($get_type == 'galleryview') {
		$image = $dir_files.'/gallery/photos/'.$get_name.'/1500/'.$get_name.'.webp';
		$exif = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$get_name.'/info.json'), false);
		$is_sensitive = $exif->sensitive;
		$has_acceptedsensitive = false;
		$anonymize_ip = new IpAnonymizer();
		$ipaddress = md5($anonymize_ip->anonymize(getip()));

		if(file_exists($dir_files.'/gallery/accepted-sensitive/'.$ipaddress.'-'.$get_name.'.json')) {
			$has_acceptedsensitive = ($is_sensitive == false ? null : (file_exists($dir_files.'/gallery/accepted-sensitive/'.$ipaddress.'-'.$get_name.'.json') ? true : false));
		}

	} elseif($get_type == 'webmention') {
		$image = $dir_files.'/posts/webmentions/*/'.$get_name.'.webp';

	} elseif($get_type == 'screenshot-desktop') {
		$image = $dir_images.'/screenshot-desktop.webp';

	} elseif($get_type == 'screenshot-laptop') {
		$image = $dir_images.'/screenshot-laptop.webp';

	} elseif($get_type == 'videocover') {
		$image = $dir_files.'/videofeed/videos/'.$get_name.'.jpg';

	} elseif($get_type == 'channelavatar') {
		$image = $dir_files.'/videofeed/channels/'.$get_name.'.jpg';
	}



	if(file_exists($image)) {
		if($is_sensitive == true AND $has_acceptedsensitive == false) {
			header("Location: ".url('gallery/photo:'.$get_name));
			exit;

		} elseif($is_sensitive == true AND $has_acceptedsensitive == true OR $is_sensitive == false) {
			$mime = getimagesize($image);

			header('Content-Type: '.$mime['mime']);
			header('Cache-Control: private, max-age='.$browser_cache);
			header('Expires: '.gmdate('D, d M Y H:i:s', time() + $browser_cache).' GMT');
			header('Content-Length: '.filesize($image));
			header('X-Sendfile: '.$image);

			ob_clean();
			echo file_get_contents($image);

			exit;
		}


	} else {
		echo 'File not found';
	}

?>
