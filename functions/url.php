<?php

	function url($string, $noqueries = false, $lang = null, $theme = null, $log = null, $option = null) {
		global $protocol, $host, $uri, $is_local, $config_folder, $get_lang, $get_theme, $get_logging;

		$url_parsed = parse_url($string);
		$url_exploded = (isset($url_parsed['path']) ? explode('/', $url_parsed['path']) : null);
		$url_option = (empty($option) ? null : explode(':', $option));
		$url_hashtag = (isset($url_parsed['fragment']) ? $url_parsed['fragment'] : null);

		$path_folder = (empty($config_folder) ? '/' : '/'.$config_folder.'/');
		$path_queries = '?lang='.(empty($lang) ? $get_lang : $lang);
		$path_queries .= '&thm='.(empty($theme) ? $get_theme : $theme);
		#$path_queries .= '&log='.(empty($log) ? $get_logging : ($log == 'off' ? '0' : $log));
		$path_queries .= (empty($option) ? '' : '&'.$url_option[0].'='.$url_option[1]);

		return $path_folder . strtok($string, '#') . ($noqueries == true ? '' : $path_queries) . (empty($url_hashtag) ? null : '#'.$url_hashtag);
	}

?>