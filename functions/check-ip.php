<?php

	function checkip($ipaddress) {
		global $config_email, $dir_files;

		$checkip = json_decode(@file_get_contents('https://check.getipintel.net/check.php?ip='.$ipaddress.'&contact='.$config_email.'&format=json'), false);


		if(isset($checkip)) {
			if($checkip->status == 'error') {
				if($checkip->result == '-1') {
					$arr_result = [
						'isverified' => false,
						'bypassed' => false,
						'message' => 'no-input',
						'occurred' => time(),
						'verified' => null
					];

				} elseif($checkip->result == '-2') {
					$arr_result = [
						'isverified' => false,
						'bypassed' => false,
						'message' => 'invalid-ip',
						'occurred' => time(),
						'verified' => null
					];

				} elseif($checkip->result == '-3') {
					$arr_result = [
						'isverified' => true,
						'bypassed' => true,
						'message' => 'local',
						'occurred' => time(),
						'verified' => time()
					];

				} elseif($checkip->result == '-4') {
					$arr_result = [
						'isverified' => false,
						'bypassed' => false,
						'message' => 'db-not-reachable',
						'occurred' => time(),
						'verified' => null
					];

				} elseif($checkip->result == '-5') {
					$arr_result = [
						'isverified' => false,
						'bypassed' => false,
						'message' => 'banned',
						'occurred' => time(),
						'verified' => null
					];

				} elseif($checkip->result == '-6') {
					$arr_result = [
						'isverified' => false,
						'bypassed' => false,
						'message' => 'no-contact-info',
						'occurred' => time(),
						'verified' => null
					];

				} else {
					$arr_result = [
						'isverified' => true,
						'bypassed' => true,
						'message' => 'nothing',
						'occurred' => time(),
						'verified' => time()
					];
				}



			} else {
				if($checkip->result >= 0.99) {
					$arr_result = [
						'isverified' => false,
						'bypassed' => false,
						'message' => 'most-likely-proxy',
						'occurred' => time(),
						'verified' => null
					];

				} elseif($checkip->result >= 0.95) {
					$arr_result = [
						'isverified' => false,
						'bypassed' => false,
						'message' => 'needs-checking',
						'occurred' => time(),
						'verified' => null
					];

				} elseif($checkip->result > 0.50 AND $checkip->result < 0.95) {
					$arr_result = [
						'isverified' => true,
						'bypassed' => true,
						'message' => 'low-risk',
						'occurred' => time(),
						'verified' => time()
					];

				} elseif($checkip->result == 0.50) {
					$arr_result = [
						'isverified' => true,
						'bypassed' => true,
						'message' => 'fifty-fifty',
						'occurred' => time(),
						'verified' => time()
					];

				} elseif($checkip->result < 0.50) {
					$arr_result = [
						'isverified' => true,
						'bypassed' => true,
						'message' => 'no-risk',
						'occurred' => time(),
						'verified' => time()
					];

				} else {
					if($checkip->BadIP == 1) {
						$arr_result = [
							'isverified' => false,
							'bypassed' => false,
							'message' => 'bad-ip',
							'occurred' => time(),
							'verified' => null
						];
					}
				}
			}


			file_put_contents($dir_files.'/visitors/'.hash('sha256', $ipaddress).'.json', json_encode($arr_result));
		}
	}

?>
