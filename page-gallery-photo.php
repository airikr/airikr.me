<?php

	use Intervention\Image\ImageManager;
	use Intervention\Image\Drivers\Imagick\Driver;
	use geertw\IpAnonymizer\IpAnonymizer;



	if(isset($_POST['upload'])) {

		require_once 'site-settings.php';

		$get_filename = safetag($_GET['nam']);

		$file_tmp = $_FILES['file']['tmp_name'];
		$file_name = $_FILES['file']['name'];
		$file_info = pathinfo($file_name);

		$file_name_cleaned = str_replace('-Pano', '', $file_info['filename']);
		$file_name_old = $dir_files.'/gallery/photos/'.$file_name;
		$file_name_800 = $dir_files.'/gallery/photos/'.$get_filename.'/800/'.$file_name_cleaned.'.webp';
		$file_name_1500 = $dir_files.'/gallery/photos/'.$get_filename.'/1500/'.$file_name_cleaned.'.webp';
		$file_name_256 = $dir_files.'/gallery/photos/'.$get_filename.'/256/'.$file_name_cleaned.'.webp';

		$post_description_se = (empty($_POST['field-description-se']) ? null : safetag($_POST['field-description-se']));
		$post_description_en = (empty($_POST['field-description-en']) ? null : safetag($_POST['field-description-en']));

		$post_check_sensitive = (isset($_POST['check-sensitive']) ? true : false);


		if(empty($file_tmp)) {
			die(simplepage('Please select a file first. <a href="'.url('upload').'">Go back</a>'));

		} elseif($file_info['extension'] == 'jpg' AND !move_uploaded_file($file_tmp, $file_name_old)) {
			die(simplepage('The file has not been uploaded. Please try again. <a href="'.url('upload').'">Go back</a>'));

		} else {
			if($file_info['extension'] == 'jpg') {
				$manager = new ImageManager(['driver' => 'imagick']);
				$exif = exif_read_data($file_name_old);
				$iscamera = false;
				$isphone = false;

				if(mb_strtolower($exif['Make']) == 'sony') {
					$iscamera = true;
				} elseif(mb_strtolower($exif['Make']) == 'panasonic') {
					$iscamera = true;
				} elseif(mb_strtolower($exif['Make']) == 'google') {
					$isphone = true;
				}

				$arr_info = [
					'uploaded' => time(),
					'description' => [
						'se' => $post_description_se,
						'en' => $post_description_en
					],
					'phone' => $isphone,
					'camera' => $iscamera,
					'sensitive' => $post_check_sensitive,
					'exif' => [
						'taken' => strtotime($exif['DateTimeOriginal']),
						'camera' => [
							'manufacturer' => $exif['Make'],
							'model' => $exif['Model']
						],
						'iso' => (int)$exif['ISOSpeedRatings'],
						'aperture' => $exif['COMPUTED']['ApertureFNumber'],
						'exposure' => $exif['ExposureTime']
					]
				];

				file_put_contents($dir_files.'/gallery/photos/'.$get_filename.'/'.$file_name_cleaned.'.json', json_encode($arr_info));

				$manager->make($file_name_old)->resize(1500, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->encode('webp', 70)->save($file_name_1500);

				$img = new Imagick($file_name_1500);
				$img->stripImage();
				$img->writeImage($file_name_1500);
				#$img->setImageProperty('keywords', 'airikr');
				#die($img->getImageProperty('keywords'));
				$img->clear();
				$img->destroy();

				$manager->make($file_name_1500)->resize(800, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->save($file_name_800);

				$manager->make($file_name_800)->resize(256, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->save($file_name_256);

				unlink($file_name_old);
			}


			header("Location: ".url('gallery/photo:'.$file_info['filename']));
			exit;
		}



	} elseif(isset($_POST['save'])) {

		require_once 'site-settings.php';

		if($is_loggedin == false) {
			header("Location: ".url('gallery/photo:'.$get_photo));
			exit;

		} else {
			$post_description_se = (empty($_POST['field-description-se']) ? null : safetag($_POST['field-description-se']));
			$post_description_en = (empty($_POST['field-description-en']) ? null : safetag($_POST['field-description-en']));
			$post_taken = (empty($_POST['field-taken']) ? null : safetag($_POST['field-taken']));
			$post_camera_manufacturer = (empty($_POST['field-camera-manufacturer']) ? null : safetag($_POST['field-camera-manufacturer']));
			$post_camera_model = (empty($_POST['field-camera-model']) ? null : safetag($_POST['field-camera-model']));
			$post_iso = (empty($_POST['field-iso']) ? null : safetag($_POST['field-iso']));
			$post_aperture = (empty($_POST['field-aperture']) ? null : safetag($_POST['field-aperture']));
			$post_exposure = (empty($_POST['field-exposure']) ? null : safetag($_POST['field-exposure']));

			$post_check_isphone = (isset($_POST['check-isphone']) ? true : false);
			$post_check_iscamera = (isset($_POST['check-iscamera']) ? true : false);
			$post_check_sensitive = (isset($_POST['check-sensitive']) ? true : false);

			$get_photo = (!isset($_GET['nam']) ? null : safetag($_GET['nam']));


			if(empty($post_taken) OR empty($post_camera_manufacturer) OR empty($post_camera_model)) {
				die(simplepage('Vänligen fyll i alla obligatoriska textfälten först. <a href="'.url('gallery/photo:'.$get_photo.'/edit').'">Gå tillbaka</a>'));

			} else {
				$exif = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$get_photo.'/'.$get_photo.'.json'), false);

				$arr_info = [
					'uploaded' => $exif->uploaded,
					'edited' => time(),
					'description' => [
						'se' => $post_description_se,
						'en' => $post_description_en
					],
					'phone' => $post_check_isphone,
					'camera' => $post_check_iscamera,
					'sensitive' => $post_check_sensitive,
					'exif' => [
						'taken' => strtotime($post_taken),
						'camera' => [
							'manufacturer' => $post_camera_manufacturer,
							'model' => $post_camera_model
						],
						'iso' => (int)$post_iso,
						'aperture' => $post_aperture,
						'exposure' => $post_exposure
					]
				];

				file_put_contents($dir_files.'/gallery/photos/'.$get_photo.'/'.$get_photo.'.json', json_encode($arr_info));

				header("Location: ".url('gallery/photo:'.$get_photo));
				exit;
			}
		}



	} elseif(isset($_GET['pag']) AND strip_tags(htmlspecialchars($_GET['pag'])) == 'delete') {

		require_once 'site-settings.php';

		$get_photo = safetag($_GET['nam']);
		$get_page = safetag($_GET['pag']);

		if($is_loggedin == false) {
			header("Location: ".url('gallery/photo:'.$get_photo));
			exit;

		} else {
			function delete_dir($folder) {
				foreach(new DirectoryIterator($folder) AS $f) {
					if($f->isDot()) continue;
					if($f->isFile()) {
						unlink($f->getPathname());

					} elseif($f->isDir()) {
						delete_dir($f->getPathname());
					}
				}

				rmdir($folder);
			}

			delete_dir($dir_files.'/gallery/photos/'.$get_photo);

			header("Location: ".url('gallery'));
			exit;
		}



	} elseif(isset($_GET['pag']) AND strip_tags(htmlspecialchars($_GET['pag'])) == 'sensitive') {

		require_once 'site-settings.php';

		$anonymize_ip = new IpAnonymizer();
		$get_photo = safetag($_GET['nam']);
		$get_type = safetag($_GET['typ']);
		$ipaddress = md5($anonymize_ip->anonymize(getip()));

		if($get_type == 'show') {
			$arr = [
				'opened' => time()
			];

			file_put_contents($dir_files.'/gallery/accepted-sensitive/'.$ipaddress.'-'.$get_photo.'.json', json_encode($arr));

		} elseif($get_type == 'hide') {
			unlink($dir_files.'/gallery/accepted-sensitive/'.$ipaddress.'-'.$get_photo.'.json');
		}

		header("Location: ".url('gallery/photo:'.$get_photo));
		exit;



	} elseif(isset($_GET['pag']) AND strip_tags(htmlspecialchars($_GET['pag'])) == 'like') {

		require_once 'site-settings.php';

		$anonymize_ip = new IpAnonymizer();
		$get_photo = safetag($_GET['nam']);
		$get_type = safetag($_GET['typ']);
		$ipaddress = md5($anonymize_ip->anonymize(getip()));

		if($get_type == 'like') {
			$arr = [
				'liked' => time()
			];

			file_put_contents($dir_files.'/gallery/likes/'.$get_photo.'-'.$ipaddress.'.json', json_encode($arr));

		} elseif($get_type == 'undo-like') {
			unlink($dir_files.'/gallery/likes/'.$get_photo.'-'.$ipaddress.'.json');
		}

		header("Location: ".url('gallery/photo:'.$get_photo));
		exit;



	} else {

		require_once 'site-header.php';



		$anonymize_ip = new IpAnonymizer();
		$get_filename = (!isset($_GET['nam']) ? null : safetag($_GET['nam']));
		$is_deleting = (!isset($_GET['del']) ? null : true);
		$is_editing = (!isset($_GET['pag']) ? null : (strip_tags(htmlspecialchars(($_GET['pag']))) == 'edit' ? true : false));
		$visitor_liked = false;
		$count_likes = 0;

		$dir_empty = glob($dir_files.'/gallery/photos/*');

		foreach(glob($dir_files.'/gallery/likes/'.$get_filename.'-*.json') AS $likes) {
			$fileinfo = pathinfo($likes);
			$count_likes++;

			if(strpos($fileinfo['filename'], md5($anonymize_ip->anonymize(getip()))) !== false) {
				list($fn, $ip) = explode('-', $fileinfo['filename']);
				$visitor_liked = true;
			}
		}







		if(!file_exists($dir_files.'/gallery/photos/'.$get_filename.'/800/'.$get_filename.'.webp')) {
			echo '<section id="photo">';
				echo '<h1>';
					echo '<a href="'.url('gallery').'">';
					echo $lang['pages']['gallery']['title'];
					echo '</a>';
					echo svgicon('chevron-right');
					echo '404';
				echo '</h1>';

				echo '<p>Fotografiet kunde inte hittas. Vänligen gå tillbaka till galleriet och välj ett annat.</p>';
			echo '</section>';



		} elseif($is_loggedin == true AND !empty($is_editing)) {
			$exif = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$get_filename.'/'.$get_filename.'.json'), false);

			echo '<section id="edit">';
				echo '<h1>';
					echo '<a href="'.url('gallery').'">';
						echo $lang['pages']['gallery']['title'];
					echo '</a>';

					echo svgicon('chevron-right');

					echo '<a href="'.url('gallery/photo:'.$get_filename).'">';
						echo (empty($exif->exif->taken) ? '' : date('Y-m-d H:i', $exif->exif->taken));
					echo '</a>';

					echo svgicon('chevron-right');
					echo $lang['pages']['gallery']['edit']['title'];
				echo '</h1>';


				echo '<div class="side-by-side">';
					echo '<div class="photo">';
						echo '<div class="image" style="background-image: url('.url('gallery:'.$get_filename, true).');"></div>';
					echo '</div>';


					echo '<form action="'.url('gallery/photo:'.$get_filename, true).'" method="POST" autocomplete="off" novalidate>';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['edit']['form']['taken'];
							echo '</div>';

							echo '<div class="value">';
								echo '<input type="datetime" name="field-taken" value="';
								echo (empty($exif->exif->taken) ? '' : date('Y-m-d H:i', $exif->exif->taken));
								echo '" placeholder="'.$lang['pages']['gallery']['form']['placeholders']['required'].'">';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['edit']['form']['camera'];
							echo '</div>';

							echo '<div class="value side-by-side">';
								echo '<input type="text" name="field-camera-manufacturer" value="';
								echo (empty($exif->exif->camera->manufacturer) ? '' : $exif->exif->camera->manufacturer);
								echo '" placeholder="'.$lang['pages']['gallery']['form']['placeholders']['required'].'">';

								echo '<input type="text" name="field-camera-model" value="';
								echo (empty($exif->exif->camera->model) ? '' : $exif->exif->camera->model);
								echo '" placeholder="'.$lang['pages']['gallery']['form']['placeholders']['required'].'">';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['edit']['form']['iso'];
							echo '</div>';

							echo '<div class="value">';
								echo '<input type="text" inputmode="numeric" pattern="[0-9]*" name="field-iso" value="';
								echo (empty($exif->exif->iso) ? '' : $exif->exif->iso);
								echo '">';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['edit']['form']['aperture'];
							echo '</div>';

							echo '<div class="value">';
								echo '<input type="text" name="field-aperture" value="';
								echo (empty($exif->exif->aperture) ? '' : $exif->exif->aperture);
								echo '">';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['edit']['form']['exposure'];
							echo '</div>';

							echo '<div class="value">';
								echo '<input type="text" name="field-exposure" value="';
								echo (empty($exif->exif->exposure) ? '' : $exif->exif->exposure);
								echo '">';
							echo '</div>';
						echo '</div>';

						echo '<div class="item description">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['form']['description']['se'];
							echo '</div>';

							echo '<div class="value">';
								echo '<textarea name="field-description-se">';
									echo (empty($exif->description->se) ? '' : $exif->description->se);
								echo '</textarea>';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['form']['description']['en'];
							echo '</div>';

							echo '<div class="value">';
								echo '<textarea name="field-description-en">';
									echo (empty($exif->description->en) ? '' : $exif->description->en);
								echo '</textarea>';
							echo '</div>';
						echo '</div>';


						echo '<div class="checkboxes">';
							echo checkbox('Fotografierades med en telefon', 'isphone', null, ($exif->phone == true ? true : false));
							echo checkbox('Fotografierades med en kamera', 'iscamera', null, ($exif->camera == true ? true : false));
							echo checkbox('Markera fotografiet som känsligt', 'sensitive', null, ($exif->sensitive == true ? true : false));
						echo '</div>';


						echo '<div class="button">';
							echo '<input type="submit" name="save" value="'.$lang['pages']['gallery']['edit']['form']['button'].'">';
						echo '</div>';
					echo '</form>';
				echo '</div>';
			echo '</section>';



		} elseif($is_loggedin == true AND !empty($is_deleting)) {
			echo '<section id="delete">';
				echo '<h1>';
					echo '<a href="'.url('gallery').'">';
						echo $lang['pages']['gallery']['title'];
					echo '</a>';

					echo svgicon('chevron-right');

					echo '<a href="'.url('gallery/photo:'.$get_filename).'">';
						echo (empty($exif->exif->taken) ? '' : date('Y-m-d H:i', $exif->exif->taken));
					echo '</a>';

					echo svgicon('chevron-right');
					echo $lang['pages']['gallery']['delete']['title'];
				echo '</h1>';

				foreach($lang['pages']['gallery']['delete']['content']['delete'] AS $content) {
					echo $Parsedown->text($content);
				}

				echo '<div class="options">';
					echo '<div><a href="'.url('gallery/photo:'.$get_filename.'/confirm-deletion').'" class="delete">';
						echo svgicon('trash');
						echo $lang['pages']['gallery']['delete']['options']['yes'];
					echo '</a></div>';

					echo '<div><a href="'.url('gallery/photo:'.$get_filename).'" class="regret">';
						echo svgicon('goback');
						echo $lang['pages']['gallery']['delete']['options']['no'];
					echo '</a></div>';
				echo '</div>';
			echo '</section>';



		} else {
			$anonymize_ip = new IpAnonymizer();
			$exif = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$get_filename.'/'.$get_filename.'.json'), false);
			$is_sensitive = $exif->sensitive;
			$has_acceptedsensitive = false;
			$ipaddress = md5($anonymize_ip->anonymize(getip()));
			$arr_photos = [];

			foreach(glob($dir_files.'/gallery/photos/*') AS $photo) {
				$file_info = pathinfo($photo);
				$exif_looped = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$file_info['filename'].'/'.$file_info['filename'].'.json'), false);

				$arr_photos[] = [
					'name' => $file_info['filename'],
					'uploaded' => $exif_looped->uploaded
				];
			}

			if(file_exists($dir_files.'/gallery/accepted-sensitive/'.$ipaddress.'-'.$get_filename.'.json')) {
				$has_acceptedsensitive = ($is_sensitive == false ? false : (file_exists($dir_files.'/gallery/accepted-sensitive/'.$ipaddress.'-'.$get_filename.'.json') ? true : false));
			}

			usort($arr_photos, function($a, $b) {
				$ad = new DateTime(date('Y-m-d H:i:s', $a['uploaded']));
				$bd = new DateTime(date('Y-m-d H:i:s', $b['uploaded']));

				if($ad == $bd) {
					return 0;
				}

				return $ad > $bd ? -1 : 1;
			});

			$current_page = array_search($get_filename, array_column($arr_photos, 'name'));
			$paging_next = (!isset($arr_photos[$current_page + 1]) ? null : $arr_photos[$current_page + 1]);
			$paging_previous = (!isset($arr_photos[$current_page - 1]) ? null : $arr_photos[$current_page - 1]);

			$c_files = count(glob($dir_files.'/gallery/photos/'.$get_filename.'/*.json'));



			echo '<section id="photo">';
				echo '<h1>';
					echo '<a href="'.url('gallery').'">';
						echo $lang['pages']['gallery']['title'];
					echo '</a>';

					echo svgicon('chevron-right');
					echo (empty($exif->exif->taken) ? '' : date('Y-m-d H:i', $exif->exif->taken));
				echo '</h1>';



				/*if($is_loggedin == true) {
					echo '<details>';
						echo '<summary>';
							echo '<div class="open">'.svgicon('details-open').'</div>';
							echo '<div class="close">'.svgicon('details-close').'</div>';
							echo $lang['pages']['gallery']['upload-photo'];
						echo '</summary>';
	
						echo '<form action="'.url('gallery/photo:'.$get_filename, true).'" method="POST" enctype="multipart/form-data" autocomplete="off" novalidate>';
							echo '<input type="hidden" name="hidden-filename" value="'.$get_filename.'">';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['pages']['upload']['choose-image'];
								echo '</div>';
	
								echo '<div class="value">';
									echo '<input type="file" name="file" accept=".jpg">';
								echo '</div>';
							echo '</div>';
	
							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['pages']['gallery']['form']['description']['se'];
								echo '</div>';
	
								echo '<div class="value">';
									echo '<textarea name="field-description-se"></textarea>';
								echo '</div>';
							echo '</div>';
	
							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['pages']['gallery']['form']['description']['en'];
								echo '</div>';
	
								echo '<div class="value">';
									echo '<textarea name="field-description-en"></textarea>';
								echo '</div>';
							echo '</div>';
	
	
							echo '<div class="checkboxes">';
								echo checkbox('Markera fotografiet som känsligt', 'sensitive');
							echo '</div>';
	
	
							echo '<div class="button">';
								echo '<input type="submit" name="upload" value="'.$lang['pages']['gallery']['form']['upload'].'">';
							echo '</div>';
						echo '</form>';
					echo '</details>';
				}*/



				echo '<div class="paging">';
					echo '<div class="previous">';
						if(isset($paging_previous)) {
							echo '<a href="'.url('gallery/photo:'.$paging_previous['name']).'">';
								echo $lang['pages']['gallery']['paging']['previous'];
							echo '</a>';

						} else {
							echo '<div class="empty no-select">';
								echo $lang['pages']['gallery']['paging']['previous'];
							echo '</div>';
						}
					echo '</div>';

					echo '<div class="number">';
						echo ($current_page + 1).' '.mb_strtolower($lang['pages']['gallery']['of']).' '.count($arr_photos);
					echo '</div>';

					echo '<div class="next">';
						if(isset($paging_next)) {
							echo '<a href="'.url('gallery/photo:'.$paging_next['name']).'">';
								echo $lang['pages']['gallery']['paging']['next'];
							echo '</a>';

						} else {
							echo '<div class="empty no-select">';
								echo $lang['pages']['gallery']['paging']['next'];
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';



				echo (($is_sensitive == true AND $has_acceptedsensitive == false) ? '' : '<a href="'.url('gallery-view:'.$get_filename, true).'">');
					echo '<div class="photo" style="background-image: url('.url('gallery:'.$get_filename, true).');">';
						if($is_sensitive == false AND $has_acceptedsensitive == false OR $has_acceptedsensitive == true) {
							echo svgicon('zoom');

						} else {
							if($is_sensitive == true AND $has_acceptedsensitive == false) {
								echo '<div class="sensitive">';
									echo '<div>';
										foreach($lang['pages']['gallery']['sensitive']['warning']['content'] AS $content) {
											echo $Parsedown->text($content);
										}

										if(($get_lang == 'se' ? !empty($exif->description->se) : !empty($exif->description->en))) {
											foreach($lang['pages']['gallery']['sensitive']['warning']['hasdesc'] AS $content) {
												echo $Parsedown->text($content);
											}
										}

										echo '<div class="options">';
											echo '<a href="'.url('gallery/photo:'.$get_filename.'/sensitive/show-image').'">';
												echo svgicon('eye') . $lang['pages']['gallery']['sensitive']['options']['show-image'];
											echo '</a>';

											echo '<div class="info">';
												foreach($lang['pages']['gallery']['sensitive']['gdpr'] AS $content) {
													echo $Parsedown->text($content);
												}
											echo '</div>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							}
						}



						if($c_files > 1) {
							echo '<div class="multiple no-select">';
								echo '<div class="navigate left">';
									echo '<a href="javascript:void(0)">';
										echo svgicon('caret-left');
									echo '</a>';
								echo '</div>';

								echo '<div class="number">1 av '.$c_files.'</div>';

								echo '<div class="navigate right">';
									echo '<a href="javascript:void(0)">';
										echo svgicon('caret-right');
									echo '</a>';
								echo '</div>';
							echo '</div>';
						}
					echo '</div>';
				echo (($is_sensitive == true AND $has_acceptedsensitive == false) ? '' : '</a>');



				if($is_sensitive == true AND $has_acceptedsensitive == true) {
					echo '<div class="options">';
						echo '<a href="'.url('gallery/photo:'.$get_filename.'/sensitive/hide-image').'">';
							echo svgicon('eye-closed') . $lang['pages']['gallery']['sensitive']['options']['hide-image'];
						echo '</a>';
					echo '</div>';
				}



				if($is_loggedin == true) {
					echo '<div class="options">';
						echo '<a href="'.url('gallery/photo:'.$get_filename.'/edit').'">';
							echo svgicon('edit') . $lang['pages']['gallery']['options']['edit'];
						echo '</a>';

						echo '<a href="'.url('gallery/photo:'.$get_filename.'/delete').'" class="delete">';
							echo svgicon('trash') . $lang['pages']['gallery']['options']['delete'];
						echo '</a>';
					echo '</div>';
				}



				if(($get_lang == 'se' ? !empty($exif->description->se) : !empty($exif->description->en))) {
					echo '<div class="description">';
						echo $Parsedown->text(($get_lang == 'se' ? $exif->description->se : $exif->description->en));
					echo '</div>';
				}

				if($badvisitor == false) {
					echo '<div class="likes">';
						if($notchecked == true) {
							echo '<div class="reason">';
								foreach($lang['pages']['gallery']['notchecked'] AS $content) {
									echo $Parsedown->text($content);
								}
							echo '</div>';
						}


						echo '<div class="like-or-not">';
							if($notchecked == false) {
								if($visitor_liked == false) {
									echo svgicon('heart');
									echo '<a href="'.url('gallery/photo:'.$get_filename.'/like').'">';
										echo $lang['pages']['gallery']['likes']['like'];
									echo '</a>';

								} else {
									echo svgicon('heart');
									echo '<a href="'.url('gallery/photo:'.$get_filename.'/undo-like').'">';
										echo $lang['pages']['gallery']['likes']['undo-like'];
									echo '</a>';
								}

								echo ' ('.(empty($count_likes) ? '0' : format_number($count_likes)).')';

							} else {
								echo svgicon('heart');
								echo '<div class="text">';
									echo $lang['pages']['gallery']['likes']['like'];
								echo '</div>';

								echo ' ('.(empty($count_likes) ? '0' : format_number($count_likes)).')';
							}
						echo '</div>';


						echo '<div class="privacy-notice">';
							echo svgicon('privacy');
							echo $Parsedown->text($lang['pages']['gallery']['likes']['gdpr']);
						echo '</div>';
					echo '</div>';
				}



				if(!empty($exif->exif->taken) OR !empty($exif->exif->camera->manufacturer) OR !empty($exif->exif->camera->model) OR !empty($exif->exif->iso) OR !empty($exif->exif->aperture) OR !empty($exif->exif->exposure)) {
					echo '<div class="info">';

						if(!empty($exif->exif->taken) OR !empty($exif->exif->camera->manufacturer) OR !empty($exif->exif->camera->model)) {
							echo '<div class="camera">';
								echo $exif->exif->camera->manufacturer.' '.$exif->exif->camera->model;
							echo '</div>';

							echo '<div class="taken">';
								echo $lang['pages']['gallery']['taken'].' '.date_($exif->exif->taken, 'day-month-year-time');
							echo '</div>';
						}


						if(!empty($exif->exif->iso) OR !empty($exif->exif->aperture) OR !empty($exif->exif->exposure)) {
							echo '<div class="info">';
								echo '<div class="iso">';
									echo '<div class="iso no-select">ISO</div>';
									echo $exif->exif->iso;
								echo '</div>';

								echo '<div class="aperture">';
									echo svgicon('gallery-aperture');
									echo $exif->exif->aperture;
								echo '</div>';

								echo '<div class="exposure">';
									echo svgicon('gallery-exposure');
									echo $exif->exif->exposure;
								echo '</div>';
							echo '</div>';
						}

					echo '</div>';
				}
			echo '</section>';
		}







		require_once 'site-footer.php';

	}

?>
