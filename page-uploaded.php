<?php

	if(isset($_GET['del'])) {

		require_once 'site-settings.php';

		$get_filename = safetag($_GET['nam']);
		$get_type = safetag($_GET['typ']);


		if($get_type == 'photo') {
			unlink($dir_files.'/uploads/'.$get_filename.'.webp');
			unlink($dir_files.'/uploads/'.$get_filename.'-thumb.webp');
			unlink($dir_files.'/uploads/'.$get_filename.'-thumb-list.webp');

		} elseif($get_type == 'cover') {
			unlink($dir_files.'/uploads/covers/'.$get_filename.'.webp');

		} elseif($get_type == 'track') {
			$file = $dir_files.'/tracks/'.urlencode($get_filename).'.gpx';
			$data = simplexml_load_file($file);
			$namespaces = $data->trk->getNamespaces(true);
			$arr_segments = [];

			foreach($data->trk->trkseg->trkpt AS $segment) {
				$segment_namespaces = $segment->getNamespaces(true);
				$segment_ext = $segment->extensions->children($namespaces['gpxtpx']);

				$arr_segments[] = [
					'timestamp' => (int)strtotime($segment->time)
				];
			}

			$date = date('Y-m-d', $arr_segments[array_key_first($arr_segments)]['timestamp']);
			$time = date('H:i', $arr_segments[array_key_first($arr_segments)]['timestamp']);

			unlink($dir_files.'/tracks/'.$get_filename.'.gpx');
			unlink($dir_files.'/tracks/json/'.strtotime($date.' '.$time).'.json');
			unlink($dir_files.'/tracks/images/'.$get_filename.'.webp');

		} elseif($get_type == 'video') {
			unlink($dir_files.'/videos/'.$get_filename.'.mp4');
			unlink($dir_files.'/videos/'.$get_filename.'.ogg');
			unlink($dir_files.'/videos/'.$get_filename.'.webm');
		}


		header("Location: ".url('admin/uploads'));
		exit;



	} else {

		require_once 'site-header.php';



		$get_filename = safetag($_GET['nam']);
		$get_type = safetag($_GET['typ']);
		$get_page = (!isset($_GET['pag']) ? null : safetag($_GET['pag']));

		if($get_type == 'photo') {
			$file = $dir_files.'/uploads/'.$get_filename.'.webp';
			$file_thumb = $dir_files.'/uploads/'.$get_filename.'-thumb.webp';
			$file_thumblist = $dir_files.'/uploads/'.$get_filename.'-thumb-list.webp';

			list($width, $height) = getimagesize($file);
			list($width_t, $height_t) = getimagesize($file_thumb);
			list($width_tl, $height_tl) = getimagesize($file_thumblist);

		} elseif($get_type == 'cover') {
			$file = $dir_files.'/uploads/covers/'.$get_filename.'.webp';

		} elseif($get_type == 'track') {
			$file = $dir_files.'/tracks/'.$get_filename.'.gpx';
			$data = simplexml_load_file($file);
			$namespaces = $data->trk->getNamespaces(true);
			$arr_segments = [];

			foreach($data->trk->trkseg->trkpt AS $segment) {
				$segment_namespaces = $segment->getNamespaces(true);
				$segment_ext = $segment->extensions->children($namespaces['gpxtpx']);

				$arr_segments[] = [
					'timestamp' => (int)strtotime($segment->time)
				];
			}

			$date = date('Y-m-d', $arr_segments[array_key_first($arr_segments)]['timestamp']);
			$time = date('H:i', $arr_segments[array_key_first($arr_segments)]['timestamp']);

		} elseif($get_type == 'video') {
			$file_mp4 = $dir_files.'/videos/'.$get_filename.'.mp4';
			$file_ogg = $dir_files.'/videos/'.$get_filename.'.ogg';
			$file_webm = $dir_files.'/videos/'.$get_filename.'.webm';
		}







		if($is_loggedin == false) {
			header("Location: ".url('admin?prev=uploaded'));
			exit;


		} else {
			if($get_page == 'delete') {
				echo '<section id="delete">';
					echo '<h1>'.$lang['pages']['uploaded']['delete']['title'].'</h1>';

					foreach($lang['pages']['uploaded']['delete']['content'] AS $content) {
						echo $Parsedown->text($content);
					}

					echo '<div class="options">';
						echo '<div><a href="'.url('uploaded:'.$get_filename.'/'.$get_type.'/delete').'" class="delete">';
							echo svgicon('trash');
							echo $lang['pages']['uploaded']['delete']['options']['yes'];
						echo '</a></div>';

						echo '<div><a href="'.url('uploaded:'.$get_filename.'/'.$get_type).'" class="regret">';
							echo svgicon('goback');
							echo $lang['pages']['uploaded']['delete']['options']['no'];
						echo '</a></div>';
				echo '</section>';



			} else {
				echo '<section id="uploaded">';
					echo '<h1>';
						echo '<a href="'.url('uploads').'">';
							echo svgicon('goback');
						echo '</a>';
						
						echo $lang['pages']['uploaded']['title'][$get_type];
					echo '</h1>';

					if($get_type == 'photo') {
						echo '<div class="image" style="background-image: url('.url('photo-thumb:'.$get_filename).');"></div>';
					} elseif($get_type == 'cover') {
						echo '<div class="image" style="background-image: url('.url('cover:'.$get_filename).');"></div>';
					}

					echo '<div class="item side-by-side">';
						echo '<div class="value">';
							echo $lang['pages']['uploaded']['filename'];
						echo '</div>';

						echo '<div class="label">';
							echo $get_filename;
						echo '</div>';
					echo '</div>';


					if($get_type != 'video') {
						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded'][($get_type == 'photo' ? 'size' : 'filesize')];
							echo '</div>';

							echo '<div class="label">';
								if($get_type == 'photo') {
									echo $width.'x'.$height.' ';
									echo '('.format_filesize(filesize($file)).')';
								} else {
									echo format_filesize(filesize($file));
								}
							echo '</div>';
						echo '</div>';
					}


					if($get_type == 'photo') {
						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['size_thumb'];
							echo '</div>';

							echo '<div class="label">';
								echo $width_t.'x'.$height_t.' ';
								echo '('.format_filesize(filesize($file_thumb)).')';
							echo '</div>';
						echo '</div>';

						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['size_thumblist'];
							echo '</div>';

							echo '<div class="label">';
								echo $width_tl.'x'.$height_tl.' ';
								echo '('.format_filesize(filesize($file_thumblist)).')';
							echo '</div>';
						echo '</div>';

						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['tag'];
							echo '</div>';

							echo '<div class="label">';
								echo '[photo='.$get_filename.']';
							echo '</div>';
						echo '</div>';


					} elseif($get_type == 'track') {
						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['tag'];
							echo '</div>';

							echo '<div class="label">';
								echo '[biking]'.strtotime($date.' '.$time).'[/biking]';
							echo '</div>';
						echo '</div>';


					} elseif($get_type == 'video') {
						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['filesize_mp4'];
							echo '</div>';

							echo '<div class="label">';
								echo format_filesize(filesize($file_mp4));
							echo '</div>';
						echo '</div>';

						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['filesize_ogg'];
							echo '</div>';

							echo '<div class="label">';
								echo format_filesize(filesize($file_ogg));
							echo '</div>';
						echo '</div>';

						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['filesize_webm'];
							echo '</div>';

							echo '<div class="label">';
								echo format_filesize(filesize($file_webm));
							echo '</div>';
						echo '</div>';

						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['filesize_total'];
							echo '</div>';

							echo '<div class="label">';
								echo format_filesize(filesize($file_mp4) + filesize($file_ogg) + filesize($file_webm));
							echo '</div>';
						echo '</div>';

						echo '<div class="item side-by-side">';
							echo '<div class="value">';
								echo $lang['pages']['uploaded']['tag'];
							echo '</div>';

							echo '<div class="label">';
								echo '[video='.$get_filename.']';
							echo '</div>';
						echo '</div>';
					}



					echo '<div class="options">';
						echo ($get_type != 'track' ? '' : '<a href="'.url('biking-track:'.strtotime($date.' '.$time)).'">'.$lang['pages']['uploaded']['options']['show'].'</a>');
						echo ($get_type != 'track' ? '' : '<a href="'.url('api/track:'.$get_filename, true).'">'.$lang['pages']['uploaded']['options']['update'].'</a>');

						echo '<a href="'.url('uploaded:'.$get_filename.'/'.$get_type.'/confirm-deletion').'" class="delete">';
							echo $lang['pages']['uploaded']['options']['delete'];
						echo '</a>';
					echo '</div>';
				echo '</section>';
			}
		}







		require_once 'site-footer.php';

	}

?>
