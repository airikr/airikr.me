<?php

	use Particle\Validator\Validator;

	function validate($require, $string, $type) {
		$v = new Validator;

		if($type == 'alpha') {
			$v->required($require)->alpha();
		} elseif($type == 'lengthbetween-u') {
			$v->required($require)->lengthBetween(5, 25);
		} elseif($type == 'lengthbetween-p') {
			$v->required($require)->lengthBetween(8, null);
		} elseif($type == 'lengthbetween-t') {
			$v->required($require)->lengthBetween(4, 100);
		} elseif($type == 'lengthbetween-n') {
			$v->required($require)->lengthBetween(2, 20);
		} elseif($type == 'lengthbetween-s') {
			$v->required($require)->lengthBetween(3, 100);
		} elseif($type == 'length-p') {
			$v->required($require)->length(10);
		} elseif($type == 'email') {
			$v->required($require)->email();
		} elseif($type == 'digits') {
			$v->required($require)->digits();
		} elseif($type == 'regex-name') {
			$v->required($require)->regex("/^[A-zåäöÅÄÖ \,\.\é\'\-]+$/i");
		} elseif($type == 'regex-password') {
			$v->required($require)->regex("/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\!\@\#\$\%\^\&\*]).{8,}$/");
		}

		return $v->validate([$require => $string])->isValid();
	}

?>