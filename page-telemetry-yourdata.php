<?php

	# Visa en heatmap på ett tangentbord, skapat på https://keyboard-layout-editor.com
	# Kunna läsa om vad till exempel localStorage gör.

	if($_GET['log'] == 0) {

		require_once 'site-settings.php';

		$dir = $dir_files.'/logs/'.hash('sha256', getip());
		$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

		foreach($files AS $file) {
			if ($file->isDir()) {
				rmdir($file->getPathname());
			} else {
				unlink($file->getPathname());
			}
		}

		rmdir($dir);

		header("Location: ".url('telemetry', null, $get_lang, $get_theme, '0'));
		exit;



	} else {

		require_once 'site-header.php';



		$dir = $dir_files.'/logs/'.hash('sha256', getip());
		$visitor = json_decode(file_get_contents($dir.'/visitor.json'), false);
		$parse_useragent = new WhichBrowser\Parser($visitor->useragent);







		echo '<section id="your-data">';
			echo '<h1>'.$lang['pages']['your-data']['title'].'</h1>';

			foreach($lang['pages']['your-data']['content'] AS $content) {
				echo $Parsedown->text($content);
			}



			echo '<details class="you">';
				echo '<summary class="no-select">';
					echo '<div class="open">'.svgicon('plus').'</div>';
					echo '<div class="close">'.svgicon('minus').'</div>';
					echo 'Om dig';
				echo '</summary>';

				echo '<div class="content">';
					echo '<div><b>Första loggningen:</b> '.date('Y-m-d, H:i', $visitor->firstvisit->timestamp).'</div>';
					echo '<div><b>IP-adress:</b> '.$visitor->ip->plaintext.'</div>';
					echo '<div><b>Användaragent:</b> '.$visitor->useragent.'</div>';
					echo '<div><b>Webbläsare:</b> '.$visitor->browser.'</div>';
					echo '<div><b>Operativsystem:</b> '.$visitor->operating_system.'</div>';
					echo '<div><b>Processorarkitektur:</b> '.$visitor->cpu.'</div>';
					echo '<div><b>Tidszon:</b> '.$visitor->timezone.'</div>';
					echo '<div><b>Tidszonsförskjutning:</b> '.$visitor->offset.' ('.ltrim(($visitor->offset / 60), '-').'h)</div>';


					echo '<h3>';
						echo svgicon('screen') . 'Skärm';
					echo '</h3>';

					echo '<div><b>Skärmupplösning:</b> '.$visitor->screen->resolution->screen.'</div>';
					echo '<div><b>Färgdjup:</b> '.$visitor->screen->colordepth.'</div>';


					echo '<h3>';
						echo svgicon('browser') . 'Webbläsaren';
					echo '</h3>';

					#echo '<div><b>Fingerprint:</b> '.$visitor->fingerprint.'</div>';
					echo '<div><b>Fingerprint:</b> '.$visitor->custom_fingerprint.'</div>';


					echo '<h3>';
						echo svgicon('browser') . 'Inställningar i webbläsaren';
					echo '</h3>';

					echo '<div><b>Tillåter kakor:</b> '.($visitor->settings->cookies ? 'Ja' : 'Nej').'</div>';
					echo '<div><b>Do Not Track:</b> '.($visitor->settings->do_not_track ? 'Ja' : 'Nej').'</div>';
					echo '<div><b>Språk:</b> '.$visitor->settings->language.'</div>';
					echo '<div><b>Flash:</b> '.($visitor->settings->flash ? 'Ja' : 'Nej').'</div>';
					echo '<div><b>Java:</b> '.($visitor->settings->java ? 'Ja' : 'Nej').'</div>';
					echo '<div><b>Silverlight:</b> '.($visitor->settings->silverlight ? 'Ja' : 'Nej').'</div>';
					echo '<div><b>localStorage:</b> '.($visitor->settings->localstorage ? 'Ja' : 'Nej').'</div>';
					echo '<div><b>Session Storage:</b> '.($visitor->settings->sessionstorage ? 'Ja' : 'Nej').'</div>';
					echo '<div><b>WebSocket:</b> '.($visitor->settings->websocket ? 'Ja' : 'Nej').'</div>';
					echo '<div><b>Geolocation:</b> '.($visitor->settings->geolocation ? 'Ja' : 'Nej').'</div>';
				echo '</div>';
			echo '</details>';



			echo '<details class="pages">';
				echo '<summary class="no-select">';
					echo '<div class="open">'.svgicon('plus').'</div>';
					echo '<div class="close">'.svgicon('minus').'</div>';
					echo 'Besökta sidor';
				echo '</summary>';

				echo '<div class="content">';
					$pages = glob($dir.'/pages/*.json');
					natsort($pages);
					foreach(array_reverse($pages) AS $page) {
						$fileinfo = pathinfo($page);
						$pageinfo = json_decode(file_get_contents($page), false);
						$page = trim(str_replace($config_folder, '', $pageinfo->filename->path), '/');
						$page_previous = (empty($pageinfo->previous_url->path) ? null : trim(str_replace($config_folder, '', $pageinfo->previous_url->path), '/'));
						$queries = parse_str($pageinfo->filename->query, $arr_query);
						$queries_previous = (empty($pageinfo->previous_url->query) ? null : parse_str($pageinfo->previous_url->query, $arr_query_prev));

						list($datetime, $microseconds) = explode('-', $fileinfo['filename']);

						echo '<details class="page">';
							echo '<summary class="no-select">';
								echo '<div class="open">'.svgicon('plus').'</div>';
								echo '<div class="close">'.svgicon('minus').'</div>';
								echo date('Y-m-d, H:i:s', strtotime($datetime));
							echo '</summary>';

							echo '<div class="content">';
								echo '<div class="went-to">';
									echo '<div><b>Sida:</b> <a href="'.url($page).'">'.$page.'</a></div>';
									echo '<div><b>Språk:</b> '.$arr_query['lang'].'</div>';
									echo '<div><b>Tema:</b> '.$arr_query['thm'].'</div>';
								echo '</div>';

								echo '<div class="previous">';
									echo '<div><b>Föregående sida:</b> <a href="'.url($page_previous).'">'.$page_previous.'</a></div>';
									echo '<div><b>Språk:</b> '.$arr_query_prev['lang'].'</div>';
									echo '<div><b>Tema:</b> '.$arr_query_prev['thm'].'</div>';
								echo '</div>';
							echo '</div>';
						echo '</details>';
					}
				echo '</div>';
			echo '</details>';



			echo '<details class="keystrokes">';
				echo '<summary class="no-select">';
					echo '<div class="open">'.svgicon('plus').'</div>';
					echo '<div class="close">'.svgicon('minus').'</div>';
					echo 'Knapptryckningar';
				echo '</summary>';

				echo '<div class="content">';
					echo '<div class="keys">';
						echo '<div class="item head side-by-side">';
							echo '<div class="when">'.$lang['pages']['your-data']['lists']['keycodes']['when'].'</div>';
							echo '<div class="key">'.$lang['pages']['your-data']['lists']['keycodes']['key'].'</div>';
							echo '<div class="keycode">'.$lang['pages']['your-data']['lists']['keycodes']['keycode'].'</div>';
							echo '<div class="page">'.$lang['pages']['your-data']['lists']['keycodes']['page'].'</div>';
						echo '</div>';

						$keystrokes = glob($dir.'/keystrokes/*.json');
						natsort($keystrokes);
						foreach(array_reverse($keystrokes) AS $keystroke) {
							$fileinfo = pathinfo($keystroke);
							$keyinfo = json_decode(file_get_contents($keystroke), false);
							$page = trim(str_replace($config_folder, '', $keyinfo->page), '/');

							list($datetime, $microseconds) = explode('-', $fileinfo['filename']);

							echo '<div class="item side-by-side">';
								echo '<div class="when">'.date('Y-m-d, H:i:s', strtotime($datetime)).'</div>';
								echo '<div class="key side-by-side">'.keycode($keyinfo->code).'</div>';
								echo '<div class="keycode">'.$keyinfo->code.'</div>';
								echo '<div class="page"><a href="'.url($page).'">'.$page.'</a></div>';
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</details>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
