<?php

	function format_filesize($size) {
		$base = log($size) / log(1024);
		$suffix = array('B', 'kB', 'MB', 'GB', 'TB')[floor($base)];
		return format_number(pow(1024, $base - floor($base))).' '.$suffix;
	}

?>