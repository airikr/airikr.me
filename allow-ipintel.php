<?php

	require_once 'site-settings.php';

	$get_previouspage = (isset($_GET['prev']) ? safetag($_GET['prev']) : '');

	if(!glob($dir_files.'/visitors/'.hash('sha256', getip()).'.json')) {
		checkip(getip());

		header("Location: ".url($get_previouspage, false, $get_lang, $get_theme));
		exit;

	} else {
		die(simplepage('Already allowed.'));
	}

?>
