<?php

	require_once 'site-settings.php';

	$post_instance = (empty($_POST['instance']) ? null : safetag($_POST['instance']));
	$post_hidden_string = $_POST['hidden-string'];



	if(empty($post_instance)) {
		die(simplepage('Gör om, gör rätt'));

	} else {
		header("Location: https://".trim($post_instance, '/')."/share?text=".$post_hidden_string);
		exit;
	}

?>