<?php

	require_once 'site-header.php';
	
	

	$reactions = glob($dir_files.'/posts/reactions/*');
	$arr_reactions = [];

	foreach($reactions AS $reaction) {
		$content = json_decode(file_get_contents($reaction), false);
		$file_info = pathinfo($reaction);
		list($post, $timestamp, $ip, $emoji) = explode('-', $file_info['filename']);
		$arr_reactions[] = [
			'post' => $post,
			'emoji' => $emoji,
			'reacted' => $timestamp,
			'ip' => $ip
		];
	}

	usort($arr_reactions, function($a, $b) {
		$ad = new DateTime(date('Y-m-d H:i:s', $a['reacted']));
		$bd = new DateTime(date('Y-m-d H:i:s', $b['reacted']));

		if($ad == $bd) {
			return 0;
		}

		return $ad > $bd ? -1 : 1;
	});







	echo '<section id="filter-reactions">';
		echo '<h1>';
			echo '<a href="'.url('admin/overview').'">';
				echo 'Översikt';
			echo '</a>';

			echo svgicon('chevron-right');

			echo 'Reaktioner';
		echo '</h1>';


		echo '<div class="head">';
			echo '<div class="reaction"></div>';

			echo '<div class="when">';
				echo 'När';
			echo '</div>';

			echo '<div class="post">';
				echo 'Inlägg';
			echo '</div>';
		echo '</div>';

		foreach($arr_reactions AS $filter) {
			$file = $dir_files.'/posts/reactions/'.$filter['post'].'-'.$filter['reacted'].'-'.$filter['ip'].'-'.$filter['emoji'];
			$fileinfo = pathinfo($file);

			list($dt, $ts, $ip, $re) = explode('-', $fileinfo['filename']);

			echo '<div>';
				echo '<div class="reaction">';
					echo svgicon('emoji-'.$re);
				echo '</div>';

				echo '<div class="when">';
					echo date_($ts, 'datetime');
				echo '</div>';

				echo '<div class="post">';
					echo '<a href="'.url('blog/'.$dt).'">';
						echo $dt;
					echo '</a>';
				echo '</div>';
			echo '</div>';
		}
	echo '</section>';







	require_once 'site-footer.php';

?>
