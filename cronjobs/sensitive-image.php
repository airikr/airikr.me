<?php

	require_once '/path/to/folder/site-settings.php';

	foreach(glob($dir_files.'/gallery/accepted-sensitive/*.json') AS $file) {
		$exif = json_decode(file_get_contents($file), false);

		if((time() - $exif->opened) > 300) {
			unlink($file);
		}
	}

?>