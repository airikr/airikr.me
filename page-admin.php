<?php

	use RobThree\Auth\TwoFactorAuth;
	use RobThree\Auth\Providers\Qr\BaconQrCodeProvider;



	if(isset($_POST['login'])) {

		require_once 'site-settings.php';

		$tfa = new TwoFactorAuth(new BaconQrCodeProvider());
		$admin = (!file_exists($dir_files.'/admin.json') ? null : json_decode(file_get_contents($dir_files.'/admin.json'), true));

		$post_hidden_previouspage = (!isset($_POST['hidden-previouspage']) ? null : safetag($_POST['hidden-previouspage']));
		$post_username = (!isset($_POST['user']) ? null : safetag($_POST['user']));
		$post_password = (!isset($_POST['pass']) ? null : safetag($_POST['pass']));
		$post_tfa = (!isset($_POST['tfa']) ? null : safetag($_POST['tfa']));

		$previous_page = (empty($post_hidden_previouspage) ? '' : '?prev='.$post_hidden_previouspage);


		if(!file_exists($dir_files.'/admin.json')) {
			die(simplepage('<div class="error">'.$Parsedown->text($lang['messages']['errors']['no-admin'].'</div>')));


		} elseif(empty($post_username) OR empty($post_password) OR !empty($admin['tfa']) AND empty($post_tfa)) {
			$error = null;
			foreach($lang['messages']['errors']['empty-fields'] AS $content) {
				$error .= $Parsedown->text($content);
			}

			if(empty($admin['tfa'])) {
				$arr_requiredfields = [
					$lang['pages']['admin']['form']['username'],
					$lang['pages']['admin']['form']['password']
				];

			} elseif(!empty($admin['tfa']) AND empty($post_tfa)) {
				$arr_requiredfields = [
					$lang['pages']['admin']['form']['username'],
					$lang['pages']['admin']['form']['password'],
					$lang['pages']['admin']['form']['tfa']
				];
			}

			die(simplepage('<div class="error">'.$error.'<ul><li>'.implode('</li><li>', $arr_requiredfields).'</li></ul></div><div class="go-back"><a href="'.url('admin'.$previous_page).'">'.$lang['goback'].'</a></div>'));


		} else {
			if($post_username != $admin['user']) {
				die(simplepage('<div class="error">'.$Parsedown->text($lang['messages']['errors']['wrong-username']).'</div><div class="go-back"><a href="'.url('admin'.$previous_page).'">'.$lang['goback'].'</a></div>'));

			} elseif(!password_verify($post_password, $admin['pass'])) {
				die(simplepage('<div class="error">'.$Parsedown->text($lang['messages']['errors']['wrong-password']).'</div><div class="go-back"><a href="'.url('admin'.$previous_page).'">'.$lang['goback'].'</a></div>'));

			} elseif(!empty($admin['tfa']) AND !empty($post_tfa) AND !is_numeric($post_tfa)) {
				die(simplepage('<div class="error">'.$Parsedown->text($lang['messages']['errors']['tfa-onlydigits']).'</div><div class="go-back"><a href="'.url('admin'.$previous_page).'">'.$lang['goback'].'</a></div>'));

			} elseif(!empty($admin['tfa']) AND !empty($post_tfa) AND strlen($post_tfa) < 6 OR !empty($admin['tfa']) AND !empty($post_tfa) AND strlen($post_tfa) > 6) {
				die(simplepage('<div class="error">'.$Parsedown->text($lang['messages']['errors']['tfa-tooshortorlong']).'</div><div class="go-back"><a href="'.url('admin'.$previous_page).'">'.$lang['goback'].'</a></div>'));

			} elseif(!empty($admin['tfa']) AND !empty($post_tfa) AND !$tfa->verifyCode($admin['tfa'], $post_tfa)) {
				die(simplepage('<div class="error">'.$Parsedown->text($lang['messages']['errors']['wrong-tfa']).'</div><div class="go-back"><a href="'.url('admin'.$previous_page).'">'.$lang['goback'].'</a></div>'));


			} else {
				$_SESSION['admin'] = '1';

				header("Location: ".url((empty($post_hidden_previouspage) ? '' : $post_hidden_previouspage)));
				exit;
			}
		}



	} elseif(isset($_POST['create'])) {

		require_once 'site-settings.php';

		if(!file_exists($dir_files.'/admin.json')) {
			$post_username = (!isset($_POST['user']) ? null : safetag($_POST['user']));
			$post_password = (!isset($_POST['pass']) ? null : safetag($_POST['pass']));
			$post_tfa = (!isset($_POST['tfa']) ? null : safetag($_POST['tfa']));

			if(empty($post_username) OR empty($post_password)) {
			$error = null;
			foreach($lang['messages']['errors']['empty-fields'] AS $content) {
				$error .= $Parsedown->text($content);
			}

			$arr_requiredfields = [
				$lang['pages']['admin']['username'],
				$lang['pages']['admin']['password']
			];

			die(simplepage('<div class="error">'.$error.'<ul><li>'.implode('</li><li>', $arr_requiredfields).'</li></ul></div><div class="go-back"><a href="'.url('admin/create').'">'.$lang['goback'].'</a></div>'));


			} else {
				$arr = [
					'user' => $post_username,
					'pass' => password_hash($post_password, PASSWORD_BCRYPT),
					'tfa' => (!empty($post_tfa) ? $post_tfa : null)
				];

				file_put_contents($dir_files.'/admin.json', json_encode($arr));

				header("Location: ".url('admin'));
				exit;
			}
		}



	} elseif(isset($_GET['logout'])) {

		require_once 'site-settings.php';

		session_destroy();
		session_unset();

		header("Location: ".url(''));
		exit;



	} else {

		require_once 'site-header.php';



		$get_page = (isset($_GET['pag']) ? safetag($_GET['pag']) : null);
		$get_tfa = (!isset($_GET['tfa']) ? null : ($_GET['tfa'] == '1' ? true : false));
		$get_previouspage = (isset($_GET['prev']) ? safetag($_GET['prev']) : null);
		$tfa = new TwoFactorAuth(new BaconQrCodeProvider());







		if($is_loggedin == true) {
			header("Location: ".url(''));
			exit;

		} elseif($get_page == 'create' AND file_exists($dir_files.'/admin.json')) {
			echo '<div class="message">';
				echo $lang['messages']['admin-exists'];
			echo '</div>';


		} else {
			echo '<section id="admin">';
				echo '<h1>'.$lang['pages']['admin']['titles'][(empty($get_page) ? 'login' : 'create')].'</h1>';

				if(!empty($get_page) AND $get_page == 'create') {
					echo '<div class="tfa">';
						echo $Parsedown->text($lang['pages']['admin']['tfa-options']);
					echo '</div>';
				}

				echo '<form action="'.url('admin'.(empty($get_page) ? '' : '/create')).'" method="POST" autocomplete="off" novalidate>';
					echo '<input type="hidden" name="hidden-previouspage" value="'.(!isset($_GET['prev']) ? '' : safetag($_GET['prev'])).'">';

					echo '<div class="side-by-side">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['admin']['form']['username'];
							echo '</div>';

							echo '<div class="value">';
								echo '<input type="text" name="user">';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['admin']['form']['password'];
							echo '</div>';

							echo '<div class="value">';
								echo '<input type="password" name="pass">';
							echo '</div>';
						echo '</div>';
					echo '</div>';

					if(empty($get_page) OR !empty($get_page) AND $get_page == 'create' AND $get_tfa == true) {
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['admin']['form']['tfa'.((!empty($get_page) AND $get_tfa == true) ? '-create' : '')];
							echo '</div>';

							echo '<div class="value">';
								echo '<input type="text" inputmode="numeric" pattern="[0-9]*" name="tfa"';
								echo ' placeholder="'.$lang['pages']['admin']['placeholders']['tfa'].'"';
								echo ((!empty($get_page) AND $get_page == 'create') ? ' value="'.$tfa->createSecret().'" readonly' : '');
								echo '>';
							echo '</div>';
						echo '</div>';
					}



					echo '<div class="button">';
						if($get_page == 'create') {
							echo '<input type="submit" name="create" value="'.$lang['pages']['admin']['buttons']['create'].'">';

						} else {
							echo '<input type="submit" name="login" value="'.$lang['pages']['admin']['buttons']['login'].'">';
						}
					echo '</div>';
				echo '</form>';
			echo '</section>';
		}







		require_once 'site-footer.php';

	}

?>
