<?php

	require_once '/path/to/folder/site-settings.php';

	foreach(glob($dir_files.'/posts/removed-nsfw-mark/*.json') AS $file) {
		$exif = json_decode(file_get_contents($file), false);

		if((time() - $exif->opened) > 600) {
			unlink($file);
		}
	}

?>