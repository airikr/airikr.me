<?php

	use Spatie\Valuestore\Valuestore;

	require_once '../site-settings.php';
	require_once '../vendor/autoload.php';

	$dir = $dir_files.'/logs/'.hash('sha256', getip());
	$visitor = Valuestore::make($dir.'/visitor.json');
	$fingerprint_exists = ($visitor->has('fingerprint') ? 'y' : 'n');



	if($fingerprint_exists == 'n') {
		$get_resolution = safetag($_GET['res']);
		$get_resolution_available = safetag($_GET['rea']);
		$get_colordepth = safetag($_GET['cde']);
		$get_cookies = safetag($_GET['coo']);
		$get_java = safetag($_GET['jva']);
		$get_javaversion = safetag($_GET['jvv']);
		$get_flash = safetag($_GET['fla']);
		$get_flashversion = safetag($_GET['flv']);
		$get_silverlight = safetag($_GET['sil']);
		$get_silverlightversion = safetag($_GET['siv']);
		$get_localstorage = safetag($_GET['isl']);
		$get_sessionstorage = safetag($_GET['iss']);
		$get_timezone = safetag($_GET['tmz']);
		$get_offset = safetag($_GET['ofs']);
		$get_donottrack = safetag($_GET['dnt']);
		$get_language = safetag($_GET['lng']);
		$get_fingerprint = safetag($_GET['fip']);
		$get_customfingerprint = safetag($_GET['cfi']);
		$get_browser = safetag($_GET['bro']);
		$get_browserversion = safetag($_GET['brv']);
		$get_browserengine = safetag($_GET['bre']);
		$get_os = safetag($_GET['ope']);
		$get_osversion = safetag($_GET['opv']);
		$get_cpu = safetag($_GET['cpu']);
		$get_websocket = safetag($_GET['wbs']);
		$get_geolocation = safetag($_GET['geo']);

		$visitor->put([
			'fingerprint' => $get_fingerprint,
			'custom_fingerprint' => $get_customfingerprint,
			'screen' => [
				'resolution' => [
					'screen' => $get_resolution,
					'available' => $get_resolution_available
				],
				'colordepth' => (int)$get_colordepth
			],
			'timezone' => $get_timezone,
			'offset' => $get_offset,
			'cpu' => $get_cpu,
			'settings' => [
				'cookies' => (boolean)$get_cookies,
				'do_not_track' => (boolean)$get_donottrack,
				'language' => $get_language,
				'flash' => (boolean)$get_flash,
				'java' => (boolean)$get_java,
				'silverlight' => (boolean)$get_silverlight,
				'localstorage' => (boolean)$get_localstorage,
				'sessionstorage' => (boolean)$get_sessionstorage,
				'websocket' => (boolean)$get_websocket,
				'geolocation' => (boolean)$get_geolocation
			]
		]);

		echo 'fingerprint info set';


	} else {
		echo 'fingerprint info exists - ignoring';
	}

?>
