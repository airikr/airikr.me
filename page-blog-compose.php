<?php

	# Lägg till anpassade taggar för video [video] och ljud [audio]
	# Kunna lägga in inläggen till kategorier. Använd kataloger för detta.

	use Spatie\Valuestore\Valuestore;
	use Intervention\Image\ImageManager;
	use Intervention\Image\Drivers\Imagick\Driver;

	$cover_max_filesize = (1048576 * 5);



	if(isset($_GET['cnl'])) {

		require_once 'site-settings.php';

		$get_cancel = safetag($_GET['cnl']);
		$get_year = (!isset($_GET['ye']) ? null : safetag($_GET['ye']));
		$get_month = (!isset($_GET['mo']) ? null : safetag($_GET['mo']));
		$get_day = (!isset($_GET['da']) ? null : safetag($_GET['da']));
		$get_hour = (!isset($_GET['ho']) ? null : safetag($_GET['ho']));
		$get_minute = (!isset($_GET['mi']) ? null : safetag($_GET['mi']));

		$datetime = $get_year . $get_month . $get_day . $get_hour . $get_minute;

		if($get_cancel == 0) {
			die(simplepage('Do you really want to cancel? <a href="'.url('blog/'.(empty($datetime) ? 'compose' : 'edit:'.$datetime).'/cancel:yes').'">Yes</a> / <a href="'.url('blog/'.(empty($datetime) ? 'compose' : 'edit:'.$datetime)).'">No</a>'));

		} else {
			unset($_SESSION['subject-se']);
			unset($_SESSION['content-se']);
			unset($_SESSION['subject-en']);
			unset($_SESSION['content-en']);
			unset($_SESSION['check-nsfw']);
			unset($_SESSION['check-notvalid']);
			unset($_SESSION['check-updatelog']);
			unset($_SESSION['check-hundreddaystooffload']);
			unset($_SESSION['hdto-day']);

			header("Location: ".url('blog'.(empty($datetime) ? '' : '/'.$datetime)));
			exit;
		}



	} elseif(isset($_POST['publish'])) {

		require_once 'site-settings.php';

		if($is_loggedin == false) {
			die(simplepage('Not logged in.'));

		} else {
			$datetime = date('YmdHi');
			$file = $dir_files.'/posts/published/'.$datetime.'.json';
			$cover = (!file_exists($dir_files.'/uploads/covers/'.$datetime.'.webp') ? null : true);
			$manager = new ImageManager(['driver' => 'imagick']);
			$uniqueid = uniqid();

			$post_subject_se = safetag($_POST['subject-se']);
			$post_content_se = safetag($_POST['content-se']);
			$post_subject_en = safetag($_POST['subject-en']);
			$post_content_en = safetag($_POST['content-en']);
			$post_check_nsfw = (isset($_POST['check-nsfw']) ? true : null);
			$post_check_notvalid = (isset($_POST['check-notvalid']) ? true : null);
			$post_check_updatelog = (isset($_POST['check-updatelog']) ? true : null);
			$post_check_hundreddaystooffload = (isset($_POST['check-hundreddaystooffload']) ? true : null);
			$post_hundreddaystooffload_day = safetag($_POST['field-hdto-day']);


			if(empty($post_subject_se) OR empty($post_subject_en) OR empty($post_content_se) OR empty($post_content_en)) {
				$_SESSION['subject-se'] = (empty($post_subject_se) ? null : $post_subject_se);
				$_SESSION['content-se'] = (empty($post_content_se) ? null : $post_content_se);
				$_SESSION['subject-en'] = (empty($post_subject_en) ? null : $post_subject_en);
				$_SESSION['content-en'] = (empty($post_content_en) ? null : $post_content_en);
				$_SESSION['check-nsfw'] = ($post_check_nsfw == false ? null : $post_check_nsfw);
				$_SESSION['check-notvalid'] = ($post_check_notvalid == false ? null : $post_check_notvalid);
				$_SESSION['check-updatelog'] = ($post_check_updatelog == false ? null : $post_check_updatelog);
				$_SESSION['check-hundreddaystooffload'] = ($post_check_hundreddaystooffload == false ? null : $post_check_hundreddaystooffload);
				$_SESSION['hdto-day'] = (empty($post_hundreddaystooffload_day) ? '' : $post_hundreddaystooffload_day);

				die(simplepage('Please enter subject and/or content first. This also applies to the translated fields. <a href="'.url('blog/compose').'">Go back</a>'));


			} else {
				if(empty($cover) AND !empty($_FILES['file']['name'])) {
					$file_tmp = $_FILES['file']['tmp_name'];
					$file_error = $_FILES['file']['error'];
					$file_size = $_FILES['file']['size'];
					$file_name_old = $dir_files.'/uploads/covers/'.$datetime.'.jpg';
					$file_name_new = $dir_files.'/uploads/covers/'.$datetime.'.webp';

					if(!move_uploaded_file($file_tmp, $file_name_old)) {
						$_SESSION['subject-se'] = (empty($post_subject_se) ? null : $post_subject_se);
						$_SESSION['content-se'] = (empty($post_content_se) ? null : $post_content_se);
						$_SESSION['subject-en'] = (empty($post_subject_en) ? null : $post_subject_en);
						$_SESSION['content-en'] = (empty($post_content_en) ? null : $post_content_en);
						$_SESSION['check-nsfw'] = ($post_check_nsfw == false ? null : $post_check_nsfw);
						$_SESSION['check-notvalid'] = ($post_check_notvalid == false ? null : $post_check_notvalid);
						$_SESSION['check-updatelog'] = ($post_check_updatelog == false ? null : $post_check_updatelog);
						$_SESSION['check-hundreddaystooffload'] = ($post_check_hundreddaystooffload == false ? null : $post_check_hundreddaystooffload);
						$_SESSION['hdto-day'] = (empty($post_hundreddaystooffload_day) ? '' : $post_hundreddaystooffload_day);

						die(simplepage('The file has not been uploaded. Please try again. <a href="'.url('upload').'">Go back</a>'));


					} elseif(!is_writable($dir_files.'/uploads/covers')) {
						$_SESSION['subject-se'] = (empty($post_subject_se) ? null : $post_subject_se);
						$_SESSION['content-se'] = (empty($post_content_se) ? null : $post_content_se);
						$_SESSION['subject-en'] = (empty($post_subject_en) ? null : $post_subject_en);
						$_SESSION['content-en'] = (empty($post_content_en) ? null : $post_content_en);
						$_SESSION['check-nsfw'] = ($post_check_nsfw == false ? null : $post_check_nsfw);
						$_SESSION['check-notvalid'] = ($post_check_notvalid == false ? null : $post_check_notvalid);
						$_SESSION['check-updatelog'] = ($post_check_updatelog == false ? null : $post_check_updatelog);
						$_SESSION['check-hundreddaystooffload'] = ($post_check_hundreddaystooffload == false ? null : $post_check_hundreddaystooffload);
						$_SESSION['hdto-day'] = (empty($post_hundreddaystooffload_day) ? '' : $post_hundreddaystooffload_day);

						die(simplepage('Folder '.$dir_files.'/uploads/covers not writeable. <a href="'.url('upload').'">Go back</a>'));


					} elseif($file_size > $cover_max_filesize) {
						$_SESSION['subject-se'] = (empty($post_subject_se) ? null : $post_subject_se);
						$_SESSION['content-se'] = (empty($post_content_se) ? null : $post_content_se);
						$_SESSION['subject-en'] = (empty($post_subject_en) ? null : $post_subject_en);
						$_SESSION['content-en'] = (empty($post_content_en) ? null : $post_content_en);
						$_SESSION['check-nsfw'] = ($post_check_nsfw == false ? null : $post_check_nsfw);
						$_SESSION['check-notvalid'] = ($post_check_notvalid == false ? null : $post_check_notvalid);
						$_SESSION['check-updatelog'] = ($post_check_updatelog == false ? null : $post_check_updatelog);
						$_SESSION['check-hundreddaystooffload'] = ($post_check_hundreddaystooffload == false ? null : $post_check_hundreddaystooffload);
						$_SESSION['hdto-day'] = (empty($post_hundreddaystooffload_day) ? '' : $post_hundreddaystooffload_day);

						die(simplepage('The file size for the cover is above '.format_filesize($cover_max_filesize).'. <a href="'.url('blog/compose').'">Go back</a>'));


					} elseif($file_error != 0) {
						die(simplepage('File error code '.$file_error));

					} else {
						$manager->make($file_name_old)->resize(900, null, function($constraint) {
							$constraint->aspectRatio();
							$constraint->upsize();
						})->encode('webp', 70)->save($file_name_new);

						$img = new Imagick($file_name_new);
						$img->stripImage();
						$img->writeImage($file_name_new);
						$img->clear();
						$img->destroy();

						unlink($file_name_old);
					}
				}

				$arr_post = [
					'id' => $uniqueid,
					'subject' => [
						'se' => $post_subject_se,
						'en' => $post_subject_en
					],
					'content' => [
						'se' => $post_content_se,
						'en' => $post_content_en
					],
					'nsfw' => (empty($post_check_nsfw) ? false : true),
					'notvalid' => (empty($post_check_notvalid) ? false : true),
					'updatelog' => (empty($post_check_updatelog) ? false : true),
					'hundreddaystooffload' => [
						'enabled' => (empty($post_check_hundreddaystooffload) ? false : true),
						'day' => (empty($post_hundreddaystooffload_day) ? null : $post_hundreddaystooffload_day)
					],
					'edited' => null
				];

				file_put_contents($file, json_encode($arr_post));

				unset($_SESSION['subject-se']);
				unset($_SESSION['content-se']);
				unset($_SESSION['subject-en']);
				unset($_SESSION['content-en']);
				unset($_SESSION['check-nsfw']);
				unset($_SESSION['check-notvalid']);
				unset($_SESSION['check-updatelog']);
				unset($_SESSION['check-hundreddaystooffload']);
				unset($_SESSION['hdto-day']);

				header("Location: ".url('blog/edit:'.$datetime));
				exit;
			}
		}



	} elseif(isset($_POST['save'])) {

		require_once 'site-settings.php';

		if($is_loggedin == false) {
			die(simplepage('Not logged in.'));

		} else {
			$get_year = (!isset($_GET['ye']) ? null : safetag($_GET['ye']));
			$get_month = (!isset($_GET['mo']) ? null : safetag($_GET['mo']));
			$get_day = (!isset($_GET['da']) ? null : safetag($_GET['da']));
			$get_hour = (!isset($_GET['ho']) ? null : safetag($_GET['ho']));
			$get_minute = (!isset($_GET['mi']) ? null : safetag($_GET['mi']));
			$datetime = $get_year . $get_month . $get_day . $get_hour . $get_minute;

			$file = $dir_files.'/posts/published/'.$get_year . $get_month . $get_day . $get_hour . $get_minute.'.json';
			$post = json_decode(file_get_contents($file), false);
			$cover = (!file_exists($dir_files.'/uploads/covers/'.$datetime.'.webp') ? null : url('cover:'.$datetime));
			$manager = new ImageManager(['driver' => 'imagick']);
			$uniqueid = $post->id;

			$post_subject_se = safetag($_POST['subject-se']);
			$post_content_se = safetag($_POST['content-se']);
			$post_subject_en = safetag($_POST['subject-en']);
			$post_content_en = safetag($_POST['content-en']);
			$post_check_nsfw = (isset($_POST['check-nsfw']) ? true : null);
			$post_check_notvalid = (isset($_POST['check-notvalid']) ? true : null);
			$post_check_updatelog = (isset($_POST['check-updatelog']) ? true : null);
			$post_check_hundreddaystooffload = (isset($_POST['check-hundreddaystooffload']) ? true : null);
			$post_hundreddaystooffload_day = safetag($_POST['field-hdto-day']);


			if(empty($post_subject_se) OR empty($post_subject_en) OR empty($post_content_se) OR empty($post_content_en)) {
				$_SESSION['subject-se'] = (empty($post_subject_se) ? '' : $post_subject_se);
				$_SESSION['content-se'] = (empty($post_content_se) ? '' : $post_content_se);
				$_SESSION['subject-en'] = (empty($post_subject_en) ? '' : $post_subject_en);
				$_SESSION['content-en'] = (empty($post_content_en) ? '' : $post_content_en);
				$_SESSION['check-nsfw'] = (empty($post_nsfw) ? '' : $post_nsfw);
				$_SESSION['check-notvalid'] = ($post_check_notvalid == false ? null : $post_check_notvalid);
				$_SESSION['check-updatelog'] = ($post_check_updatelog == false ? null : $post_check_updatelog);
				$_SESSION['check-hundreddaystooffload'] = ($post_check_hundreddaystooffload == false ? null : $post_check_hundreddaystooffload);
				$_SESSION['hdto-day'] = (empty($post_hundreddaystooffload_day) ? '' : $post_hundreddaystooffload_day);

				$url_parsed = parse_url($uri);
				$url_strip = (empty($config_folder) ? ltrim($url_parsed['path'], '/') : ltrim($url_parsed['path'], '/airikr/'));

				die(simplepage('Please enter subject and/or content first. This also applies to the translated fields. <a href="'.url($url_strip).'">Go back</a>'));


			} else {
				if(empty($cover) AND !empty($_FILES['file']['name'])) {
					$file_tmp = $_FILES['file']['tmp_name'];
					$file_error = $_FILES['file']['error'];
					$file_size = $_FILES['file']['size'];
					$file_name_old = $dir_files.'/uploads/covers/'.$datetime.'.jpg';
					$file_name_new = $dir_files.'/uploads/covers/'.$datetime.'.webp';

					if(empty($file_tmp)) {
						$_SESSION['subject-se'] = (empty($post_subject_se) ? null : $post_subject_se);
						$_SESSION['content-se'] = (empty($post_content_se) ? null : $post_content_se);
						$_SESSION['subject-en'] = (empty($post_subject_en) ? null : $post_subject_en);
						$_SESSION['content-en'] = (empty($post_content_en) ? null : $post_content_en);
						$_SESSION['check-nsfw'] = (empty($post_nsfw) ? '' : $post_nsfw);
						$_SESSION['check-notvalid'] = ($post_check_notvalid == false ? null : $post_check_notvalid);
						$_SESSION['check-updatelog'] = ($post_check_updatelog == false ? null : $post_check_updatelog);
						$_SESSION['check-hundreddaystooffload'] = ($post_check_hundreddaystooffload == false ? null : $post_check_hundreddaystooffload);
						$_SESSION['hdto-day'] = (empty($post_hundreddaystooffload_day) ? '' : $post_hundreddaystooffload_day);

						die(simplepage('Please select a file first. <a href="'.url('upload').'">Go back</a>'));


					} elseif(!move_uploaded_file($file_tmp, $file_name_old)) {
						$_SESSION['subject-se'] = (empty($post_subject_se) ? null : $post_subject_se);
						$_SESSION['content-se'] = (empty($post_content_se) ? null : $post_content_se);
						$_SESSION['subject-en'] = (empty($post_subject_en) ? null : $post_subject_en);
						$_SESSION['content-en'] = (empty($post_content_en) ? null : $post_content_en);
						$_SESSION['check-nsfw'] = (empty($post_nsfw) ? '' : $post_nsfw);
						$_SESSION['check-notvalid'] = ($post_check_notvalid == false ? null : $post_check_notvalid);
						$_SESSION['check-updatelog'] = ($post_check_updatelog == false ? null : $post_check_updatelog);
						$_SESSION['check-hundreddaystooffload'] = ($post_check_hundreddaystooffload == false ? null : $post_check_hundreddaystooffload);
						$_SESSION['hdto-day'] = (empty($post_hundreddaystooffload_day) ? '' : $post_hundreddaystooffload_day);

						die(simplepage('The file has not been uploaded. Please try again. <a href="'.url('upload').'">Go back</a>'));


					} elseif($file_size > $cover_max_filesize) {
						$_SESSION['subject-se'] = (empty($post_subject_se) ? null : $post_subject_se);
						$_SESSION['content-se'] = (empty($post_content_se) ? null : $post_content_se);
						$_SESSION['subject-en'] = (empty($post_subject_en) ? null : $post_subject_en);
						$_SESSION['content-en'] = (empty($post_content_en) ? null : $post_content_en);
						$_SESSION['check-nsfw'] = (empty($post_nsfw) ? '' : $post_nsfw);
						$_SESSION['check-notvalid'] = ($post_check_notvalid == false ? null : $post_check_notvalid);
						$_SESSION['check-updatelog'] = ($post_check_updatelog == false ? null : $post_check_updatelog);
						$_SESSION['check-hundreddaystooffload'] = ($post_check_hundreddaystooffload == false ? null : $post_check_hundreddaystooffload);
						$_SESSION['hdto-day'] = (empty($post_hundreddaystooffload_day) ? '' : $post_hundreddaystooffload_day);

						$url_parsed = parse_url($uri);
						$url_strip = (empty($config_folder) ? ltrim($url_parsed['path'], '/') : ltrim($url_parsed['path'], '/airikr/'));

						die(simplepage('The file size for the cover is above '.format_filesize($cover_max_filesize).'. <a href="'.url($url_strip).'">Go back</a>'));


					} else {
						$manager->make($file_name_old)->resize(900, null, function($constraint) {
							$constraint->aspectRatio();
							$constraint->upsize();
						})->encode('webp', 70)->save($file_name_new);

						$img = new Imagick($file_name_new);
						$img->stripImage();
						$img->writeImage($file_name_new);
						$img->clear();
						$img->destroy();

						unlink($file_name_old);
					}
				}

				$arr_post = [
					'id' => $uniqueid,
					'subject' => [
						'se' => $post_subject_se,
						'en' => $post_subject_en
					],
					'content' => [
						'se' => $post_content_se,
						'en' => $post_content_en
					],
					'nsfw' => (empty($post_check_nsfw) ? false : true),
					'notvalid' => (empty($post_check_notvalid) ? false : true),
					'updatelog' => (empty($post_check_updatelog) ? false : true),
					'hundreddaystooffload' => [
						'enabled' => (empty($post_check_hundreddaystooffload) ? false : true),
						'day' => (empty($post_hundreddaystooffload_day) ? null : $post_hundreddaystooffload_day)
					],
					'edited' => time()
				];

				file_put_contents($file, json_encode($arr_post));

				unset($_SESSION['subject-se']);
				unset($_SESSION['content-se']);
				unset($_SESSION['subject-en']);
				unset($_SESSION['content-en']);
				unset($_SESSION['check-nsfw']);
				unset($_SESSION['check-notvalid']);
				unset($_SESSION['check-updatelog']);
				unset($_SESSION['check-hundreddaystooffload']);
				unset($_SESSION['hdto-day']);

				header("Location: ".url('blog/'.$datetime));
				exit;
			}
		}



	} elseif(isset($_GET['del'])) {
		require_once 'site-settings.php';

		if($is_loggedin == false) {
			die(simplepage('Not logged in.'));

		} else {
			$get_year = (!isset($_GET['ye']) ? null : safetag($_GET['ye']));
			$get_month = (!isset($_GET['mo']) ? null : safetag($_GET['mo']));
			$get_day = (!isset($_GET['da']) ? null : safetag($_GET['da']));
			$get_hour = (!isset($_GET['ho']) ? null : safetag($_GET['ho']));
			$get_minute = (!isset($_GET['mi']) ? null : safetag($_GET['mi']));
			$datetime = $get_year . $get_month . $get_day . $get_hour . $get_minute;

			$type = (file_exists($dir_files.'/posts/published/'.$datetime.'.json') ? 'published' : (file_exists($dir_files.'/posts/drafts/'.$datetime.'.json') ? 'drafts' : null));


			if(safetag($_GET['del']) == 'cover') {
				unlink($dir_files.'/uploads/covers/'.$datetime.'.webp');
				/*$file = $dir_files.'/posts/'.$type.'/'.$datetime.'.json';
				$valuestore = Valuestore::make($file);

				$valuestore->put([
					'subject' => $valuestore->get('subject'),
					'cover' => null,
					'content' => $valuestore->get('content'),
					'language' => $valuestore->get('language'),
					'edited' => $valuestore->get('edited')
				]);*/

				header("Location: ".url('blog/edit:'.$datetime));
				exit;


			} elseif(safetag($_GET['del']) == 'post') {
				unlink($dir_files.'/posts/'.$type.'/'.$datetime.'.json');

				if(file_exists($dir_files.'/uploads/covers/'.$datetime.'.webp')) {
					unlink($dir_files.'/uploads/covers/'.$datetime.'.webp');
				}

				header("Location: ".url('blog'));
				exit;
			}
		}



	} else {

		require_once 'site-header.php';



		$get_page = (!isset($_GET['pag']) ? null : safetag($_GET['pag']));
		$get_type = (!isset($_GET['typ']) ? null : safetag($_GET['typ']));
		$get_year = (!isset($_GET['ye']) ? null : safetag($_GET['ye']));
		$get_month = (!isset($_GET['mo']) ? null : safetag($_GET['mo']));
		$get_day = (!isset($_GET['da']) ? null : safetag($_GET['da']));
		$get_hour = (!isset($_GET['ho']) ? null : safetag($_GET['ho']));
		$get_minute = (!isset($_GET['mi']) ? null : safetag($_GET['mi']));
		$datetime = $get_year . $get_month . $get_day . $get_hour . $get_minute;



		if($is_loggedin == false) {
			header("Location: ".url('blog'.(empty($datetime) ? '' : '/'.$datetime)));
			exit;


		} else {
			$is_editing = false;
			$is_published = false;
			$is_drafted = false;
			$is_empty = false;

			if(!empty($get_year) AND !empty($get_month) AND !empty($get_day) AND !empty($get_hour) AND !empty($get_minute)) {
				$is_editing = true;

				if(file_exists($dir_files.'/posts/published/'.$datetime.'.json')) {
					$is_published = true;
					$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$datetime.'.json'), false);

				} elseif(file_exists($dir_files.'/posts/drafts/'.$datetime.'.json')) {
					$is_drafted = true;
					$post = json_decode(file_get_contents($dir_files.'/posts/drafted/'.$datetime.'.json'), false);

				} else {
					$is_empty = true;
				}


				if($is_empty == false) {
					$edit_subject_se = (isset($post->subject->se) ? $post->subject->se : (isset($post->subject->original) ? $post->subject->original : $post->subject));
					$edit_subject_en = (isset($post->subject->en) ? $post->subject->en : (isset($post->subject->translated) ? $post->subject->translated : $post->subject));
					$edit_cover = (!file_exists($dir_files.'/uploads/covers/'.$datetime.'.webp') ? null : url('cover:'.$datetime));
					$edit_content_se = (isset($post->content->se) ? $post->content->se : (isset($post->content->original) ? $post->content->original : $post->content));
					$edit_content_en = (isset($post->content->en) ? $post->content->en : (isset($post->content->translated) ? $post->content->translated : $post->content));
					$edit_check_nsfw = (isset($post->nsfw) ? $post->nsfw : null);
					$edit_check_notvalid = (isset($post->notvalid) ? $post->notvalid : null);
					$edit_check_updatelog = (isset($post->updatelog) ? $post->updatelog : null);
					$edit_check_hundreddaystooffload = (isset($post->hundreddaystooffload->enabled) ? $post->hundreddaystooffload->enabled : null);
					$edit_hundreddaystooffload_day = (isset($post->hundreddaystooffload->day) ? $post->hundreddaystooffload->day : null);

				} else {
					header("Location: ".url('blog/compose'));
					exit;
				}
			}







			if($get_page == 'delete') {
				echo '<section id="delete">';
					echo '<h1>'.$lang['pages']['blog']['compose']['delete']['title'].'</h1>';

					foreach($lang['pages']['blog']['compose']['delete']['content'][$get_type] AS $content) {
						echo $Parsedown->text($content);
					}

					echo '<div class="options">';
						echo '<div><a href="'.url('blog/edit:'.$datetime.'/delete:'.$get_type).'" class="delete">';
							echo svgicon('trash');
							echo $lang['pages']['blog']['compose']['delete']['options']['yes'];
						echo '</a></div>';

						echo '<div><a href="'.url('blog/edit:'.$datetime).'" class="regret">';
							echo svgicon('goback');
							echo $lang['pages']['blog']['compose']['delete']['options']['no'];
						echo '</a></div>';
					echo '</div>';
				echo '</section>';



			} else {
				echo '<section id="blog-post-compose">';
					echo '<h1>';
						if($is_editing == false) {
							echo $lang['pages']['blog']['compose']['title']['new'];
						} else {
							echo $lang['pages']['blog']['compose']['title']['editing'];
						}
					echo '</h1>';

					echo '<form action="'.url('blog/'.($is_editing == false ? 'compose' : 'edit:'.$datetime)).'" method="POST" enctype="multipart/form-data">';
						echo '<input type="hidden" name="type" value="'.($is_published == true ? 'published' : ($is_drafted == true ? 'drafted' : 'new')).'">';

						echo '<details open>';
							echo '<summary>'.svgicon('flag-se') . $lang['pages']['blog']['compose']['language']['swedish'].'</summary>';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['pages']['blog']['compose']['subject'];
								echo '</div>';

								echo '<div class="value">';
									echo '<input type="text" name="subject-se"';
									echo (!isset($_SESSION['subject-se']) ? ($is_editing == false ? '' : ' value="'.$edit_subject_se.'"') : ' value="'.$_SESSION['subject-se'].'"');
									echo ' tabindex="1">';
								echo '</div>';
							echo '</div>';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['pages']['blog']['compose']['content'];
								echo '</div>';

								echo '<div class="value">';
									echo '<textarea name="content-se" tabindex="2">';
										echo (!isset($_SESSION['content-se']) ? ($is_editing == false ? '' : $edit_content_se) : $_SESSION['content-se']);
									echo '</textarea>';
								echo '</div>';
							echo '</div>';
						echo '</details>';


						echo '<details>';
							echo '<summary>'.svgicon('flag-en') . $lang['pages']['blog']['compose']['language']['english'].'</summary>';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['pages']['blog']['compose']['subject'];
								echo '</div>';

								echo '<div class="value">';
									echo '<input type="text" name="subject-en"';
									echo (!isset($_SESSION['subject-en']) ? ($is_editing == false ? '' : ' value="'.$edit_subject_en.'"') : ' value="'.$_SESSION['subject-en'].'"');
									echo ' tabindex="3">';
								echo '</div>';
							echo '</div>';

							echo '<div class="item">';
								echo '<div class="label">';
									echo $lang['pages']['blog']['compose']['content'];
								echo '</div>';

								echo '<div class="value">';
									echo '<textarea name="content-en" tabindex="4">';
										echo (!isset($_SESSION['content-en']) ? ($is_editing == false ? '' : $edit_content_en) : $_SESSION['content-en']);
									echo '</textarea>';
								echo '</div>';
							echo '</div>';
						echo '</details>';



						echo '<div class="item cover">';
							echo '<div class="label">';
								echo $lang['pages']['blog']['compose']['cover'];
							echo '</div>';

							echo '<div class="value">';
								if(empty($edit_cover)) {
									echo '<input type="file" name="file" accept="image/jpeg" tabindex="5">';

								} else {
									echo '<div class="cover">';
										echo '<div class="image" style="background-image: url('.$edit_cover.');"></div>';
									echo '</div>';
								}
							echo '</div>';
						echo '</div>';



						echo '<div class="checkboxes">';
							echo checkbox($lang['checkboxes']['nsfw'], 'nsfw', null, (!isset($_SESSION['check-nsfw']) ? ($is_editing == false ? false : $edit_check_nsfw) : $_SESSION['check-nsfw']));
							echo checkbox($lang['checkboxes']['post-not-valid'], 'notvalid', null, (!isset($_SESSION['check-notvalid']) ? ($is_editing == false ? false : $edit_check_notvalid) : $_SESSION['check-notvalid']));
							echo checkbox($lang['checkboxes']['updatelog'], 'updatelog', null, (!isset($_SESSION['check-updatelog']) ? ($is_editing == false ? false : $edit_check_updatelog) : $_SESSION['check-updatelog']));
							echo checkbox($lang['checkboxes']['hundreddaystooffload'], 'hundreddaystooffload', null, (!isset($_SESSION['check-hundreddaystooffload']) ? ($is_editing == false ? false : $edit_check_hundreddaystooffload) : $_SESSION['check-hundreddaystooffload']));

							echo '<div class="day">';
								echo $lang['pages']['blog']['compose']['hundreddaystooffload']['day'].' ';
								echo '<input type="text" name="field-hdto-day" inputmode="numeric" pattern="[0-9]*"';
								echo (!isset($_SESSION['hdto-day']) ? ($is_editing == false ? '' : ' value="'.$edit_hundreddaystooffload_day.'"') : ' value="'.$_SESSION['hdto-day'].'"');
								echo '>';
								echo ' '.mb_strtolower($lang['pages']['blog']['compose']['hundreddaystooffload']['of']).' 100';
							echo '</div>';
						echo '</div>';



						if($is_editing == true) {
							echo '<div class="options">';
								if(!empty($edit_cover)) {
									echo '<div><a href="'.url('blog/edit:'.$datetime.'/confirm-deletion:cover').'" class="side-by-side">';
										echo svgicon('trash') . $lang['pages']['blog']['compose']['options']['delete-cover'];
									echo '</a></div>';
								}

								echo '<div><a href="'.url('blog/edit:'.$datetime.'/confirm-deletion:post').'" class="side-by-side">';
									echo svgicon('trash') . $lang['pages']['blog']['compose']['options']['delete-post'];
								echo '</a></div>';
							echo '</div>';
						}



						echo '<div class="button'.($is_editing == false ? '' : ' editing').'">';
							if($is_editing == false) {
								echo '<input type="submit" name="publish" value="'.$lang['pages']['blog']['compose']['buttons']['publish'].'" tabindex="6">';
								#echo '<input type="submit" name="draft" value="'.$lang['pages']['blog']['compose']['buttons']['save-as-draft']['save'].'">';

								echo '<a href="'.url('blog/compose/cancel').'" class="cancel">';
									echo $lang['pages']['blog']['compose']['buttons']['cancel'];
								echo '</a>';

							} else {
								if($is_published == true) {
									echo '<input type="submit" name="save" value="'.$lang['pages']['blog']['compose']['buttons']['publish'].'" tabindex="6">';
									#echo '<input type="submit" name="draft" value="'.$lang['pages']['blog']['compose']['buttons']['save-as-draft']['published'].'">';

									echo '<a href="'.url('blog/'.$datetime).'" class="read">';
										echo $lang['pages']['blog']['compose']['options']['read'];
									echo '</a>';

									echo '<a href="'.url('blog/edit:'.$datetime.'/cancel').'" class="cancel editing">';
										echo $lang['pages']['blog']['compose']['buttons']['cancel'];
									echo '</a>';

								} elseif($is_drafted == true) {
									echo '<input type="submit" name="publish" value="'.$lang['pages']['blog']['compose']['buttons']['publish'].'" tabindex="6">';
									#echo '<input type="submit" name="draft" value="'.$lang['pages']['blog']['compose']['buttons']['save-as-draft']['save'].'">';
								}
							}
						echo '</div>';
					echo '</form>';
				echo '</section>';
			}
		}







		require_once 'site-footer.php';

	}

?>
