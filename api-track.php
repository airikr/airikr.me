<?php

	# Räkna antalet kilometer per timme (inte hastigheten)

	header('Content-Type: application/json;charset=utf-8');

	require_once 'site-settings.php';

	#use DerickR\GPX\Reader;
	#use Intervention\Image\ImageManager;
	#use Intervention\Image\Drivers\Imagick\Driver;
	use phpGPX\Models\GpxFile;
	use phpGPX\Models\Link;
	use phpGPX\Models\Metadata;
	use phpGPX\Models\Point;
	use phpGPX\Models\Segment;
	use phpGPX\Models\Track;



	$get_track = urlencode(safetag($_GET['trk']));
	$file = $dir_files.'/tracks/'.$get_track.'.gpx';
	$data = @simplexml_load_file($file);

	if(file_exists($file)) {
		$namespaces = $data->trk->getNamespaces(true);
		$ext = $data->trk->extensions->children($namespaces['gpxtrkx']);
		#$manager = new ImageManager(['driver' => 'imagick']);

		$movingtime_seconds = $ext->TrackStatsExtension->MovingTime;
		$movingtime_hours = floor($movingtime_seconds / 3600);
		$movingtime_minutes = floor(($movingtime_seconds - 3600 * $movingtime_hours) / 60);
		$distance = $ext->TrackStatsExtension->Distance;
		$speed_top = $ext->TrackStatsExtension->MaxSpeed;
		$speed_average = $distance / $movingtime_seconds;

		$arr_segments = [];
		$arr_elevations = [];

		# Helper Classes and Functions
		/*class LatLon {
			public $lat;
			public $lon;
			public $height;

			function __construct($lat, $lon, $height = 0) {
				$this->lat = $lat;
				$this->lon = $lon;
				$this->height = $height;
			}
		}

		function lon2x($lon, $pixels = 32768) {
			return (($lon + 180) / 360) * $pixels;
		}

		function lat2y($lat, $pixels = 32768) {
			return ((atanh(sin(deg2rad(-$lat))) / M_PI) + 1) * ($pixels / 2);
		}*/



		foreach($data->trk->trkseg->trkpt AS $segment) {
			$segment_namespaces = $segment->getNamespaces(true);
			$segment_ext = $segment->extensions->children($namespaces['gpxtpx']);
			$private_latitude = substr($segment->attributes()->lat, 0, 5) === '59.32';
			$private_longitude = substr($segment->attributes()->lon, 0, 5) === '13.48';

			$arr_segments[] = [
				'datetime' => new \DateTime($segment->time),
				'timestamp' => (int)strtotime($segment->time),
				'coordinates' => [
					'latitude' => (($private_latitude AND $private_longitude) ? 'private' : (float)$segment->attributes()->lat),
					'longitude' => (($private_latitude AND $private_longitude) ? 'private' : (float)$segment->attributes()->lon)
				],
				'elevation' => [
					'm' => (float)$segment->ele,
					'ft' => (float)format_number(($segment->ele * 3.280839895), 1, '.')
				],
				'speed' => [
					'ms' => (float)format_number((float)$segment_ext->TrackPointExtension->speed, 1, '.'),
					'kmh' => (float)format_number(($segment_ext->TrackPointExtension->speed * 3.6), 1, '.'),
					'mph' => (float)format_number(($segment_ext->TrackPointExtension->speed * 2.236936292), 1, '.')
				]
			];

			$arr_elevations[] = (float)$segment->ele;
		}

		$date = date('Y-m-d', $arr_segments[array_key_first($arr_segments)]['timestamp']);
		$time = date('H:i', $arr_segments[array_key_first($arr_segments)]['timestamp']);

		$arr = [
			'occurred' => [
				'date' => $date,
				'time' => $time,
				'filename' => $get_track
			],
			'movingtime' => [
				'hours' => (int)$movingtime_hours,
				'minutes' => (int)$movingtime_minutes
			],
			'distance' => [
				'm' => (int)$distance,
				'km' => (float)format_number(($distance / 1000), 1, '.'),
				'mi' => (float)format_number(($distance / 1609.344), 1, '.')
			],
			'speed' => [
				'top' => [
					'ms' => (float)format_number((float)$speed_top, 1, '.'),
					'kmh' => (float)format_number(($speed_top * 3.6), 1, '.'),
					'mph' => (float)format_number(($speed_top * 2.236936292), 1, '.')
				],
				'average' => [
					'ms' => (float)format_number((float)$speed_average, 1, '.'),
					'kmh' => (float)format_number(($speed_average * 3.6), 1, '.'),
					'mph' => (float)format_number(($speed_average * 2.236936292), 1, '.')
				]
			],
			'elevation' => [
				'max' => [
					'cm' => (max($arr_elevations) * 100),
					'm' => max($arr_elevations),
					'ft' => (max($arr_elevations) * 3.28084)
				],
				'min' => [
					'cm' => (min($arr_elevations) * 100),
					'm' => min($arr_elevations),
					'ft' => (min($arr_elevations) * 3.28084)
				]
			],
			'segment_count' => count($arr_segments),
			'segments' => $arr_segments
		];


		if(!file_exists($dir_files.'/tracks/json/'.strtotime($date.' '.$time).'.json')) {
			file_put_contents($dir_files.'/tracks/json/'.strtotime($date.' '.$time).'.json', json_encode($arr));
		}

// Creating sample link object for metadata
/*$link = new Link();
$link->href = "https://sibyx.github.io/phpgpx";
$link->text = 'phpGPX Docs';

// GpxFile contains data and handles serialization of objects
$gpx_file = new GpxFile();

// Creating sample Metadata object
$gpx_file->metadata = new Metadata();

// Time attribute is always \DateTime object!
$gpx_file->metadata->time = new \DateTime();

// Description of GPX file
$gpx_file->metadata->description = 'Downloaded from '.$site_domain.'/biking-track:'.strtotime($date.' '.$time).'. Owner: '.$config_webmaster.'. Coordinates close to owner\'s home is set to 0.0.';

// Adding link created before to links array of metadata
// Metadata of GPX file can contain more than one link
$gpx_file->metadata->links[] = $link;

// Creating track
$track = new Track();

// Name of track
$track->name = '';

// Type of data stored in track
$track->type = 'BIKING';

// Source of GPS coordinates
$track->source = $data->attributes()->creator;

// Creating Track segment
$segment = new Segment();


foreach ($arr_segments as $segpoint) {
	$point = new Point(Point::TRACKPOINT);
	$point->latitude = ($segpoint['coordinates']['latitude'] == 'private' ? '0.0' : $segpoint['coordinates']['longitude']);
	$point->longitude = ($segpoint['coordinates']['longitude'] == 'private' ? '0.0' : $segpoint['coordinates']['longitude']);
	$point->elevation = $segpoint['elevation']['m'];
	$point->time = $segpoint['datetime'];

	$segment->points[] = $point;
}

// Add segment to segment array of track
$track->segments[] = $segment;

// Recalculate stats based on received data
$track->recalculateStats();

// Add track to file
$gpx_file->tracks[] = $track;

// Serialized data as JSON
#$gpx_file->save('CreatingFileFromScratchExample.json', \phpGPX\phpGPX::JSON_FORMAT);

// Direct GPX output to browser

header('Content-Type: application/gpx+xml');
header('Content-Disposition: attachment; filename='.str_replace('.', '', $site_domain).'-biking-'.date('c', strtotime($date.' '.$time)).'.gpx');

echo $gpx_file->toXML()->saveXML();
exit();*/



		/*if(!file_exists($dir_files.'/tracks/images/'.$get_track.'.webp')) {

			# Code from https://gist.github.com/derickr/0e06c26835ff8ddcf3d62f10403adb8c

			# Configuration
			$zoomLevel = 13;
			$yourLocation = new LatLon(59.32857, 13.48539);
			$yourLocationSize = 20;
			$drawYourLocationBox = true;
			$hideFromYourLocationBox = true;

			# Constants, Don't Change
			$tileSize = 256;
			$file_name = $dir_files.'/tracks/'.$get_track.'.jpg';

			# Get track data
			$trackData = (new Reader($file))->getTrack()->getTrackData();

			# Finding Boundaries
			$minX = $minY = PHP_INT_MAX;
			$maxX = $maxY = PHP_INT_MIN;

			foreach($trackData AS $point) {
				if($point[0] < $minX) {
					$minX = $point[0];
				}

				if($point[0] > $maxX) {
					$maxX = $point[0];
				}

				if($point[1] < $minY) {
					$minY = $point[1];
				}

				if($point[1] > $maxY) {
					$maxY = $point[1];
				}
			}

			$p1 = new LatLon($minY, $minX);
			$p2 = new LatLon($maxY, $maxX);

			# Convert Coordinates to Pixels
			$pixels = pow(2, $zoomLevel) * $tileSize;

			$x1 = lon2x($p1->lon, $pixels);
			$x2 = lon2x($p2->lon, $pixels);
			$y1 = lat2y($p2->lat, $pixels);
			$y2 = lat2y($p1->lat, $pixels);

			$tx1 = floor($x1 / $tileSize);
			$tx2 = ceil($x2 / $tileSize);
			$ty1 = floor($y1 / $tileSize);
			$ty2 = ceil($y2 / $tileSize);

			$xOrig = $tx1 * $tileSize;
			$yOrig = $ty1 * $tileSize;

			$yourX = lon2x($yourLocation->lon, $pixels);
			$yourY = lat2y($yourLocation->lat, $pixels);

			$hideX = (floor($yourX / $yourLocationSize) * $yourLocationSize) - $xOrig;
			$hideY = (floor($yourY / $yourLocationSize) * $yourLocationSize) - $yOrig;

			# Allocate Image
			$map = imagecreatetruecolor(
				($tx2 - $tx1) * $tileSize,
				($ty2 - $ty1) * $tileSize
			);

			$white = imagecolorallocate($map, 255, 255, 255);
			$black = imagecolorallocate($map, 0, 0, 0);
			$red = imagecolorallocate($map, 200, 0, 0);
			$blue = imagecolorallocate($map, 0, 0, 200);

			# Create Image From Tiles
			$ctxt = stream_context_create(['http' => [ 'method' => 'GET', 'header' => 'User-Agent: '.$config_webmaster.'/'.$config_email]]);

			for($i = $tx1; $i < $tx2; $i++) {
				for($j = $ty1; $j < $ty2; $j++) {
					$im = imagecreatefromstring(file_get_contents('https://tile.openstreetmap.org/'.$zoomLevel.'/'.$i.'/'.$j.'.png', false, $ctxt));
					imagecopy($map, $im, ($i - $tx1) * $tileSize, ($j - $ty1) * $tileSize, 0, 0, $tileSize, $tileSize);
				}
			}

			if($drawYourLocationBox) {
				imageline($map, round($hideX), round($hideY), round($hideX + $yourLocationSize), round($hideY), $blue);
				imageline($map, round($hideX + $yourLocationSize), round($hideY), round($hideX + $yourLocationSize), round($hideY + $yourLocationSize), $blue);
				imageline($map, round($hideX + $yourLocationSize), round($hideY + $yourLocationSize), round($hideX), round($hideY + $yourLocationSize), $blue);
				imageline($map, round($hideX), round($hideY + $yourLocationSize), round($hideX), round($hideY), $blue);
			}

			# Draw Lines
			imagesetthickness($map, 2);
			imageantialias($map, true);

			for($i = 0; $i < count($trackData) - 2; $i++) {
				$x1 = lon2x($trackData[$i][0], $pixels) - $xOrig;
				$x2 = lon2x($trackData[$i+1][0], $pixels) - $xOrig;
				$y1 = lat2y($trackData[$i][1], $pixels) - $yOrig;
				$y2 = lat2y($trackData[$i+1][1], $pixels) - $yOrig;

				if($hideFromYourLocationBox) {
					if($x1 > $hideX && $x1 < ($hideX + $yourLocationSize) && $y1 > $hideY && $y1 < ($hideY + $yourLocationSize)) {
						continue;
					}

					if($x2 > $hideX && $x2 < ($hideX + $yourLocationSize) && $y2 > $hideY && $y2 < ($hideY + $yourLocationSize)) {
						continue;
					}
				}

				imageline($map, round($x1), round($y1), round($x2), round($y2), $red);
			}

			# Add Required Attribution
			$text = '(c) OpenStreetMap contributors, CC-BY-SA';
			$font = 'fonts/victor-mono-400.ttf';
			imagefilledrectangle($map, 0, 0, 600, 12, $white);
			imagettftext($map, 7, 0, 5, 10, $black, $font, $text);

			# Write Image
			imagejpeg($map, $file_name);

			$manager->make($file_name)->encode('webp', 70)->save($dir_files.'/tracks/images/'.$get_track.'.webp');

			$manager->make($file_name)->resize(100, null, function($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			})->save($dir_files.'/tracks/images/'.$get_track.'-thumb.webp');

			unlink($file_name);

		}*/



	} else {
		$arr = [
			'error' => 'File not found.',
			'file' => $get_track
		];
	}



	header("Location: ".url('biking-track:'.strtotime($arr['occurred']['date'].' '.$arr['occurred']['time'])));
	exit;

?>
