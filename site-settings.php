<?php session_start();

	use Jaybizzle\CrawlerDetect\CrawlerDetect;
	use ParagonIE\Halite\KeyFactory;

	require_once 'site-config.php';

	ini_set('display_errors', ($debugging == false ? 0 : 1));
	ini_set('display_startup_errors', ($debugging == false ? 0 : 1));
	error_reporting(E_ALL);

	date_default_timezone_set($config_timezone);

	$protocol = (stripos($_SERVER['SERVER_PROTOCOL'], 'https') === 0 ? 'https://' : 'http://');
	$host = $_SERVER['HTTP_HOST'];
	$uri = strtok($_SERVER['REQUEST_URI'], '?');
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	$is_local = (($host == 'localhost' OR strpos($host, '192.168') !== false) ? true : false);
	$is_loggedin = (isset($_SESSION['admin']) ? true : false);
	$is_reading = false;

	$filename = basename($_SERVER['PHP_SELF']);
	$filename_get = (!empty($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : null);
	$currentpage = trim(str_replace('/'.basename(__DIR__).'/', '', (strpos($uri, '?') === false ? $uri : substr($uri, 0, strpos($uri, '?')))), '/');

	$robots = ['nofollow','nosnippet','noarchive','noimageindex','noai','noimageai','notranslate','nositelinkssearchb'];

	$dir_files = $config_root.'/files/airikr';
	$dir_css = 'css';
	$dir_css_pages = 'css/pages';
	$dir_css_pages_minified = 'css/pages/minified';
	$dir_fonts = 'fonts';
	$dir_functions = 'functions';
	$dir_images = 'images';
	$dir_js = 'js';
	$dir_languages = 'languages';
	$dir_posts = 'posts';

	require_once 'vendor/autoload.php';

	$Parsedown = new Parsedown();
	$CrawlerDetect = new CrawlerDetect;

	require_once 'site-functions.php';

	$get_lang = (isset($_GET['lang']) ? safetag($_GET['lang']) : 'se');
	$get_theme = (isset($_GET['thm']) ? safetag($_GET['thm']) : 'dark');
	$get_logging = (isset($_GET['log']) ? safetag($_GET['log']) : '0');

	require_once $dir_languages.'/'.(isset($_GET['lang']) ? (file_exists($dir_languages.'/'.$get_lang.'.php') ? $get_lang : 'en') : 'se').'.php';

	#$is_logging = ((isset($_GET['log']) AND safetag($_GET['log'])) == 1 ? true : false);
	$is_logging = false;

	create_folder($dir_files.'/logs');

	if($is_logging == true) {
		create_folder($dir_files.'/logs/'.hash('sha256', getip()));
		create_folder($dir_files.'/logs/'.hash('sha256', getip()).'/pages');
		create_folder($dir_files.'/logs/'.hash('sha256', getip()).'/keystrokes');
		create_folder($dir_files.'/logs/'.hash('sha256', getip()).'/mouse');
	}

	create_folder($dir_files.'/posts/comments');
	create_folder($dir_files.'/posts/webmentions');
	create_folder($dir_files.'/posts/published');
	create_folder($dir_files.'/posts/reactions');
	create_folder($dir_files.'/posts/removed-nsfw-mark');
	create_folder($dir_files.'/posts/marked-as-read');
	create_folder($dir_files.'/tracks');
	create_folder($dir_files.'/tracks/json');
	create_folder($dir_files.'/videos');
	create_folder($dir_files.'/visitors');
	create_folder($dir_files.'/uploads');
	create_folder($dir_files.'/uploads/covers');
	create_folder($dir_files.'/gallery');
	create_folder($dir_files.'/gallery/likes');
	create_folder($dir_files.'/gallery/accepted-sensitive');
	create_folder($dir_files.'/guestbook');
	create_folder($dir_files.'/videofeed');
	create_folder($dir_files.'/videofeed/videos');
	create_folder($dir_files.'/videofeed/channels');

	$arr_beginswith = [
		'<h', '<u', '<l', '<div', '</', '<video'
	];

	$arr_reactions = [
		[
			'emoji' => 'happy',
			'title' => $lang['tooltips']['emoji-happy']
		],
		[
			'emoji' => 'laugh',
			'title' => $lang['tooltips']['emoji-laugh']
		],
		[
			'emoji' => 'sad',
			'title' => $lang['tooltips']['emoji-sad']
		],
		[
			'emoji' => 'angry',
			'title' => $lang['tooltips']['emoji-angry']
		],
		[
			'emoji' => 'thumbup',
			'title' => $lang['tooltips']['emoji-like']
		],
		[
			'emoji' => 'thumbdown',
			'title' => $lang['tooltips']['emoji-dislike']
		],
		[
			'emoji' => 'heart',
			'title' => $lang['tooltips']['emoji-heart']
		]
	];

	$arr_devices = [
		[
			'sid' => 134921431,
			'icon' => 'device-desktop',
			'tooltip' => $lang['tooltips']['devices']['desktop'],
			'code' => 'C56E58BC C156B4DC 251FC9F7 F8DFAC4A 8FB0FD65 AEC2117A 4E2DE070 F88C5C56'
		],
		[
			'sid' => 2076366448,
			'icon' => 'device-laptop',
			'tooltip' => $lang['tooltips']['devices']['laptop'],
			'code' => '7445532C 33F99DC1 2AB93E93 11584160 8E778F2D 4AEA487B CD212361 2B83F236'
		],
		[
			'sid' => 165313305,
			'icon' => 'device-smartphone',
			'tooltip' => $lang['tooltips']['devices']['smartphone'],
			'code' => '921FCA11 E39C4387 72C6D77D 00353B0D 2B702FC9 B6D08CF9 24701E3F DCF5F615'
		]
	];



	if(!file_exists($dir_files.'/encryption.key')) {
		$enckey = KeyFactory::generateEncryptionKey();
		KeyFactory::save($enckey, $dir_files.'/encryption.key');
	}



	$metadata_title = null;
	$metadata_image_width = 1200;
	$metadata_image_height = 628;

	if($filename == 'index.php') {
		$metadata_title = $lang['nav']['about'];
		$metadata_description = $lang['metadata']['descriptions']['about'];
		$metadata_image = url('images/selfie.webp', true);

	} elseif($filename == 'page-me.php') {
		$metadata_title = $lang['pages']['me']['metadata']['title'];
		$metadata_description = $lang['metadata']['descriptions']['me'];
		$metadata_image = url('images/metadata-me.webp', true);

	} elseif($filename == 'page-slogan.php') {
		$metadata_title = $lang['pages']['slogan']['title'];
		$metadata_description = $lang['metadata']['descriptions']['slogan'];
		$metadata_image = url('images/metadata-universal.webp', true);

	} elseif($filename == 'page-uses.php') {
		$metadata_title = $lang['pages']['uses']['title'];
		$metadata_description = $lang['metadata']['descriptions']['uses'];
		$metadata_image = url('images/metadata-universal.webp', true);

	} elseif($filename == 'page-biking.php') {
		$metadata_title = $lang['nav']['biking'];
		$metadata_description = $lang['metadata']['descriptions']['biking'];
		$metadata_image = url('images/metadata-biking.webp', true);

	} elseif($filename == 'page-biking-track.php') {
		$get_track = safetag($_GET['trk']);
		$data = json_decode(file_get_contents($dir_files.'/tracks/json/'.$get_track.'.json'), true);

		#list($width, $height) = getimagesize($dir_files.'/tracks/images/'.$data['occurred']['filename'].'.webp');

		$metadata_title = $lang['nav']['biking'].' @ '.$data['occurred']['date'].', '.$data['occurred']['time'];
		$metadata_description = $data['occurred']['date'].', '.$data['occurred']['time'].': ';
		$metadata_description .= format_number($data['distance']['km']).' km';
		$metadata_description .= ', '.$data['movingtime']['hours'] . $lang['pages']['biking']['details']['hour'].' '.$data['movingtime']['minutes'] . $lang['pages']['biking']['details']['minute'];
		/*$metadata_image = url('track:'.$data['occurred']['filename'], true);
		$metadata_image_width = $width;
		$metadata_image_height = $height;*/

	} elseif($filename == 'page-blog-posts.php') {
		$metadata_title = $lang['nav']['blog'];
		$metadata_description = $lang['metadata']['descriptions']['blog'];
		$metadata_image = url('metadata:blogposts', true);

	} elseif($filename == 'page-blog-post.php') {
		$get_year = safetag($_GET['ye']);
		$get_month = safetag($_GET['mo']);
		$get_day = safetag($_GET['da']);
		$get_hour = safetag($_GET['ho']);
		$get_minute = safetag($_GET['mi']);
		$datetime = $get_year . $get_month . $get_day . $get_hour . $get_minute;

		if(file_exists($dir_files.'/posts/published/'.$datetime.'.json')) {
			$is_reading = true;
			$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$datetime.'.json'), false);
			$page_title = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);
			$metadata_title = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);
			$metadata_image = null;
			$metadata_description = ((!isset($post->nsfw) OR (isset($post->nsfw) AND $post->nsfw == false)) ? strip_tags($Parsedown->text(($get_lang == 'se' ? $post->content->se : $post->content->en))) : $lang['metadata']['descriptions']['nsfw']);

			if(isset($post->updatelog) AND $post->updatelog == true) {
				$metadata_image = url($dir_images.'/metadata-updatelog.webp', true);

			} elseif(isset($post->nsfw) AND $post->nsfw == true) {
				$metadata_image = url($dir_images.'/metadata-nsfw.webp', true);

			} elseif(isset($post->hundreddaystooffload->enabled) AND $post->hundreddaystooffload->enabled == true) {
				$metadata_image = url($dir_images.'/100DaysToOffload.webp', true);

			} else {
				if(file_exists($dir_files.'/uploads/covers/'.$datetime.'.webp')) {
					$metadata_image = url('cover:'.$datetime, true);
				}
			}
		}

	} elseif($filename == 'page-gallery-photo.php') {
		$get_photo = safetag($_GET['nam']);

		if(file_exists($dir_files.'/gallery/photos/'.$get_photo.'/800/'.$get_photo.'.webp')) {
			$photo = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$get_photo.'/800/'.$get_photo.'.webp'), false);
			$exif = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$get_photo.'/'.$get_photo.'.json'), false);
			$is_sensitive = $exif->sensitive;
			$metadata_title = $get_photo;
			$metadata_image = ($is_sensitive == false ? url('gallery-view:'.$get_photo, true) : url('images/metadata-gallery-sensitivephoto.webp', true));
			$metadata_description = $lang['metadata']['descriptions']['gallery']['photo'.($is_sensitive == false ? '' : '-sensitive')];
		}

	} elseif($filename == 'page-gallery-photos.php') {
		$metadata_title = $lang['pages']['gallery']['title'];
		$metadata_description = $lang['metadata']['descriptions']['gallery']['photos'];
		$metadata_image = url('images/metadata-universal.webp', true);

	} elseif($filename == 'page-copyright.php') {
		$metadata_title = $lang['pages']['copyright']['title'];
		$metadata_description = $lang['metadata']['descriptions']['copyright'];
		$metadata_image = url('images/metadata-universal.webp', true);

	} elseif($filename == 'page-privacy.php') {
		$metadata_title = $lang['footer']['privacy'];
		$metadata_description = $lang['metadata']['descriptions']['privacy'];
		$metadata_image = url('images/metadata-universal.webp', true);

	} elseif($filename == 'page-chat.php') {
		$metadata_title = $lang['pages']['chat']['title'];
		$metadata_description = $lang['metadata']['descriptions']['chat'];
		$metadata_image = url('images/airikr-chat-'.$lang['metadata']['language'].'.webp', true);

	} elseif($filename == 'page-contact.php') {
		$metadata_title = $lang['footer']['contact'];
		$metadata_description = $lang['metadata']['descriptions']['contact'];
		$metadata_image = url('images/metadata-universal.webp', true);

	} elseif($filename == 'page-links.php') {
		$metadata_title = $lang['nav']['links'];
		$metadata_description = $lang['metadata']['descriptions']['links'];
		$metadata_image = url('images/metadata-universal.webp', true);

	} elseif($filename == 'page-guestbook.php') {
		$metadata_title = $lang['nav']['guestbook'];
		$metadata_description = $lang['metadata']['descriptions']['guestbook'];
		$metadata_image = url('images/metadata-universal.webp', true);

	} else {
		$metadata_description = $lang['metadata']['descriptions']['default'];
		$metadata_image = null;
	}



	if($is_loggedin == true) {
		if(!file_exists($dir_files.'/admin.json')) {
			session_unset();
			session_destroy();

			die(simplepage('<div class="error">'.$Parsedown->text($lang['messages']['errors']['admin-notexists']).'</div>'));
		}
	}



	# Save information about the visitor if `$is_logging` is set to `true`
	if($is_logging == true) {
		$date = DateTime::createFromFormat('U.u', microtime(TRUE));
		$file_info = pathinfo($filename);
		$file_visitor = $dir_files.'/logs/'.hash('sha256', getip()).'/visitor.json';
		$file_page = $dir_files.'/logs/'.hash('sha256', getip()).'/pages/'.$date->format('YmdHis-u').'.json';

		$parse_url_previous = (isset($_SERVER['HTTP_REFERER']) ? parse_url($_SERVER['HTTP_REFERER']) : null);
		$parse_url_current = parse_url($_SERVER['REQUEST_URI']);
		$parse_useragent = new WhichBrowser\Parser($useragent);

		$arr_datetime = [
			'date' => [
				'year' => (int)$date->format('Y'),
				'month' => (int)$date->format('m'),
				'day' => (int)$date->format('d')
			],
			'time' => [
				'hour' => (int)$date->format('H'),
				'minute' => (int)$date->format('i'),
				'second' => (int)$date->format('s'),
				'microsecond' => (int)$date->format('u')
			]
		];

		$arr_visitor = [
			'ip' => [
				'plaintext' => getip(),
				'hashed' => [
					'sha256' => hash('sha256', getip()),
					'md5' => hash('md5', getip())
				]
			],
			'useragent' => $_SERVER['HTTP_USER_AGENT'],
			'browser' => $parse_useragent->browser->toString(),
			'operating_system' => $parse_useragent->os->toString(),
			'firstvisit' => [
				'timestamp' => time(),
				'detailed' => $arr_datetime
			]
		];

		$arr_page = [
			'filename' => [
				'path' => (empty($parse_url_current['path']) ? null : $parse_url_current['path']),
				'query' => (empty($parse_url_current['query']) ? null : $parse_url_current['query']),
				'fragment' => (empty($parse_url_current['fragment']) ? null : $parse_url_current['fragment'])
			],
			'previous_url' => [
				'scheme' => (empty($parse_url_previous['scheme']) ? null : $parse_url_previous['scheme']),
				'domain' => (empty($parse_url_previous['host']) ? null : $parse_url_previous['host']),
				'path' => (empty($parse_url_previous['path']) ? null : $parse_url_previous['path']),
				'query' => (empty($parse_url_previous['query']) ? null : $parse_url_previous['query']),
				'fragment' => (empty($parse_url_previous['fragment']) ? null : $parse_url_previous['fragment'])
			],
			'visited' => $arr_datetime
		];

		if(!file_exists($file_visitor)) {
			file_put_contents($file_visitor, json_encode($arr_visitor));
		}

		file_put_contents($file_page, json_encode($arr_page), LOCK_EX);
	}



	if($filename != 'open-visitorfile.php') {
		$hashed_ip = hash('sha256', getip());
		$badvisitor = false;
		$notchecked = true;

		if(glob($dir_files.'/visitors/'.$hashed_ip.'.json')) {
			$visitorinfo = json_decode(file_get_contents($dir_files.'/visitors/'.$hashed_ip.'.json'), true);
			$notchecked = false;

			if($visitorinfo['bypassed'] == false AND $visitorinfo['isverified'] == true) {
				$badvisitor = true;

				if($filename == 'page-admin.php' OR $filename == 'page-links.php') {
					http_response_code(403);
					die(simplepage('403 Forbidden'));
				}
			}
		}
	}

	if(strpos($filename, 'blog') !== false AND $CrawlerDetect->isCrawler() AND strpos(mb_strtolower($_SERVER['HTTP_USER_AGENT']), 'mastodon') === false AND strpos(mb_strtolower($_SERVER['HTTP_USER_AGENT']), 'telegram') === false) {
		die(simplepage('Access denied'));
	}



	$arr_adminpages = [
		'page-admin-overview.php',
		'page-admin-filter-guestbook.php',
		'page-admin-filter-reactions.php',
		'page-admin-upload.php',
		'page-admin-uploads.php'
	];

	if(in_array($filename, $arr_adminpages) AND $is_loggedin == false) {
		header("Location: ".url('admin'));
		exit;
	}

?>
