<?php

	$get_year = (!isset($_GET['ye']) ? '' : safetag($_GET['ye']));
	$get_month = (!isset($_GET['mo']) ? '' : safetag($_GET['mo']));
	$get_day = (!isset($_GET['da']) ? '' : safetag($_GET['da']));
	$get_hours = (!isset($_GET['ho']) ? '' : safetag($_GET['ho']));
	$get_minutes = (!isset($_GET['mi']) ? '' : safetag($_GET['mi']));

	$arr_functions = [
		'[erusev/parsedown](https://github.com/erusev/parsedown): enables Markdown for all text on the website.',
		'[matthiasmullie/minify](https://github.com/matthiasmullie/minify): minifying CSS.',
		'[intervention/image](https://github.com/intervention/image): minimise and convert uploaded images.',
		'[whichbrowser/parser](https://github.com/whichbrowser/parser): analyse user agents (not currently used).',
		'[paragonie/halite](https://github.com/paragonie/halite): encrypt and decrypt text and files.',
		'[geertw/ip-anonymizer](https://github.com/geertw/php-ip-anonymizer): anonymise IP addresses.',
		'[multiavatar/multiavatar-php](https://github.com/multiavatar/multiavatar-php): generate display images for the comments on the blog.',
		'[jaybizzle/crawler-detect](https://github.com/jaybizzle/crawler-detect): detecting bots and similar shit.',
		'[robthree/twofactorauth](https://github.com/robthree/twofactorauth): generate and verify two-step verification codes.',
		'[chillerlan/php-qrcode](https://github.com/chillerlan/php-qrcode): generate QR codes.',
		'[PHPMailer/PHPMailer](https://github.com/PHPMailer/PHPMailer): emails me when someone has submitted a new guestbook post.',
		'[oscarotero/Embed](https://github.com/oscarotero/Embed): loads the metadata from web pages on the "[Metadata]('.url('metadata').')" page.'
	];



	$lang = [
		'metadata' => [
			'iso6391' => 'en',
			'language' => 'en',
			'locale' => 'en_GB',
			'descriptions' => [
				'default' => 'Read about Erik, follow his blog, contact him, and more.',
				'about' => 'Read who Erik is.',
				'blog' => 'Read Erik\'s blog.',
				'slogan' => 'Read Erik\'s slogan: my life, my data, my requirements.',
				'privacy' => 'Read the privacy policy for '.$site_title,
				'biking' => 'Follow Erik\'s cycling adventure.',
				'biking-track' => 'See more about one of Erik\'s cycling adventures.',
				'copyright' => 'Read the copyright for Erik\'s material.',
				'chat' => 'Read the rules for Airikr Chat, among others.',
				'contact' => 'Find out how you can contact Erik.',
				'links' => 'See Erik\'s collection of links to his projects and more.',
				'me' => 'Get information mainly on how to contact me.',
				'guestbook' => 'Read and/or sign my guestbook.',
				'nsfw' => 'NSFW post. Open the link and select "Yes" if you want to read it.',
				'uses' => 'Find out what Erik uses.',
				'rss' => [
					'blog' => 'Read Erik\'s thoughts on privacy, cycling and more.',
					'comments' => 'Follow the comments on Erik\'s blog.',
					'biking' => 'Follow Erik\'s journey to a better and healthier life by bike.',
					'gallery' => 'Follow Erik\'s uploads to his gallery.',
					'guestbook' => 'Follow the guestbook entries published in Erik\'s guestbook.'
				],
				'gallery' => [
					'photos' => 'See all the photos that Erik has uploaded to his gallery.',
					'photo' => 'Take a closer look at a photograph that Erik has uploaded to his gallery.',
					'photo-sensitive' => 'Open the link and make a selection if you want to display the sensitive image.'
				]
			]
		],

		'ipintel' => [
			'title' => 'Please approve external data storing',
			'content' => [
				'I am tired of robots spamming mainly the guestbook. Have therefore added an API called [IP Intelligence](https://getipintel.net/) (abbreviated IPIntel).',
				'But since the IP address your device uses is then stored on a server that I don\'t manage, I want you to approve this. Go to the [privacy page]('.url('privacy').') and read point 1 about what is stored at IPIntel.',
				'You can choose to ignore this and continue browsing around on '.$site_title.'. Just keep in mind that you can\'t do anything active then, such as reacting to a blog post.'
			],
			'agree' => 'Approve'
		],

		'nav' => [
			'open' => 'Show the menu',
			'close' => 'Hide the menu',
			'about' => 'About',
			'blog' => 'Blog',
			'gallery' => 'Gallery',
			'biking' => 'Biking',
			'links' => 'Links',
			'guestbook' => 'Guestbook'
		],

		'footer' => [
			'contact' => 'Contact',
			'privacy' => 'Privacy',
			'ai' => 'AI',
			'rss' => 'RSS',
			'colophone' => 'Colophone',
			'sitemap' => 'Sitemap',
			'chat' => 'Airikr Chat',
			'source-code' => 'Source code',
			'creative-commons' => 'Creative commons',
			'login' => 'Log in',
			'logout' => 'Log out',
			'telemetrysimulation' => [
				'enable' => 'Telemetry simulator',
				'disable' => 'Turn off',
				'your-data' => 'Your data'
			],
			'admin' => 'Admin',
			'language' => 'Language',
			'theme' => 'Theme',
			'themes' => [
				'dark' => 'Dark',
				'light' => 'Light'
			],
			'average-speed' => 'Average speed',
			'wpm' => 'Words per minute',
			'loading-time' => 'It took **{lt} seconds** to load the page.'
		],

		'messages' => [
			'no-blogstats' => 'Could not find any statistics',
			'no-posts' => 'There are no posts yet',
			'no-posts-search' => 'Could not find any posts',
			'no-images' => 'There are no uploaded images yet',
			'no-tracks' => 'There are no uploaded GPX tracks yet',
			'no-videos' => 'There are no uploaded videos yet',
			'no-covers' => 'There are no uploaded cover images yet',
			'no-comments' => 'Could not find any comments',
			'no-photos' => 'Could not find any photographs',
			'no-posts-guestbook' => 'It was empty. Maybe you want to be the first? :)',
			'no-webmentions' => 'Here it was empty',
			'admin-exists' => 'There is already an admin',
			'guestbook-thanks' => 'Thank you for your post :)',
			'disclaimers' => [
				'post-not-valid' => [
					'This post contains information that is no longer valid. For example, a feature on the blog may have been updated.'
				],
				'old-post' => [
					'This post is older than 3 years. As my personality changes over time, the text below may not be accurate today.'
				]
			],
			'errors' => [
				'not-admin' => 'You do not have access to this.',
				'empty-fields' => [
					'# Mandatory text fields not filled in',
					'At least one of the following text fields is not yet filled in. Please fill in the missing ones in order to send your comment.'
				],
				'notvalid-email' => [
					'# Not valid e-mail address',
					'The email address you have entered '.(isset($_SESSION['email']) ? '('.$_SESSION['email'].')' : '').', is not valid.',
					'A valid address should have the following format: `namn@domän.tld`'
				],
				'notvalid-url' => [
					'# Not valid web address',
					'A valid URL should contain the secure protocol (`https://`) and consist of `domain.tld` (for example airikr.me).'
				],
				'wrong-username' => 'You have entered the wrong username.',
				'wrong-password' => 'You have entered the wrong password. Please try again.',
				'wrong-tfa' => 'You have entered the wrong two-step verification code. Please try again.',
				'tfa-onlydigits' => 'The two-step verification code may only contain numbers. Please try again.',
				'tfa-tooshortorlong' => 'The two-step verification code can only be 6 digits long. Please try again.',
				'no-admin' => 'The web page has no users with administrative rights. [Create one now]('.url('admin/create').').',
				'admin-notexists' => 'The `admin.json` has been deleted for some reason. [Recreate the admin account]('.url('admin/create').').',
				'onlydigits-wpm' => 'Words per minute can only contain numbers.',
				'keyword-empty' => 'You must enter a search term first. [Show all blog posts]('.url('blog').').',
				'keyword-tooshort' => 'The keyword must contain at least 3 letters. [Show all blog posts]('.url('blog').').',
				'keyword-onlychars' => 'The search term may only contain letters. [Show all blog posts]('.url('blog').').'
			]
		],

		'tooltips' => [
			'filter-by-year' => 'Filter by year',
			'filter-by-month' => 'Filter by month',
			'filter-by-date' => 'Filter by date',
			'emoji-happy' => 'Happy',
			'emoji-laugh' => 'Laugh',
			'emoji-sad' => 'Sad',
			'emoji-angry' => 'Angry',
			'emoji-like' => 'Like',
			'emoji-dislike' => 'Dislike',
			'emoji-heart' => 'Love',
			'your-reaction' => 'Your reaction',
			'rss-comments' => 'Follow the comments feed via RSS',
			'read-post' => 'Read the blog post',
			'goto-comment' => 'Go to the comment in the post',
			'directlink-comment' => 'Direct link to the comment',
			'old-comment-system' => 'The comment was published before the new system was introduced',
			'devices' => [
				'desktop' => 'Stationary PC',
				'laptop' => 'Laptop',
				'smartphone' => 'Smartphone'
			]
		],

		'checkboxes' => [
			'nsfw' => 'Mark the post as sensitive',
			'post-not-valid' => 'The post contains information that is no longer valid or accurate',
			'updatelog' => 'The post is a changelog',
			'hundreddaystooffload' => 'This post is part of #100DaysToOffload'
		],

		'rss' => [
			'titles' => [
				'blog' => 'Blog',
				'comments' => 'Comments',
				'biking' => 'Biking',
				'gallery' => 'Gallery',
				'guestbook' => 'Guestbook'
			],
			'occurred' => 'When',
			'movingtime' => 'Moving time',
			'distance' => 'Distance',
			'speed-top' => 'Top speed',
			'speed-average' => 'Average speed',
			'hour' => 'h',
			'minute' => 'm',
			'camera' => 'Camera',
			'taken' => 'Taken',
			'iso' => 'ISO',
			'aperture' => 'Apoerture',
			'exposure' => 'Exposure'
		],

		'months' => [
			1 => 'January',
			2 => 'February',
			3 => 'Mars',
			4 => 'April',
			5 => 'May',
			6 => 'June',
			7 => 'July',
			8 => 'August',
			9 => 'September',
			10 => 'October',
			11 => 'November',
			12 => 'December'
		],

		'days' => [
			1 => 'Mon',
			2 => 'Tue',
			3 => 'Wed',
			4 => 'Thu',
			5 => 'Fri',
			6 => 'Sat',
			7 => 'Sun'
		],

		'goback' => 'Go back',
		'summertime' => 'Summer time',
		'wintertime' => 'Winter time',



		'pages' => [
			'about' => [
				'about' => [
					'## Hello and welcome!',
					'Erik is my name, but because I love history, I call myself Airikr. I was born in the summer of 1985 ('.age(1985, 7, 6, 1838).' years ago) on the archipelago island of Hammarö in Värmland County, Sweden.',

					'## Disease and diagnoses',
					'I was born with [antiphospholipid syndrome](https://en.wikipedia.org/wiki/Antiphospholipid_syndrome). At the age of 10 I was diagnosed with DAMP, and at the age of 28 I was rediagnosed with mild Asperger\'s syndrome and severe attention deficit disorder.',

					'## Interests',
					'Privacy, history, photography, cycling and discovering places are closest to my heart.'
				],
				'slogan' => 'My life, my data, my demands!',
				'videofeed' => [
					'title' => 'Latest video clips'
				]
			],


			'slogan' => [
				'title' => 'My life, my data, my demands',
				'content' => [
					'I can be seen as very concerned about my privacy, but also about the privacy of others. I want to be able to decide how my own data is processed outside Sweden\'s borders, but also within those borders. Everyone should be concerned about their privacy, but unfortunately this is not the case.',
					'Big companies like Google, Apple and Microsoft can do pretty much whatever they want with our data - many don\'t give a damn. Some people think, "but I live in the EU, the GDPR should take care of my privacy for me". No, this is not the case.',
					'The GDPR exists to make it easier to manage your personal data. It does not prevent third parties from taking pictures and videos of your children, for example, and using them for purposes such as AI without asking your permission first.',
					'If you don\'t want your personal data to fall into the wrong hands, you have to take care of it yourself. No one can do anything about your online privacy but you.',
					'Below I explain what I mean by my slogan.',
				],
				'mylife' => [
					'title' => 'My life',
					'content' => [
						'I should have the right to decide where my own data is stored, how it is processed and what it is used for. This is my life, not someone else\'s.'
					]
				],
				'mydata' => [
					'title' => 'My data',
					'content' => [
						'I should have the right to easily access and manage my own data. If I want to delete any data that belongs to me as a person (for example my date of birth), the owner of the server should comply if I want to delete my data.'
					]
				],
				'mydemands' => [
					'title' => 'My demands',
					'content' => [
						'I should have the right to decide how my own data is stored and how it is processed. If it is processed in a way I don\'t like, I should be able to demand that the owner of the server be able to handle my data according to my requirements.'
					]
				]
			],



			'blog' => [
				'write-empty' => 'Write a post to get started',
				'nav' => [
					'rss' => 'Subscribe via RSS',
					'write-notempty' => 'Write a new post'
				],
				'subtitles' => [
					'filtering' => 'Showing posts from',
					'filtering-date' => 'Showing posts from',
					'searching' => 'Showing posts with the keyword',
					'hundreddaystooffload' => 'Showing posts included in '.link_('100DaysToOffload' ,'https://100daystooffload.com'),
					'filter' => [
						'label' => 'Filter',
						'posts' => 'Posts',
						'comments' => 'Comments',
						'reactions' => 'Reactions',
						'hundreddaystooffload' => '100DaysToOffload'
					]
				],
				'statistics' => [
					'title' => 'Statistics',
					'content' => [
						'The first post was published **{firstpost}** and the blog today has **{posts} posts**, of which **{posts_hundreddaystooffload}** of them have the hashtag 100DaysToOffload. Since the start, a total of **{reactions} reactions** have been made.',
						'All posts contain **{images} pictures** and **{links} links**, and contain a total of **{words} words**. This corresponds to **{compare_lotr}** of The Lord of the Rings trilogy and **{compare_thebible}** of the King James Bible.',
						'As this is a static blog, all files take a total of **{totalsize}**.'
					]
				],
				'calendar' => [
					'title' => 'Calendar',
					'months' => [
						1 => 'January',
						2 => 'February',
						3 => 'March',
						4 => 'April',
						5 => 'May',
						6 => 'June',
						7 => 'July',
						8 => 'August',
						9 => 'September',
						10 => 'October',
						11 => 'November',
						12 => 'December'
					]
				],
				'reactions' => [
					'notchecked' => [
						'You must approve the external data storage before you can react.'
					]
				],
				'list' => [
					'published' => 'Published',
					'subject' => 'Subject',
					'reactions' => [
						'title' => 'Reactions',
						'info' => 'The 100 most recent reactions are listed below.',
						'and-counting' => 'More reactions and the number is continuously increasing.'
					]
				],
				'search' => [
					'placeholder' => 'Search the posts',
					'year' => 'Year',
					'month' => 'Month',
					'button' => 'Search'
				],
				'read' => [
					'of' => 'Of',
					'table-of-content' => 'Table of content',
					'word' => 'Word',
					'words' => 'Words',
					'edit' => 'Edit',
					'goto' => 'Go to',
					'reaction' => 'Reaction',
					'reactions' => 'Reactions',
					'comment' => 'Comment',
					'comments' => 'Comments',
					'paging' => [
						'showall' => 'Show all posts',
						'random' => 'Random post',
						'previous' => 'Newer',
						'next' => 'Older'
					],
					'hundreddaystooffload' => '100DaysToOffload',
					'controversial-thought' => 'Controversial thought',
					'controversial-thought-readmore' => 'Controversial idea (open the link to read it)',
					'spoiler' => 'Spoiler',
					'spoiler-readmore' => 'Spoiler (open the link to read it)',
					'video' => 'Video',
					'webmentions' => [
						'If the post has been shared within the fediverse, such as on Mastodon, favourites, boosts and replies will be listed below.'
					],
					'nsfw' => [
						'hide-post' => 'Hide the post again',
						'content' => [
							'This post may be seen as offensive, causing offence, or similar. There may be a risk that the post contains controversial ideas from the author.',
							'If you want to know how your data is handled if you want to read the post, please go to [the privacy page]('.url('privacy').') and read point 3.6.',
							'Want to read the post? [Yes]('.url('blog/'.$get_year . $get_month . $get_day . $get_hours . $get_minutes.'/remove-nsfw-mark').') / [No]('.url('blog/'.$get_year . $get_month . $get_day . $get_hours . $get_minutes.'/random-post').')'
						]
					],
					'readstatus' => [
						'title' => 'Read status',
						'desc' => [
							'Please read point 3.6 of the [privacy policy]('.url('privacy').') to find out how your data is handled.'
						],
						'options' => [
							'read' => 'Mark this post as read',
							'unread' => 'Mark this post as unread'
						],
						'reader' => 'Reader',
						'readers' => 'Readers'
					],
					'share' => [
						'title' => 'Share this post'
					]
				],
				'comment' => [
					'new' => 'Leave a comment',
					'manage' => 'Manage a comment',
					'options' => [
						'manage' => 'Manage'
					],
					'terms' => [
						'## Terms',
						'If your comment does not follow the rules listed below, it will be deleted without prior notice.',
						"1. Stick to the same topic as the blog post.\n1. Follow the laws of Sweden. Posts that do not follow the laws of the country will be reported to the police.\n1. Do not spam the feed.\n1. Respect others. We are all different."
					],
					'delete' => [
						'## You will soon be able to delete your comment',
						'Creating a static blog from scratch is not easy. It requires a lot of time and smart thinking. For now, you can\'t delete your comment, but you will be able to do so shortly. When will this happen? Unknown. Please be patient :)',
						'If you can\'t wait, [contact me]('.url('contact').') and give me the password for your comment. I will then manually delete your comment.'
					],
					'form' => [
						'name' => 'Name',
						'email' => 'Email aadress',
						'website' => 'Web address',
						'password' => 'Password',
						'comment' => 'Comments',
						'cancel' => 'Cancel',
						'delete' => 'Delete',
						'send' => 'Send',
						'save' => 'Save',
						'descriptions' => [
							'password' => [
								'If you want to be able to change or delete your comment, please save the password above in a safe place. The password is based on the current time in microseconds (based on when the page was loaded).',
								'One tip is to copy the password, publish the comment and then save the password based on when the comment was published, and for which post.'
							],
							'password-editing' => [
								'Enter the password you received before posting your comment in the text field above.'
							]
						],
						'placeholders' => [
							'name' => [
								'Eric',
								'Jacob',
								'Henric',
								'Anders',
								'Carl',
								'Michael',
								'Jan',
								'Jenny',
								'Carin',
								'Lena',
								'Bengt',
								'Anna',
								'Caroline',
								'Catharina',
								'Jannika',
								'Elin',
								'Rebecka'
							],
							'email' => 'exemple@domain.tld',
							'email-leave-empty' => 'Leave blank if address is not to be changed',
							'website' => 'https://'
						]
					],
					'subtitles' => [
						'comments' => 'Comments'
					]
				],
				'compose' => [
					'title' => [
						'new' => 'Write a new blog post',
						'editing' => 'Edit an existing post'
					],
					'language' => [
						'swedish' => 'Swedish',
						'english' => 'English'
					],
					'subject' => 'Subject',
					'content' => 'Content',
					'cover' => 'Cover',
					'warning' => [
						'post' => 'The option below gives you no warning before action.',
						'post-n-cover' => 'The options below give you no warning before action.'
					],
					'hundreddaystooffload' => [
						'day' => 'Day',
						'of' => 'Of'
					],
					'options' => [
						'delete-cover' => 'Remove the cover image',
						'delete-post' => 'Delete the post',
						'read' => 'Read the post'
					],
					'buttons' => [
						'publish' => 'Publish',
						'save-to-draft' => [
							'published' => 'Unpublish and save to draft',
							'save' => 'Save for draft'
						],
						'cancel' => 'Cancel'
					],
					'delete' => [
						'title' => 'Confirm the removal',
						'content' => [
							'cover' => [
								'You are about to permanently remove the cover image from the server.',
								'Do you want to continue?'
							],
							'post' => [
								'You are about to permanently delete the post and will not be able to undo it, if you choose to continue. However, any attached material will not be removed.',
								'Do you want to continue?'
							]
						],
						'options' => [
							'yes' => 'Yes',
							'no' => 'No'
						]
					]
				]
			],



			'gallery' => [
				'title' => 'Gallery',
				'rss' => 'Subscribe via RSS',
				'upload-photo' => 'Upload a photo',
				'taken' => 'Taken on',
				'of' => 'Of',
				'form' => [
					'description' => [
						'se' => 'Swedish description',
						'en' => 'English description'
					],
					'placeholders' => [
						'required' => 'Required'
					],
					'upload' => 'Upload'
				],
				'notchecked' => [
					'You need to approve the external data storage before you can like the photo.'
				],
				'paging' => [
					'previous' => 'Previous',
					'next' => 'Next'
				],
				'options' => [
					'edit' => 'Edit',
					'delete' => 'Delete'
				],
				'sensitive' => [
					'warning' => [
						'content' => [
							'The photograph contains what some people might consider to be **offensive**, **scary**, or similar.'
						],
						'hasdesc' => [
							'Please read the description below before you decide to proceed.'
						]
					],
					'options' => [
						'show-image' => 'Show the image',
						'hide-image' => 'Hide the image again'
					],
					'gdpr' => [
						'If you want to know how your data is handled if you want to view the photograph, please go to [the privacy page]('.url('privacy').') and read point 3.4.'
					]
				],
				'likes' => [
					'like' => 'Like',
					'undo-like' => 'Undo like',
					'gdpr' => 'If you want to know how your data is handled when you like the photo, please go to the [privacy policy]('.url('privacy').') and read point 3.5.'
				],
				'edit' => [
					'title' => 'Edit',
					'form' => [
						'taken' => 'Taken',
						'camera' => 'Camera & model',
						'iso' => 'ISO',
						'aperture' => 'Aperture',
						'exposure' => 'Exposure',
						'button' => 'Save'
					]
				],
				'delete' => [
					'title' => 'Delete',
					'content' => [
						'delete' => [
							'You are about to permanently delete the photograph. If you choose to continue, you will not be able to undo it and will have to upload the photograph again.',
							'Do you want to continue?'
						]
					],
					'options' => [
						'yes' => 'Yes',
						'no' => 'No'
					]
				]
			],



			'biking' => [
				'title' => 'Biking',
				'nav' => [
					'rss' => 'Subscribe via RSS'
				],
				'intro' => [
					'Cycling is one of my biggest hobbies. I have always had budget bikes from mainly Biltema and travelled hundreds of kilometers with them since 2004. But in April 2024 I bought my first long distance bike (Scott Sub Sport 30).',
					'The list below this text lists the journeys of at least 10 km that I have made by long-distance bicycle.'
				],
				'specifications' => [
					"- Växlar: Shimano Altud SL-M315\n- Vevställ: Shimano Altus FC-TY501 Black 48x38x26T\n- Kedja: KMC Z8\n- Kassett: Shimano CS-HG31 11-32T\n- Bromshandtag: Shimano BL-M200\n- Bromsar: Shimano BR-M200\n- Styrstång: \n- Sadelstolpe: \n- Sadel: "
				],
				'list' => [
					'when' => 'When',
					'distance' => 'Distance',
					'movingtime' => 'Moving time',
					'hour' => 'h',
					'minute' => 'm',
					'expected-traveltime' => 'Expected journey time',
					'destination' => 'Destination'
				],
				'details' => [
					'show-all-tracks' => 'Show all tracks',
					'movingtime' => 'Moving time',
					'distance' => 'Distance',
					'speed-top' => 'Top speed',
					'speed-average' => 'Average speed',
					'hour' => 'h',
					'minute' => 'm',
					'elevation-max' => 'Highest elevation',
					'elevation-min' => 'Lowest elevation'
				],
				'statistics' => [
					'title' => 'Statistics',
					'number-of' => 'Number of journeys',
					'duration-min' => 'Longest distance',
					'duration-max' => 'Shortest distance',
					'speed-min' => 'Maximum speed',
					'speed-max' => 'Lowest speed',
					'time-min' => 'Longest time',
					'time-max' => 'Shortest time',
					'elevation-max' => 'Highest elevation',
					'elevation-min' => 'Lowest elevation'
				],
				'plans' => [
					'title' => 'My planned trips',
					'content' => [
						'Below you can see what cycling trips I have planned. I have more travel plans than these, such as cycling to and from England. But as these require boat journeys, they will not be listed below. Only the trips that do not require a boat are listed.'
					]
				],
				'seasons' => [
					'winter' => 'Winter',
					'spring' => 'Spring',
					'summer' => 'Summer',
					'autumn' => 'Autumn'
				],
				'destinations' => [
					'westcoast' => 'West coast',
					'summercabin' => 'Summer cabin'
				]
			],



			'chat' => [
				'title' => 'Airikr Chat',
				'intro' => [
					'title' => 'Introduction',
					'content' => [
						'Many chat services violate your privacy without you being able to do anything about it. [Telegram](https://telegram.org) is the only chat service that allows you to have better control over your data (settings), while having zero control at all (storage).',
						'[Signal](https://signal.org/sv/) is a good option for many, despite the fact that this service requires your mobile phone number. The service is also based in the US and relies on Big Tech (see source [1](https://reddit.airikr.me/r/degoogle/comments/ml24an/i_just_realized_that_signal_messaging_app_uses/) & [2](https://reddit.airikr.me/r/signal/comments/pq43v1/why_is_signal_using_google_stun_servers/)). This is a **big** no-no if you want to use a privacy-friendly chat service.',
						'[SimpleX Chat](https://simplex.chat/) is by far the best option, but it literally ate up my phone\'s battery when I last tested the service. It was also (for me) impossible to synchronise your account via the computer.',
						'In the end, I installed [Snikket](https://snikket.org) on my own server. This gives me and those who have an account on Airikr Chat advantages, including (and most importantly) owning your own data and no one else\'s.',

						'### "But what about WhatsApp?"',
						'WhatsApp is owned by Meta and should therefore be banned from use in the EU, preferably worldwide. You should **never** feed Big Tech with your data, especially not when they are as hungry as Meta is!'
					]
				],
				'rules' => [
					'title' => 'Rules',
					'content' => [
						"1. All communication must be done through end-to-end encryption with [OMEMO](https://conversations.im/omemo/).\n1. Follow the laws of Sweden.\n1. Child pornography, racism, nazism and the like are strictly forbidden.\n1. Keep a good and respectable tone.\n1. Respect others. We are all different."
					]
				],
				'guide' => [
					'title' => 'Get started',
					'content' => [
						"1. Contact me. If I know who you are, I will send you an invitation link.\n1. Open the invitation link.\n1. If you have the Snikket app installed, select \"Open the app\". Otherwise, select \"register an account manually\" under Options.\n1. Enter your username and password and then select the button to create your account.\n1. Log in to your new account.\n1. Once logged in, choose to start a new chat and select for example me (Airikr) in the list of users."
					]
				],
				'faq' => [
					'title' => 'Questions and answers',
					'content' => [
						'### 1. What kind of clients can I use?',
						'Below I have listed some XMPP clients that are compatible with my chat server, [Snikket](https://snikket.org). There are of course many more.',

						"- **Android:** monocles chat ([Play Store](https://play.google.com/store/apps/details?id=eu.monocles.chat) (costs) / [F-Droid](https://f-droid.org/packages/de.monocles.chat/) (free)), Snikket ([Play Store](https://play.google.com/store/apps/details?id=org.snikket.android&amp;hl=sv&amp;gl=US) / [F-Droid](https://f-droid.org/en/packages/org.snikket.android/)), Conversations ([Play Store](https://play.google.com/store/apps/details?id=eu.siacs.conversations) (costs) / [F-Droid](https://f-droid.org/packages/eu.siacs.conversations/) (free))\n- **iOS:** [Snikket](https://apps.apple.com/us/app/snikket/id1545164189), [Siskin IM](https://apps.apple.com/de/app/siskin-im/id1153516838?platform=iphone)\n- **macOS:** [Beagle IM](https://beagle.im/)\n- **Windows, macOS & Linux:** [Gajim](https://gajim.org), [Swift](https://swift.im/)\n- **Web:** [monocles chat](https://monocles.chat/login)",
						'Please note that you will need to manually activate OMEMO if you are using any of the following clients: Gajim, Beagle IM, Siskin IM.',

						'### 2. Why should I use Airikr Chat?',
						"- No company, big or small, owns your data - you do.\n- You have several different clients to choose from. If you're not happy with one client, try another.\n- No one but you and the recipient can see what you write to each other using OMEMO.\n- You can export your data at any time via the control panel at [chat.airikr.me](https://chat.airikr.me).\n- All data you've sent that is stored on the server is permanently deleted after 7 days.",

						'### 3. Are you trying to compete with larger chat services?',
						'No, and I would never want to do that either. I am a simple private person who likes to own my own data and wants to allow others to have the same opportunity - nothing else.',

						'### 4. Where is my data stored?',
						'All data is stored on a server at [Obehosting](https://obe.net/foretag/data-center/) via [HostUp](https://hostup.se/en) in Stockholm.',

						'### 5. How is my data stored?',
						'All messages and files sent with OMEMO are stored with the encryption method AES256-GCM on the server for 7 days, before they are permanently deleted. Nobody but you and the recipient can read what you send to each other.',
						'However, if you do not use OMEMO, what you send will be stored encrypted on the server for it to read.',

						'### 6. How can I report a person who breaks the rules?',
						'Take a screenshot of the message with the username of the rule breaker visible, and then send the screenshot to me.',

						'### 7. What is the difference between Airikr Chat and other chat services, such as Telegram?',
						'The main differences are that [Snikket](https://snikket.org) (which Airikr Chat uses) is within the fediverse, and you have the freedom to talk about anything and everything, as long as you don\'t break any of the [rules]('.url('chat/rules').').',

						'#### The Fediverse?',
						'The Fediverse allows you to be on one server (for example chat.airikr.me) and communicate with other people on another server (for example snikket.linuxkompis.se). In other words, it doesn\'t matter which server you have your account on.',

						'#### Speak freely?',
						'Unlike chat services managed by companies, such as Meta and Discord, Airikr Chat is run by a single person: [Airikr]('.url('').'). If you stick to the [rules]('.url('chat/rules').') framework, you can talk about anything.',

						'### 8. But I have nothing to hide, right?',
						'Claiming that you have nothing to hide does not mean that you have to be careful about which chat service you choose.',
						'Big Tech (Microsoft, Google, Discord, Meta, Spotify, etc.) loves to collect as much information as possible about their users. The more information, the better, because then they make more money.',
						'Such companies are hostile to privacy. Unfortunately, there are those who do not know that they are being persecuted and that their lives are literally being sold to, among others, advertising companies. Either that or they simply do not care. People are very lazy, especially now that our lives are becoming increasingly digital.',
						'The "I have nothing to hide" argument doesn\'t hold water, because Big Tech and other companies get to know everything about you, especially when you feed them your life via their chat services.',
						'That\'s why it\'s extra important to choose a chat service that isn\'t anti-privacy. This is where [Snikket](https://snikket.org) comes in, and it\'s why [I]('.url('').') have created Airikr Chat. Not to compete with other chat services, but to keep my life more private and less stalked.'
					]
				]
			],



			'contact' => [
				'title' => 'Contact me',
				'content' => [
					'Do you have any questions about something specific about the website, or are you wondering about something in general? You can contact me via either email or via any of the services listed below.',
					'My email address is **hi atty airikr dotty me**. Send without encryption as I do not use PGP/GPG. If you want to communicate with me encrypted, choose XMPP.'
				]
			],



			'me' => [
				'metadata' => [
					'title' => 'Me'
				],
				'languages' => [
					'title' => 'Languages',
					'list' => [
						'swedish' => 'Swedish',
						'english' => 'English',
						'japanese' => 'Japanese'
					]
				],
				'pronounses' => [
					'he' => 'He',
					'his' => 'His',
					'him' => 'Him'
				],
				'xmpp' => [
					'my-devices' => 'My devices',
					'devices' => [
						'Below you can see the devices I use. **Please check them carefully!** If you are communicating with a device not listed below, you are not communicating with me.'
					]
				],
				'location' => 'Hammarö, Värmland County, Sweden',
				'skills' => [
					'title' => 'Skills',
					'list' => [
						'php' => 'PHP',
						'jquery' => 'jQuery',
						'html' => 'HTML',
						'css' => 'CSS',
						'lightroom' => 'Lightroom Classic',
						'photography' => 'Photography',
						'git' => 'Git',
						'javascript' => 'JavaScript',
						'genealogy' => 'Genealogy',
						'bicycling' => 'Bicycling',
						'cooking' => 'Cooking',
						'painting' => 'Painting'
					],
					'languages' => 'Languages',
					'hardwares' => 'Hardwares',
					'softwares' => 'Softwares',
					'programming' => 'Programming',
					'other' => 'Other'
				],
				'qr' => [
					'title' => 'Share this page'
				]
			],



			'links' => [
				'title' => 'Links',
				'content' => [
					'The list shows all my published projects. It also shows the people I find interesting.'
				],
				'notchecked' => [
					$site_title.' cannot determine whether you are a human or not. Therefore, you will not have access to the links that should otherwise be listed here. Please authorise the external data storage if you want to see the links.'
				],
				'subtitles' => [
					'projects' => 'My projects',
					'people' => 'Interesting people'
				]
			],



			'uses' => [
				'title' => 'Uses',
				'pc' => [
					'desktop' => 'Desktop',
					'laptop' => 'Laptop',
					'psu' => 'PSU',
					'motherboard' => 'Motherboard',
					'cpu' => 'CPU',
					'gpu' => 'GPU',
					'ram' => 'RAM',
					'storage' => 'Storage',
					'screen' => 'Monitor',
					'screens' => 'Monitors',
					'mouse' => 'Mouse',
					'keyboard' => 'Keyboard',
					'mic' => 'Microphone',
					'headset' => 'Headset',
					'manufacturer' => 'Manufacturer',
					'model' => 'Model',
					'os' => 'Operating system'
				],
				'softwares' => [
					'pc' => 'Softwares (PC)',
					'phone' => 'Softwares (Android)',
					'software' => 'Software',
					'softwares' => 'Softwares',
					'selfhosting' => 'Self-hosting'
				],
				'screenshotinfos' => [
					'desktop' => 'The background images are taken by me.',
					'laptop' => 'The background image is taken by me.'
				]
			],



			'guestbook' => [
				'title' => 'Guestbook',
				'content' => [
					'Feel free to say hello :)',
					'All posts are reviewed by me first, before they are published.',
					'Just remember that you cannot change or delete your post after it has been published. So be careful what you write.',
					'The IP address will not be stored anonymised. It will be stored together with your post. Please read point 3.3 of [the privacy page]('.url('privacy').') for more information.'
				],
				'terms' => [
					'## Terms',
					'If you publish a post that does not comply with the conditions listed below, it will be deleted without prior notice.',
					"1. Follow the laws of Sweden. Posts that do not follow the laws of the country will be reported to the police.\n1. Do not spam the feed.\n1. Respect others. We are all different.\n1. Add something to your post."
				],
				'notchecked' => [
					'## Must accept external data storage first',
					'I understand if you don\'t want to authorise the IP address of the device you\'re using to be stored in another server that I don\'t manage, but that\'s to make '.$site_title.' a more human location.',
					'Please authorise the external data storage before you can sign the guestbook.'
				],
				'badvisitor' => [
					'## Inhuman form detected',
					'You have been classified as a robot or similar by [IP Intelligence](https://getipintel.net/). You are therefore not allowed to sign my guestbook.',
					'If you are a human and that IP Intelligence has done wrong, [contact me]('.url('contact').').'
				],
				'nav' => [
					'rss' => 'Subscribe via RSS'
				],
				'sign' => 'Sign the guestbook',
				'form' => [
					'name' => 'Your name, please',
					'website' => 'Your website',
					'message' => 'Your message',
					'button' => 'Send',
					'cancel' => 'Cancel'
				],
				'posts' => [
					'name' => 'Name',
					'delete' => 'Delete'
				]
			],



			'privacy' => [
				'title' => 'Privacy',
				'intro' => [
					'## Foreword',
					$site_domain.' does not use any JavaScript, but this will most likely change in the future. However, as JavaScript is not used, your IP address will be sent in plain text to the server.',
					'**The IP address is not sent to my server until you interact with some feature**, for example reacting to a blog post.',
					'I have no intention of knowing your IP address and I honestly don\'t care about the information. The only reason IP addresses are stored is to make it easier to manage the features that you visitors can use, such as reacting to a blog post.',
					'If you want to read more about how your personal data is stored, read on.',


					'## 1. IP Intelligence',
					'[IP Intelligence](https://getipintel.net/) (IPIntel for short) is used to make '.$site_title.' a better place for both me and for you, the visitors of the website. IPIntel stores the following about you to make their service better:',
					"- The IP address of the device\n- The timestamp when the call was made\n- The response code given during the call",
					'IPIntel will not know your IP address until you have authorised the external data storage. If you authorise it, your data will be sent to their server, while a JSON file is stored on the server where '.$site_title.' is located. The file name is the IP address of the device in hashed form and the file is valid for 6 hours.',


					'## 2. What the website uses for analytics',
					$site_title.' uses [Plausible](https://plausible.io) to collect anonymised information about visitors. IP addresses are not collected by Plausible and are thus GDPR friendly.',
					'Even if Plausible has already collected information about your visit, you can choose not to allow the service to collect information about you on future visits. To do this, disable JavaScript for '.$site_title.'.',


					'## 3. Storage of personal data',
					'When a visitor interacts with features on '.$site_title.', the IP address of the device they are using will be stored on the server where the web page is hosted. What these functions are and how the IP address is stored is described below.',
					'The storage of the IP address of the device, whether anonymised or not, is necessary for the website to identify you as a visitor.',
					'**Example when an IP address is anonymised:** 192.168.123.500 becomes 192.168.123.0.',

					'### 3.1. Reactions for blog posts',
					'When a visitor chooses to react to a blog post, an empty file will be stored on the server. The file name contains the post\'s publication date and time, when the reaction took place, the hash and anonymized IP address, and what reaction the visitor chose.',
					'When the same visitor changes their mind, the file will be deleted from the server.',

					/*'### 3.2. Comments for blog posts',
					'When you choose to post a comment, the email address and IP address of the device you are using will be stored encrypted. All other information will be stored in plain text as they will be displayed publicly.',
					'The IP address will not be anonymised for security reasons.',*/

					'### 3.3. Guestbook posts',
					'When you choose to publish a guestbook post, the IP address of the device you use will be stored encrypted. All other information will be stored in plain text as they will be displayed publicly.',
					'The IP address will not be anonymised for security reasons.',

					'### 3.4. Viewing sensitive images in the gallery',
					'If a photograph is marked as sensitive, information about this will be displayed. A hint as to why it is marked as sensitive can be found in the description of the photograph.',
					'When you choose to view the photograph despite the warning, the IP address of the device you are using will be anonymised and hashed with MD5, and stored in the filename of the file created. A timestamp of when you chose to view the photograph will be in the JSON file created.',
					'The webpage will check when you chose to view the photo, and automatically remove the file from the server after 5 minutes. The warning will then be displayed again. You can manually delete the file by selecting "Hide image again" under the photograph.',

					'### 3.5. Like a photo in the gallery',
					'When you like a photo in the gallery, the IP address of the device you are using will be anonymised and hashed with MD5, along with the file name of the photo. A timestamp of when you chose to view the photo will be in the JSON file created.',

					'### 3.6. Viewing sensitive blog posts',
					'If a blog post is marked as sensitive, this information will be displayed. You are then given two choices: to accept and read the post anyway, or to go to a random post.',
					'When you choose to read the blog post despite the warning, the IP address of the device you are using will be anonymised and hashed with MD5, and stored in the filename of the file that is created. A timestamp of when you chose to read the post will be in the JSON file that is created.',
					'The webpage will check when you chose to read the post, and automatically remove the file from the server after 10 minutes. The warning will then be displayed again. You can manually delete the file by selecting "Hide post again" above the post.',

					'### 3.7. Reading status of blog posts',
					'When entering the read status of a blog post, an empty file will be stored on the server. The file name will contain the post\'s publication date and the visitor\'s anonymized IP address. When the same visitor chooses to mark the post as unread, the file will be deleted from the server.',


					'## 4. Contact',
					'If you have questions, complaints, or anything else to say, please contact me through any of the services listed on the "[Contact]('.url('contact').')" page.'
				]
			],



			'copyright' => [
				'title' => 'Copyright',
				'content' => [
					'All material used on '.$site_title.' and its subdomains (unless otherwise noted), is created by the owner, [Erik Edgren]('.url('').'). The materials are licensed under [CC-BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.sv).'
				],
				'creativecommons' => [
					'by' => [
						'**BY** - You must acknowledge that Erik Edgren is the owner and creator of the material. You must also link to this page if you have shared the material.'
					],
					'nc' => [
						'**NC** - You may not use the material for commercial purposes.'
					],
					'nd' => [
						'**ND** - You may not remix, transform, or build upon the material and then distribute the material.'
					]
				]
			],



			'uploads' => [
				'title' => 'Uploaded images',
				'subtitles' => [
					'photos' => 'Images',
					'covers' => 'Covers',
					'tracks' => 'GPX tracks',
					'videos' => 'Video clips'
				],
				'upload' => 'Upload a new image',
				'upload-image' => 'Upload a new image',
				'list' => [
					'filename' => 'File name',
					'filesize' => 'File size'
				],
				'file' => 'File',
				'files' => 'Files'
			],



			'rss' => [
				'title' => 'RSS feeds',
				'content' => [
					'Instead of email newsletters, you can follow me via RSS. There are a number of clients you can use, including [Miniflux](https://miniflux.app/) and [Feeder](https://github.com/spacecowboy/Feeder).'
				],
				'list' => [
					[
						'name' => 'Blog posts',
						'url' => url('rss/blog')
					],
					[
						'name' => 'Comments for blog posts',
						'url' => url('rss/comments')
					],
					[
						'name' => 'Gallery',
						'url' => url('rss/gallery')
					],
					[
						'name' => 'Biking',
						'url' => url('rss/biking')
					],
					[
						'name' => 'Guestbook posts',
						'url' => url('rss/guestbook')
					]
				]
			],



			'sitemap' => [
				'title' => 'Sitemap',
				'content' => [
					'Below are listed all pages and subpages (including those that are hidden) so that you can easily navigate on '.$site_title.'.',
					'These pages are actually designed to allow search engines (mainly Google) to easily find the most important pages on a website, and then index them. But the sole purpose of this sitemap is to let visitors find pages on '.$site_domain.' as quickly as possible.'
				],
				'uses' => 'Uses',
				'metadata' => 'Metadata'
			],



			'upload' => [
				'title' => 'Upload an image',
				'choose-image' => 'Choose an image',
				'button' => 'Upload'
			],



			'uploaded' => [
				'title' => [
					'photo' => 'Information about an image',
					'cover' => 'Information about a cover',
					'track' => 'Information about a track',
					'video' => 'Information about a video'
				],
				'filename' => 'File name',
				'filesize' => 'File size',
				'filesize_mp4' => 'File size (MP4)',
				'filesize_ogg' => 'File size (OGG)',
				'filesize_webm' => 'File size (WebM)',
				'filesize_total' => 'Total file size',
				'size' => 'Size',
				'size_thumb' => 'Size (posts)',
				'size_thumblist' => 'Size (list)',
				'tag' => 'Tag',
				'options' => [
					'show' => 'Show',
					'update' => 'Update',
					'delete' => 'Delete'
				],
				'delete' => [
					'title' => 'Confirm the deletion',
					'content' => [
						'You are about to permanently remove the file from the server. If you have included the material in any blog posts, you need to go to those posts and manually remove the tag for the material.',
						'If you choose to continue, you will not be able to regret your choice.'
					],
					'options' => [
						'yes' => 'Yes',
						'no' => 'No'
					]
				]
			],



			'telemetry' => [
				'title' => 'Telemetry simulator',
				'subtitles' => [
					'without-javascript' => 'Without JavaScript',
					'with-javascript' => 'With JavaScript',
					'browser-properties' => 'Browser properties'
				],
				'content' => [
					'disabled' => [
						'The purpose of the telemetry simulation is to show you how much data Big Tech companies (Google, Microsoft, Meta, etc.) store about you. Data that can be linked directly to you personally and thus be able to create a personal database about you for themselves and also for third parties to whom they sell the data. In other words, they are making money off your life.',
						'The simulator is limited to only work on '.$site_domain.'.',
						'Please note that what is stored about you in the simulator is only a fraction of what, for example, Facebook stores about you. The simulator was created only to give you a sense of how much is stored at Big Tech.',

						'## How the information is stored?',
						'When the simulation is started, a directory will be created on the server. The name of the directory is the IP address of the device you are visiting '.$site_domain.' with. The address is of course hashed with SHA-256. The directory contains JSON files that house everything listed under "What is stored".',
						'No one but you and me (<a href="'.url('').'">Erik</a>) can access the files, but I don\'t care about your data. Your data is and will remain yours only (as it should be). You also have full control over how long the directory with the files will be stored on the server (but a maximum of 10 minutes after inactivity).',
						'And since this is a simulation, everything will be stored in plain text on the server, just like Big Tech.',

						'## Will everything about me be automatically deleted?',
						'Yes. After you have been inactive for 10 minutes, all your files will be permanently deleted. You can also choose to delete the files yourself by selecting "Turn off" in the footer.',
					],
					'whatlogs' => [
						'title' => 'What is stored?',
						'without-js' => [
							'IP address',
							'Useragent',
							'When the simulation was started',
							'Page',
							'Date and time'
						],
						'with-js' => [
							'stuff' => [
								'Screen',
								'Resolution',
								'Color depth',
								'Timezone',
								'Timezone offset',
								'Inputs',
								#'Musmarkörens position och vilken knapp som användes',
								'Fingerprint',
								'Browser',
								'Operating system',
								'Processor architecture',
								'The language of the system'
							],
							'browser' => [
								'Default language',
								'If cookies is allowed',
								'If Do Not Track is activated or not',
								'Whether Geolocation and/or WebSocket are supported or not',
								'Whether or not localStorage and/or session storage is supported',
								'Whether Java, Flash, and/or Silverlight is installed or not'
							]
						]
					],
					'enabled' => [
						'**Semulation has started!**',
						'Browse around here at '.$site_domain.' for a few minutes and then go to the "Your data" page (you can find the link in the footer). All the data that has been saved about you and your visit can be found on that page.'
					]
				],
				'start' => 'Start the simulation'
			],



			'your-data' => [
				'title' => 'Din data',
				'content' => [
					'Nedan kan du hitta allt som telemetrisimuleringen har loggat om dig och vad du har gjort på '.$site_domain.'. Listan är långt ifrån komplett jämfört med det som Big Tech hämtar om dig. Detta är på grund av att simuleringen endast ska logga det som inte kräver din uppmärksamhet.',
					'Du kan stänga av simuleringen och radera all loggning om dig och ditt besök, genom att välja "Stäng av simuleringen" i sidfoten.'
				],
				'lists' => [
					'keycodes' => [
						'when' => 'Registrerades',
						'key' => 'Knapp',
						'keycode' => 'Kod',
						'page' => 'Sida'
					]
				],
				'unknown' => 'Okänd'
			],



			'admin' => [
				'titles' => [
					'login' => 'Log in',
					'create' => 'Create an admin account'
				],
				'form' => [
					'username' => 'Username',
					'password' => 'Password',
					'tfa' => 'Two-step verification code',
					'tfa-create' => 'Two-step verification'
				],
				'tfa-options' => 'Create the admin account [with 2FA]('.url('admin/create-with-tfa').') or [without]('.url('admin/create').').',
				'placeholders' => [
					'tfa' => 'Only fill in if you have activated 2FA'
				],
				'buttons' => [
					'create' => 'Create',
					'login' => 'Login'
				]
			],



			'information' => [
				'titles' => [
					'colophone' => 'Colophone',
					'ai' => 'AI'
				],
				'contents' => [
					'colophone' => [
						$site_title.' is a self-build by the owner, [Erik Edgren]('.url('').'). The source code that formed the basis of the website can be seen at [git.airikr.me](https://git.airikr.me/airikr/template). The source code for '.$site_title.' can be found at [codeberg.org](https://codeberg.org/airikr/airikr.me). This will always be up-to-date.',
						'The web page is a so-called static web page. This means that it uses JSON files that act as a database, instead of, for example, SQLite.',

						'## JavaScript',
						'At the time of writing, '.$site_title.' does not use JavaScript. Everything is built using HTML, CSS and PHP. However, there are plans to launch a feature that requires JavaScript.',

						'## Functions',
						'Below you can see which functions are used on '.$site_title.', and what they are used for.',
						"- ".implode("\n- ", $arr_functions),
						$site_title.' also uses [IP Intelligence](https://getipintel.net/) to keep mainly bots away from mainly the communication features of the website, including the guestbook.',

						'## Fonts',
						$site_title.' uses only one font: [Iosevka](https://github.com/be5invis/Iosevka).',

						'## Icons',
						'The icons that can be seen everywhere on '.$site_title.', are from [Tabler Icons](https://github.com/tabler/tabler-icons).'
					],
					'ai' => [
						'AI has not been used, and will never be used, to compose blog posts and descriptions for uploaded photographs to the gallery. AI has also not written the source code for '.$site_title.'.',
						'AI should (according to [me]('.url('').')) only be used for more important things, such as predicting weather disasters at an earlier stage than before. AI is being misused far too much.'
					]
				]
			]
		]
	];

?>
