<?php

	use Intervention\Image\ImageManager;
	use Intervention\Image\Drivers\Imagick\Driver;
	use geertw\IpAnonymizer\IpAnonymizer;



	if(isset($_POST['upload'])) {

		require_once 'site-settings.php';

		$file_tmp = $_FILES['file']['tmp_name'];
		$file_name = $_FILES['file']['name'];
		$file_info = pathinfo($file_name);

		$file_name_cleaned = str_replace('-Pano', '', $file_info['filename']);
		$file_name_old = $dir_files.'/gallery/photos/'.$file_name;
		$file_name_800 = $dir_files.'/gallery/photos/'.$file_name_cleaned.'/800/'.$file_name_cleaned.'.webp';
		$file_name_1500 = $dir_files.'/gallery/photos/'.$file_name_cleaned.'/1500/'.$file_name_cleaned.'.webp';
		$file_name_256 = $dir_files.'/gallery/photos/'.$file_name_cleaned.'/256/'.$file_name_cleaned.'.webp';

		$post_description_se = (empty($_POST['field-description-se']) ? null : safetag($_POST['field-description-se']));
		$post_description_en = (empty($_POST['field-description-en']) ? null : safetag($_POST['field-description-en']));

		$post_check_sensitive = (isset($_POST['check-sensitive']) ? true : false);


		if(empty($file_tmp)) {
			die(simplepage('Please select a file first. <a href="'.url('upload').'">Go back</a>'));

		} elseif($file_info['extension'] == 'jpg' AND !move_uploaded_file($file_tmp, $file_name_old)) {
			die(simplepage('The file has not been uploaded. Please try again. <a href="'.url('upload').'">Go back</a>'));

		} else {
			if($file_info['extension'] == 'jpg') {
				$manager = new ImageManager(['driver' => 'imagick']);
				$exif = exif_read_data($file_name_old);
				$iscamera = false;
				$isphone = false;

				create_folder($dir_files.'/gallery/photos/'.$file_name_cleaned);
				create_folder($dir_files.'/gallery/photos/'.$file_name_cleaned.'/256');
				create_folder($dir_files.'/gallery/photos/'.$file_name_cleaned.'/800');
				create_folder($dir_files.'/gallery/photos/'.$file_name_cleaned.'/1500');

				if(mb_strtolower($exif['Make']) == 'sony') {
					$iscamera = true;
				} elseif(mb_strtolower($exif['Make']) == 'panasonic') {
					$iscamera = true;
				} elseif(mb_strtolower($exif['Make']) == 'google') {
					$isphone = true;
				}

				$arr_info = [
					'uploaded' => time(),
					'description' => [
						'se' => $post_description_se,
						'en' => $post_description_en
					],
					'phone' => $isphone,
					'camera' => $iscamera,
					'sensitive' => $post_check_sensitive,
					'exif' => [
						'taken' => strtotime($exif['DateTimeOriginal']),
						'camera' => [
							'manufacturer' => $exif['Make'],
							'model' => $exif['Model']
						],
						'iso' => (int)$exif['ISOSpeedRatings'],
						'aperture' => $exif['COMPUTED']['ApertureFNumber'],
						'exposure' => $exif['ExposureTime']
					]
				];

				file_put_contents($dir_files.'/gallery/photos/'.$file_name_cleaned.'/'.$file_name_cleaned.'.json', json_encode($arr_info));

				$manager->make($file_name_old)->resize(1500, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->encode('webp', 70)->save($file_name_1500);

				$img = new Imagick($file_name_1500);
				$img->stripImage();
				$img->writeImage($file_name_1500);
				#$img->setImageProperty('keywords', 'airikr');
				#die($img->getImageProperty('keywords'));
				$img->clear();
				$img->destroy();

				$manager->make($file_name_1500)->resize(800, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->save($file_name_800);

				$manager->make($file_name_800)->resize(256, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->save($file_name_256);

				unlink($file_name_old);
			}


			header("Location: ".url('gallery'));
			exit;
		}



	} else {

		require_once 'site-header.php';



		$get_filename = (!isset($_GET['nam']) ? null : safetag($_GET['nam']));
		$is_deleting = (!isset($_GET['del']) ? null : true);
		$is_editing = (!isset($_GET['pag']) ? null : (strip_tags(htmlspecialchars(($_GET['pag']))) == 'edit' ? true : false));

		$dir_empty = glob($dir_files.'/gallery/photos/*');







		echo '<section id="gallery">';
			echo '<h1>';
				echo $lang['pages']['gallery']['title'];
			echo '</h1>';

			if($is_loggedin == true) {
				echo '<details>';
					echo '<summary>';
						echo '<div class="open">'.svgicon('details-open').'</div>';
						echo '<div class="close">'.svgicon('details-close').'</div>';
						echo $lang['pages']['gallery']['upload-photo'];
					echo '</summary>';

					echo '<form action="'.url('gallery', true).'" method="POST" enctype="multipart/form-data" autocomplete="off" novalidate>';
						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['upload']['choose-image'];
							echo '</div>';

							echo '<div class="value">';
								echo '<input type="file" name="file" accept=".jpg">';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['form']['description']['se'];
							echo '</div>';

							echo '<div class="value">';
								echo '<textarea name="field-description-se"></textarea>';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['gallery']['form']['description']['en'];
							echo '</div>';

							echo '<div class="value">';
								echo '<textarea name="field-description-en"></textarea>';
							echo '</div>';
						echo '</div>';


						echo '<div class="checkboxes">';
							echo checkbox('Markera fotografiet som känsligt', 'sensitive');
						echo '</div>';


						echo '<div class="button">';
							echo '<input type="submit" name="upload" value="'.$lang['pages']['gallery']['form']['upload'].'">';
						echo '</div>';
					echo '</form>';
				echo '</details>';
			}



			echo '<div class="gallery">';
				if(count($dir_empty) == 0) {
					echo '<div class="message">';
						echo $lang['messages']['no-photos'];
					echo '</div>';

				} else {
					$arr_photos = [];

					echo '<div class="rss side-by-side">';
						echo '<a href="'.url('rss/gallery?lang='.$get_lang, true).'">';
							echo svgicon('rss') . $lang['pages']['gallery']['rss'];
						echo '</a>';
					echo '</div>';

					echo '<div class="photos">';
						foreach(glob($dir_files.'/gallery/photos/*') AS $photo) {
							$file_info = pathinfo($photo);
							$exif_looped = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$file_info['filename'].'/'.$file_info['filename'].'.json'), false);

							$arr_photos[] = [
								'name' => $file_info['filename'],
								'uploaded' => $exif_looped->uploaded
							];
						}

						usort($arr_photos, function($a, $b) {
							$ad = new DateTime(date('Y-m-d H:i:s', $a['uploaded']));
							$bd = new DateTime(date('Y-m-d H:i:s', $b['uploaded']));

							if($ad == $bd) {
								return 0;
							}

							return $ad > $bd ? -1 : 1;
						});

						foreach($arr_photos AS $file) {
							$anonymize_ip = new IpAnonymizer();
							$file_info = pathinfo($file['name']);
							$file_name = $file_info['filename'];
							$exif = json_decode(file_get_contents($dir_files.'/gallery/photos/'.$file_name.'/'.$file_name.'.json'), false);
							$ipaddress = md5($anonymize_ip->anonymize(getip()));
							$is_sensitive = $exif->sensitive;
							$has_acceptedsensitive = ($is_sensitive == false ? false : (file_exists($dir_files.'/gallery/accepted-sensitive/'.$ipaddress.'-'.$file_name.'.json') ? true : false));
							$device = null;

							if($exif->camera == true) {
								$device = 'camera';
							} elseif($exif->phone == true) {
								$device = 'phone';
							}

							echo '<a href="'.url('gallery/photo:'.$file_name).'">';
								echo '<div class="photo" style="background-image: url('.url('gallery-thumb:'.$file_name, true).');">';
									echo ($has_acceptedsensitive == true ? '' : ($is_sensitive == false ? '' : '<div class="sensitive"></div>'));

									echo '<div class="uppicorner">';
										echo ($is_sensitive == false ? '' : svgicon('warning-sensitive'));
										echo (empty($device) ? '' : svgicon('device-'.$device));
										#echo '<div class="multiple no-select">4</div>';
									echo '</div>';
								echo '</div>';
							echo '</a>';
						}
					echo '</div>';
				}
			echo '</div>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>