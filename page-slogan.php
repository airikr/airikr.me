<?php

	require_once 'site-header.php';







	echo '<section id="slogan">';
		echo '<h1>'.$lang['pages']['slogan']['title'].'</h1>';

		foreach($lang['pages']['slogan']['content'] AS $content) {
			echo $Parsedown->text($content);
		}


		echo '<hr>';


		echo '<div class="rules">';
			echo '<h2 class="first">';
				echo svgicon('lifebuoy') . $lang['pages']['slogan']['mylife']['title'];
			echo '</h2>';

			foreach($lang['pages']['slogan']['mylife']['content'] AS $mylife) {
				echo $Parsedown->text($mylife);
			}


			echo '<h2>';
				echo svgicon('database') . $lang['pages']['slogan']['mydata']['title'];
			echo '</h2>';

			foreach($lang['pages']['slogan']['mydata']['content'] AS $mydata) {
				echo $Parsedown->text($mydata);
			}


			echo '<h2>';
				echo svgicon('hand-stop') . $lang['pages']['slogan']['mydemands']['title'];
			echo '</h2>';

			foreach($lang['pages']['slogan']['mydemands']['content'] AS $mydemands) {
				echo $Parsedown->text($mydemands);
			}
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
