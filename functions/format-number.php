<?php

	function format_number($string, $decimal = 0, $symbol = ',', $thousands = ' ') {
		if($string == '0') {
			return '0';

		} else {
			return number_format($string, (int)$decimal, $symbol, $thousands);
		}
	}

?>
