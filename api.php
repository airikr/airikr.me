<?php

	require_once 'site-settings.php';

	header('Content-Type: application/json;charset=utf-8');

	$dir_posts = $dir_files.'/posts/published';
	$dir_reactions = $dir_files.'/posts/reactions';
	$dir_videos = $dir_files.'/videos';
	$dir_uploads = $dir_files.'/uploads';
	$dir_gallery = $dir_files.'/gallery/photos';
	$dir_guestbook = $dir_files.'/guestbook';
	$dir_tracks = $dir_files.'/tracks';

	$arr = [];
	$arr_posts = [];
	$arr_posts_timestamp = [];
	$arr_calendar = [];
	$arr_content = [];
	$arr_photos_camera = [];
	$arr_photos_phone = [];
	$words = 0;
	$c_images = 0;
	$c_links = 0;
	$c_ga_camera = 0;
	$c_ga_phone = 0;
	$c_hundreddaystooffload = 0;
	$c_g_posts = 0;
	$c_g_waiting = 0;
	$c_g_total = 0;
	$posts = glob($dir_posts.'/*.json');
	$characters = 0;

	foreach(array_reverse($posts) AS $poststats) {
		$fileinfo = pathinfo($poststats);
		$post = json_decode(file_get_contents($poststats), false);
		$content_se = tags($Parsedown->text($post->content->se));
		$content_en = tags($Parsedown->text($post->content->en));
		$images = explode('<img ', ($lang['metadata']['language'] == 'se' ? $content_se : $content_en));
		$links = explode('<a ', ($lang['metadata']['language'] == 'se' ? $content_se : $content_en));
		$words += str_word_count(strip_tags(($lang['metadata']['language'] == 'se' ? $content_se : $content_en)));
		$characters += mb_strlen(strip_tags(($lang['metadata']['language'] == 'se' ? $content_se : $content_en)), 'UTF-8');
		$arr_content[] = strip_tags(($lang['metadata']['language'] == 'se' ? $content_se : $content_en));
		$c_images += count($images);
		$c_links += count($links);
	}

	$c_b_posts = iterator_count(new FilesystemIterator($dir_posts, FilesystemIterator::SKIP_DOTS));
	$c_reactions = iterator_count(new FilesystemIterator($dir_reactions, FilesystemIterator::SKIP_DOTS));

	$post_first = $posts[0];
	$post_first = mb_substr($fileinfo['filename'], 0, 4) . mb_substr($fileinfo['filename'], 4, 2) . mb_substr($fileinfo['filename'], 6, 2) . mb_substr($fileinfo['filename'], 8, 2) . mb_substr($fileinfo['filename'], 10, 2);

	$post_latest_info = pathinfo(end($posts));
	$post_latest = $post_latest_info['filename'];



	$ga_photos = new FilesystemIterator($dir_gallery, FilesystemIterator::SKIP_DOTS);
	$c_ga_photos = iterator_count($ga_photos);

	$g_posts = new FilesystemIterator($dir_guestbook, FilesystemIterator::SKIP_DOTS);
	$c_g_total = iterator_count($g_posts);



	foreach($posts AS $post) {
		$fileinfo = pathinfo($post);
		$post = json_decode(file_get_contents($post), false);
		$date = date('Y-m-d', strtotime($fileinfo['filename']));
		$arr_posts_timestamp = [strtotime($fileinfo['filename'])];

		if(isset($post->hundreddaystooffload->enabled) AND $post->hundreddaystooffload->enabled == true) {
			$c_hundreddaystooffload += 1;
		}

		$arr_posts[] = [
			'date' => $date,
			'hundreddaystooffload' => ((!isset($post->hundreddaystooffload->enabled) OR $post->hundreddaystooffload->enabled == false) ? false : true)
		];
	}

	foreach($arr_posts AS $stats) {
		$timestamp = strtotime($stats['date']);

		if(!isset($arr_calendar[$timestamp])) {
			$arr_calendar[$timestamp] = array_merge($stats, ['posts' => 0]);
		}

		$arr_calendar[$timestamp]['posts']++;
	}

	foreach($ga_photos AS $photo) {
		$data = json_decode(file_get_contents($photo . substr($photo, strrpos($photo, '/')).'.json'), false);

		if($data->camera == true) { $c_ga_camera++; }
		if($data->phone == true) { $c_ga_phone++; }
	}

	foreach($g_posts AS $gpost) {
		$data = json_decode(file_get_contents($gpost), false);

		if($data->is_accepted == true) { $c_g_posts++; }
		if($data->is_accepted == false) { $c_g_waiting++; }
	}

	ksort($arr_calendar);
	$arr_calendar = array_values($arr_calendar);

	$d1 = DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime($post_first)));
	$d2 = DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime($post_latest)));
	$diff = $d1->diff($d2);



	$total_distance = 0;
	$total_hours = 0;
	$total_minutes = 0;
	$arr_distances = [];
	$arr_durations = [];
	$arr_speeds = [];
	$arr_elevations = [];
	$arr_segments = [];
	$tracks = glob($dir_tracks.'/json/*.json');

	foreach(array_reverse($tracks) AS $track) {
		$fileinfo = pathinfo($track);
		$data = json_decode(file_get_contents($track), true);

		$arr_distances[] = [
			'file' => $fileinfo['filename'],
			'km' => (float)$data['distance']['km']
		];

		$arr_durations[] = [
			'file' => $fileinfo['filename'],
			'time' => (int)(($data['movingtime']['hours'] * 3600) + ($data['movingtime']['minutes'] * 60))
		];

		$arr_speeds[] = [
			'file' => $fileinfo['filename'],
			'kmh' => (float)$data['speed']['top']['kmh']
		];

		$arr_segments[] = [
			'timestamp' => (int)strtotime($data['occurred']['date'].' '.$data['occurred']['time']),
			'time' => [
				'hours' => (int)$data['movingtime']['hours'],
				'minutes' => (int)$data['movingtime']['minutes']
			],
			'distance' => [
				'm' => (float)$data['distance']['m'],
				'km' => (float)$data['distance']['km'],
				'mi' => (float)$data['distance']['mi']
			],
			'speed' => [
				'top' => [
					'ms' => (float)$data['speed']['top']['ms'],
					'kmh' => (float)$data['speed']['top']['kmh'],
					'mph' => (float)$data['speed']['top']['mph']
				],
				'average' => [
					'ms' => (float)$data['speed']['average']['ms'],
					'kmh' => (float)$data['speed']['average']['kmh'],
					'mph' => (float)$data['speed']['average']['mph']
				]
			],
			'elevations' => [
				'highest' => [
					'cm' => (float)$data['elevation']['max']['cm'],
					'm' => (float)$data['elevation']['max']['m'],
					'ft' => (float)$data['elevation']['max']['ft']
				],
				'lowest' => [
					'cm' => (float)$data['elevation']['min']['cm'],
					'm' => (float)$data['elevation']['min']['m'],
					'ft' => (float)$data['elevation']['min']['ft']
				]
			],
			'points' => (array)$data['segments']
		];
	}

	foreach($arr_distances AS $dik => $div) {
		$arr_distance[$dik] = $div['km'].'-'.$div['file'];
	}

	foreach($arr_speeds AS $spk => $spv) {
		$arr_speed[$spk] = $spv['kmh'].'-'.$spv['file'];
	}

	foreach($arr_durations AS $duk => $duv) {
		$arr_duration[$duk] = $duv['time'].'-'.$duv['file'];
	}

	if(isset($arr_elevations_min) AND isset($arr_elevations_max)) {
		foreach($arr_elevations_max AS $eak => $eav) {
			$arr_elevations_max[$eak] = $eav['m'].'-'.$eav['file'];
		}

		foreach($arr_elevations_min AS $eik => $eiv) {
			$arr_elevations_min[$eik] = $eiv['m'].'-'.$eiv['file'];
		}
	}

	list($midi_km, $midi_file) = explode('-', min($arr_distance));
	list($madi_km, $madi_file) = explode('-', max($arr_distance));
	list($misp_kmh, $misp_file) = explode('-', min($arr_speed));
	list($masp_kmh, $masp_file) = explode('-', max($arr_speed));
	list($midu_time, $midu_file) = explode('-', min($arr_duration));
	list($madu_time, $madu_file) = explode('-', max($arr_duration));

	if(isset($arr_elevations_min) AND isset($arr_elevations_max)) {
		list($miei_m, $miei_file) = explode('-', min($arr_elevations_min));
		list($maei_m, $maei_file) = explode('-', max($arr_elevations_max));
	}



	$c_visitor_bypassed = 0;
	$c_visitor_verified = 0;
	$c_visitor_mostlikelyproxy = 0;
	$c_visitor_lowrisk = 0;
	$c_visitor_fiftyfifty = 0;
	$c_visitor_norisk = 0;
	$c_visitor_needschecking = 0;
	$c_visitor_badip = 0;

	foreach(glob($dir_files.'/visitors/*.json') AS $visitor) {
		$data = json_decode(file_get_contents($visitor), true);
		$c_visitor_bypassed += ($data['bypassed'] == true ? 1 : 0);
		$c_visitor_verified += ($data['verified'] == true ? 1 : 0);
		$c_visitor_mostlikelyproxy += ($data['message'] == 'most-likely-proxy' ? 1 : 0);
		$c_visitor_lowrisk += ($data['message'] == 'low-risk' ? 1 : 0);
		$c_visitor_fiftyfifty += ($data['message'] == 'fifty-fifty' ? 1 : 0);
		$c_visitor_norisk += ($data['message'] == 'no-risk' ? 1 : 0);
		$c_visitor_needschecking += ($data['message'] == 'needs-checking' ? 1 : 0);
		$c_visitor_badip += ($data['message'] == 'bad-ip' ? 1 : 0);
	}



	$arr = [
		'blog' => [
			'first_post' => (int)$post_first,
			'last_post' => (int)$post_latest,
			'difference' => [
				'years' => (int)$diff->format('%y'),
				'months' => (int)$diff->format('%m'),
				'days' => (int)$diff->format('%d')
			],
			'posts' => [
				'total' => (int)$c_b_posts,
				'hundreddaystooffload' => [
					'yes' => (int)$c_hundreddaystooffload,
					'no' => (int)($c_b_posts - $c_hundreddaystooffload)
				]
			],
			'reactions' => (int)$c_reactions,
			'images' => (int)$c_images,
			'links' => (int)$c_links,
			'words' => (int)$words,
			'characters' => (int)$characters,
			'average' => [
				'posts' => [
					'per_day' => (int)format_number(((int)$c_b_posts / 24), 0, '', ''),
					'per_week' => (int)format_number(((int)$c_b_posts / 7), 0, '', ''),
					'per_month' => (int)format_number(((int)$c_b_posts / 31), 0, '', ''),
					'per_year' => (int)format_number(((int)$c_b_posts / 365), 0, '', ''),
					'words' => (int)format_number(((int)$words / (int)$c_b_posts), 0, '', ''),
					'characters' => (int)format_number(((int)$characters / (int)$c_b_posts), 0, '', '')
				],
				'reactions' => [
					'per_day' => (int)format_number(((int)$c_reactions / 24), 0, '', ''),
					'per_week' => (int)format_number(((int)$c_reactions / 7), 0, '', ''),
					'per_month' => (int)format_number(((int)$c_reactions / 31), 0, '', ''),
					'per_year' => (int)format_number(((int)$c_reactions / 365), 0, '', '')
				]
			],
			'calendar' => $arr_calendar
		],


		'guestbook' => [
			'total' => (int)$c_g_total,
			'accepted' => (int)$c_g_posts,
			'waiting_for_review' => (int)$c_g_waiting
		],


		'gallery' => [
			'photos' => (int)$c_ga_photos,
			'with_camera' => (int)$c_ga_camera,
			'with_phone' => (int)$c_ga_phone,
			'average' => [
				'per_day' => (int)format_number(((int)$c_ga_photos / 24), 0, '', ''),
				'per_week' => (int)format_number(((int)$c_ga_photos / 7), 0, '', ''),
				'per_month' => (int)format_number(((int)$c_ga_photos / 31), 0, '', ''),
				'per_year' => (int)format_number(((int)$c_ga_photos / 365), 0, '', '')
			]
		],


		'bicycle' => [
			'count' => count($tracks),
			'distances' => [
				'longest' => [
					'timestamp' => (int)$madi_file,
					'value' => (float)$madi_km
				],
				'shortest' => [
					'timestamp' => (int)$midi_file,
					'value' => (float)$midi_km
				]
			],
			'speeds' => [
				'highest' => [
					'timestamp' => (int)$masp_file,
					'value' => (float)$masp_kmh
				],
				'lowest' => [
					'timestamp' => (int)$misp_file,
					'value' => (float)$misp_kmh
				]
			],
			'travelling_times' => [
				'fastest' => [
					'timestamp' => (int)$madi_file,
					'value' => (float)$madi_km
				],
				'slowest' => [
					'timestamp' => (int)$midi_file,
					'value' => (float)$midi_km
				]
			],
			'elevations' => [
				'highest' => [
					'timestamp' => (isset($arr_elevations_max) ? (int)$maei_file : null),
					'value' => (isset($arr_elevations_max) ? (float)$maei_m : null)
				],
				'lowest' => [
					'timestamp' => (isset($arr_elevations_min) ? (int)$miei_file : null),
					'value' => (isset($arr_elevations_min) ? (float)$miei_m : null)
				]
			]
		],


		'visitors' => [
			'readme' => 'Visit data is based on cache files that are deleted 6 hours after they were created.',
			'bypassed' => $c_visitor_bypassed,
			'verified' => $c_visitor_verified,
			'most-likely-proxy' => $c_visitor_mostlikelyproxy,
			'low-risk' => $c_visitor_lowrisk,
			'fifty-fifty' => $c_visitor_fiftyfifty,
			'no-risk' => $c_visitor_norisk,
			'needs-checking' => $c_visitor_needschecking,
			'bad-ip' => $c_visitor_badip
		],


		'total_filesize_in_bytes' => [
			'total' => (int)dirsize($dir_posts) + (int)dirsize($dir_videos) + (int)dirsize($dir_uploads) + (int)dirsize($dir_gallery) + (int)dirsize($dir_guestbook) + (int)dirsize($dir_tracks),
			'blog' => [
				'posts' => (int)dirsize($dir_posts),
				'videos' => (int)dirsize($dir_videos),
				'photos' => (int)dirsize($dir_uploads)
			],
			'gallery' => (int)dirsize($dir_gallery),
			'guestbook' => (int)dirsize($dir_guestbook),
			'bicycle_tracks' => (int)dirsize($dir_tracks)
		]
	];

	file_put_contents($dir_files.'/stats.json', json_encode($arr));

	if(isset($_GET['upd'])) {
		header("Location: ".url('admin/overview'));
		exit;

	} else {
		echo json_encode($arr);
	}

?>
