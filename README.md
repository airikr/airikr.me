This is my personal website that does not use MySQL, PostreSQL, or SQLite, nor JavaScript. The website does not call any third party, but everything is run locally with caching.

It is based on my own template which can be found at https://git.airikr.me/airikr/template.

If you want to use my website under GPL 3.0, please do the following:
1. Put all the files in a directory of your choice on your web server.
1. The website stores uploaded files and blog posts in a separate directory outside the root directory, so create it based on the value of `$dir_files` in `site-settings.php`.
1. Change `/airikr/` in `.htaccess` to the name of the directory you put the files in.
1. Open the terminal and go to the directory you put all the files in, and run `composer install`.
1. Everything should work fine now. Visit the website to confirm this. Necessary folders for the folder you created in point 2, will be created as soon as you visit the web page.

## Report bugs
If you come across a bug, report it by creating a new issue.

## Known bugs
So far none.

## What is planned
- Add categories and/or tags to blog posts.
- Add more statistics to [the Biking page](https://airikr.me/biking?lang=en).
- Add statistics to the blog.