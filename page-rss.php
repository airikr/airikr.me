<?php

	require_once 'site-header.php';







	echo '<section id="rss">';
		echo '<div class="pagelogo">'.svgicon('rss').'</div>';

		echo '<h1>'.$lang['pages']['rss']['title'].'</h1>';

		foreach($lang['pages']['rss']['content'] AS $content) {
			echo $Parsedown->text($content);
		}


		echo '<div class="list">';
			foreach($lang['pages']['rss']['list'] AS $list) {
				echo '<div>';
					echo '<a href="'.$list['url'].'">';
						echo svgicon('rss') . $list['name'];
					echo '</a>';
				echo '</div>';
			}
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>