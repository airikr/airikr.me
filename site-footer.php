<?php

				echo '</main>';
				$lt_page_end = getrusage();


				if($filename != 'page-me.php') {
					echo '<footer class="lang-'.$get_lang.'">';
						echo '<div class="top">';
							echo '<a href="'.url('rss').'">';
								echo $lang['footer']['rss'];
							echo '</a>';

							echo '<div class="delimiter"></div>';

							echo '<a href="'.url('contact').'">';
								echo $lang['footer']['contact'];
							echo '</a>';

							echo '<div class="delimiter"></div>';

							echo '<a href="'.url('colophone').'">';
								echo $lang['footer']['colophone'];
							echo '</a>';

							echo '<div class="delimiter"></div>';

							echo '<a href="'.url('ai').'">';
								echo $lang['footer']['ai'];
							echo '</a>';

							echo '<div class="delimiter"></div>';

							echo '<a href="'.url('privacy').'">';
								echo $lang['footer']['privacy'];
							echo '</a>';

							echo '<div class="delimiter"></div>';

							echo '<a href="'.url('copyright').'">';
								echo $lang['footer']['creative-commons'];
							echo '</a>';
						echo '</div>';



						echo '<div class="bottom">';
							echo '<div class="left">';
								echo '<a href="'.url('sitemap').'">';
									echo $lang['footer']['sitemap'];
								echo '</a>';

								echo '<div class="delimiter"></div>';

								echo '<a href="'.url('chat').'">';
									echo $lang['footer']['chat'];
								echo '</a>';

								echo '<div class="delimiter"></div>';

								echo link_($lang['footer']['source-code'] . svgicon('external-link'), 'https://codeberg.org/airikr/airikr.me');
							echo '</div>';

							echo '<div class="right">';
								if($is_loggedin == false) {
									if($badvisitor == false) {
										echo '<a href="'.url('admin'.(empty(ltrim($uri, (empty($config_folder) ? '/' : '/airikr/'))) ? '' : '/previous-page:'.ltrim($uri, (empty($config_folder) ? '/' : '/airikr/')))).'">';
											echo $lang['footer']['login'];
										echo '</a>';
									}

								} else {
									echo '<a href="'.url('admin/overview').'">';
										echo $lang['footer']['admin'];
									echo '</a>';

									echo '<div class="delimiter"></div>';

									echo '<a href="'.url('admin/logout').'">';
										echo $lang['footer']['logout'];
									echo '</a>';
								}
							echo '</div>';


							/*echo '<div class="right">';
								if($filename == 'page-blog-post.php') {
									echo '<div class="options side-by-side">';
										echo svgicon('options');

										echo '<div class="side-by-side">';
											echo '<div class="wpm">';
												echo $lang['footer']['wpm'].':';
												echo '<form action="" method="POST" id="wpm">';
													echo '<input type="text" name="wpm" value="'.$get_wpm.'" maxlength="4">';
													echo '<input type="submit" name="recalculate" value="OK">';
												echo '</form>';
											echo '</div>';
										echo '</div>';
									echo '</div>';
								}

								if($filename == 'page-biking.php') {
									echo '<div class="options side-by-side">';
										echo svgicon('options');

										echo '<div class="side-by-side">';
											echo '<div class="average-speed">';
												echo $lang['footer']['average-speed'].':';
												echo '<form action="" method="POST" id="avs">';
													echo '<input type="text" name="averagespeed" value="'.$get_averagespeed.'" maxlength="3">';
													echo 'km/h';

													echo '<input type="submit" name="recalculate" value="OK">';
												echo '</form>';
											echo '</div>';
										echo '</div>';
									echo '</div>';
								}


								*echo '<div class="logging">';
									if($is_logging == false) {
										echo '<a href="'.url('telemetry').'" aria-label="Read why to log">'.$lang['footer']['telemetrysimulation']['enable'].'</a>';
									} else {
										echo '<a href="'.url('telemetry/your-data').'" aria-label="See all your data">'.$lang['footer']['telemetrysimulation']['your-data'].'</a>';
										echo '<a href="'.url('telemetry/delete', null, $get_lang, $get_theme, 'off').'" class="turnoff" aria-label="Shutdown the simulation">'.$lang['footer']['telemetrysimulation']['disable'].'</a>';
									}
								echo '</div>';*


								echo '<div class="indieweb">';
									echo '<a href="https://xn--sr8hvo.ws/%F0%9F%8D%8B%F0%9F%8C%8A%F0%9F%91%B6/previous" class="previous">';
										echo svgicon('arrow-left');
									echo '</a>';

									echo '<a href="https://xn--sr8hvo.ws/" class="goto">';
										echo 'IndieWeb';
									echo '</a>';

									echo '<a href="https://xn--sr8hvo.ws/%F0%9F%8D%8B%F0%9F%8C%8A%F0%9F%91%B6/next" class="next">';
										echo svgicon('arrow-right');
									echo '</a>';
								echo '</div>';
							echo '</div>';*/
						echo '</div>';


						echo '<div class="settings">';
							echo '<div class="home">';
								echo '<a href="'.url('').'">';
									echo svgicon('home');
								echo '</a>';
							echo '</div>';

							echo '<div class="language">';
								if($lang['metadata']['language'] == 'en') {
									echo '<a href="'.url($currentpage, null, 'se', $get_theme, $get_logging).'">';
										echo svgicon('flag-se');
									echo '</a>';

								} else {
									echo '<a href="'.url($currentpage, null, 'en', $get_theme, $get_logging).'">';
										echo svgicon('flag-en');
									echo '</a>';
								}
							echo '</div>';

							echo '<div class="theme '.($get_theme == 'light' ? 'dark' : 'light').'">';
								if($get_theme == 'light') {
									echo '<a href="'.url($currentpage, null, $get_lang, 'dark', $get_logging).'">';
										echo svgicon('theme-dark');
									echo '</a>';
	
								} else {
									echo '<a href="'.url($currentpage, null, $get_lang, 'light', $get_logging).'">';
										echo svgicon('theme-light');
									echo '</a>';
								}
							echo '</div>';
						echo '</div>';
					echo '</footer>';
				}
			echo '</section>';


			if($host == 'airikr.me') {
				echo '<script defer data-domain="airikr.me" src="https://analytics.airikr.me/js/script.js"></script>';
			}

			/*if($is_logging == true) {
				echo '<script src="'.url('js/jquery.min.js', true).'"></script>';
				echo '<script defer src="'.url('js/main.js?t='.time(), true).'"></script>';
				echo '<script defer src="'.url('node_modules/clientjs/dist/client.min.js', true).'"></script>';
			}*/



		echo '</body>';
	echo '</html>';

?>
