<?php

	# Include the required file
	require_once 'site-settings.php';

	use MatthiasMullie\Minify;

	$arr_singlefiles = [
		'desktop.css',
		'portable.css',
		'theme-dark.css',
		'theme-light.css'
	];

	foreach($arr_singlefiles AS $singlefile) {
		$fileinfo = pathinfo($singlefile);
		$minify = new Minify\CSS('css/'.$fileinfo['filename'].'.css');
		$minify->minify('css/'.$fileinfo['filename'].'.min.css');
	}

	foreach(glob('css/pages/*.css') AS $cssp) {
		$fileinfo = pathinfo($cssp);
		$minify = new Minify\CSS($cssp);
		$minify->minify('css/pages/minified/'.$fileinfo['filename'].'.css');
	}

	header("Location: ".url(''));
	exit;

?>