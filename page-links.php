<?php

	$arr_projects = [
		[
			'name' => 'Serenum',
			'address' => 'serenum.org'
		],
		[
			'name' => 'Keizai',
			'address' => 'keizai.se'
		],
		[
			'name' => 'Koroth',
			'address' => 'koroth.se'
		],
		[
			'name' => 'E.P.S.K.',
			'address' => 'genealogy.airikr.me'
		],
		[
			'name' => 'Hammarö Fotoklubb',
			'address' => 'hammarofotoklubb.se'
		],
		[
			'name' => 'Papolis Clinic',
			'address' => 'papolisclinic.se'
		],
		[
			'name' => 'Enc',
			'address' => 'enc.airikr.me'
		]
	];

	$arr_people = [
		[
			'name' => 'joelchrono',
			'address' => 'joelchrono.xyz'
		],
		[
			'name' => 'Kev Quirk',
			'address' => 'kevquirk.com'
		],
		[
			'name' => 'Hund',
			'address' => 'hund.linuxkompis.se'
		]
	];



	if(isset($_GET['json'])) {
		header('Content-Type: application/json;charset=utf-8');

		$arr = [
			'projects' => $arr_projects,
			'people' => $arr_people
		];

		echo json_encode($arr);



	} else {

		require_once 'site-header.php';



		usort($arr_projects, fn($a, $b) => $a['name'] <=> $b['name']);
		usort($arr_people, fn($a, $b) => $a['name'] <=> $b['name']);







		echo '<section id="links">';
			echo '<h1>'.$lang['pages']['links']['title'].'</h1>';

			if($notchecked == false) {
				foreach($lang['pages']['links']['content'] AS $content) {
					echo $Parsedown->text($content);
				}

				echo '<div class="side-by-side">';
					echo '<div class="projects">';
						echo '<h2>'.$lang['pages']['links']['subtitles']['projects'].'</h2>';

						echo '<ul>';
							foreach($arr_projects AS $project) {
								echo '<li>'.link_($project['name'], 'https://'.$project['address']).'</li>';
							}
						echo '</ul>';
					echo '</div>';


					echo '<div class="people">';
						echo '<h2>'.$lang['pages']['links']['subtitles']['people'].'</h2>';

						echo '<ul>';
							foreach($arr_people AS $person) {
								echo '<li>'.link_($person['name'], 'https://'.$person['address']).'</li>';
							}
						echo '</ul>';
					echo '</div>';
				echo '</div>';

			} else {
				foreach($lang['pages']['links']['notchecked'] AS $content) {
					echo $Parsedown->text($content);
				}
			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
