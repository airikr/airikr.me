<?php

	require_once 'site-header.php';







	echo '<section id="telemetry">';
		echo '<h1>'.$lang['pages']['telemetry']['title'].'</h1>';
		echo '<div class="cover" style="background-image: url('.url('metadata:telemetry', true).');"></div>';

		foreach($lang['pages']['telemetry']['content'][($is_logging == false ? 'disabled' : 'enabled')] AS $content) {
			echo $Parsedown->text($content);
		}

		if($is_logging == false) {
			echo '<details>';
				echo '<summary>'.svgicon('database') . $lang['pages']['telemetry']['content']['whatlogs']['title'].'</summary>';

				echo '<h3>'.svgicon('telemetry') . $lang['pages']['telemetry']['subtitles']['without-javascript'].'</h3>';
				foreach($lang['pages']['telemetry']['content']['whatlogs']['without-js'] AS $nojs) {
					echo '<div class="data">'.$nojs.'</div>';
				}

				echo '<h3>'.svgicon('javascript') . $lang['pages']['telemetry']['subtitles']['with-javascript'].'</h3>';
				foreach($lang['pages']['telemetry']['content']['whatlogs']['with-js']['stuff'] AS $js) {
					echo '<div class="data">'.$js.'</div>';
				}

				echo '<h3>'.svgicon('javascript') . svgicon('browser') . $lang['pages']['telemetry']['subtitles']['browser-properties'].'</h3>';
				echo '<ul>';
					foreach($lang['pages']['telemetry']['content']['whatlogs']['with-js']['browser'] AS $jsbrowser) {
						echo '<li>'.$jsbrowser.'</li>';
					}
				echo '</ul>';
			echo '</details>';

			echo '<div class="start">';
				echo '<a href="'.url($currentpage, null, $get_lang, $get_theme, '1').'">';
					echo $lang['pages']['telemetry']['start'];
				echo '</a>';
			echo '</div>';
		}
	echo '</section>';







	require_once 'site-footer.php';

?>
