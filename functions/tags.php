<?php

	function strp_bbcode_biking($str) {
		return str_replace('[biking]', '', str_replace('[/biking]', '', $str));
	}

	function strp_bbcode_contro($str) {
		return str_replace('[contro]', '', str_replace('[/contro]', '', $str));
	}

	function tags($string, $rss = false) {
		global $dir_files, $lang;

		if(strpos($string, 'biking') !== false) {
			preg_match_all('#\[biking\](.*?)\[/biking\]#i', $string, $matches, PREG_SET_ORDER);
			for($i = 0, $j = count($matches); $i < $j; $i++) {
				$timestamp = $matches[$i][1];
				$data = json_decode(file_get_contents($dir_files.'/tracks/json/'.$timestamp.'.json'), true);

				$biking = '<div class="biking">';
					$biking .= '<a href="'.url('biking-track:'.$timestamp).'">';
						$biking .= '<div class="item">';
							$biking .= '<div class="value">'.$data['movingtime']['hours'] . $lang['pages']['biking']['details']['hour'].' '.$data['movingtime']['minutes'] . $lang['pages']['biking']['details']['minute'].'</div>';
							$biking .= '<div class="label">'.$lang['pages']['biking']['details']['movingtime'].'</div>';
						$biking .= '</div>';
						$biking .= '<div class="item">';
							$biking .= '<div class="value">'.format_number($data['distance']['km']).' km</div>';
							$biking .= '<div class="label">'.$lang['pages']['biking']['details']['distance'].'</div>';
						$biking .= '</div>';
						$biking .= '<div class="item">';
							$biking .= '<div class="value">'.format_number($data['speed']['top']['kmh']).' km/h</div>';
							$biking .= '<div class="label">'.$lang['pages']['biking']['details']['speed-top'].'</div>';
						$biking .= '</div>';
						$biking .= '<div class="item">';
							$biking .= '<div class="value">'.format_number($data['speed']['average']['kmh']).' km/h</div>';
							$biking .= '<div class="label">'.$lang['pages']['biking']['details']['speed-average'].'</div>';
						$biking .= '</div>';
					$biking .= '</a>';
				$biking .= '</div>';

				$string = strp_bbcode_biking(str_replace($timestamp, $biking, $string));
			}
		}

		if($rss == false) {
			$bbcode = [
				'/(\[photo\=([A-z0-9]+)\])/' => link_(svgicon('zoom').'<img src="'.url('photo-thumb:$2', true).'">', url('photo:$2', true), 'img'),
				'/(\[photo\=([A-z0-9]+) desc\=(.*)\])/' => link_(svgicon('zoom').'<img src="'.url('photo-thumb:$2', true).'">', url('photo:$2', true), 'img').'<span class="img-desc">$3</span>',
				'/(\[video\=([A-z0-9\-]+)\])/' => '<video width="100%" height="100%" controls><source src="'.url('video:$2.mp4', true).'" type="video/mp4"><source src="'.url('video:$2.ogg', true).'" type="video/ogg"><source src="'.url('video:$2.webm', true).'" type="video/webm">No support.</video>',
				'/(\[contro\](.*)\[\/contro\])/' => '<details class="controversial"><summary class="no-select"><div class="open">'.svgicon('details-open').'</div><div class="close">'.svgicon('details-close').'</div>'.svgicon('warning') . $lang['pages']['blog']['read']['controversial-thought'].'</summary><div class="content">$2</div></details>',
				'/(\[spoiler\](.*)\[\/spoiler\])/' => '<details class="spoiler"><summary class="no-select"><div class="open">'.svgicon('details-open').'</div><div class="close">'.svgicon('details-close').'</div>'.svgicon('spoiler') . $lang['pages']['blog']['read']['spoiler'].'</summary><div class="content">$2</div></details>'
			];

		} else {
			$bbcode = [
				'/(\[photo\=([A-z0-9]+)\])/' => '<img src="'.url('photo-thumb:$2', true).'">',
				'/(\[photo\=([A-z0-9]+) desc\=(.*)\])/' => '<img src="'.url('photo-thumb:$2', true).'"><span class="img-desc">$3</span>',
				'/(\[video\=([A-z0-9\-]+)\])/' => '-- '.$lang['pages']['blog']['read']['video'].' --',
				'/(\[contro\](.*)\[\/contro\])/' => '-- '.$lang['pages']['blog']['read']['controversial-thought-readmore'].' --',
				'/(\[spoiler\](.*)\[\/spoiler\])/' => '-- '.$lang['pages']['blog']['read']['spoiler-readmore'].' --'
			];
		}

		return preg_replace(array_keys($bbcode), array_values($bbcode), $string);
	}

?>
