<?php

	use Intervention\Image\ImageManager;
	use Intervention\Image\Drivers\Imagick\Driver;

	require_once '/path/to/folder/site-settings.php';

	$manager = new ImageManager(['driver' => 'imagick']);
	$arr = [];

	function delete_dir(string $dir): void {
		$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files AS $file) {
			if($file->isDir()) {
				rmdir($file->getPathname());
			} else {
				unlink($file->getPathname());
			}
		}

		rmdir($dir);
	}



	foreach(glob($dir_files.'/posts/published/*.json') AS $post) {
		$fileinfo = pathinfo($post);
		$data = json_decode(file_get_contents('https://webmention.io/api/mentions.jf2?target='.$site_url.'/blog/'.$fileinfo['filename']), true);

		if(!empty($data['children'])) {
			delete_dir($dir_files.'/posts/webmentions/'.$fileinfo['filename']);
			create_folder($dir_files.'/posts/webmentions/'.$fileinfo['filename']);

			foreach($data['children'] AS $entry) {
				$avatar = $dir_files.'/posts/webmentions/'.$fileinfo['filename'].'/'.seo($entry['author']['name']);

				if($entry['wm-property'] == 'repost-of') {
					$type = 'boost';
				} elseif($entry['wm-property'] == 'like-of') {
					$type = 'favourite';
				} elseif($entry['wm-property'] == 'in-reply-to' OR $entry['wm-property'] == 'mention-of') {
					$type = 'reply';
				} elseif($entry['wm-property'] == 'bookmark-of') {
					$type = 'bookmark';
				}

				$arr[] = [
					'type' => $type,
					'name' => $entry['author']['name'],
					'url' => $entry['author']['url'],
					'avatar' => $entry['author']['photo'],
					'reply' => (!isset($entry['content']['html']) ? null : $entry['content']['html']),
					'reply_sent' => (!isset($entry['published']) ? null : strtotime($entry['published']))
				];

				if(!file_exists($avatar.'.webp')) {
					file_put_contents($avatar.'.png', file_get_contents($entry['author']['photo']));

					$oldmask = umask(0);
					chmod($avatar.'.png', 0777);
					umask($oldmask);

					$manager->make($avatar.'.png')->encode('webp', 70)->save($avatar.'.webp');
					unlink($avatar.'.png');
				}
			}

			file_put_contents($dir_files.'/posts/webmentions/'.$fileinfo['filename'].'/webmention.json', json_encode($arr));
		}
	}

?>