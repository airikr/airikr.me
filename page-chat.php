<?php

	require_once 'site-header.php';



	$show = (isset($_GET['sho']) ? safetag($_GET['sho']) : false);







	echo '<section id="chat">';
		echo '<header>';
			echo '<img src="'.url('images/airikr-chat-'.$lang['metadata']['language'].'.webp', true).'">';
		echo '</header>';


		echo '<details class="intro">';
			echo '<summary class="no-select">';
				echo '<div class="open">'.svgicon('details-open').'</div>';
				echo '<div class="close">'.svgicon('details-close').'</div>';
				echo svgicon('info') . $lang['pages']['chat']['intro']['title'];
			echo '</summary>';

			echo '<div class="content">';
				foreach($lang['pages']['chat']['intro']['content'] AS $content) {
					echo $Parsedown->text($content);
				}
			echo '</div>';
		echo '</details>';


		echo '<details class="rules"'.($show == false ? '' : ($show == 'rules' ? ' open' : '')).'>';
			echo '<summary class="no-select">';
				echo '<div class="open">'.svgicon('details-open').'</div>';
				echo '<div class="close">'.svgicon('details-close').'</div>';
				echo svgicon('rules') . $lang['pages']['chat']['rules']['title'];
			echo '</summary>';

			echo '<div class="content">';
				foreach($lang['pages']['chat']['rules']['content'] AS $content) {
					echo $Parsedown->text($content);
				}
			echo '</div>';
		echo '</details>';


		echo '<details class="guide"'.($show == false ? '' : ($show == 'join' ? ' open' : '')).'>';
			echo '<summary class="no-select">';
				echo '<div class="open">'.svgicon('details-open').'</div>';
				echo '<div class="close">'.svgicon('details-close').'</div>';
				echo svgicon('guide') . $lang['pages']['chat']['guide']['title'];
			echo '</summary>';

			echo '<div class="content">';
				foreach($lang['pages']['chat']['guide']['content'] AS $content) {
					echo $Parsedown->text($content);
				}
			echo '</div>';
		echo '</details>';


		echo '<details class="faq"'.($show == false ? '' : ($show == 'faq' ? ' open' : '')).'>';
			echo '<summary class="no-select">';
				echo '<div class="open">'.svgicon('details-open').'</div>';
				echo '<div class="close">'.svgicon('details-close').'</div>';
				echo svgicon('faq') . $lang['pages']['chat']['faq']['title'];
			echo '</summary>';

			echo '<div class="content">';
				foreach($lang['pages']['chat']['faq']['content'] AS $content) {
					echo $Parsedown->text($content);
				}
			echo '</div>';
		echo '</details>';
	echo '</section>';







	require_once 'site-footer.php';

?>
