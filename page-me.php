<?php

	use chillerlan\QRCode\{QRCode, QROptions};
	use chillerlan\QRCode\Common\EccLevel;

	require_once 'site-header.php';



	$datetime = new DateTime('now', new DateTimeZone('Europe/Stockholm'));

	$arr_skills_hardware = [
		[
			'string' => $lang['pages']['me']['skills']['list']['photography'],
			'icon' => 'photography',
			'skills' => 3,
			'category' => 'hardware'
		]
	];

	$arr_skills_language = [
		[
			'string' => $lang['pages']['me']['languages']['list']['swedish'],
			'icon' => 'flag-se',
			'skills' => 5,
			'category' => 'language'
		],
		[
			'string' => $lang['pages']['me']['languages']['list']['english'],
			'icon' => 'flag-en',
			'skills' => 4,
			'category' => 'language'
		],
		[
			'string' => $lang['pages']['me']['languages']['list']['japanese'],
			'icon' => 'flag-jp',
			'skills' => 1,
			'category' => 'language'
		]
	];

	$arr_skills_software = [
		[
			'string' => $lang['pages']['me']['skills']['list']['lightroom'],
			'icon' => 'lightroom',
			'skills' => 3,
			'category' => 'software'
		]
	];

	$arr_skills_programming = [
		[
			'string' => $lang['pages']['me']['skills']['list']['php'],
			'icon' => 'php',
			'skills' => 4,
			'category' => 'programming'
		],
		[
			'string' => $lang['pages']['me']['skills']['list']['html'],
			'icon' => 'html',
			'skills' => 4,
			'category' => 'programming'
		],
		[
			'string' => $lang['pages']['me']['skills']['list']['javascript'],
			'icon' => 'javascript',
			'skills' => 1,
			'category' => 'programming'
		],
		[
			'string' => $lang['pages']['me']['skills']['list']['jquery'],
			'icon' => 'jquery',
			'skills' => 3,
			'category' => 'programming'
		],
		[
			'string' => $lang['pages']['me']['skills']['list']['css'],
			'icon' => 'css',
			'skills' => 3,
			'category' => 'programming'
		],
		[
			'string' => $lang['pages']['me']['skills']['list']['git'],
			'icon' => 'git',
			'skills' => 1,
			'category' => 'programming'
		]
	];

	$arr_skills_other = [
		[
			'string' => $lang['pages']['me']['skills']['list']['genealogy'],
			'icon' => null,
			'skills' => 3,
			'category' => 'other'
		],
		[
			'string' => $lang['pages']['me']['skills']['list']['bicycling'],
			'icon' => null,
			'skills' => 4,
			'category' => 'other'
		],
		[
			'string' => $lang['pages']['me']['skills']['list']['cooking'],
			'icon' => null,
			'skills' => 2,
			'category' => 'other'
		],
		[
			'string' => $lang['pages']['me']['skills']['list']['painting'],
			'icon' => null,
			'skills' => 1,
			'category' => 'other'
		]
	];

	usort($arr_skills_hardware, fn($a, $b) => $a['string'] <=> $b['string']);
	usort($arr_skills_software, fn($a, $b) => $a['string'] <=> $b['string']);
	usort($arr_skills_programming, fn($a, $b) => $a['string'] <=> $b['string']);
	usort($arr_skills_language, fn($a, $b) => $a['string'] <=> $b['string']);

	$options = new QROptions([
		'version' => 7,
		'eccLevel' => EccLevel::M,
		'outputBase64' => false,
		'scale' => 10
	]);




	echo '<section id="me">';
		echo '<div class="bg"></div>';

		echo '<div class="avatar">';
			echo '<div class="pronouns">';
				foreach($lang['pages']['me']['pronounses'] AS $pronouns) {
					echo mb_strtolower($pronouns) . (next($lang['pages']['me']['pronounses']) ? ', ' : '');
				}
			echo '</div>';

			echo '<div class="gender">';
				echo svgicon('male');
			echo '</div>';
		echo '</div>';



		echo '<div class="content">';
			echo '<div class="links">';
				echo link_('Mastodon', 'https://mst.airikr.me/@edgren');
				echo link_('PeerTube', 'https://video.airikr.me/@edgren');
				echo link_('Codeberg', 'https://codeberg.org/airikr');
				echo link_('SweClockers', 'https://www.sweclockers.com/medlem/30944');
			echo '</div>';



			echo '<div class="xmpp">';
				echo '<div class="address">';
					echo '<a href="xmpp://airikr@chat.airikr.me">';
						echo svgicon('brand-xmpp');
						echo 'airikr@chat.airikr.me';
					echo '</a>';
				echo '</div>';

				echo '<details class="devices">';
					echo '<summary class="no-select">';
						echo '<div class="open">'.svgicon('details-open').'</div>';
						echo '<div class="close">'.svgicon('details-close').'</div>';
						echo $lang['pages']['me']['xmpp']['my-devices'];
					echo '</summary>';

					echo '<div class="content">';
						echo '<div class="info">';
							foreach($lang['pages']['me']['xmpp']['devices'] AS $content) {
								echo $Parsedown->text($content);
							}
						echo '</div>';

						echo '<div class="devices">';
							foreach($arr_devices AS $device) {
								$qrdata = 'xmpp:airikr@chat.airikr.me?message;omemo-sid-'.$device['sid'].'='.mb_strtolower(str_replace(' ', '', $device['code']));

								echo '<details title="'.$device['tooltip'].'">';
									echo '<summary>';
										echo '<div class="icon">'.svgicon($device['icon']).'</div>';
										echo '<div class="code">'.wordwrap($device['code'], 35, '<br>').'</div>';
									echo '</summary>';

									echo '<div class="qr">'.(new QRCode($options))->render($qrdata).'</div>';
									echo '<div class="address">'.$qrdata.'</div>';
								echo '</details>';
							}
						echo '</div>';
					echo '</div>';
				echo '</details>';
			echo '</div>';



			echo '<div class="place">';
				echo svgicon('map-pin');

				echo '<div class="text">';
					echo $lang['pages']['me']['location'];
					echo '<div class="datetime">';
						echo date_(strtotime($datetime->format('Y-m-d H:i')), 'fully-detailed');
					echo '</div>';
				echo '</div>';
			echo '</div>';



			echo '<div class="skills">';
				echo '<h2>';
					echo $lang['pages']['me']['skills']['title'];
				echo '</h2>';

				echo '<h3 class="first">'.$lang['pages']['me']['skills']['languages'].'</h3>';
				foreach($arr_skills_language AS $skill) {
					echo '<div>';
						echo '<div class="string">';
							echo svgicon($skill['icon']) . $skill['string'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="skills no-select">';
							for($i = 0; $i < 5; $i++) {
								echo '<div class="dot'.($i >= $skill['skills'] ? ' inactive' : '').'"></div>';
							}
						echo '</div>';
					echo '</div>';
				}


				echo '<h3>'.$lang['pages']['me']['skills']['hardwares'].'</h3>';
				foreach($arr_skills_hardware AS $skill) {
					echo '<div>';
						echo '<div class="string">';
							echo svgicon($skill['icon']) . $skill['string'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="skills no-select">';
							for($i = 0; $i < 5; $i++) {
								echo '<div class="dot'.($i >= $skill['skills'] ? ' inactive' : '').'"></div>';
							}
						echo '</div>';
					echo '</div>';
				}


				echo '<h3>'.$lang['pages']['me']['skills']['softwares'].'</h3>';
				foreach($arr_skills_software AS $skill) {
					echo '<div>';
						echo '<div class="string">';
							echo svgicon($skill['icon']) . $skill['string'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="skills no-select">';
							for($i = 0; $i < 5; $i++) {
								echo '<div class="dot'.($i >= $skill['skills'] ? ' inactive' : '').'"></div>';
							}
						echo '</div>';
					echo '</div>';
				}


				echo '<h3>'.$lang['pages']['me']['skills']['programming'].'</h3>';
				foreach($arr_skills_programming AS $skill) {
					echo '<div>';
						echo '<div class="string">';
							echo svgicon($skill['icon']) . $skill['string'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="skills no-select">';
							for($i = 0; $i < 5; $i++) {
								echo '<div class="dot'.($i >= $skill['skills'] ? ' inactive' : '').'"></div>';
							}
						echo '</div>';
					echo '</div>';
				}


				echo '<h3>'.$lang['pages']['me']['skills']['other'].'</h3>';
				foreach($arr_skills_other AS $skill) {
					echo '<div>';
						echo '<div class="string">';
							echo $skill['string'];
						echo '</div>';

						echo '<div class="line"></div>';

						echo '<div class="skills no-select">';
							for($i = 0; $i < 5; $i++) {
								echo '<div class="dot'.($i >= $skill['skills'] ? ' inactive' : '').'"></div>';
							}
						echo '</div>';
					echo '</div>';
				}
			echo '</div>';



			echo '<div class="qr">';
				echo '<h2>'.$lang['pages']['me']['qr']['title'].'</h2>';
				echo '<img src="'.url($dir_images.'/qr-code.webp').'">';
			echo '</div>';
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
