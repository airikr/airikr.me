<?php

	$get_year = (!isset($_GET['ye']) ? '' : safetag($_GET['ye']));
	$get_month = (!isset($_GET['mo']) ? '' : safetag($_GET['mo']));
	$get_day = (!isset($_GET['da']) ? '' : safetag($_GET['da']));
	$get_hours = (!isset($_GET['ho']) ? '' : safetag($_GET['ho']));
	$get_minutes = (!isset($_GET['mi']) ? '' : safetag($_GET['mi']));

	$arr_functions = [
		'[erusev/parsedown](https://github.com/erusev/parsedown): möjliggör Markdown för all text på webbsidan.',
		'[matthiasmullie/minify](https://github.com/matthiasmullie/minify): minifiera CSS.',
		'[intervention/image](https://github.com/intervention/image): förminska och konvertera uppladdade bilder.',
		'[whichbrowser/parser](https://github.com/whichbrowser/parser): analysera användaragenter (används för tillfället inte).',
		'[paragonie/halite](https://github.com/paragonie/halite): kryptera och avkryptera text och filer.',
		'[geertw/ip-anonymizer](https://github.com/geertw/php-ip-anonymizer): anonymisera IP-adresser.',
		'[multiavatar/multiavatar-php](https://github.com/multiavatar/multiavatar-php): generera visningsbilder till kommentarerna på bloggen.',
		'[jaybizzle/crawler-detect](https://github.com/jaybizzle/crawler-detect): detektera botar och liknande skit.',
		'[robthree/twofactorauth](https://github.com/robthree/twofactorauth): generera och verifiera tvåstegsverifieringskoder.',
		'[chillerlan/php-qrcode](https://github.com/chillerlan/php-qrcode): generera QR-koder.',
		'[PHPMailer/PHPMailer](https://github.com/PHPMailer/PHPMailer): skickar e-postmeddelanden till mig när någon har skickat ett nytt gästboksinlägg.',
		'[oscarotero/Embed](https://github.com/oscarotero/Embed): läser in metadatan från webbsidor på "[Metadata]('.url('metadata').')"-sidan.'
	];



	$lang = [
		'metadata' => [
			'iso6391' => 'sv',
			'language' => 'se',
			'locale' => 'sv_SE',
			'descriptions' => [
				'default' => 'Läs om Erik, följ hans blogg, kontakta han, med mera.',
				'about' => 'Läs vem Erik är.',
				'blog' => 'Läs Eriks blogg.',
				'slogan' => 'Läs Eriks slagord: mitt liv, min data, mina krav.',
				'privacy' => 'Läs integritetspolicyn för '.$site_title,
				'biking' => 'Följ Eriks cykeläventyr.',
				'biking-track' => 'Se mer om ett av Eriks cykeläventyr.',
				'copyright' => 'Läs uppehovsrätten för Eriks material.',
				'chat' => 'Läs bland annat reglerna för Airikr Chat.',
				'contact' => 'Ta reda på hur du kan kontakta Erik.',
				'links' => 'Se Eriks samling av länkar till bland annat hans projekt.',
				'me' => 'Få information om främst hur du kan kontakta mig.',
				'guestbook' => 'Läs och/eller signera min gästbok.',
				'nsfw' => 'Stötande inlägg. Öppna länken och välj "Ja" om du vill läsa det.',
				'uses' => 'Ta reda på vad Erik använder.',
				'rss' => [
					'blog' => 'Läs Eriks tankar inom integritet, cykling och annat.',
					'comments' => 'Följ kommentarerna på Eriks blogg.',
					'biking' => 'Följ Eriks resa till ett bättre och sundare liv på cykeln.',
					'gallery' => 'Följ Eriks uppladdningar till hans galleri.',
					'guestbook' => 'Följ de gästboksinlägg som publiceras i Eriks gästbok.'
				],
				'gallery' => [
					'photos' => 'Se alla bilder som Erik har laddat upp till sitt galleri.',
					'photo' => 'Ta en närmare titt på ett fotografi som Erik har laddat upp till sitt galleri.',
					'photo-sensitive' => 'Öppna länken och gör ett val om du vill visa den känsliga bilden.'
				]
			]
		],

		'ipintel' => [
			'title' => 'Vänligen godkänn extern datalagring',
			'content' => [
				'Jag är trött på att robotar ska spamma främst gästboken. Har därför lagt in en API som heter [IP Intelligence](https://getipintel.net/) (förkortas IPIntel).',
				'Men eftersom den IP-adress som din enhet använder lagras då på en server som jag inte hanterar, vill jag att du ska godkänna detta. Gå till [integritetsidan]('.url('privacy').') och läs punkt 1 om vad som lagras hos IPIntel.',
				'Du kan välja att ignorera detta och fortsätta surfa runt på '.$site_title.'. Tänk bara på att du inte kan göra något aktivt då, som till exempel reagera på ett blogginlägg.'
			],
			'agree' => 'Godkänn'
		],

		'nav' => [
			'open' => 'Visa menyn',
			'close' => 'Göm menyn',
			'about' => 'Om',
			'blog' => 'Blogg',
			'gallery' => 'Galleri',
			'biking' => 'Cykling',
			'links' => 'Länkar',
			'guestbook' => 'Gästbok'
		],

		'footer' => [
			'contact' => 'Kontakta',
			'privacy' => 'Integritet',
			'ai' => 'AI',
			'rss' => 'RSS',
			'colophone' => 'Kolofon',
			'sitemap' => 'Sitemap',
			'chat' => 'Airikr Chat',
			'source-code' => 'Källkod',
			'creative-commons' => 'Kreativ allmänning',
			'login' => 'Logga in',
			'logout' => 'Logga ut',
			'telemetrysimulation' => [
				'enable' => 'Telemetrisimulator',
				'disable' => 'Stäng av',
				'your-data' => 'Din data'
			],
			'admin' => 'Admin',
			'language' => 'Språk',
			'theme' => 'Tema',
			'themes' => [
				'dark' => 'Mörkt',
				'light' => 'Ljust'
			],
			'average-speed' => 'Genomsnittlig hastighet',
			'wpm' => 'Ord per minut',
			'loading-time' => 'Det tog **{lt} sekunder** att läsa in sidan.'
		],

		'messages' => [
			'no-blogstats' => 'Kunde inte hitta någon statistik',
			'no-posts' => 'Det finns inga inlägg än',
			'no-posts-search' => 'Kunde inte hitta några inlägg',
			'no-images' => 'Det finns inga uppladdade bilder än',
			'no-tracks' => 'Det finns inga uppladdade GPX-spår än',
			'no-videos' => 'Det finns inga uppladdade videoklipp än',
			'no-covers' => 'Det finns inga uppladdade omslagsbilder än',
			'no-comments' => 'Kunde inte hitta några kommentarer',
			'no-photos' => 'Kunde inte hitta några fotografier',
			'no-posts-guestbook' => 'Här var det tomt. Du kanske vill bli den första? :)',
			'no-webmentions' => 'Här var det tomt',
			'admin-exists' => 'Det finns redan en admin',
			'guestbook-thanks' => 'Tack för ditt inlägg :)',
			'disclaimers' => [
				'post-not-valid' => [
					'Det här inlägget innehåller information som inte längre gäller. Det kan till exempel vara att en funktion på bloggen har uppdaterats.'
				],
				'old-post' => [
					'Det här inlägget är äldre än 3 år. Då min personlighet förändras med tiden, kan texten nedan kanske inte stämma i dagsläget.'
				]
			],
			'errors' => [
				'not-admin' => 'Du har inte tillgång till detta.',
				'empty-fields' => [
					'# Obligatoriska textfält ej ifyllda',
					'Minst ett av följande textfält är inte ännu ifyllda.'
				],
				'notvalid-email' => [
					'# Ej giltig e-postadress',
					'Den e-postadress som du har angett '.(isset($_SESSION['email']) ? '('.$_SESSION['email'].')' : '').', är inte giltig.',
					'En giltig adress ska ha följande upplägg: `namn@domän.tld`'
				],
				'notvalid-url' => [
					'# Ej giltig webbadress',
					'En giltig webbadress ska innehålla det säkra protokollet (`https://`) och bestå av `domän.tld` (till exempel airikr.me).'
				],
				'wrong-username' => 'Du har angett fel användarnamn.',
				'wrong-password' => 'Du har angett fel lösenord. Vänligen försök igen.',
				'wrong-tfa' => 'Du har angett fel tvåstegsverifieringskod. Vänligen försök igen.',
				'tfa-onlydigits' => 'Tvåstegsverifingskoden får endast innehålla siffror. Vänligen försök igen.',
				'tfa-tooshortorlong' => 'Tvåstegsverifingskoden får endast vara 6 siffror lång. Vänligen försök igen.',
				'no-admin' => 'Webbsidan har inga användare med administrativa rättigheter. [Skapa en nu]('.url('admin/create').').',
				'admin-notexists' => '`admin.json` har av någon anledning blivit borttagen. [Återskapa adminkontot]('.url('admin/create').').',
				'onlydigits-wpm' => 'Ord per minut kan endast innehålla siffror.',
				'keyword-empty' => 'Du måste ange ett sökord först. [Visa alla blogginläggen]('.url('blog').').',
				'keyword-tooshort' => 'Sökordet måste innehålla minst 3 bokstäver. [Visa alla blogginläggen]('.url('blog').').',
				'keyword-onlychars' => 'Sökordet får endast innehålla bokstäver. [Visa alla blogginläggen]('.url('blog').').'
			]
		],

		'tooltips' => [
			'filter-by-year' => 'Filtrera efter år',
			'filter-by-month' => 'Filtrera efter månad',
			'filter-by-date' => 'Filtrera efter datum',
			'emoji-happy' => 'Glad',
			'emoji-laugh' => 'Roligt',
			'emoji-sad' => 'Ledsen',
			'emoji-angry' => 'Arg',
			'emoji-like' => 'Gillar',
			'emoji-dislike' => 'Ogillar',
			'emoji-heart' => 'Älskar',
			'your-reaction' => 'Din reaktion',
			'rss-comments' => 'Följ kommentarsflödet via RSS',
			'read-post' => 'Läs inlägget',
			'goto-comment' => 'Gå till kommentaren i inlägget',
			'directlink-comment' => 'Direktlänk till kommentaren',
			'old-comment-system' => 'Kommentaren publicerades innan det nya systemet infördes',
			'devices' => [
				'desktop' => 'Stationär dator',
				'laptop' => 'Bärbar dator',
				'smartphone' => 'Smart telefon'
			]
		],

		'checkboxes' => [
			'nsfw' => 'Markera inlägget som känsligt',
			'post-not-valid' => 'Inlägget innehåller information som inte längre gäller eller stämmer',
			'updatelog' => 'Inlägget är en ändringslogg',
			'hundreddaystooffload' => 'Inlägget ingår i #100DaysToOffload'
		],

		'rss' => [
			'titles' => [
				'blog' => 'Blogg',
				'comments' => 'Kommentarer',
				'biking' => 'Cykling',
				'gallery' => 'Galleri',
				'guestbook' => 'Gästbok'
			],
			'occurred' => 'När',
			'movingtime' => 'Rörelsetid',
			'distance' => 'Distans',
			'speed-top' => 'Topphastighet',
			'speed-average' => 'Genomsnittlig hastighet',
			'hour' => 't',
			'minute' => 'm',
			'camera' => 'Kamera',
			'taken' => 'Togs',
			'iso' => 'ISO',
			'aperture' => 'Bländaröppning',
			'exposure' => 'Exponering'
		],

		'months' => [
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Mars',
			4 => 'April',
			5 => 'Maj',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Augusti',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'December'
		],

		'days' => [
			1 => 'Mån',
			2 => 'Tis',
			3 => 'Ons',
			4 => 'Tors',
			5 => 'Fre',
			6 => 'Lör',
			7 => 'Sön'
		],

		'goback' => 'Gå tillbaka',
		'summertime' => 'Sommartid',
		'wintertime' => 'Vintertid',



		'pages' => [
			'about' => [
				'about' => [
					'## Hej och välkommen!',
					'Erik heter jag, men eftersom jag älskar historia, kallar jag mig för Airikr. Föddes under sommaren 1985 ('.age(1985, 7, 6, 1838).' år sen) på skärgårdsön Hammarö i Värmlands län.',

					'## Sjukdom och diagnoser',
					'Är född med [antifosfolipidsyndrom](https://www.karolinska.se/vard/funktion/funktion-medicinsk-diagnostik-karolinska/vardgivare/kul/centrum-for-sallsynta-diagnoser/regionala-expertteam/antifosfolipidsyndrom-aps/). Som 10-åring diagnoserades jag med DAMP, och som 28-åring omdiagnoserades jag med lätt Aspergers syndrom och grov uppmärksamhetsstörning.',

					'## Intressen',
					'Integritet, historia, fotografering, cykling och att upptäcka platser står mig närmast i hjärtat.'
				],
				'slogan' => 'Mitt liv, min data, mina krav!',
				'videofeed' => [
					'title' => 'Senaste videoklippen'
				]
			],


			'slogan' => [
				'title' => 'Mitt liv, min data, mina krav',
				'content' => [
					'Man kan se mig som mycket mån om min integritet, men även om andras. Jag vill kunna bestämma hur min egna data ska behandlas utanför Sveriges gränser, men även inom dessa gränser. Alla borde egentligen vara mån om sin integritet, men tyvärr är fallet inte så.',
					'Stora företag som Google, Apple och Microsoft, kan göra i stort sett precis vad de vill med vår data - många bryr sig inte ett skvatt. Det finns de som tänker, "men jag bor ju i EU, GDPR ska då kunna ta hand om min integritet åt mig". Nej, så är inte fallet.',
					'GDPR finns för att man ska kunna hantera sin personliga data lättare. Det hindrar inte tredjeparten från att ta till exempel bilder och videos på ens barn och använda dessa till bland annat AI utan att fråga om lov först.',
					'Om man inte vill att ens personliga data ska hamna i orätta händer, måste man ta hand om det själv. Ingen annan än du själv kan göra något åt ditt privatliv online.',
					'Nedan berättar jag vad jag menar med mitt slagord.',
				],
				'mylife' => [
					'title' => 'Mitt liv',
					'content' => [
						'Jag ska ha rätten till att kunna bestämma var min egna data ska läggas upp, hur datan ska behandlas och vad den ska användas till. Det är mitt liv det handlar om, inte någon annans.'
					]
				],
				'mydata' => [
					'title' => 'Min data',
					'content' => [
						'Jag ska ha rätten till att enkelt kunna komma åt och hantera min egna data. Om jag vill radera någon data som tillhör mig som person (till exempel mitt födelsedatum), så ska ägaren av servern rätta sig efter om jag vill ta bort min data.'
					]
				],
				'mydemands' => [
					'title' => 'Mina krav',
					'content' => [
						'Jag ska ha rätten till att kunna bestämma hur min egna data ska ligga lagrad och hur den ska behandlas. Om den behandlas på ett sätt som jag inte gillar, ska jag kunna kräva att ägaren av servern ska kunna hantera min data efter mina krav.'
					]
				]
			],



			'blog' => [
				'write-empty' => 'Skriv ett inlägg för att komma igång',
				'nav' => [
					'rss' => 'Prenumerera via RSS',
					'write-notempty' => 'Skriv ett nytt inlägg'
				],
				'subtitles' => [
					'filtering' => 'Visar inlägg från',
					'filtering-date' => 'Visar inlägg från den',
					'searching' => 'Visar inlägg med sökordet',
					'hundreddaystooffload' => 'Visar inlägg som ingår i '.link_('100DaysToOffload' ,'https://100daystooffload.com'),
					'filter' => [
						'label' => 'Filtrera',
						'posts' => 'Inlägg',
						'comments' => 'Kommentarer',
						'reactions' => 'Reaktioner',
						'hundreddaystooffload' => '100DaysToOffload'
					]
				],
				'statistics' => [
					'title' => 'Statistik',
					'content' => [
						'Det första inlägget publicerades **{firstpost}** och bloggen har i dag **{posts} inlägg**, varav **{posts_hundreddaystooffload}** av dom har hashtaggen 100DaysToOffload. Sen starten, har totalt **{reactions} reaktioner** gjorts.',
						'Alla inlägg innehåller **{images} bilder** och **{links} länkar**, och innehåller totalt **{words} ord**. Det motsvarar **{compare_lotr}** av The Lord of the Rings-trilogin och **{compare_thebible}** av King James-bibeln.',
						'Eftersom detta är en statisk blogg, tar alla filer totalt **{totalsize}**.'
					]
				],
				'calendar' => [
					'title' => 'Kalender',
					'months' => [
						1 => 'Januari',
						2 => 'Februari',
						3 => 'Mars',
						4 => 'April',
						5 => 'Maj',
						6 => 'Juni',
						7 => 'Juli',
						8 => 'Augusti',
						9 => 'September',
						10 => 'Oktober',
						11 => 'November',
						12 => 'December'
					]
				],
				'reactions' => [
					'notchecked' => [
						'Du måste bekräfta den externa datalagringen innan du kan reagera.'
					]
				],
				'list' => [
					'published' => 'Publicerades',
					'subject' => 'Rubrik',
					'reactions' => [
						'title' => 'Reaktioner',
						'info' => 'Nedan listas de 100 senaste reaktionerna.',
						'and-counting' => 'Fler reaktioner och antalet ökas kontinueligt.'
					]
				],
				'search' => [
					'placeholder' => 'Sök bland inläggen',
					'year' => 'År',
					'month' => 'Månad',
					'button' => 'Sök'
				],
				'read' => [
					'of' => 'Av',
					'table-of-content' => 'Innehållförteckning',
					'word' => 'Ord',
					'words' => 'Ord',
					'edit' => 'Ändra',
					'goto' => 'Gå till',
					'reaction' => 'Reaktion',
					'reactions' => 'Reaktioner',
					'comment' => 'Kommentar',
					'comments' => 'Kommentarer',
					'paging' => [
						'showall' => 'Visa alla inlägg',
						'random' => 'Slumpmässigt inlägg',
						'previous' => 'Nyare',
						'next' => 'Äldre'
					],
					'hundreddaystooffload' => '100DaysToOffload',
					'controversial-thought' => 'Kontroversiell tanke',
					'controversial-thought-readmore' => 'Kontroversiell tanke (öppna länken för att läsa den)',
					'spoiler' => 'Spoiler',
					'spoiler-readmore' => 'Spoiler (öppna länken för att läsa den)',
					'video' => 'Video',
					'webmentions' => [
						'Om inlägget har delats inom fediverse, som till exempel på Mastodon, kommer favoriter, boostningar och svar att listas nedan.'
					],
					'nsfw' => [
						'hide-post' => 'Dölj inlägget igen',
						'content' => [
							'Det här inlägg kan ses som stötande, framkalla förargelse, eller liknande. Det kan finnas en risk att inlägget innehåller kontroversiella tankar från skribenten.',
							'Om du vill veta hur din data hanteras om du vill läsa inlägget, gå då till [integritetspolicyn]('.url('privacy').') och läs punkt 3.6.',
							'Vill du läsa inlägget? [Ja]('.url('blog/'.$get_year . $get_month . $get_day . $get_hours . $get_minutes.'/remove-nsfw-mark').') / [Nej]('.url('blog/'.$get_year . $get_month . $get_day . $get_hours . $get_minutes.'/random-post').')'
						]
					],
					'readstatus' => [
						'title' => 'Lässtatus',
						'desc' => [
							'Läs punkt 3.7 på [integritetspolicyn]('.url('privacy').') för och läsa om hur din data hanteras.'
						],
						'options' => [
							'read' => 'Markera inlägget som läst',
							'unread' => 'Markera inlägget som oläst'
						],
						'reader' => 'Läsare',
						'readers' => 'Läsare'
					],
					'share' => [
						'title' => 'Dela inlägget'
					]
				],
				'comment' => [
					'new' => 'Skriv en kommentar',
					'manage' => 'Hantera en kommentar',
					'options' => [
						'manage' => 'Hantera'
					],
					'terms' => [
						'## Villkor',
						'Om du publicerar en kommentar som inte går efter de regler som listas nedan, kommer den att raderas utan förvarning.',
						"1. Håll dig till samma ämne som blogginlägget.\n1. Följ de lagar som Sverige har. Inlägg som inte följer landets lagar, kommer att polisanmälas.\n1. Spamma inte flödet.\n1. Respektera andra. Vi är alla olika."
					],
					'delete' => [
						'## Du kommer snart kunna ta bort din kommentar',
						'Att skapa en statisk blogg från grunden är inte lätt. Det kräver mycket tid och smart tänkande. För tillfället kan du inte ta bort din kommentar, men du kommer att kunna göra det inom kort. När? Okänt. Vänligen ha tålamod :)',
						'Om du inte kan vänta, [kontakta mig]('.url('contact').') och ge mig lösenordet för din kommentar. Jag kommer då manuellt ta bort din kommentar.'
					],
					'form' => [
						'name' => 'Namn',
						'email' => 'E-postadress',
						'website' => 'Webbadress',
						'password' => 'Lösenord',
						'comment' => 'Kommentar',
						'cancel' => 'Avbryt',
						'delete' => 'Ta bort',
						'send' => 'Skicka',
						'save' => 'Spara',
						'descriptions' => [
							'password' => [
								'Om du vill kunna ändra eller ta bort din kommentar, spara då lösenordet ovan på ett säkert ställe. Lösenordet är baserat på den nuvarande tiden i mikrosekunder (baserat på när sidan lästes in).',
								'Ett tips är att kopiera lösenordet, publicera kommentaren och därefter spara lösenordet baserat på när kommentaren publicerades, och för vilket inlägg.'
							],
							'password-editing' => [
								'Ange det lösenord som du fick innan du publicerade din kommentar, i textfältet ovan.'
							]
						],
						'placeholders' => [
							'name' => [
								'Erik',
								'Jakob',
								'Henrik',
								'Anders',
								'Karl',
								'Mikael',
								'Jan',
								'Jenny',
								'Karin',
								'Lena',
								'Bengt',
								'Anna',
								'Karolina',
								'Katarina',
								'Jannika',
								'Elin',
								'Rebecka'
							],
							'email' => 'exempel@domän.tld',
							'email-leave-empty' => 'Lämna tom om adressen ska ej ändras',
							'website' => 'https://'
						]
					],
					'subtitles' => [
						'comments' => 'Kommentarer'
					]
				],
				'compose' => [
					'title' => [
						'new' => 'Skriv ett nytt blogginlägg',
						'editing' => 'Ändra ett befintligt inlägg'
					],
					'language' => [
						'swedish' => 'Svenska',
						'english' => 'Engelska'
					],
					'subject' => 'Rubrik',
					'content' => 'Innehåll',
					'cover' => 'Omslagsbild',
					'warning' => [
						'post' => 'Alternativet nedan ger dig ingen förvarning innan åtgärd.',
						'post-n-cover' => 'Alternativen nedan ger dig ingen förvarning innan åtgärd.'
					],
					'hundreddaystooffload' => [
						'day' => 'Dag',
						'of' => 'Av'
					],
					'options' => [
						'delete-cover' => 'Ta bort omslagsbilden',
						'delete-post' => 'Ta bort inlägget',
						'read' => 'Läs inlägget'
					],
					'buttons' => [
						'publish' => 'Publicera',
						'save-to-draft' => [
							'published' => 'Avpublicera och spara till utkast',
							'save' => 'Spara till utkast'
						],
						'cancel' => 'Avbryt'
					],
					'delete' => [
						'title' => 'Bekräfta borttagningen',
						'content' => [
							'cover' => [
								'Du är på väg att permanent ta bort omslagsbilden från servern.',
								'Vill du fortsätta?'
							],
							'post' => [
								'Du är på väg att permanent ta bort inlägget och kommer inte att kunna ångra dig, om du väljer att fortsätta. Eventuella bifogat material kommer dock inte att tas bort.',
								'Vill du fortsätta?'
							]
						],
						'options' => [
							'yes' => 'Ja',
							'no' => 'Nej'
						]
					]
				]
			],



			'gallery' => [
				'title' => 'Galleri',
				'rss' => 'Prenumerera via RSS',
				'upload-photo' => 'Ladda upp ett fotografi',
				'taken' => 'Togs den',
				'of' => 'Av',
				'form' => [
					'description' => [
						'se' => 'Svensk beskrivning',
						'en' => 'Engelsk beskrivning'
					],
					'placeholders' => [
						'required' => 'Obligatorisk'
					],
					'upload' => 'Ladda upp'
				],
				'notchecked' => [
					'Du måste bekräfta den externa datalagringen innan du kan gilla fotografiet.'
				],
				'paging' => [
					'previous' => 'Föregående',
					'next' => 'Nästa'
				],
				'options' => [
					'edit' => 'Ändra',
					'delete' => 'Ta bort'
				],
				'sensitive' => [
					'warning' => [
						'content' => [
							'Fotografiet innehåller sånt som vissa kan se som **stötande**, **skrämmande**, eller liknande.'
						],
						'hasdesc' => [
							'Vänligen läs beskrivningen nedan innan du väljer att fortsätta.'
						]
					],
					'options' => [
						'show-image' => 'Visa bilden',
						'hide-image' => 'Dölj bilden igen'
					],
					'gdpr' => [
						'Om du vill veta hur din data hanteras om du vill titta på fotografiet, gå då till [integritetspolicyn]('.url('privacy').') och läs punkt 3.4.'
					]
				],
				'likes' => [
					'like' => 'Gilla',
					'undo-like' => 'Ångra gillningen',
					'gdpr' => 'Om du vill veta hur din data hanteras när du gillar fotografiet, gå då till [integritetspolicyn]('.url('privacy').') och läs punkt 3.5.'
				],
				'edit' => [
					'title' => 'Ändra',
					'form' => [
						'taken' => 'Togs',
						'camera' => 'Kamera & modell',
						'iso' => 'ISO',
						'aperture' => 'Bländaröppning',
						'exposure' => 'Exponering',
						'button' => 'Spara'
					]
				],
				'delete' => [
					'title' => 'Ta bort',
					'content' => [
						'delete' => [
							'Du är på väg att permanent ta bort fotografiet. Om du väljer att fortsätta, kommer du inte kunna ångra dig och måste ladda upp fotografiet igen.',
							'Vill du fortsätta?'
						]
					],
					'options' => [
						'yes' => 'Ja',
						'no' => 'Nej'
					]
				]
			],



			'biking' => [
				'title' => 'Cykling',
				'nav' => [
					'rss' => 'Prenumerera via RSS'
				],
				'intro' => [
					'Cykling är en av mina största hobbies. Har alltid haft budgetcyklar från främst Biltema och rest hundratals mil med dom sen 2004. Men under april 2024 skaffade jag min första långfärdscykel (Scott Sub Sport 30).',
					'Listan nedanför denna text, listas de minst 1 mil långa resorna som jag har gjort med långfärdscykeln.'
				],
				'specifications' => [
					"- Växlar: Shimano Altud SL-M315\n- Vevställ: Shimano Altus FC-TY501 Black 48x38x26T\n- Kedja: KMC Z8\n- Kassett: Shimano CS-HG31 11-32T\n- Bromshandtag: Shimano BL-M200\n- Bromsar: Shimano BR-M200\n- Styrstång: \n- Sadelstolpe: \n- Sadel: "
				],
				'list' => [
					'when' => 'När',
					'distance' => 'Distans',
					'movingtime' => 'Rörelsetid',
					'hour' => 't',
					'minute' => 'm',
					'expected-traveltime' => 'Förväntad åktid',
					'destination' => 'Destination'
				],
				'details' => [
					'show-all-tracks' => 'Visa alla spår',
					'movingtime' => 'Rörelsetid',
					'distance' => 'Distans',
					'speed-top' => 'Topphastighet',
					'speed-average' => 'Genomsnittlig hastighet',
					'hour' => 't',
					'minute' => 'm',
					'elevation-max' => 'Högsta höjd',
					'elevation-min' => 'Lägsta höjd'
				],
				'statistics' => [
					'title' => 'Statistik',
					'number-of' => 'Antalet cykelturer',
					'duration-min' => 'Längst sträcka',
					'duration-max' => 'Kortast sträcka',
					'speed-min' => 'Högsta hastigheten',
					'speed-max' => 'Lägsta hastigheten',
					'time-min' => 'Längst tid',
					'time-max' => 'Kortast tid',
					'elevation-max' => 'Högsta höjd',
					'elevation-min' => 'Lägsta höjd'
				],
				'plans' => [
					'title' => 'Mina planerade resor',
					'content' => [
						'Nedan kan du se vad jag har för planerade cykelresor. Jag har fler reseplaner än dessa, så som att cykla till och från England. Men då dessa kräver båtresor, kommer de inte att listas nedan. Endast de resor som inte kräver båt listas.'
					]
				],
				'seasons' => [
					'winter' => 'Vintern',
					'spring' => 'Våren',
					'summer' => 'Sommaren',
					'autumn' => 'Hösten'
				],
				'destinations' => [
					'westcoast' => 'Västkusten',
					'summercabin' => 'Sommarstugan'
				]
			],



			'chat' => [
				'title' => 'Airikr Chat',
				'intro' => [
					'title' => 'Introduktion',
					'content' => [
						'Många chattjänster kränker ens integritet utan att man kan göra något åt det. [Telegram](https://telegram.org) är den enda chattjänst som låter en ha bättre kontroll över sin data (inställningar), samtidigt som man har noll kontroll alls (lagring).',
						'[Signal](https://signal.org/sv/) är ett bra alternativ för många, trots att den tjänsten kräver ditt mobilnummer. Tjänsten är dessutom baserad i USA och är beroende av Big Tech (se källa [1](https://reddit.airikr.me/r/degoogle/comments/ml24an/i_just_realized_that_signal_messaging_app_uses/) & [2](https://reddit.airikr.me/r/signal/comments/pq43v1/why_is_signal_using_google_stun_servers/)). Detta är ett **stort** nej om man vill använda en integritetsvänlig chattjänst.',
						'[SimpleX Chat](https://simplex.chat/) är det allra bästa alternativet, men den bokstavligen talat åt upp telefonens batteri när jag testade tjänsten senast. Det var dessutom (för mig) en omöjlighet att synka sitt konto via datorn.',
						'Till slut installerade jag [Snikket](https://snikket.org) på min egna server. Detta ger mig och de som har ett konto på Airikr Chat fördelar, bland annat (och den absolut viktigaste) att man äger sin egna data och ingen annan.',

						'### "Men WhatsApp då?"',
						'WhatsApp ägs av Meta och borde därmed vara förbjudet att användas inom EU, helst i hela världen. Man ska **aldrig** mata Big Tech med ens data, främst inte när de är såpass hungriga som vad Meta är!'
					]
				],
				'rules' => [
					'title' => 'Regler',
					'content' => [
						"1. All kommunikation måste ske med helvägskrypteringsmetoden [OMEMO](https://conversations.im/omemo/).\n1. Följ de lagar som finns i Sverige.\n1. Barnpornografi, rasism, nazism och liknande är strikt förbjudet.\n1. Håll en god och respektabel ton.\n1. Respektera andra. Vi är alla olika."
					]
				],
				'guide' => [
					'title' => 'Kom igång',
					'content' => [
						"1. Kontakta mig. Om jag vet vem du är, så får du en inbjudningslänk av mig.\n1. Öppna inbjudningslänken.\n1. Om du har Snikket-appen installerad, välj \"Öppna appen\". Välj annars \"registrera ett konto manuellt\" under Alternativ.\n1. Ange ditt användarnamn och lösenord och välj sen knappen för att skapa ditt konto.\n1. Logga in på ditt nya konto.\n1. Väl inloggad, välj att starta en ny chatt och välj till exempel mig (Airikr) i listan över användare."
					]
				],
				'faq' => [
					'title' => 'Frågor och svar',
					'content' => [
						'### 1. Vad för klienter kan jag använda?',
						'Nedan har jag listat några XMPP-klienter som är kompatibla med min chattserver, [Snikket](https://snikket.org). Det finns såklart många fler.',

						"- **Android:** monocles chat ([Play Butik](https://play.google.com/store/apps/details?id=eu.monocles.chat) (kostar) / [F-Droid](https://f-droid.org/packages/de.monocles.chat/) (gratis)), Snikket ([Play Butik](https://play.google.com/store/apps/details?id=org.snikket.android&amp;hl=sv&amp;gl=US) / [F-Droid](https://f-droid.org/en/packages/org.snikket.android/)), Conversations ([Play Butik](https://play.google.com/store/apps/details?id=eu.siacs.conversations) (kostar) / [F-Droid](https://f-droid.org/packages/eu.siacs.conversations/) (gratis))\n- **iOS:** [Snikket](https://apps.apple.com/us/app/snikket/id1545164189), [Monal](https://monal-im.org/), [Siskin IM](https://apps.apple.com/de/app/siskin-im/id1153516838?platform=iphone)\n- **macOS:** [Monal](https://monal-im.org/), [Beagle IM](https://beagle.im/)\n- **Windows, macOS & Linux:** [Gajim](https://gajim.org), [Swift](https://swift.im/)\n- **Web:** [monocles chat](https://monocles.chat/login)",
						'Tänk på att du måste manuellt aktivera OMEMO om du använder någon av följande klienter: Gajim, Beagle IM, Siskin IM.',

						'### 2. Varför skulle jag använda Airikr Chat?',
						"- Inget stort eller litet företag äger din data - du gör det.\n- Du har flera olika klienter att välja mellan. Om du inte är nöjd med en klient, testa en annan.\n- Ingen annan än du och mottagaren kan se det ni skriver till varann med OMEMO.\n- Du kan när som helst exportera din data via kontrollpanelen på [chat.airikr.me](https://chat.airikr.me).\n- All data som du har skickat och som lagras på servern, raderas permanent efter 7 dagar.",

						'### 3. Försöker du konkurrera med större chattjänster?',
						'Nej, och det skulle jag aldrig vilja göra heller. Jag är en enkel privatperson som gillar att äga min egna data och vill låta andra att få samma möjlighet - ingenting annat.',

						'### 4. Var ligger min data lagrad?',
						'All data lagras på en server hos [Obehosting](https://obe.net/foretag/data-center/) via [HostUp](https://hostup.se) i Stockholm.',

						'### 5. Hur är min data lagrad?',
						'Alla meddelanden och filer som skickas med OMEMO, lagras med krypteringsmetoden AES256-GCM på servern i 7 dagar, innan de permanent raderas. Ingen annan än du och mottagaren kan läsa det ni skickar till varann.',
						'Om man däremot inte använder OMEMO, kommer det man skickar att lagras krypterade på servern som den sen kan läsa.',

						'### 6. Hur kan jag rapportera in en person som bryter mot reglerna?',
						'Ta en skärmdump på meddelandet med användarnamnet på den som bryter reglerna synligt, och skicka sen skärmdumpen till mig.',

						'### 7. Vad är skillnaden mellan Airikr Chat och andra chattjänster, som till exempel Telegram?',
						'De största skillnaderna är att [Snikket](https://snikket.org) (som Airikr Chat använder) är inom fediversumet, och att du har friheten till att prata om allt och ingenting, så länge du inte bryter mot någon av [reglerna]('.url('chat/rules').').',

						'#### Fediversumet?',
						'Fediversumet låter dig att vara på en server (till exempel chat.airikr.me) och kommunicera med andra personer på en annan server (till exempel snikket.linuxkompis.se). Med andra ord kvittar det vilken server du har ditt konto på.',

						'#### Prata fritt?',
						'Till skillnad från chattjänster som hanteras av företag, till exempel Meta och Discord, drivs Airikr Chat av en enda person: [Airikr]('.url('').'). Om man håller sig till [reglernas]('.url('chat/rules').') ramar, så får man prata om precis vad som helst.',

						'### 8. Men jag har ju inget att dölja?',
						'Att hävda att man inte har något att dölja, betyder inte att man måste vara aktsam med vilken chattjänst man väljer.',
						'Big Tech (Microsoft, Google, Discord, Meta, Spotify, med flera) älskar att samla in så mycket som det bara går om deras användare. Ju mer information, desto bättre, för då tjänar de mer pengar. Bland annat TV4 arbetar på samma sätt också.',
						'Sådana företag är integritetsfientliga. Tyvärr finns det de som inte vet om att de blir förföljda och att deras liv blir bokstavligen talat sålda till bland annat reklamföretag. Antingen det eller så bryr de sig helt enkelt inte. Människor är väldigt lata, främst nu när våra liv blir alltmer digitala.',
						'Argumentet "jag har inget att dölja" håller alltså inte, för Big Tech och andra företag får veta precis allt om dig, främst när man matar dom med ens liv via deras chattjänster.',
						'Därför är det extra viktigt att välja en chattjänst som inte är integritetsfientlig. Det är här [Snikket](https://snikket.org) kommer in i bilden, och det är därför [jag]('.url('').') har skapat Airikr Chat. Inte för att konkurrera med andra chattjänster, utan för att hålla mitt liv mer privat och mindre förföljt.'
					]
				]
			],



			'contact' => [
				'title' => 'Kontakta mig',
				'content' => [
					'Har du några frågor om något specifikt om webbsidan, eller undrar du över något rent generellt? Du kan kontakta mig via antingen e-post eller via några av tjänsterna listade nedan.',
					'Min e-postadress är **hi atty airikr dotty me**. Skicka utan kryptering då jag inte använder PGP/GPG. Om du vill kommunicera mig krypterat, välj XMPP.'
				]
			],



			'me' => [
				'metadata' => [
					'title' => 'Jag'
				],
				'languages' => [
					'title' => 'Språk',
					'list' => [
						'swedish' => 'Svenska',
						'english' => 'Engelska',
						'japanese' => 'Japanska'
					]
				],
				'pronounses' => [
					'he' => 'Han',
					'his' => 'Hans',
					'him' => 'Honom'
				],
				'xmpp' => [
					'my-devices' => 'Mina enheter',
					'devices' => [
						'Nedan kan du se de enheter som jag använder. **Kontrollera dessa noga!** Om du kommunicerar med en vars enhet inte listas nedan, så är det inte mig du kommunicerar med.'
					]
				],
				'location' => 'Hammarö, Värmland, Sverige',
				'skills' => [
					'title' => 'Kunskaper',
					'list' => [
						'php' => 'PHP',
						'jquery' => 'jQuery',
						'html' => 'HTML',
						'css' => 'CSS',
						'lightroom' => 'Lightroom Classic',
						'photography' => 'Fotografering',
						'git' => 'Git',
						'javascript' => 'JavaScript',
						'genealogy' => 'Släktforskning',
						'bicycling' => 'Cykling',
						'cooking' => 'Matlagning',
						'painting' => 'Målning'
					],
					'languages' => 'Språk',
					'hardwares' => 'Hårdvaror',
					'softwares' => 'Mjukvaror',
					'programming' => 'Programmering',
					'other' => 'Annat'
				],
				'qr' => [
					'title' => 'Dela denna sida'
				]
			],



			'links' => [
				'title' => 'Länkar',
				'content' => [
					'Listan visar alla mina publicerade projekt. Den visar även de personer som jag finner intressanta.'
				],
				'notchecked' => [
					$site_title.' kan inte avgöra om du är en människa eller inte. Därför har du inte tillgång till de länkar som annars ska listas här. Godkänn den externa datalagringen om du vill se länkarna.'
				],
				'subtitles' => [
					'projects' => 'Mina projekt',
					'people' => 'Intressanta personer'
				]
			],



			'uses' => [
				'title' => 'Använder',
				'pc' => [
					'desktop' => 'Stationär dator',
					'laptop' => 'Bärbar dator',
					'psu' => 'Nätaggregat',
					'motherboard' => 'Moderkort',
					'cpu' => 'Processor',
					'gpu' => 'Grafikkort',
					'ram' => 'Arbetsminne',
					'storage' => 'Lagring',
					'screen' => 'Skärm',
					'screens' => 'Skärmar',
					'mouse' => 'Datormus',
					'keyboard' => 'Tangentbord',
					'mic' => 'Mikrofon',
					'headset' => 'Headset',
					'manufacturer' => 'Tillverkare',
					'model' => 'Modell',
					'os' => 'Operativsystem'
				],
				'softwares' => [
					'pc' => 'Mjukvaror (PC)',
					'phone' => 'Mjukvaror (Android)',
					'software' => 'Mjukvara',
					'softwares' => 'Mjukvaror',
					'selfhosting' => 'Självhosting'
				],
				'screenshotinfos' => [
					'desktop' => 'Bakgrundsbilderna är tagna av mig.',
					'laptop' => 'Bakgrundsbilden är tagen av mig.'
				]
			],



			'guestbook' => [
				'title' => 'Gästbok',
				'content' => [
					'Säg gärna hej :)',
					'Alla inlägg granskas av mig först, innan de publiceras.',
					'Tänk bara på att du inte kan ändra eller ta bort ditt inlägg efter att det har blivit publicerat. Var därför noga med vad du skriver.',
					'IP-adressen kommer inte att lagras anonymiserad. Den lagras tillsammans med ditt inlägg. Läs punkt 3.3 på [integritets-sidan]('.url('privacy').') för mer information.'
				],
				'terms' => [
					'## Villkor',
					'Om du publicerar ett inlägg som inte går efter de villkor som listas nedan, kommer det att raderas utan förvarning.',
					"1. Följ de lagar som Sverige har. Inlägg som inte följer landets lagar, kommer att polisanmälas.\n1. Spamma inte flödet.\n1. Respektera andra. Vi är alla olika.\n1. Tillför något i ditt inlägg."
				],
				'notchecked' => [
					'## Måste acceptera extern datalagring först',
					'Jag förstår om du inte vill godkänna att IP-adressen för den enhet du använder lagras i en annan server som jag inte hanterar, men det är för att göra '.$site_title.' till en mer mänsklig plats.',
					'Vänligen godkänn den externa datalagringen innan du kan signerar gästboken.'
				],
				'badvisitor' => [
					'## Omänsklig form detekterad',
					'Du har klassats som en robot eller liknande av [IP Intelligence](https://getipintel.net/). Du får därför inte signera i min gästbok.',
					'Om du är en människa och att IP Intelligence har gjort fel, [kontakta mig]('.url('contact').').'
				],
				'nav' => [
					'rss' => 'Prenumerera via RSS'
				],
				'sign' => 'Signera gästboken',
				'form' => [
					'name' => 'Ditt namn',
					'website' => 'Din webbsida',
					'message' => 'Ditt meddelande',
					'button' => 'Skicka',
					'cancel' => 'Avbryt'
				],
				'posts' => [
					'name' => 'Namn',
					'delete' => 'Ta bort'
				]
			],



			'privacy' => [
				'title' => 'Integritet',
				'intro' => [
					'## Förord',
					$site_domain.' använder inte någon JavaScript, men detta kommer högst troligt att ändras i framtiden. Men i och med att JavaScript inte används, kommer din IP-adress att skickas i klartext till servern.',
					'**IP-adressen skickas inte till min server förrän du interagerar med någon funktion**, till exempel reagerar på ett blogginlägg.',
					'Jag har ingen avsikt att veta din IP-adress och jag bryr mig helt ärligt inte informationen. Den enda anledningen till att IP-adresser lagras, är att enklare kunna hantera de funktioner som ni besökare kan använda, så som att reagera på ett blogginlägg.',
					'Om ni vill läsa mer om hur er persondata lagras, läs vidare.',


					'## 1. IP Intelligence',
					'[IP Intelligence](https://getipintel.net/) (förkortat IPIntel) används för att göra '.$site_title.' till en bättre plats för både mig och för ni som besöker webbsidan. IPIntel lagrar följande om dig för att göra deras tjänst bättre:',
					"- Enhetens IP-adress (kan knytas till dig)\n- Tidsstämpeln när anropet gjordes\n- Svarskoden som gavs vid anropet",
					'IPIntel får inte reda på din IP-adress förrän du har godkänt den externa datalagringen. Om du godkänner den, kommer din data att skickas till deras server, samtidigt som en JSON-fil lagras på den server som '.$site_title.' ligger på. Filnamnet är enhetens IP-adress i hashad form och filen är giltig i 6 timmar.',


					'## 2. Vad webbsidan använder för analytik',
					$site_title.' använder sig av [Plausible](https://plausible.io) för att samla in anonymiserad information om besökarna. IP-adresser samlas inte in av Plausible och är därmed GDPR-vänlig.',
					'Även om Plausible redan har samlat in information om ditt besök, så kan man välja att inte tillåta tjänsten att samla information om dig vid framtida besök. För att göra detta, inaktiverar du JavaScript för '.$site_title.'.',


					'## 3. Lagring av personuppgifter',
					'När en besökare interagerar med funktioner på '.$site_title.', kommer IP-adressen för den enhet man använder att lagras på servern som webbsidan ligger på. Vad dessa funktioner är och hur IP-adressen lagras, beskrivs nedan.',
					'Lagringen av enhetens IP-adress, vare sig den anonymiseras eller inte, är nödvändigt för att webbsidan ska kunna identifiera besökare.',
					'**Exempel när en IP-adress anonymiseras:** 192.168.123.500 blir 192.168.123.0.',

					'### 3.1. Reaktioner för blogginlägg',
					'När en besökare väljer att reagera på ett blogginlägg, kommer en tom fil att lagras på servern. Filnamnet innehåller inläggets publiceringsdatum och tid, när reaktionen ägde rum, hashad och anonymiserad IP-adress, samt vad för reaktion besökaren valde.',
					'När samma besökare ångrar sin reaktion, kommer filen att raderas från servern.',

					/*'### 3.2. Kommentarer för blogginlägg',
					'När man väljer att publicera en kommentar, kommer e-postadressen och IP-adressen för den enhet man använder att lagras krypterade. All annan information lagras i klartext då de kommer att visas offentligt.',
					'IP-adressen kommer inte att anonymiseras på grund av säkerhetsskäl.',*/

					'### 3.3. Gästboksinlägg',
					'När man väljer att publicera ett gästboksinlägg, kommer IP-adressen för den enhet man använder att lagras krypterad. All annan information lagras i klartext då de kommer att visas offentligt.',
					'IP-adressen kommer inte att anonymiseras på grund av säkerhetsskäl.',

					'### 3.4. Visning av känsliga bilder i galleriet',
					'Om ett fotografi är markerad som känslig, kommer information om detta att visas. Man kan få en hint till varför den är markerad som känslig i beskrivningen för fotografiet.',
					'När man väljer att visa fotografiet trots varningen, kommer IP-adressen av den enhet man använder att anonymiseras och hashas med MD5, och lagras i filnamnet på den fil som skapas. En tidsstämpel om när man valde att visa fotografiet, kommer att stå i den JSON-fil som skapas.',
					'Webbsidan går efter när man valde att visa fotografiet, och tar automatiskt bort filen från servern efter 5 minuter. Varningen kommer då att visas igen. Man kan manuellt ta bort filen genom att välja "Dölj bilden igen" under fotografiet.',

					'### 3.5. Gilla ett fotografi i galleriet',
					'När du gillar ett fotografi i galleriet, kommer IP-adressen av den enhet man använder att anonymiseras och hashas med MD5, tillsammans med bildens filnamn. En tidsstämpel om när man valde att visa fotografiet, kommer att stå i den JSON-fil som skapas.',

					'### 3.6. Visning av känsliga blogginlägg',
					'Om ett blogginlägg är markerat som känsligt, kommer informationen om detta att visas. Man får då två val: att acceptera och läsa inlägget ändå, eller att gå till ett slumpmässigt inlägg.',
					'När man väljer att läsa blogginlägget trots varningen, kommer IP-adressen av den enhet man använder att anonymiseras och hashas med MD5, och lagras i filnamnet på den fil som skapas. En tidsstämpel om när man valde att läsa inlägget, kommer att stå i den JSON-fil som skapas.',
					'Webbsidan går efter när man valde att läsa inlägget, och tar automatiskt bort filen från servern efter 10 minuter. Varningen kommer då att visas igen. Man kan manuellt ta bort filen genom att välja "Dölj inlägget igen" över inlägget.',

					'### 3.7. Lässtatus för blogginlägg',
					'När man anger sin lässtatus för ett blogginlägg som läst, kommer en tom fil att lagras på servern. Filnamnet kommer innehålla inläggets publiceringsdatum och besökarens anonymiserade IP-adress. När samma besökare väljer att markera inlägget som oläst, kommer filen att raderas från servern.',


					'## 4. Kontakt',
					'Om ni har frågor, klagomål, eller något annat att säga, kontakta mig genom någon av de tjänster som listas på "[Kontakta]('.url('contact').')"-sidan.'
				]
			],



			'copyright' => [
				'title' => 'Kreativ allmänning',
				'content' => [
					'All material som används på '.$site_title.' och dess underdomäner (om inget annat har nämnts), är skapat av ägaren, [Erik Edgren]('.url('').'). Materialen går under [CC-BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.sv).'
				],
				'creativecommons' => [
					'by' => [
						'**BY** - Du måste berätta att Erik Edgren är ägaren och skaparen till materialet. Du måste även länka till denna sida om du har delat materialet.'
					],
					'nc' => [
						'**NC** - Du får inte använda materialet för kommersiella ändamål.'
					],
					'nd' => [
						'**ND** - Du får inte remixa, omvandla, eller bygga vidare på materialet och därefter distribuera materialet.'
					]
				]
			],



			'uploads' => [
				'title' => 'Uppladdade filer',
				'subtitles' => [
					'photos' => 'Bilder',
					'covers' => 'Omslag',
					'tracks' => 'GPX-spår',
					'videos' => 'Videoklipp'
				],
				'upload' => 'Ladda upp en ny fil',
				'upload-file' => 'Ladda upp en fil',
				'list' => [
					'filename' => 'Filnamn',
					'filesize' => 'Filstorlek'
				],
				'file' => 'Fil',
				'files' => 'Filer'
			],



			'rss' => [
				'title' => 'RSS-flöden',
				'content' => [
					'Istället för nyhetsbrev via e-post, kan du följa mig via RSS. Det finns en hel del klienter som du kan använda, bland annat [Miniflux](https://miniflux.app/) och [Feeder](https://github.com/spacecowboy/Feeder).'
				],
				'list' => [
					[
						'name' => 'Blogginlägg',
						'url' => url('rss/blog')
					],
					[
						'name' => 'Kommentarer för blogginlägg',
						'url' => url('rss/comments')
					],
					[
						'name' => 'Galleriet',
						'url' => url('rss/gallery')
					],
					[
						'name' => 'Cykelresor',
						'url' => url('rss/biking')
					],
					[
						'name' => 'Gästboksinlägg',
						'url' => url('rss/guestbook')
					]
				]
			],



			'sitemap' => [
				'title' => 'Sitemap',
				'content' => [
					'Nedan listas alla sidor och undersidor (inklusive de som är dolda) så att du ska enkelt kunna navigera på '.$site_title.'.',
					'Sådana här sidor är egentligen till för och låta sökmotorer (främst Google) att enkelt hitta de viktigaste sidorna på en webbsida, och sen indexera dom. Men det enda syftet med denna sitemap, är att låta besökarna hitta sidor på '.$site_domain.' så snabbt som det bara går.'
				],
				'uses' => 'Använder',
				'metadata' => 'Metadata'
			],



			'upload' => [
				'title' => 'Ladda upp en fil',
				'choose-image' => 'Välj en fil',
				'button' => 'Ladda upp'
			],



			'uploaded' => [
				'title' => [
					'photo' => 'Information om en bild',
					'cover' => 'Information om ett omslag',
					'track' => 'Information om ett spår',
					'video' => 'Information om en video'
				],
				'filename' => 'Filnamn',
				'filesize' => 'Filstorlek',
				'filesize_mp4' => 'Filstorlek (MP4)',
				'filesize_ogg' => 'Filstorlek (OGG)',
				'filesize_webm' => 'Filstorlek (WebM)',
				'filesize_total' => 'Total filstorlek',
				'size' => 'Storlek',
				'size_thumb' => 'Storlek (inlägg)',
				'size_thumblist' => 'Storlek (lista)',
				'tag' => 'Tagg',
				'options' => [
					'show' => 'Visa',
					'update' => 'Uppdatera',
					'delete' => 'Ta bort'
				],
				'delete' => [
					'title' => 'Bekräfta borttagningen',
					'content' => [
						'Du är på väg att permanent ta bort filen från servern. Om du har inkluderat materialet i något blogginlägg, måste du gå till dessa inlägget och manuellt ta bort taggen för materialet.',
						'Om du väljer att fortsätta, kommer du inte kunna ångra ditt val.'
					],
					'options' => [
						'yes' => 'Ja',
						'no' => 'Nej'
					]
				]
			],



			'telemetry' => [
				'title' => 'Telemetrisimulator',
				'subtitles' => [
					'without-javascript' => 'Utan JavaScript',
					'with-javascript' => 'Med JavaScript',
					'browser-properties' => 'Webbläsarens egenskaper'
				],
				'content' => [
					'disabled' => [
						'Syftet med telemetrisimuleringen, är att visa dig hur mycket data Big Tech-företag (Google, Microsoft, Meta, med flera) lagrar om dig. Data som kan länkas direkt till dig personligen och därmed kunna skapa en personlig databas om dig åt dom själva och även för tredjepart som de säljer datan till. Med andra ord, de tjänar pengar på ditt liv.',
						'Simulatorn är begränsad till att endast fungera på '.$site_domain.'.',
						'Vänligen notera att det som lagras om dig i simulatorn, är bara en bråkdel av vad till exempel Facebook lagrar om dig. Simulatorn skapades endast för att ge dig ett humm om hur mycket som lagras hos Big Tech.',

						'## Hur lagras informationen?',
						'När simuleringen startas, kommer en katalog att skapas på servern. Namnet på katalogen är IP-adressen för den enhet du besöker '.$site_domain.' med. Adressen är såklart hashad med SHA-256. Katalogen innehåller JSON-filer som huserar allt som listas under "Vad lagras".',
						'Ingen annan än du och jag (<a href="'.url('').'">Erik</a>) kan komma åt filerna, men jag bryr mig inte om din data. Din data är och förblir endast din (som det ska vara). Du har även full kontroll över hur länge katalogen med filerna ska ligga lagrad på servern (men max 10 minuter efter inaktivitet).',
						'Och eftersom det här är en simulering, kommer allt att lagras i klartext på servern, precis som hos Big Tech.',

						'## Kommer allt om mig att raderas automatiskt?',
						'Ja. När du har varit inaktiv i 10 minuter, kommer alla dina filer att permanent raderas. Du kan också själv välja att ta bort filerna genom att välja "Stäng av" i sidfoten.',
					],
					'whatlogs' => [
						'title' => 'Vad lagras?',
						'without-js' => [
							'IP-adress',
							'Användaragent',
							'Simuleringen påbörjades',
							'Sida',
							'Datum och klockslag'
						],
						'with-js' => [
							'stuff' => [
								'Skärm',
								'Skärmupplösning',
								'Färgdjup',
								'Tidszon',
								'Tidszonsförskjutning',
								'Inmatningar',
								#'Musmarkörens position och vilken knapp som användes',
								'Fingeravtryck',
								'Webbläsare',
								'Operativsystem',
								'Processorarkitektur',
								'Systemets språk'
							],
							'browser' => [
								'Förvalt språk',
								'Om kakor tillåts',
								'Om Do Not Track är aktiverat eller inte',
								'Om Geolocation och/eller WebSocket stöds eller inte',
								'Om localStorage och/eller lagring av sessioner stöds eller inte',
								'Om Java, Flash, och/eller Silverlight är installerat eller inte'
							]
						]
					],
					'enabled' => [
						'**Simuleringen har startats!**',
						'Surfa runt här på '.$site_domain.' i några minuter och gå sen till "Din data"-sidan (du hittar länken i sidfoten). All data som har sparats om dig och ditt besök, finns på den sidan.'
					]
				],
				'start' => 'Starta simuleringen'
			],



			'your-data' => [
				'title' => 'Din data',
				'content' => [
					'Nedan kan du hitta allt som telemetrisimuleringen har loggat om dig och vad du har gjort på '.$site_domain.'. Listan är långt ifrån komplett jämfört med det som Big Tech hämtar om dig. Detta är på grund av att simuleringen endast ska logga det som inte kräver din uppmärksamhet.',
					'Du kan stänga av simuleringen och radera all loggning om dig och ditt besök, genom att välja "Stäng av simuleringen" i sidfoten.'
				],
				'lists' => [
					'keycodes' => [
						'when' => 'Registrerades',
						'key' => 'Knapp',
						'keycode' => 'Kod',
						'page' => 'Sida'
					]
				],
				'unknown' => 'Okänd'
			],



			'admin' => [
				'titles' => [
					'login' => 'Logga in',
					'create' => 'Skapa ett adminkonto'
				],
				'form' => [
					'username' => 'Användarnamn',
					'password' => 'Lösenord',
					'tfa' => 'Tvåstegsverifieringskod',
					'tfa-create' => 'Tvåstegsverifiering'
				],
				'tfa-options' => 'Skapa adminkontot [med 2FA]('.url('admin/create-with-tfa').') eller [utan]('.url('admin/create').').',
				'placeholders' => [
					'tfa' => 'Fyll endast i du har aktiverat 2FA'
				],
				'buttons' => [
					'create' => 'Skapa',
					'login' => 'Logga in'
				]
			],



			'information' => [
				'titles' => [
					'colophone' => 'Kolofon',
					'ai' => 'AI'
				],
				'contents' => [
					'colophone' => [
						$site_title.' är ett egetbygge av ägaren, [Erik Edgren]('.url('').'). Den källkod som lade grund för webbsidan, kan ses på [git.airikr.me](https://git.airikr.me/airikr/template). Källkoden för '.$site_title.' kan ses på [codeberg.org](https://codeberg.org/airikr/airikr.me). Denna kommer alltid att vara uppdaterad.',
						'Webbsidan är en så kallad statisk webbsida. Detta betyder att den använder JSON-filer som agerar som databas, istället för till exempel SQLite.',

						'## JavaScript',
						'I skrivande stund använder '.$site_title.' ingen JavaScript. Allt är byggt med hjälp av HTML, CSS och PHP. Dock finns det planer på att lansera en funktion som kräver JavaScript.',

						'## Funktioner',
						'Nedan kan du se vilka funktioner används på '.$site_title.', och vad de används till.',
						"- ".implode("\n- ", $arr_functions),
						$site_title.' använder även [IP Intelligence](https://getipintel.net/) för att hålla främst robotar borta från främst de kommunikationsfunktioner som finns på webbsidan, bland annat gästboken.',

						'## Teckensnitt',
						$site_title.' använder bara ett teckensnitt: [Iosevka](https://github.com/be5invis/Iosevka).',

						'## Ikoner',
						'De ikoner som kan ses lite överallt på '.$site_title.', är från [Tabler Icons](https://github.com/tabler/tabler-icons).'
					],
					'ai' => [
						'AI har inte använts, och kommer aldrig att användas, till att komponera blogginlägg och beskrivningar till uppladdade fotografier till galleriet. AI har inte heller skrivit källkoden till '.$site_title.'.',
						'AI ska (enligt [mig]('.url('').')) endast användas till viktigare saker, som till exempel att förutse väderkatastrofer under ett tidigare skede än tidigare. AI missbrukas alldeles för mycket.'
					]
				]
			]
		]
	];

?>
