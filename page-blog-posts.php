<?php

	# Lägg till kategorier



	if(isset($_POST['search'])) {

		require_once 'site-settings.php';

		$post_keyword = safetag($_POST['field-keyword']);
		$post_year = (!isset($_POST['list-years']) ? null : safetag($_POST['list-years']));
		$post_month = (!isset($_POST['list-months']) ? null : safetag($_POST['list-months']));

		if(!empty($post_keyword) AND strlen($post_keyword) < 3) {
			die(simplepage($Parsedown->text($lang['messages']['errors']['keyword-tooshort'])));

		} elseif(!empty($post_keyword) AND !preg_match('([a-z0-9åäö]+)', $post_keyword)) {
			die(simplepage($Parsedown->text($lang['messages']['errors']['keyword-onlychars'])));

		} else {
			$year = (empty($post_year) ? null : $post_year);
			$month = (empty($post_month) ? null : $post_month);

			if(empty($post_keyword) AND empty($year) AND empty($month)) {
				header("Location: ".url('blog'));
				exit;

			} else {
				header("Location: ".url('blog/search:'.(empty($post_keyword) ? 'none' : mb_strtolower($post_keyword)) . ((!empty($post_year) OR !empty($post_month)) ? '/from:'.$year . $month : '')));
				exit;
			}
		}



	} else {

		require_once 'site-header.php';



		$get_show = (isset($_GET['sh']) ? safetag($_GET['sh']) : null);
		$dir_empty = !(new \FilesystemIterator($dir_files.'/posts/published'))->valid();
		$view_comments = false;
		$view_reactions = false;

		if(!empty($get_show) AND $get_show == 'comments') {
			$view_comments = true;
			$comments = glob($dir_files.'/posts/comments/*.json');
			$arr_comments = [];

			foreach($comments AS $comment) {
				$content = json_decode(file_get_contents($comment), false);
				$file_info = pathinfo($comment);
				list($post, $id) = explode('-', $file_info['filename']);
				$arr_comments[] = [
					'id' => $id,
					'post' => $post,
					'published' => $content->published
				];
			}

			usort($arr_comments, function($a, $b) {
				$ad = new DateTime(date('Y-m-d H:i:s', $a['published']));
				$bd = new DateTime(date('Y-m-d H:i:s', $b['published']));

				if($ad == $bd) {
					return 0;
				}

				return $ad > $bd ? -1 : 1;
			});


		} elseif(!empty($get_show) AND $get_show == 'reactions') {
			$view_reactions = true;
			$reactions = glob($dir_files.'/posts/reactions/*');
			$arr_reactions = [];

			foreach($reactions AS $reaction) {
				$content = json_decode(file_get_contents($reaction), false);
				$file_info = pathinfo($reaction);
				list($post, $timestamp, $ip, $emoji) = explode('-', $file_info['filename']);
				$arr_reactions[] = [
					'post' => $post,
					'emoji' => $emoji,
					'reacted' => $timestamp,
					'ip' => $ip
				];
			}

			usort($arr_reactions, function($a, $b) {
				$ad = new DateTime(date('Y-m-d H:i:s', $a['reacted']));
				$bd = new DateTime(date('Y-m-d H:i:s', $b['reacted']));

				if($ad == $bd) {
					return 0;
				}

				return $ad > $bd ? -1 : 1;
			});


		} else {
			$get_keyword = (!isset($_GET['kw']) ? null : safetag($_GET['kw']));
			$get_year = (!isset($_GET['ye']) ? null : safetag($_GET['ye']));
			$get_month = (!isset($_GET['mo']) ? null : safetag($_GET['mo']));
			$get_date = (!isset($_GET['dt']) ? null : safetag($_GET['dt']));

			$is_filtering = ((!empty($get_year) OR !empty($get_month) OR !empty($get_date) OR !empty($get_keyword) OR !empty($get_show)) ? true : false);

			$posts = glob($dir_files.'/posts/published/*.json');
			natsort($posts);
		}







		echo '<section id="blog-posts">';
			if($dir_empty) {
				echo '<div class="message">';
					echo '<div>'.$lang['messages']['no-posts'].'</div>';

					if($is_loggedin == true) {
						echo '<div class="link"><a href="'.url('blog/compose').'">'.$lang['pages']['blog']['write-empty'].'</a></div>';
					}
				echo '</div>';



			} else {
				echo '<nav>';
					echo '<div class="rss side-by-side">';
						echo '<a href="'.url('rss'.($view_comments == false ? '' : '/comments').'?lang='.$get_lang, true).'">';
							echo svgicon('rss') . $lang['pages']['blog']['nav']['rss'];
						echo '</a>';
					echo '</div>';

					if($is_loggedin == true) {
						echo '<div class="compose">';
							echo '<a href="'.url('blog/compose').'" class="side-by-side">';
								echo svgicon('write') . $lang['pages']['blog']['nav']['write-notempty'];
							echo '</a>';
						echo '</div>';
					}

					echo '<div class="filter'.($is_loggedin == false ? ' notloggedin' : '').'">';
						echo svgicon('filter');

						echo '<div>';
							echo $lang['pages']['blog']['subtitles']['filter']['label'].': ';

							echo '<a href="'.url('blog').'">';
								echo mb_strtolower($lang['pages']['blog']['subtitles']['filter']['posts']);
							echo '</a>';

							/*echo '<a href="'.url('blog/comments').'">';
								echo mb_strtolower($lang['pages']['blog']['subtitles']['filter']['comments']);
							echo '</a>';*/

							echo '<a href="'.url('blog/reactions').'">';
								echo mb_strtolower($lang['pages']['blog']['subtitles']['filter']['reactions']);
							echo '</a>';

							echo '<a href="'.url('blog/100daystooffload').'">';
								echo $lang['pages']['blog']['subtitles']['filter']['hundreddaystooffload'];
							echo '</a>';
						echo '</div>';
					echo '</div>';
				echo '</nav>';





				if($view_comments == true) {
					echo '<div class="comments">';
						echo '<h2>Kommentarer</h2>';

						$multiavatar = new Multiavatar();
						foreach($arr_comments AS $comment) {
							$file = $dir_files.'/posts/comments/'.$comment['post'].'-'.$comment['id'].'.json';
							$fileinfo = pathinfo($file);
							$comment = json_decode(file_get_contents($file), false);
							$avatar = $multiavatar($comment->about->name . $comment->about->website . (empty($comment->about->website) ? null : $comment->about->website), null, null);

							if(substr_count($fileinfo['filename'], '-') == 2) {
								list($dt, $ts, $ip) = explode('-', $fileinfo['filename']);
							} else {
								list($dt, $ts) = explode('-', $fileinfo['filename']);
							}

							$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$dt.'.json'), false);
							$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);

							echo '<div class="comment">';
								echo '<div class="avatar">';
									echo $avatar;
								echo '</div>';

								echo '<div class="content">';
									echo '<div class="info">';
										echo '<div class="name">';
											echo $comment->about->name;
										echo '</div>';

										echo '<div class="published">';
											echo '<a href="'.url('blog/'.$dt.'#comment-'.$comment->id).'" title="'.$lang['tooltips']['goto-comment'].'">';
												echo date_($comment->published, 'datetime');
											echo '</a>';
										echo '</div>';
									echo '</div>';

									echo '<div class="post">';
										echo '<a href="'.url('blog/'.$dt.'/'.mb_strtolower(seo(trim($subject)))).'" title="'.$lang['tooltips']['read-post'].'">';
											echo $subject;
										echo '</a>';
									echo '</div>';

									echo '<div class="content">';
										echo $Parsedown->text($comment->comment);
									echo '</div>';
								echo '</div>';
							echo '</div>';
						}
					echo '</div>';



				} elseif($view_reactions == true) {
					echo '<div class="reactions">';
						echo '<h2>'.$lang['pages']['blog']['list']['reactions']['title'].'</h2>';

						echo '<div class="info">';
							echo svgicon('info');
							echo $Parsedown->text($lang['pages']['blog']['list']['reactions']['info']);
						echo '</div>';


						$c_reactions = 0;
						foreach($arr_reactions AS $reaction) {
							$c_reactions++;
							$file = $dir_files.'/posts/reactions/'.$reaction['post'].'-'.$reaction['reacted'].'-'.$reaction['ip'].'-'.$reaction['emoji'];
							$fileinfo = pathinfo($file);

							list($dt, $ts, $ip) = explode('-', $fileinfo['filename']);

							$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$dt.'.json'), false);
							$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);

							if($c_reactions <= 100) {
								echo '<div class="reaction">';
									echo '<div class="emoji-n-when">';
										echo '<div class="emoji">';
											echo svgicon('emoji-'.$reaction['emoji']);
										echo '</div>';

										echo '<div class="when">';
											echo date_($reaction['reacted'], 'datetime');
										echo '</div>';
									echo '</div>';

									echo '<div class="subject">';
										echo '<a href="'.url('blog/'.$dt.'/'.mb_strtolower(seo(trim($subject)))).'">';
											echo $subject;
										echo '</a>';
									echo '</div>';
								echo '</div>';
							}
						}

						echo '<div class="and-counting">';
							echo '+ '.($c_reactions - 100).' '.mb_strtolower($lang['pages']['blog']['list']['reactions']['and-counting']);
						echo '</div>';
					echo '</div>';



				} else {
					echo '<details class="statistics">';
						echo '<summary class="no-select">';
							echo '<div class="open">'.svgicon('details-open').'</div>';
							echo '<div class="close">'.svgicon('details-close').'</div>';
							echo svgicon('statistics') . $lang['pages']['blog']['statistics']['title'];
						echo '</summary>';

						echo '<div class="content">';

							if(!file_exists($dir_files.'/stats.json')) {
								echo '<div class="message">';
									echo $lang['messages']['no-blogstats'];
								echo '</div>';

							} else {
								$words_lotr = 481103;
								$words_thebible = 783137;
								$stats = json_decode(file_get_contents($dir_files.'/stats.json'), true);

								$arr_stats = [
									'{firstpost}' => date_(strtotime($stats['blog']['first_post']), 'day-month-year-time'),
									'{posts}' => format_number($stats['blog']['posts']['total']),
									'{posts_hundreddaystooffload}' => format_number($stats['blog']['posts']['hundreddaystooffload']['yes']),
									'{reactions}' => format_number($stats['blog']['reactions']),
									'{reactions_perday}' => format_number($stats['blog']['average']['reactions']['per_day']),
									'{images}' => format_number($stats['blog']['images']),
									'{links}' => format_number($stats['blog']['links']),
									'{words}' => format_number($stats['blog']['words']),
									'{compare_lotr}' => format_number((($stats['blog']['words'] / $words_lotr) * 100), 1).'%',
									'{compare_thebible}' => format_number((($stats['blog']['words'] / $words_thebible) * 100), 1).'%',
									'{totalsize}' => format_filesize(
										$stats['total_filesize_in_bytes']['blog']['posts'] +
										$stats['total_filesize_in_bytes']['blog']['videos'] +
										$stats['total_filesize_in_bytes']['blog']['photos']
									)
								];

								foreach($lang['pages']['blog']['statistics']['content'] AS $content) {
									echo str_replace(array_keys($arr_stats), $arr_stats, $Parsedown->text($content));
								}
							}

						echo '</div>';
					echo '</details>';



					echo '<details class="calendar">';
						echo '<summary class="no-select">';
							echo '<div class="open">'.svgicon('details-open').'</div>';
							echo '<div class="close">'.svgicon('details-close').'</div>';
							echo svgicon('calendar') . $lang['pages']['blog']['calendar']['title'];
						echo '</summary>';

						echo '<div class="content">';
							$postdays = json_decode(file_get_contents($dir_files.'/stats.json'), false);
							$arr_postdays = [];

							foreach($postdays->blog->calendar AS $postday) {
								$arr_postdays[] = $postday->date;
							}

							for($m = 1; $m < 13; $m++) {
								echo renderMonth($m, '2025', $arr_postdays);
							}

							/*$factory = new CalendR\Calendar;
							$year = $factory->getYear((empty($get_year) ? date('Y') : (int)$get_year));
							$postdays = json_decode(file_get_contents($dir_files.'/stats.json'), false);
							$arr_postdays = [];

							foreach($postdays->blog->calendar AS $postday) {
								$arr_postdays[] = $postday->date;
							}

							echo '<div class="year">';
								echo (empty($get_year) ? date('Y') : (int)$get_year);
							echo '</div>';

							echo '<div class="months">';
								foreach($year AS $month) {
									$month = $factory->getMonth((empty($get_year) ? date('Y') : (int)$get_year), $month->format('n'));

									echo '<div class="month'.($month->format('n') == date('n') ? ' current' : '').'">';
										echo '<div class="label">';
											echo $lang['pages']['blog']['calendar']['months'][$month->format('n')];
										echo '</div>';

										echo '<div class="days">';
											foreach($month AS $week) {
												foreach($week AS $day) {
													if(in_array($day->format('Y-m-d'), $arr_postdays) AND $month->format('m') == $day->format('m')) {
														echo '<a href="'.url('blog/date:'.$day->format('Ymd')).'" title="'.$day->format('Y-m-d').'">';
															echo '<div class="day active';
															echo (($month->format('m') != $day->format('m')) ? ' outside-month' : '');
															echo (($day->format('mj') == date('mj')) ? ' current' : '');
															echo '"></div>';
														echo '</a>';

													} else {
														echo '<div class="day';
														echo (($month->format('m') != $day->format('m')) ? ' outside-month' : '');
														echo (($day->format('mj') == date('mj')) ? ' current' : '');
														echo '" title="'.$day->format('Y-m-d').'"></div>';
													}
												}
											}
										echo '</div>';
									echo '</div>';
								}*/
							echo '</div>';
						echo '</div>';
					echo '</details>';



					echo '<form action="'.url('blog').'" method="POST" autocomplete="off" novalidate>';
						echo '<div class="icon"><div class="circle"></div>'.svgicon('zoom').'</div>';

						echo '<input type="search" name="field-keyword" tabindex="1" placeholder="'.$lang['pages']['blog']['search']['placeholder'].'"';
						echo ((empty($get_keyword) OR !empty($get_keyword) AND $get_keyword == 'none') ? '' : ' value="'.$get_keyword.'"');
						echo '>';

						echo '<select name="list-years">';
							echo '<option value="">'.$lang['pages']['blog']['search']['year'].'</option>';

							echo '<option value="2025"';
							echo (($is_filtering == false OR $is_filtering == true AND empty($get_year)) ? null : ($get_year == 2025 ? ' selected' : ''));
							echo '>2025</option>';

							echo '<option value="2024"';
							echo (($is_filtering == false OR $is_filtering == true AND empty($get_year)) ? null : ($get_year == 2024 ? ' selected' : ''));
							echo '>2024</option>';
						echo '</select>';

						echo '<select name="list-months">';
							echo '<option value="">'.$lang['pages']['blog']['search']['month'].'</option>';

							for($i = 1; $i < 13; $i++) {
								echo '<option value="'.$i.'"'.(($is_filtering == false OR $is_filtering == true AND empty($get_month)) ? null : ($get_month == $i ? ' selected' : '')).'>'.$lang['months'][$i].'</option>';
							}
						echo '</select>';

						echo '<input type="submit" name="search" value="'.$lang['pages']['blog']['search']['button'].'">';
					echo '</form>';


					if($is_filtering == true) {
						echo '<h2>';
							if(empty($get_show) AND empty($get_keyword) AND $get_keyword != 'none' AND !empty($get_year) AND empty($get_month) AND empty($get_day)) {
								echo $lang['pages']['blog']['subtitles']['filtering'].' '.$get_year;

							} elseif(empty($get_show) AND empty($get_keyword) AND $get_keyword != 'none' AND empty($get_year) AND !empty($get_month) AND empty($get_day)) {
								echo $lang['pages']['blog']['subtitles']['filtering'].' ';
								echo ($lang['metadata']['language'] == 'se' ? mb_strtolower(date_($get_month, 'month')) : date_($get_month, 'month'));

							} elseif(empty($get_show) AND empty($get_keyword) AND $get_keyword != 'none' AND empty($get_year) AND empty($get_month) AND empty($get_day) AND !empty($get_date)) {
								echo $lang['pages']['blog']['subtitles']['filtering-date'].' '.date_(strtotime($get_date), 'day-month-year');

							} elseif(empty($get_show) AND !empty($get_keyword) AND $get_keyword != 'none') {
								echo $lang['pages']['blog']['subtitles']['searching'].' '.$get_keyword;

							} elseif(!empty($get_show) AND $get_show == 'hundreddaystooffload') {
								echo $Parsedown->text($lang['pages']['blog']['subtitles']['hundreddaystooffload']);
							}
						echo '</h2>';
					}


					echo '<div class="posts">';
						$current_month = null;
						$current_day = null;
						$arr_posts = [];
						$c_posts = 0;

						foreach(array_reverse($posts) AS $post) {
							$fileinfo = pathinfo($post);
							$post = json_decode(file_get_contents($post), false);
							$published_year = mb_substr($fileinfo['filename'], 0, 4);
							$published_month = mb_substr($fileinfo['filename'], 4, 2);
							$published_day = mb_substr($fileinfo['filename'], 6, 2);
							$published_hour = mb_substr($fileinfo['filename'], 8, 2);
							$published_minute = mb_substr($fileinfo['filename'], 10, 2);
							$datetime = $published_year . $published_month . $published_day . $published_hour . $published_minute;

							$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);
							$content = ($get_lang == 'se' ? $post->content->se : $post->content->en);
							$cover = (!file_exists($dir_files.'/uploads/covers/'.$datetime.'.webp') ? null : url('cover:'.$datetime, true));

							$string = '';

							if($is_filtering == false AND empty($get_show) AND empty($get_keyword) OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND empty($get_year) AND empty($get_month) AND strpos(mb_strtolower($subject), $get_keyword) !== false OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND empty($get_year) AND empty($get_month) AND strpos(mb_strtolower($content), $get_keyword) !== false OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND $get_keyword == 'none' AND !empty($get_year) AND $get_year == $published_year OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND $get_keyword == 'none' AND !empty($get_month) AND $get_month == $published_month OR
							$is_filtering == true AND empty($get_show) AND empty($get_keyword) AND !empty($get_date) AND $get_date == $published_year . $published_month . $published_day OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND strpos(mb_strtolower($subject), $get_keyword) !== false AND !empty($get_year) AND empty($get_month) AND (int)$get_year == (int)$published_year OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND strpos(mb_strtolower($content), $get_keyword) !== false AND !empty($get_year) AND empty($get_month) AND (int)$get_year == (int)$published_year OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND strpos(mb_strtolower($subject), $get_keyword) !== false AND empty($get_year) AND !empty($get_month) AND (int)$get_month == (int)$published_month OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND strpos(mb_strtolower($content), $get_keyword) !== false AND empty($get_year) AND !empty($get_month) AND (int)$get_month == (int)$published_month OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND strpos(mb_strtolower($subject), $get_keyword) !== false AND !empty($get_year) AND (int)$get_year == (int)$published_year AND !empty($get_month) AND (int)$get_month == (int)$published_month OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND strpos(mb_strtolower($content), $get_keyword) !== false AND !empty($get_year) AND (int)$get_year == (int)$published_year AND !empty($get_month) AND (int)$get_month == (int)$published_month OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND strpos(mb_strtolower($subject), $get_keyword) !== false AND !empty($get_date) AND $get_date == $published_year . $published_month . $published_day OR
							$is_filtering == true AND empty($get_show) AND !empty($get_keyword) AND strpos(mb_strtolower($content), $get_keyword) !== false AND !empty($get_date) AND $get_date == $published_year . $published_month . $published_day OR
							$is_filtering == true AND !empty($get_show) AND $get_show == '100daystooffload' AND isset($post->hundreddaystooffload->enabled) AND $post->hundreddaystooffload->enabled == true OR
							$is_filtering == true AND !empty($get_show) AND $get_show == 'updatelog' AND isset($post->updatelog) AND $post->updatelog == true) {

								$c_posts++;

								if($is_filtering == false) {
									if($current_month != $published_month) {
										$string .= '<div class="new-month">';
											$string .= '<div>'.$lang['months'][(int)$published_month].'</div>';
										$string .= '</div>';

										$current_month = $published_month;
									}

									/*if($current_day != $published_day) {
										$string .= '<div class="new-day"></div>';
										$current_day = $published_day;
									}*/
								}


								$string .= '<div class="item">';
									$string .= '<div class="published">';
										$string .= '<a href="'.url('blog/year:'.$published_year).'" class="filter" title="'.$lang['tooltips']['filter-by-year'].'">'.$published_year.'</a>';
										$string .= '-';
										$string .= '<a href="'.url('blog/month:'.(int)$published_month).'" class="filter" title="'.$lang['tooltips']['filter-by-month'].'">'.$published_month.'</a>';
										$string .= '-';
										$string .= '<a href="'.url('blog/date:'.$published_year . $published_month . $published_day).'" class="filter" title="'.$lang['tooltips']['filter-by-date'].'">'.$published_day.'</a>';
										$string .= ', '.$published_hour.':'.$published_minute;
									$string .= '</div>';

									$string .= '<div class="subject">';
										$string .= '<a href="'.url('blog/'.$fileinfo['filename'].'/'.mb_strtolower(seo(trim($subject)))).'" class="side-by-side">';
											if(empty($cover) AND (isset($post->updatelog) AND $post->updatelog == false) AND (isset($post->hundreddaystooffload->enabled) AND $post->hundreddaystooffload->enabled == false)) {
												$string .= '<div class="no-cover"></div>';

											} else {
												$string .= '<div class="cover" style="background-image: url(';
												if(isset($post->updatelog) AND $post->updatelog == true) {
													$string .= url($dir_images.'/metadata-updatelog.webp', true);

												} elseif(isset($post->hundreddaystooffload->enabled) AND $post->hundreddaystooffload->enabled == true) {
													$string .= url($dir_images.'/100DaysToOffload.webp', true);

												} else {
													$string .= $cover;
												}
												$string .= ');"></div>';
											}

											$string .= $subject;
										$string .= '</a>';
									$string .= '</div>';
								$string .= '</div>';

							}

							$arr_posts[] = $string;
						}


						if($c_posts == 0) {
							echo '<div class="empty">';
								echo $lang['messages']['no-posts-search'];
							echo '</div>';

						} else {
							echo implode('', $arr_posts);
						}
					echo '</div>';
				}
			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
