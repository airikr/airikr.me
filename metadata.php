<?php

	use Embed\Embed;

	require_once 'site-settings.php';

	if(isset($_POST['check'])) {
		$get_url = (empty($_POST['field-url']) ? null : safetag($_POST['field-url']));

		if(empty($get_url)) {
			header("Location: ".url('metadata', true));
			exit;

		} else {
			$parse_url = parse_url($get_url);
			$url = $parse_url['host'];
			$url .= ($parse_url['path'] == '/' ? '' : $parse_url['path']);
			$url .= (!isset($parse_url['query']) ? '' : '?'.$parse_url['query']);

			header("Location: ".url('metadata/'.$url, true));
			exit;
		}



	} else {

		require_once 'site-settings.php';

		$get_url = (empty($_GET['url']) ? null : safetag($_GET['url']));

?><!DOCTYPE html>
<html>
	<head>
		<title><?php echo 'Metadata'; ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="robots" content="noindex, nofollow">
		<meta name="google" content="notranslate">

		<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=" rel="icon" type="image/x-icon">

		<?php $desktop = file_get_contents((($minified == true AND file_exists($dir_css.'/desktop.min.css')) ? $dir_css.'/desktop.min.css' : $dir_css.'/desktop.css')); ?>

		<style type="text/css">
			<?php
				echo '@font-face{font-family:"Victor Mono";font-style:normal;font-weight:400;font-display:swap;src:url('.url($dir_fonts.'/victor-mono-400.woff2', true).');unicode-range:U+000-5FF}@font-face{font-family:"Victor Mono";font-style:italic;font-weight:400;font-display:swap;src:url('.url($dir_fonts.'/victor-mono-400-italic.woff2', true).');unicode-range:U+000-5FF}@font-face{font-family:"Victor Mono";font-style:normal;font-weight:700;font-display:swap;src:url('.url($dir_fonts.'/victor-mono-700.woff2', true).');unicode-range:U+000-5FF}';
				echo $desktop;
			?>
			main {
				margin: 50px auto;
				width: 600px;
			}

			main > form > [name="field-url"] {
				width: calc(100% - 96px);
			}

			main > form > [name="check"] {
				margin-left: 10px;
			}

			main > .message {
				font-size: 112%;
				padding: 30px 20px;
				text-align: center;
			}

			main > .result {
				background-color: var(--darker);
				border: 1px solid var(--line);
				border-radius: 10px;
				margin-top: 20px;
			}

			main > .result > .image {
				background-color: var(--image);
				border-top-right-radius: 9px;
				border-top-left-radius: 9px;
				height: 312px;
			}

			main > .result > .image > .image {
				background-position: center;
				background-size: cover;
				background-repeat: no-repeat;
				border-top-right-radius: 9px;
				border-top-left-radius: 9px;
				height: inherit;
				width: 100%;
			}

			main > .result > .text {
				padding: 20px;
			}

			main > .result > .text > .provider {
				align-items: center;
				display: flex;
				flex-flow: row nowrap;
			}

			main > .result > .text > .provider > .image {
				background-color: var(--image);
				background-position: center;
				background-size: cover;
				background-repeat: no-repeat;
				border-radius: 3px;
				height: 24px;
				margin-right: 10px;
				width: 24px;
			}

			main > .result > .text > .title {
				font-size: 140%;
				font-weight: 700;
				padding: 10px 0;
			}

			.no-select {
				cursor: default;

				user-select: none;
				-moz-user-select: none;
				-khtml-user-select: none;
				-webkit-user-select: none;
				-webkit-user-drag: none;
			}



			@media only screen and (max-width: 645px) {
				main {
					margin: 20px;
					width: calc(100% - (20px * 2));
				}
			}
		</style>
	</head>
	<body>



		<main><?php

			echo '<form action="'.url('metadata'.(empty($get_url) ? '' : '/'.$get_url), true).'" method="POST" autocomplete="off" novalidate>';
				echo '<input type="url" name="field-url"'.(empty($get_url) ? '' : ' value="https://'.$get_url.'"').'>';
				echo '<input type="submit" name="check" value="Check">';
			echo '</form>';

			if(empty($get_url)) {
				echo '<div class="message">';
					echo 'Enter URL to start';
				echo '</div>';

			} else {
				$embed = new Embed();
				$info = $embed->get('https://'.$get_url);

				echo '<div class="result no-select">';
					echo '<div class="image">';
						echo '<div class="image"'.($info->image == '' ? '' : ' style="background-image: url('.$info->image.');"').'></div>';
					echo '</div>';

					echo '<div class="text">';
						echo '<div class="provider">';
							echo '<div class="image"'.($info->favicon == '' ? '' : ' style="background-image: url('.$info->favicon.');"').'></div>';
							echo $info->providerName;
						echo '</div>';

						echo '<div class="title">';
							echo $info->title;
						echo '</div>';

						echo '<div class="description">';
							echo $info->description;
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}

		?></main>



	</body>
</html>

<?php } ?>