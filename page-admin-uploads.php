<?php

	require_once 'site-header.php';









	if($is_loggedin == false) {
		header("Location: ".url('admin?prev=uploads'));
		exit;



	} else {
		$dir_nophotos = !(new \FilesystemIterator($dir_files.'/uploads'))->valid();
		$dir_nocovers = !(new \FilesystemIterator($dir_files.'/uploads/covers'))->valid();
		$dir_notracks = !(new \FilesystemIterator($dir_files.'/tracks'))->valid();
		$dir_novideos = !(new \FilesystemIterator($dir_files.'/videos'))->valid();

		$count_files_images = 0;
		$total_filesize_images = 0;
		$count_files_covers = 0;
		$total_filesize_covers = 0;
		$count_files_tracks = 0;
		$total_filesize_tracks = 0;
		$count_files_videos = 0;
		$total_filesize_videos = 0;

		if(!$dir_nophotos) {
			$images = glob($dir_files.'/uploads/*.webp');
			natsort($images);

			foreach($images AS $image) {
				$fileinfo = pathinfo($image);

				if(strpos($fileinfo['filename'], 'thumb') === false) {
					$count_files_images++;
					$total_filesize_images += filesize($image);
				}
			}
		}

		if(!$dir_nocovers) {
			$covers = glob($dir_files.'/uploads/covers/*.webp');
			natsort($covers);

			foreach($covers AS $cover) {
				$fileinfo = pathinfo($cover);

				if(strpos($fileinfo['filename'], 'thumb') === false) {
					$count_files_covers++;
					$total_filesize_covers += filesize($cover);
				}
			}
		}

		if(!$dir_notracks) {
			$tracks = glob($dir_files.'/tracks/*.gpx');
			natsort($tracks);

			foreach($tracks AS $track) {
				$fileinfo = pathinfo($track);

				if(strpos($fileinfo['filename'], 'thumb') === false) {
					$count_files_tracks++;
					$total_filesize_tracks += filesize($track);
				}
			}
		}

		if(!$dir_novideos) {
			$videos = glob($dir_files.'/videos/*.mp4');
			natsort($videos);

			foreach($videos AS $video) {
				$fileinfo = pathinfo($video);

				if(strpos($fileinfo['filename'], 'thumb') === false) {
					$count_files_videos++;
					$total_filesize_videos += filesize($video);
				}
			}
		}



		echo '<section id="uploads">';
			echo '<h1>'.$lang['pages']['uploads']['title'].'</h1>';

			echo '<nav>';
				echo '<a href="'.url('admin/upload').'" class="side-by-side">';
					echo svgicon('upload') . $lang['pages']['uploads']['upload'];
				echo '</a>';
			echo '</nav>';


			echo '<details>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';

					echo '<div class="side-by-side">';
						echo '<div>'.$lang['pages']['uploads']['subtitles']['photos'].'</div>';
						echo '<div class="stats">';
							echo format_number($count_files_images, 0);
							echo ' '.mb_strtolower(($count_files_images == 1 ? $lang['pages']['uploads']['file'] : $lang['pages']['uploads']['files']));
							echo ' ('.format_filesize($total_filesize_images).')';
						echo '</div>';
					echo '</div>';
				echo '</summary>';

				if($dir_nophotos) {
					echo '<div class="message">';
						echo $lang['messages']['no-images'];
					echo '</div>';


				} else {
					echo '<div class="images">';
						foreach(array_reverse($images) AS $image) {
							$fileinfo = pathinfo($image);

							if(strpos($fileinfo['filename'], 'thumb') === false) {
								echo '<div class="image">';
									echo '<a href="'.url('uploaded:'.$fileinfo['filename'].'/photo').'">';
										echo '<div class="image" style="background-image: url('.url('photo:'.$fileinfo['filename']).');"></div>';
									echo '</a>';
								echo '</div>';
							}
						}
					echo '</div>';
				}
			echo '</details>';







			echo '<details>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';

					echo '<div class="side-by-side">';
						echo '<div>'.$lang['pages']['uploads']['subtitles']['covers'].'</div>';
						echo '<div class="stats">';
							echo format_number($count_files_covers, 0);
							echo ' '.mb_strtolower(($count_files_covers == 1 ? $lang['pages']['uploads']['file'] : $lang['pages']['uploads']['files']));
							echo ' ('.format_filesize($total_filesize_covers).')';
						echo '</div>';
					echo '</div>';
				echo '</summary>';


				if($dir_nocovers) {
					echo '<div class="message">';
						echo $lang['messages']['no-covers'];
					echo '</div>';

				} else {
					echo '<div class="item head side-by-side">';
						echo '<div class="filename">';
							echo $lang['pages']['uploads']['list']['filename'];
						echo '</div>';

						echo '<div class="filesize">';
							echo $lang['pages']['uploads']['list']['filesize'];
						echo '</div>';
					echo '</div>';

					foreach(array_reverse($covers) AS $cover) {
						$fileinfo = pathinfo($cover);

						if(strpos($fileinfo['filename'], 'thumb') === false) {
							echo '<div class="item side-by-side">';
								echo '<div class="filename">';
									echo '<a href="'.url('uploaded:'.$fileinfo['filename'].'/cover').'">';
										echo '<div class="image" style="background-image: url('.url('cover:'.$fileinfo['filename']).');"></div>';
										echo $fileinfo['filename'];
									echo '</a>';
								echo '</div>';

								echo '<div class="filesize">';
									echo format_filesize(filesize($cover));
								echo '</div>';
							echo '</div>';
						}
					}

					echo '<div class="item stats side-by-side">';
						echo '<div class="filename">';
							echo format_number($count_files_covers, 0);
							echo ' '.mb_strtolower(($count_files_covers == 1 ? $lang['pages']['uploads']['file'] : $lang['pages']['uploads']['files']));
						echo '</div>';

						echo '<div class="filesize">';
							echo format_filesize($total_filesize_covers);
						echo '</div>';
					echo '</div>';
				}
			echo '</details>';







			echo '<details>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';

					echo '<div class="side-by-side">';
						echo '<div>'.$lang['pages']['uploads']['subtitles']['tracks'].'</div>';
						echo '<div class="stats">';
							echo format_number($count_files_tracks, 0);
							echo ' '.mb_strtolower(($count_files_tracks == 1 ? $lang['pages']['uploads']['file'] : $lang['pages']['uploads']['files']));
							echo ' ('.format_filesize($total_filesize_tracks).')';
						echo '</div>';
					echo '</div>';
				echo '</summary>';


				if($dir_notracks) {
					echo '<div class="message">';
						echo $lang['messages']['no-tracks'];
					echo '</div>';

				} else {
					echo '<div class="item head side-by-side">';
						echo '<div class="filename">';
							echo $lang['pages']['uploads']['list']['filename'];
						echo '</div>';

						echo '<div class="filesize">';
							echo $lang['pages']['uploads']['list']['filesize'];
						echo '</div>';
					echo '</div>';

					foreach(array_reverse($tracks) AS $track) {
						$fileinfo = pathinfo($track);

						if(strpos($fileinfo['filename'], 'thumb') === false) {
							echo '<div class="item side-by-side">';
								echo '<div class="filename">';
									echo '<a href="'.url('uploaded:'.$fileinfo['filename'].'/track').'">';
										echo '<div class="image" style="background-image: url('.url('track-thumb:'.$fileinfo['filename']).');"></div>';
										echo $fileinfo['filename'];
									echo '</a>';
								echo '</div>';

								echo '<div class="filesize">';
									echo format_filesize(filesize($track));
								echo '</div>';
							echo '</div>';
						}
					}
				}
			echo '</details>';







			echo '<details>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';

					echo '<div class="side-by-side">';
						echo '<div>'.$lang['pages']['uploads']['subtitles']['videos'].'</div>';
						echo '<div class="stats">';
							echo format_number($count_files_videos, 0);
							echo ' '.mb_strtolower(($count_files_videos == 1 ? $lang['pages']['uploads']['file'] : $lang['pages']['uploads']['files']));
							echo ' ('.format_filesize($total_filesize_videos).')';
						echo '</div>';
					echo '</div>';
				echo '</summary>';


				if($dir_novideos) {
					echo '<div class="message">';
						echo $lang['messages']['no-videos'];
					echo '</div>';

				} else {
					echo '<div class="item head side-by-side">';
						echo '<div class="filename">';
							echo $lang['pages']['uploads']['list']['filename'];
						echo '</div>';

						echo '<div class="filesize">';
							echo $lang['pages']['uploads']['list']['filesize'];
						echo '</div>';
					echo '</div>';

					foreach(array_reverse($videos) AS $video) {
						$fileinfo = pathinfo($video);

						if(strpos($fileinfo['filename'], 'thumb') === false) {
							echo '<div class="item side-by-side">';
								echo '<div class="filename">';
									echo '<a href="'.url('uploaded:'.$fileinfo['filename'].'/video').'">';
										echo $fileinfo['filename'];
									echo '</a>';
								echo '</div>';

								echo '<div class="filesize">';
									echo format_filesize(filesize($video));
								echo '</div>';
							echo '</div>';
						}
					}
				}
			echo '</details>';



			echo '<div class="stats">';
				echo '<div>';
					$total_filesize = $total_filesize_images + $total_filesize_covers + $total_filesize_tracks + $total_filesize_videos;

					echo 'Total <b>';
					echo format_number($count_files_images + $count_files_covers + $count_files_tracks + $count_files_videos, 0);
					echo ' '.mb_strtolower((format_number($count_files_images + $count_files_covers + $count_files_tracks, 0) == 1 ? $lang['pages']['uploads']['file'] : $lang['pages']['uploads']['files']));
					echo '</b> som tar <b>';
					echo format_filesize($total_filesize);
					echo ' ('.format_number((($total_filesize / disk_free_space('/')) * 100), 2).'%)';
					echo '</b>.';
				echo '</div>';

				echo '<div>';
					echo 'Det är <b>'.format_filesize(disk_free_space('/')).'</b> ledigt diskutrymme på servern.';
				echo '</div>';
			echo '</div>';
		echo '</section>';
	}







	require_once 'site-footer.php';

?>
