<?php

	$arr_uses = [
		'hardwares' => [
			'desktop' => [
				'psu' => 'be quiet! Straight Power 11 850W',
				'motherboard' => 'MSI B550 Gaming Gen3',
				'cpu' => 'AMD Ryzen 7 5700X 3.4 GHz 36MB',
				'gpu' => [
					'Gigabyte Geforce RTX 3070 8GB GAMING OC 2.0'
				],
				'memory' => 'Kingston Fury 32GB (2x16GB) DDR4 3200MHz CL',
				'storage' => [
					'WD Blue SN570 2TB',
					'Samsung 870 EVO SATA SSD 250GB',
					'PNY CS900 SATA 1TB'
				],
				'monitors' => [
					'Acer 24" HA240Y IPS 75 Hz'
				],
				'mouse' => 'Arrogant Typhoon 2',
				'keyboard' => 'SteelSeries Apex 3 TKL',
				'mic' => 'Blue Snowball',
				'headset' => 'Razer BLACKSHARK V2 X USB'
			],
			'laptop' => [
				'manufacturer' => 'Lenovo',
				'model' => 'IdeaPad 1-14ADA05',
				'cpu' => 'AMD Athlon Silver 3050e',
				'gpu' => 'AMD Radeon Graphics',
				'memory' => '4 GB DDR4 SDRAM 2400 MHz',
				'storage' => '64 GB eMMC 5.1'
			],
			'camera' => [
				'manufacturer' => 'Sony',
				'model' => 'α6400 (ILCE-5400)',
				'lenses' => [
					'Samyang F2.0/12mm',
					'Sony E 18-135mm f/3,5-5,6 OSS'
				]
			],
			'phone' => [
				'manufacturer' => 'Google',
				'model' => 'Pixel 6a',
				'os' => 'GrapheneOS'
			],
			'other' => [
				[
					'name' => 'GoPro HERO 12 Black',
					'icon' => 'actioncamera'
				],
				[
					'name' => 'Marshall Major V',
					'icon' => 'headset'
				],
				[
					'name' => 'Targus CityLitePro Secure',
					'icon' => 'backpack'
				]
			]
		],


		'softwares' => [
			'operatingsystems' => [
				[
					'name' => 'EndeavourOS Xfce',
					'primary' => true,
					'installation' => 'internal',
					'purpose' => [
						'everyday use'
					]
				],
				[
					'name' => 'Windows 10',
					'primary' => false,
					'installation' => 'internal',
					'purpose' => [
						'gaming'
					]
				],
				[
					'name' => 'Windows 10',
					'primary' => false,
					'installation' => 'virtual',
					'purpose' => [
						'photo editing',
						'fooling around'
					]
				]
			],
			'softwares' => [
				'pc' => [
					'Shotcut',
					'GIMP',
					'Adobe Lightroom Classic',
					'OBS Studio',
					'Gramps',
					'Skanlite',
					'SmartGit',
					'FreeFileSync',
					'Flameshot',
					'Mullvad VPN',
					'SpeedCrunch',
					'VirtualBox',
					'KColorChooser',
					'Sveriges dödbok',
					'VSCodium',
					'Lite XL',
					'Skanlite',
					'Vivaldi',
					'Gajim',
					'Firefox',
					'Joplin',
					'Thunderbird',
					'Telegram',
					'Steam',
					'Lutris',
					'Mumble'
				],
				'phone' => [
					'monocles chat',
					'Mumla',
					'Privacy Browser',
					'Droid-ify',
					'AntennaPod',
					'Auxio',
					'BankID',
					'Swish',
					'Trail Sense',
					'MedTimer',
					'Bitwarden',
					'Swedbank',
					'Arcticons Material You',
					'Fossify Calendar',
					'Fossify Contacts',
					'Fossify Calculator',
					'Fossify Voice Recorder',
					'Fossify File Manager',
					'Fossify Music Player',
					'Fossify Gallery',
					'Fossify Messages',
					'OSS Card Wallet',
					'Bura',
					'Open Camera',
					'Grocy',
					'Healthy Battery Charging',
					'DAVx5',
					'Download Navi',
					'FairEmail',
					'Loop Habits Tracker',
					'Image Toolbox',
					'Jottacloud',
					'Joplin',
					'Pinkt',
					'Capy Reader',
					'mpv',
					'Moshidon',
					'Mull',
					'OpenTracks',
					'OsmAnd~',
					'Rethink',
					'Seal',
					'Sportstats',
					'QR Scanner (PFA)',
					'Suntimes',
					'Syncthing',
					'URL Radio',
					'Vanadium',
					'Unexpected Keyboard',
					'TriPeaks',
					'Voyager',
					'OSS Card Wallet'
				],
				'server' => [
					[
						'name' => 'Opengist',
						'url' => 'https://github.com/thomiceli/opengist'
					],
					[
						'name' => 'Joplin',
						'url' => 'https://joplinapp.org/'
					],
					[
						'name' => 'Mumble',
						'url' => 'https://www.mumble.info/'
					],
					[
						'name' => 'Redlib',
						'url' => 'https://github.com/redlib-org/redlib'
					],
					[
						'name' => 'NocoDB',
						'url' => 'https://github.com/nocodb/nocodb'
					],
					[
						'name' => 'Owncast',
						'url' => 'https://github.com/owncast/owncast'
					],
					[
						'name' => 'Pingvin Share',
						'url' => 'https://github.com/stonith404/pingvin-share'
					],
					[
						'name' => 'Forgejo',
						'url' => 'https://forgejo.org/'
					],
					[
						'name' => 'linkding',
						'url' => 'https://github.com/sissbruecker/linkding'
					],
					[
						'name' => 'Invidious',
						'url' => 'https://github.com/iv-org/invidious'
					],
					[
						'name' => 'ShotShare',
						'url' => 'https://github.com/mdshack/shotshare'
					],
					[
						'name' => 'SearxNG',
						'url' => 'https://github.com/searxng/searxng-docker'
					],
					[
						'name' => 'Miniflux',
						'url' => 'https://miniflux.app/'
					],
					[
						'name' => 'Vaultwarden',
						'url' => 'https://github.com/dani-garcia/vaultwarden'
					],
					[
						'name' => 'Plausible',
						'url' => 'https://plausible.io'
					],
					[
						'name' => 'Grocy',
						'url' => 'https://github.com/grocy/grocy'
					],
					[
						'name' => 'Snikket',
						'url' => 'https://snikket.org'
					],
					[
						'name' => 'Full-Text RSS',
						'url' => 'https://fivefilters.org/full-text-rss/'
					],
					[
						'name' => 'Baïkal',
						'url' => 'https://github.com/sabre-io/Baikal'
					],
					[
						'name' => 'Kanboard',
						'url' => 'https://kanboard.org'
					],
					[
						'name' => 'Jellyfin',
						'url' => 'https://jellyfin.org'
					],
					[
						'name' => 'Syncthing',
						'url' => 'https://syncthing.net'
					],
					[
						'name' => 'GoToSocial',
						'url' => 'https://gotosocial.org'
					],
					[
						'name' => 'Readeck',
						'url' => 'https://codeberg.org/readeck/readeck'
					]
				]
			]
		]
	];



	if(isset($_GET['json'])) {
		header('Content-Type: application/json;charset=utf-8');
		echo json_encode($arr_uses);

	} else {

		require_once 'site-header.php';

		$show = (isset($_GET['sho']) ? safetag($_GET['sho']) : false);

		$c_gpus = count($arr_uses['hardwares']['desktop']['gpu']);
		$c_storage = count($arr_uses['hardwares']['desktop']['storage']);
		$c_monitors = count($arr_uses['hardwares']['desktop']['monitors']);

		$arr_softwares_phone = [];
		foreach($arr_uses['softwares']['softwares']['phone'] AS $software) {
			$arr_softwares_phone[] = $software;
		}

		$arr_softwares_pc = [];
		foreach($arr_uses['softwares']['softwares']['pc'] AS $software) {
			$arr_softwares_pc[] = $software;
		}

		$arr_selfhosting = [];
		foreach($arr_uses['softwares']['softwares']['server'] AS $selfhosted) {
			$arr_selfhosting[] = [
				'name' => $selfhosted['name'],
				'url' => $selfhosted['url']
			];
		}

		asort($arr_softwares_pc, SORT_NATURAL | SORT_FLAG_CASE);
		asort($arr_softwares_phone, SORT_NATURAL | SORT_FLAG_CASE);

		function sort_by_name($a, $b) {
			return strcmp($a['name'], $b['name']);
		}

		usort($arr_selfhosting, 'sort_by_name');
		usort($arr_uses['hardwares']['other'], 'sort_by_name');







		echo '<section id="uses">';
			echo '<h1>'.$lang['pages']['uses']['title'].'</h1>';

			echo '<details class="desktop"'.($show == false ? '' : ($show == 'desktop' ? ' open' : '')).'>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';
					echo svgicon('device-desktop') . $lang['pages']['uses']['pc']['desktop'];
				echo '</summary>';

				echo '<div class="content">';
					echo '<div class="screenshot">';
						echo link_('<div class="image double" style="background-image: url('.url('image:desktop').');"></div>', url('screenshot:desktop', true));
						echo '<div class="info">';
							echo $lang['pages']['uses']['screenshotinfos']['desktop'];
						echo '</div>';
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['psu'].'</div>';
						echo $arr_uses['hardwares']['desktop']['psu'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['motherboard'].'</div>';
						echo $arr_uses['hardwares']['desktop']['motherboard'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['cpu'].'</div>';
						echo $arr_uses['hardwares']['desktop']['cpu'];
					echo '</div>';

					echo '<div'.($c_gpus == 1 ? '' : ' class="multiple"').'>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['gpu'].'</div>';

						if($c_gpus == 1) {
							echo implode('', $arr_uses['hardwares']['desktop']['gpu']);
						} else {
							echo '<div class="items">';
								echo '<div>- '.implode('</div><div>- ', $arr_uses['hardwares']['desktop']['gpu']).'</div>';
							echo '</div>';
						}
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['ram'].'</div>';
						echo $arr_uses['hardwares']['desktop']['memory'];
					echo '</div>';


					echo '<div'.($c_storage == 1 ? '' : ' class="multiple"').'>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['storage'].'</div>';

						if($c_storage == 1) {
							echo implode('', $arr_uses['hardwares']['desktop']['storage']);
						} else {
							echo '<div class="items">';
								echo '<div>- '.implode('</div><div>- ', $arr_uses['hardwares']['desktop']['storage']).'</div>';
							echo '</div>';
						}
					echo '</div>';


					echo '<div'.($c_monitors == 1 ? '' : ' class="multiple"').'>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['screen'.($c_monitors == 1 ? '' : 's')].'</div>';

						if($c_monitors == 1) {
							echo implode('', $arr_uses['hardwares']['desktop']['monitors']);
						} else {
							echo '<div class="items">';
								echo '<div>- '.implode('</div><div>- ', $arr_uses['hardwares']['desktop']['monitors']).'</div>';
							echo '</div>';
						}
					echo '</div>';


					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['mouse'].'</div>';
						echo $arr_uses['hardwares']['desktop']['mouse'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['keyboard'].'</div>';
						echo $arr_uses['hardwares']['desktop']['keyboard'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['headset'].'</div>';
						echo $arr_uses['hardwares']['desktop']['headset'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['os'].'</div>';
						foreach($arr_uses['softwares']['operatingsystems'] AS $os) {
							if($os['primary'] == true) {
								echo $os['name'];
							}
						}
					echo '</div>';
				echo '</div>';
			echo '</details>';



			echo '<details class="laptop"'.($show == false ? '' : ($show == 'laptop' ? ' open' : '')).'>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';
					echo svgicon('device-laptop') . $lang['pages']['uses']['pc']['laptop'];
				echo '</summary>';

				echo '<div class="content">';
					echo '<div class="screenshot">';
						echo link_('<div class="image single" style="background-image: url('.url('image:laptop').');"></div>', url('screenshot:laptop', true));
						echo '<div class="info">';
							echo $lang['pages']['uses']['screenshotinfos']['laptop'];
						echo '</div>';
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['manufacturer'].'</div>';
						echo $arr_uses['hardwares']['laptop']['manufacturer'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['model'].'</div>';
						echo $arr_uses['hardwares']['laptop']['model'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['cpu'].'</div>';
						echo $arr_uses['hardwares']['laptop']['cpu'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['gpu'].'</div>';
						echo $arr_uses['hardwares']['laptop']['gpu'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['ram'].'</div>';
						echo $arr_uses['hardwares']['laptop']['memory'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['storage'].'</div>';
						echo $arr_uses['hardwares']['laptop']['storage'];
					echo '</div>';

					echo '<div>';
						echo '<div class="label">'.$lang['pages']['uses']['pc']['os'].'</div>';
						foreach($arr_uses['softwares']['operatingsystems'] AS $os) {
							if($os['primary'] == true) {
								echo $os['name'];
							}
						}
					echo '</div>';
				echo '</div>';
			echo '</details>';



			echo '<details class="softwares"'.($show == false ? '' : ($show == 'softwares_pc' ? ' open' : '')).'>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';
					echo svgicon('software') . $lang['pages']['uses']['softwares']['pc'];
				echo '</summary>';

				echo '<div class="content">';
					echo '<div class="statistics">';
						echo svgicon('statistics');
						echo count($arr_softwares_pc).' '.mb_strtolower($lang['pages']['uses']['softwares']['software'.(count($arr_softwares_pc) == 1 ? '' : 's')]);
					echo '</div>';

					echo '<div class="list">';
						foreach($arr_softwares_pc AS $software) {
							echo '<div>'.$software.'</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</details>';



			echo '<details class="softwares"'.($show == false ? '' : ($show == 'softwares_phone' ? ' open' : '')).'>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';
					echo svgicon('software') . $lang['pages']['uses']['softwares']['phone'];
				echo '</summary>';

				echo '<div class="content">';
					echo '<div class="statistics">';
						echo svgicon('statistics');
						echo count($arr_softwares_phone).' '.mb_strtolower($lang['pages']['uses']['softwares']['software'.(count($arr_softwares_phone) == 1 ? '' : 's')]);
					echo '</div>';

					echo '<div class="list">';
						foreach($arr_softwares_phone AS $software) {
							echo '<div>'.$software.'</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</details>';



			echo '<details class="softwares"'.($show == false ? '' : ($show == 'selfhosting' ? ' open' : '')).'>';
				echo '<summary>';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';
					echo svgicon('selfhosting') . $lang['pages']['uses']['softwares']['selfhosting'];
				echo '</summary>';

				echo '<div class="content">';
					echo '<div class="statistics">';
						echo svgicon('statistics');
						echo count($arr_selfhosting).' '.mb_strtolower($lang['pages']['uses']['softwares']['software'.(count($arr_selfhosting) == 1 ? '' : 's')]);
					echo '</div>';

					echo '<div class="list">';
						foreach($arr_selfhosting AS $selfhosted) {
							echo '<div>';
								echo link_($selfhosted['name'], $selfhosted['url']);
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</details>';



			echo '<div class="camera">';
				echo '<div class="icon">'.svgicon('device-camera').'</div>';

				echo '<div class="specifications">';
					echo '<div class="title">';
						echo $arr_uses['hardwares']['camera']['manufacturer'].' '.$arr_uses['hardwares']['camera']['model'];
					echo '</div>';

					echo '<div class="lenses">';
						foreach($arr_uses['hardwares']['camera']['lenses'] AS $lens) {
							echo '<div>'.$lens.'</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</div>';



			echo '<div class="phone">';
				echo '<div class="icon">'.svgicon('device-phone').'</div>';

				echo '<div class="specifications">';
					echo '<div class="title">';
						echo $arr_uses['hardwares']['phone']['manufacturer'].' '.$arr_uses['hardwares']['phone']['model'];
					echo '</div>';

					echo '<div class="os">';
						echo $arr_uses['hardwares']['phone']['os'];
					echo '</div>';
				echo '</div>';
			echo '</div>';



			echo '<div class="more">';
				foreach($arr_uses['hardwares']['other'] AS $other) {
					echo '<div>';
						echo svgicon($other['icon']);
						echo $other['name'];
					echo '</div>';
				}
			echo '</div>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
