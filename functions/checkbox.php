<?php

	function checkbox($string, $id, $desc = null, $checked = null, $disabled = false) {
		$content = '<div class="checkbox'.(!empty($desc) ? '' : ' nodesc').'">';
			$content .= '<div>';
				$content .= '<input type="checkbox" name="check-'.$id.'" id="'.$id.'"';
				$content .= ($checked == true ? ' checked' : null);
				$content .= ($disabled == false ? null : ' disabled').'>';

				$content .= '<div class="icon-checkbox is-unchecked">'.svgicon('unchecked').'</div>';
				$content .= '<div class="icon-checkbox is-checked">'.svgicon('checked').'</div>';

				$content .= '<label for="'.$id.'">'.$string.'</label>';
			$content .= '</div>';

			if(!empty($desc)) {
				$content .= '<div class="desc">'.$desc.'</div>';
			}
		$content .= '</div>';


		return $content;
	}

?>