<?php

	function seo($text) {
		$letters = ['å', 'ä', 'ö', ',', '...', '!', '?', '&', '"', ':', '\''];
		$seofriendly = ['a', 'a', 'o', '', '', '', '', '', '', '', ''];

		$text = str_replace($letters, $seofriendly, html_entity_decode(mb_strtolower($text)));
		$text = str_replace(' ', '-', $text);

		return $text;
	}

?>
