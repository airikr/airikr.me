<?php

	if(isset($_GET['upd'])) {

		require_once 'site-settings.php';

		$count_videos = 0;
		$videofeed = json_decode(file_get_contents('https://video.airikr.me/api/v1/accounts/edgren/videos'), true);
		$arr = [];

		foreach($videofeed['data'] AS $video) {
			$count_videos++;

			if($count_videos <= 15) {
				$file_info = pathinfo('https://video.airikr.me'.$video['channel']['avatars'][0]['path']);
				$arr[] = [
					'id' => $video['id'],
					'title' => $video['name'],
					'url' => $video['url'],
					'channel' => $file_info['filename']
				];

				if(!file_exists($dir_files.'/videofeed/videos/'.$video['id'].'.jpg')) {
					copy('https://video.airikr.me'.$video['thumbnailPath'], $dir_files.'/videofeed/videos/'.$video['id'].'.jpg');
				}

				if(!file_exists($dir_files.'/videofeed/channels/'.$video['id'].'.jpg')) {
					copy('https://video.airikr.me'.$video['channel']['avatars'][0]['path'], $dir_files.'/videofeed/channels/'.$file_info['filename'].'.jpg');
				}
			}
		}

		file_put_contents($dir_files.'/videofeed/feed.json', json_encode($arr));

		header("Location: ".url(''));
		exit;



	} else {

		require_once 'site-header.php';



		$videofeed = null;
		if(file_exists($dir_files.'/videofeed/feed.json')) {
			$count_videos = 0;
			$videofeed = json_decode(file_get_contents($dir_files.'/videofeed/feed.json'), true);
		}







		echo '<section id="start">';
			echo '<div class="about">';
				echo '<div class="avatar"></div>';
				echo '<div class="info">';
					foreach($lang['pages']['about']['about'] AS $content) {
						echo $Parsedown->text($content);
					}
				echo '</div>';
			echo '</div>';

			echo '<div class="slogan">';
				echo '<a href="'.url('slogan').'">';
					echo $lang['pages']['about']['slogan'];
				echo '</a>';
			echo '</div>';



			if(!empty($videofeed)) {
				echo '<div class="videos">';
					echo '<h2>'.svgicon('play') . $lang['pages']['about']['videofeed']['title'].'</h2>';

					echo '<div class="items">';
						foreach($videofeed AS $video) {
							$count_videos++;

							echo '<div>';
								echo link_('<div class="cover" style="background-image: url('.url('video-cover:'.$video['id'], true).');"><div class="channel" style="background-image: url('.url('channel-avatar:'.$video['channel'], true).');"></div></div>', $video['url']);
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';
			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>