<?php

	# Visa skillnaden mellan valt spår och andra, till exempel om medelhastigheten är snabbare än majoriteten

	require_once 'site-header.php';


	$files = glob($dir_files.'/tracks/json/*.json');
	$current_page = array_search($dir_files.'/tracks/json/'.strtotime($data['occurred']['date'].', '.$data['occurred']['time']).'.json', $files);
	$paging_next = ($current_page + 1);
	$paging_previous = ($current_page - 1);







	echo '<section id="track">';
		echo '<h1>';
			echo svgicon('bicycle');
			echo $data['occurred']['date'].', '.$data['occurred']['time'];
		echo '</h1>';

		echo '<div class="show-all logged'.($is_loggedin == true ? 'in' : 'out').'">';
			echo '<a href="'.url('biking').'" class="side-by-side">';
				echo svgicon('path') . $lang['pages']['biking']['details']['show-all-tracks'];
			echo '</a>';
		echo '</div>';

		if($is_loggedin == true) {
			echo '<div class="update">';
				echo '<a href="'.url('api/track:'.$data['occurred']['filename']).'">';
					echo svgicon('update') . $lang['pages']['uploaded']['options']['update'];
				echo '</a>';
			echo '</div>';

			echo '<div class="share">';
				echo svgicon('share');
				echo '[biking]'.strtotime($data['occurred']['date'].' '.$data['occurred']['time']).'[/biking]';
			echo '</div>';
		}



		echo '<div class="paging">';
			echo '<div class="previous">';
				if(isset($files[$paging_next])) {
					$next_info = pathinfo($files[$paging_next]);

					echo '<a href="'.url('biking-track:'.$next_info['filename']).'" class="side-by-side">';
						echo '<div class="desktop">'.svgicon('arrow-left') . $lang['pages']['blog']['read']['paging']['previous'].'</div>';
						echo '<div class="portable">'.svgicon('arrow-left').'</div>';
					echo '</a>';

				} else {
					echo '<div class="empty no-select">';
						echo '<div class="desktop">'.svgicon('arrow-left') . $lang['pages']['blog']['read']['paging']['previous'].'</div>';
						echo '<div class="portable">'.svgicon('arrow-left').'</div>';
					echo '</div>';
				}
			echo '</div>';

			echo '<div class="next">';
				if(isset($files[$paging_previous])) {
					$previous_info = pathinfo($files[$paging_previous]);

					echo '<a href="'.url('biking-track:'.$previous_info['filename']).'" class="side-by-side">';
						echo '<div class="desktop">'.$lang['pages']['blog']['read']['paging']['next'] . svgicon('arrow-right').'</div>';
						echo '<div class="portable">'.svgicon('arrow-right').'</div>';
					echo '</a>';

				} else {
					echo '<div class="empty no-select">';
						echo '<div class="desktop">'.$lang['pages']['blog']['read']['paging']['next'] . svgicon('arrow-right').'</div>';
						echo '<div class="portable">'.svgicon('arrow-right').'</div>';
					echo '</div>';
				}
			echo '</div>';
		echo '</div>';



		echo '<div class="items">';
			echo '<div class="item">';
				echo '<div class="label">';
					echo $lang['pages']['biking']['details']['movingtime'];
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo $data['movingtime']['hours'] . $lang['pages']['biking']['details']['hour'].' '.$data['movingtime']['minutes'] . $lang['pages']['biking']['details']['minute'];
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo $lang['pages']['biking']['details']['distance'];
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['distance']['km']).' km';
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo $lang['pages']['biking']['details']['speed-top'];
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['speed']['top']['kmh']).' km/h';
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo $lang['pages']['biking']['details']['speed-average'];
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['speed']['average']['kmh']).' km/h';
				echo '</div>';
			echo '</div>';


			if(isset($data['elevation'])) {
				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['pages']['biking']['details']['elevation-max'];
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value">';
						echo format_number($data['elevation']['max']['m']).' m';
					echo '</div>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['pages']['biking']['details']['elevation-min'];
					echo '</div>';

					echo '<div class="line"></div>';

					echo '<div class="value">';
						echo format_number($data['elevation']['min']['m']).' m';
					echo '</div>';
				echo '</div>';
			}
		echo '</div>';



		#echo '<img src="'.url('track:'.$data['occurred']['filename'], true).'" class="map">';
	echo '</section>';







	require_once 'site-footer.php';

?>
