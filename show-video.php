<?php

	require_once 'site-settings.php';



	$get_filename = (isset($_GET['nam']) ? safetag($_GET['nam']) : null);
	$get_extension = (isset($_GET['ext']) ? safetag($_GET['ext']) : null);

	$file = $dir_files.'/videos/'.$get_filename;

	if(file_exists($file.'.mp4')) {
		header('Content-Type: video/'.$get_extension);
		/*header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($file.'.mp4').'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');*/
		header('Content-Length: '.filesize($file.'.'.$get_extension));
		readfile($file.'.'.$get_extension);
		exit;

		/*echo '<video width="100%" height="100%" controls>';
			echo '<source src="'.url($file.'.mp4').'" type="video/mp4">';
			echo '<source src="'.url($file.'.ogv').'" type="video/ogv">';
			echo '<source src="'.url($file.'.webm').'" type="video/webm">';
			echo 'Your browser does not support the video tag.';
		echo '</video>';*/

	} else {
		echo 'File not exists.';
	}

?>
