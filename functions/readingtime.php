<?php

	function readingtime($text, $wps = 150, $pure = false) {
		$wps = (empty($wps) ? 150 : $wps);
		$words = str_word_count(strip_tags($text));
		$minutes = floor($words / $wps);
		$seconds = floor($words % $wps / ($wps / 60));

		if($pure == false) {
			if(1 <= $minutes) {
				$estimated_time = '~ '.$minutes.' min'.($minutes == 1 ? '' : 's');
			} else {
				$estimated_time = '< 1 min';
			}

		} else {
			$estimated_time = $minutes;
		}

		return $estimated_time;
	}

?>
