<?php

	function dirsize($path) {
		$bytestotal = 0;
		$path = realpath($path);

		if($path !== false AND $path != '' AND file_exists($path)) {
			foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) AS $object) {
				$bytestotal += $object->getSize();
			}
		}

		return $bytestotal;
	}

?>