<?php

	# Skapa länkar till rubriker och underrubriker

	use geertw\IpAnonymizer\IpAnonymizer;



	# Set words per minute (WPM)
	if(isset($_POST['recalculate'])) {

		require_once 'site-settings.php';

		$url_parsed = parse_url($uri);
		$url_strip = (empty($config_folder) ? ltrim($url_parsed['path'], '/') : ltrim($url_parsed['path'], '/airikr/'));

		$get_option = (is_numeric($_POST['wpm']) ? safetag($_POST['wpm']) : null);


		if(empty($get_option)) {
			die(simplepage($lang['messages']['onlydigits-wpm'].'. <a href="'.url($url_strip).'">'.$lang['goback'].'</a>'));

		} else {
			header("Location: ".url($url_strip, false, null, null, null, 'wpm:'.ltrim($get_option, 0)));
			exit;
		}



	# Handle reactions
	} elseif(isset($_GET['re'])) {

		require_once 'site-settings.php';

		$anonymize_ip = new IpAnonymizer();

		$url_parsed = parse_url($uri);
		$url_strip = (empty($config_folder) ? ltrim($url_parsed['path'], '/') : ltrim($url_parsed['path'], '/airikr/'));

		list($page, $datetime, $reaction) = explode('/', $url_strip);

		$get_reaction = ($_GET['re'] ? safetag($_GET['re']) : null);
		$ipaddress = md5($anonymize_ip->anonymize(getip()));
		$file_reactions = $dir_files.'/posts/reactions/'.$datetime.'.json';
		$file_telemetry = $dir_files.'/posts/reactions/'.$datetime.'-'.time().'-'.$ipaddress.'-'.$get_reaction;
		$reaction = (!file_exists($file_reactions) ? null : json_decode(file_get_contents($file_reactions), true));
		$visitor_reacted = false;

		foreach(glob($dir_files.'/posts/reactions/'.$datetime.'-*') AS $chosen_reaction) {
			$fileinfo = pathinfo($chosen_reaction);

			if(strpos($fileinfo['filename'], $ipaddress) !== false) {
				list($dt, $ts, $ip, $re) = explode('-', $fileinfo['filename']);
				$visitor_reacted = true;
			}
		}


		if($visitor_reacted == true) {
			unlink($dir_files.'/posts/reactions/'.$dt.'-'.$ts.'-'.$ip.'-'.$re);

			if($re != $get_reaction) {
				file_put_contents($file_telemetry, $useragent);
			}
		} else {
			file_put_contents($file_telemetry, $useragent);
		}


		header("Location: ".url($page.'/'.$datetime));
		exit;



	# Mark the post as read
	} elseif(isset($_GET['mar'])) {

		require_once 'site-settings.php';

		$anonymize_ip = new IpAnonymizer();
		$ipaddress = md5($anonymize_ip->anonymize(getip()));
		$datetime = safetag($_GET['ye']) . safetag($_GET['mo']) . safetag($_GET['da']) . safetag($_GET['ho']) . safetag($_GET['mi']);
		$file = $dir_files.'/posts/marked-as-read/'.$datetime.'-'.$ipaddress;

		file_put_contents($file, $useragent);

		header("Location: ".url('blog/'.$datetime));
		exit;



	# Mark the post as not read
	} elseif(isset($_GET['maur'])) {

		require_once 'site-settings.php';

		$anonymize_ip = new IpAnonymizer();
		$ipaddress = md5($anonymize_ip->anonymize(getip()));
		$datetime = safetag($_GET['ye']) . safetag($_GET['mo']) . safetag($_GET['da']) . safetag($_GET['ho']) . safetag($_GET['mi']);
		$file = $dir_files.'/posts/marked-as-read/'.$datetime.'-'.$ipaddress;

		unlink($file);

		header("Location: ".url('blog/'.$datetime));
		exit;



	# Create JSON-file and remove the NSFW mark
	} elseif(isset($_GET['ren'])) {

		require_once 'site-settings.php';

		$datetime = safetag($_GET['ye']) . safetag($_GET['mo']) . safetag($_GET['da']) . safetag($_GET['ho']) . safetag($_GET['mi']);
		$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$datetime.'.json'), false);
		$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);

		$anonymize_ip = new IpAnonymizer();
		$ipaddress = md5($anonymize_ip->anonymize(getip()));

		$arr = [
			'opened' => time()
		];

		file_put_contents($dir_files.'/posts/removed-nsfw-mark/'.$datetime.'-'.$ipaddress.'.json', json_encode($arr));

		header("Location: ".url('blog/'.$datetime.'/'.mb_strtolower(seo(trim($subject)))));
		exit;



	# Delete the file for the NSFW post
	} elseif(isset($_GET['rmn'])) {

		require_once 'site-settings.php';

		$datetime = safetag($_GET['ye']) . safetag($_GET['mo']) . safetag($_GET['da']) . safetag($_GET['ho']) . safetag($_GET['mi']);
		$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$datetime.'.json'), false);
		$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);

		$anonymize_ip = new IpAnonymizer();
		$ipaddress = md5($anonymize_ip->anonymize(getip()));

		$arr = [
			'opened' => time()
		];

		unlink($dir_files.'/posts/removed-nsfw-mark/'.$datetime.'-'.$ipaddress.'.json');

		header("Location: ".url('blog/'.$datetime.'/'.mb_strtolower(seo(trim($subject)))));
		exit;



	# Remove everything that has been stored in session cookies
	/*} elseif(isset($_GET['cnl']) AND $_GET['cnl'] == 'comment') {

		require_once 'site-settings.php';

		unset($_SESSION['name']);
		unset($_SESSION['email']);
		unset($_SESSION['website']);
		unset($_SESSION['comment']);

		$datetime = safetag($_GET['ye']) . safetag($_GET['mo']) . safetag($_GET['da']) . safetag($_GET['ho']) . safetag($_GET['mi']);
		$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$datetime.'.json'), false);
		$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);

		header("Location: ".url('blog/'.$datetime.'/'.mb_strtolower(seo(trim($subject))).'#comments'));
		exit;



	} elseif(isset($_POST['send-comment'])) {

		require_once 'site-settings.php';

		$post_name = safetag($_POST['name']);
		$post_email = safetag($_POST['email']);
		$post_website = safetag($_POST['website']);
		$post_password = safetag($_POST['password']);
		$post_comment = safetag($_POST['comment']);

		$uniqueid = uniqid();
		$file = $dir_files.'/posts/comments/'.$datetime.'-'.$uniqueid.'.json';


		if(empty($post_name) OR empty($post_email) OR empty($post_comment)) {
			$_SESSION['name'] = (empty($post_name) ? null : $post_name);
			$_SESSION['email'] = (empty($post_email) ? null : $post_email);
			$_SESSION['website'] = (empty($post_website) ? null : $post_website);
			$_SESSION['comment'] = (empty($post_comment) ? null : $post_comment);

			$error = null;
			foreach($lang['messages']['errors']['empty-fields'] AS $content) {
				$error .= $Parsedown->text($content);
			}

			$arr_requiredfields = [
				$lang['pages']['blog']['comment']['form']['name'],
				$lang['pages']['blog']['comment']['form']['email'],
				$lang['pages']['blog']['comment']['form']['comment']
			];

			die(simplepage('<div class="error">'.$error.'<ul><li>'.implode('</li><li>', $arr_requiredfields).'</li></ul></div><div class="go-back"><a href="'.url('blog/'.$datetime).'">'.$lang['goback'].'</a></div>'));


		} else {
			if(!filter_var($post_email, FILTER_VALIDATE_EMAIL)) {
				$_SESSION['name'] = (empty($post_name) ? null : $post_name);
				$_SESSION['email'] = (empty($post_email) ? null : $post_email);
				$_SESSION['website'] = (empty($post_website) ? null : $post_website);
				$_SESSION['comment'] = (empty($post_comment) ? null : $post_comment);

				$error = null;
				foreach($lang['messages']['errors']['notvalid-email'] AS $content) {
					$error .= $Parsedown->text($content);
				}

				die(simplepage('<div class="error">'.$error.'</div><div class="go-back"><a href="'.url('blog/'.$datetime).'">'.$lang['goback'].'</a></div>'));


			} elseif(!empty($post_website) AND !filter_var($post_website, FILTER_VALIDATE_URL)) {
				$_SESSION['name'] = (empty($post_name) ? null : $post_name);
				$_SESSION['email'] = (empty($post_email) ? null : $post_email);
				$_SESSION['website'] = (empty($post_website) ? null : $post_website);
				$_SESSION['comment'] = (empty($post_comment) ? null : $post_comment);

				$error = null;
				foreach($lang['messages']['errors']['notvalid-url'] AS $content) {
					$error .= $Parsedown->text($content);
				}

				die(simplepage('<div class="error">'.$error.'</div><div class="go-back"><a href="'.url('blog/'.$datetime).'">'.$lang['goback'].'</a></div>'));


			} else {
				$arr_comment = [
					'id' => $uniqueid,
					'id-comment' => null,
					'ip' => endecrypt(getip()),
					'about' => [
						'name' => $post_name,
						'email' => endecrypt($post_email),
						'website' => $post_website
					],
					'password' => password_hash($post_password, PASSWORD_BCRYPT),
					'comment' => $post_comment,
					'published' => time()
				];

				file_put_contents($file, json_encode($arr_comment));

				unset($_SESSION['name']);
				unset($_SESSION['email']);
				unset($_SESSION['website']);
				unset($_SESSION['comment']);

				$datetime = safetag($_GET['ye']) . safetag($_GET['mo']) . safetag($_GET['da']) . safetag($_GET['ho']) . safetag($_GET['mi']);
				$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$datetime.'.json'), false);
				$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);

				header("Location: ".url('blog/'.$datetime.'/'.mb_strtolower(seo(trim($subject))).'#comment-'.$uniqueid));
				exit;
			}
		}



	} elseif(isset($_POST['save-comment'])) {

		require_once 'site-settings.php';

		$get_uniqueid = safetag($_GET['cmt']);

		$post_name = safetag($_POST['name']);
		$post_email = safetag($_POST['email']);
		$post_website = safetag($_POST['website']);
		$post_comment = safetag($_POST['comment']);
		$post_password = safetag($_POST['password']);

		$file = $dir_files.'/posts/comments/'.$datetime.'-'.$get_uniqueid.'.json';
		$edit = json_decode(file_get_contents($dir_files.'/posts/comments/'.$datetime.'-'.$get_uniqueid.'.json'), false);


		if(empty($post_name) OR empty($post_comment) OR empty($post_password)) {
			$_SESSION['name'] = (empty($post_name) ? null : $post_name);
			$_SESSION['email'] = (empty($post_email) ? null : $post_email);
			$_SESSION['website'] = (empty($post_website) ? null : $post_website);
			$_SESSION['comment'] = (empty($post_comment) ? null : $post_comment);

			$error = null;
			foreach($lang['messages']['errors']['empty-fields'] AS $content) {
				$error .= $Parsedown->text($content);
			}

			$arr_requiredfields = [
				$lang['pages']['blog']['comment']['form']['name'],
				$lang['pages']['blog']['comment']['form']['email'],
				$lang['pages']['blog']['comment']['form']['comment'],
				$lang['pages']['blog']['comment']['form']['password']
			];

			die(simplepage('<div class="error">'.$error.'<ul><li>'.implode('</li><li>', $arr_requiredfields).'</li></ul></div><div class="go-back"><a href="'.url('blog/'.$datetime.'/comment/'.$get_uniqueid.'/edit').'">'.$lang['goback'].'</a></div>'));


		} else {
			if(!empty($post_email) AND !filter_var($post_email, FILTER_VALIDATE_EMAIL)) {
				$_SESSION['name'] = (empty($post_name) ? null : $post_name);
				$_SESSION['email'] = (empty($post_email) ? null : $post_email);
				$_SESSION['website'] = (empty($post_website) ? null : $post_website);
				$_SESSION['comment'] = (empty($post_comment) ? null : $post_comment);

				$error = null;
				foreach($lang['messages']['errors']['notvalid-email'] AS $content) {
					$error .= $Parsedown->text($content);
				}

				die(simplepage('<div class="error">'.$error.'</div><div class="go-back"><a href="'.url('blog/'.$datetime.'/comment/'.$get_uniqueid.'/edit').'">'.$lang['goback'].'</a></div>'));


			} elseif(!empty($post_website) AND !filter_var($post_website, FILTER_VALIDATE_URL)) {
				$_SESSION['name'] = (empty($post_name) ? null : $post_name);
				$_SESSION['email'] = (empty($post_email) ? null : $post_email);
				$_SESSION['website'] = (empty($post_website) ? null : $post_website);
				$_SESSION['comment'] = (empty($post_comment) ? null : $post_comment);

				$error = null;
				foreach($lang['messages']['errors']['notvalid-url'] AS $content) {
					$error .= $Parsedown->text($content);
				}

				die(simplepage('<div class="error">'.$error.'</div><div class="go-back"><a href="'.url('blog/'.$datetime.'/comment/'.$get_uniqueid.'/edit').'">'.$lang['goback'].'</a></div>'));


			} elseif(!password_verify($post_password, $edit->password)) {
				$_SESSION['name'] = (empty($post_name) ? null : $post_name);
				$_SESSION['email'] = (empty($post_email) ? null : $post_email);
				$_SESSION['website'] = (empty($post_website) ? null : $post_website);
				$_SESSION['comment'] = (empty($post_comment) ? null : $post_comment);

				die(simplepage('<div class="error">'.$lang['messages']['errors']['wrong-password'].'</div><div class="go-back"><a href="'.url('blog/'.$datetime.'/comment/'.$get_uniqueid.'/edit').'">'.$lang['goback'].'</a></div>'));


			} else {
				$arr_comment = [
					'id' => $edit->id,
					'id-comment' => null,
					'ip' => endecrypt(getip()),
					'about' => [
						'name' => $post_name,
						'email' => (empty($post_email) ? $edit->about->email : endecrypt($post_email)),
						'website' => $post_website
					],
					'password' => $edit->password,
					'comment' => $post_comment,
					'published' => $edit->published,
					'edited' => time()
				];

				file_put_contents($file, json_encode($arr_comment));

				unset($_SESSION['name']);
				unset($_SESSION['email']);
				unset($_SESSION['website']);
				unset($_SESSION['comment']);

				$datetime = safetag($_GET['ye']) . safetag($_GET['mo']) . safetag($_GET['da']) . safetag($_GET['ho']) . safetag($_GET['mi']);
				$post = json_decode(file_get_contents($dir_files.'/posts/published/'.$datetime.'.json'), false);
				$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);

				header("Location: ".url('blog/'.$datetime.'/'.mb_strtolower(seo(trim($subject))).'#comment-'.$edit->id));
				exit;
			}
		}*/



	} else {

		require_once 'site-header.php';







		if(!file_exists($dir_files.'/posts/published/'.$datetime.'.json')) {
			header("Location: ".url('blog'));
			exit;


		} else {
			$get_wpm = (!isset($_GET['wpm']) ? 150 : safetag($_GET['wpm']));

			$anonymize_ip = new IpAnonymizer();
			$ipaddress = md5($anonymize_ip->anonymize(getip()));
			$file_reaction = $dir_files.'/posts/reactions/'.$datetime.'.json';
			$files = glob($dir_files.'/posts/published/*.json');
			$current_page = array_search($dir_files.'/posts/published/'.$datetime.'.json', $files);
			$paging_next = ($current_page + 1);
			$paging_previous = ($current_page - 1);
			$reactions = count(glob($dir_files.'/posts/reactions/'.$datetime.'/*'));
			$subject = ($get_lang == 'se' ? $post->subject->se : $post->subject->en);
			$share_text = ((!isset($post->hundreddaystooffload->enabled) OR $post->hundreddaystooffload->enabled == false) ? '' : '%0A%0A'.urlencode('Post no. '.$post->hundreddaystooffload->day.' of 100 of #100DaysToOffload'));
			$share_url = urlencode($site_url.'/blog/'.$datetime.'/'.mb_strtolower(seo(trim($subject))).'?lang='.$lang['metadata']['language']) . $share_text;
			$share_string = $share_url;
			$is_nsfw = (isset($post->nsfw) ? $post->nsfw : null);
			$removednsfwmark = ((empty($is_nsfw) OR $is_nsfw == false) ? null : (file_exists($dir_files.'/posts/removed-nsfw-mark/'.$datetime.'-'.$ipaddress.'.json') ? true : false));

			$emoji_happy = '0';
			$emoji_laugh = '0';
			$emoji_sad = '0';
			$emoji_angry = '0';
			$emoji_thumbup = '0';
			$emoji_thumbdown = '0';
			$emoji_heart = '0';

			preg_match_all('/<h\d>([^<]*)<\/h\d>/iU', tags($Parsedown->text($metadata_description)), $titles);
			$count_titles = count($titles[0]);

			$anonymize_ip = new IpAnonymizer();
			$visitor_reacted = false;
			$count_reactions = 0;
			$count_readers = 0;

			foreach(glob($dir_files.'/posts/marked-as-read/'.$datetime.'-*') AS $read) {
				$count_readers++;
			}

			foreach(glob($dir_files.'/posts/reactions/'.$datetime.'-*') AS $chosen_reaction) {
				$fileinfo = pathinfo($chosen_reaction);
				$count_reactions++;

				$emoji_happy += substr_count($fileinfo['filename'], 'happy');
				$emoji_laugh += substr_count($fileinfo['filename'], 'laugh');
				$emoji_sad += substr_count($fileinfo['filename'], 'sad');
				$emoji_angry += substr_count($fileinfo['filename'], 'angry');
				$emoji_thumbup += substr_count($fileinfo['filename'], 'thumbup');
				$emoji_thumbdown += substr_count($fileinfo['filename'], 'thumbdown');
				$emoji_heart += substr_count($fileinfo['filename'], 'heart');

				if(strpos($fileinfo['filename'], md5($anonymize_ip->anonymize(getip()))) !== false) {
					list($dt, $ts, $ip, $re) = explode('-', $fileinfo['filename']);
					$visitor_reacted = true;
				}
			}

			$count_comments = 0;

			foreach(glob($dir_files.'/posts/comments/'.$datetime.'-*') AS $comment) {
				$count_comments++;
			}

			$arr_countreactions = [
				'happy' => $emoji_happy,
				'laugh' => $emoji_laugh,
				'sad' => $emoji_sad,
				'angry' => $emoji_angry,
				'thumbup' => $emoji_thumbup,
				'thumbdown' => $emoji_thumbdown,
				'heart' => $emoji_heart
			];



			echo '<section id="blog-post">';
				echo '<h1>'.$subject.'</h1>';

				echo '<div class="info side-by-side">';
					echo '<div class="avatar-n-published side-by-side">';
						echo '<a href="'.url('').'">';
							echo '<div class="avatar" style="background-image: url('.url($dir_images.'/selfie.webp').');"></div>';
						echo '</a>';

						echo '<div class="published">';
							echo '<a href="'.url('blog/year:'.$get_year).'" class="filter" title="'.$lang['tooltips']['filter-by-year'].'">'.$get_year.'</a>';
							echo '-';
							echo '<a href="'.url('blog/month:'.(int)$get_month).'" class="filter" title="'.$lang['tooltips']['filter-by-month'].'">'.$get_month.'</a>';
							echo '-';
							echo '<a href="'.url('blog/date:'.$get_year . $get_month . $get_day).'" class="filter" title="'.$lang['tooltips']['filter-by-date'].'">'.$get_day.'</a>';
							echo ', '.$get_hour.':'.$get_minute;
						echo '</div>';
					echo '</div>';

					echo '<div class="rd-n-edit side-by-side">';
						echo '<div class="wall"></div>';

						echo '<div class="word-count">';
							echo format_number(str_word_count($metadata_description), 0).' '.mb_strtolower((str_word_count($metadata_description) == 1 ? $lang['pages']['blog']['read']['word'] : $lang['pages']['blog']['read']['words']));
							echo ', '.$get_wpm.' WPM';
						echo '</div>';

						echo '<div class="reading-time">';
							echo readingtime($metadata_description, $get_wpm);
						echo '</div>';


						if($is_loggedin == true) {
							echo '<div class="wall admin"></div>';

							echo '<div class="edit">';
								echo '<a href="'.url('blog/edit:'.$get_year . $get_month . $get_day . $get_hour . $get_minute).'">';
									echo $lang['pages']['blog']['read']['edit'];
								echo '</a>';
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';

				echo '<div class="info-small">';
					echo '<div class="info">';
						echo '<div>';
							echo (empty($count_readers) ? 0 : format_number($count_readers));
							echo ' '.mb_strtolower($lang['pages']['blog']['read']['readstatus']['reader'.($count_readers == 1 ? '' : 's')]);
						echo '</div>';
					echo '</div>';

					echo '<div class="shortcuts">';
						echo '<a href="#reactions">'.$count_reactions.' '.mb_strtolower($lang['pages']['blog']['read']['reaction'.($count_reactions == 1 ? '' : 's')]).'</a>';
						#echo '<a href="#comments">'.$count_comments.' '.mb_strtolower($lang['pages']['blog']['read']['comment'.($count_comments == 1 ? '' : 's')]).'</a>';
					echo '</div>';
				echo '</div>';



				if(empty($is_nsfw) OR $is_nsfw == false OR $is_nsfw == true AND $removednsfwmark == true) {
					if($removednsfwmark == true) {
						echo '<div class="restore-nsfw">';
							echo '<a href="'.url('blog/'.$datetime.'/restore-nsfw-mark').'">';
								echo svgicon('shield') . $lang['pages']['blog']['read']['nsfw']['hide-post'];
							echo '</a>';
						echo '</div>';
					}

					if($lang['metadata']['language'] == 'en') {
						echo '<div class="disclaimer using-translation">';
							echo '<p>This post has been translated into English using either DeepL or Google Translate. Some words and/or sentences may therefore be a little strange.</p>';
						echo '</div>';
					}

					if(isset($post->notvalid) AND $post->notvalid == true) {
						echo '<div class="disclaimer'.($lang['metadata']['language'] == 'en' ? ' translation-notice-above' : '').'">';
							foreach($lang['messages']['disclaimers']['post-not-valid'] AS $notvalid) {
								echo $Parsedown->text($notvalid);
							}
						echo '</div>';

					} elseif(strtotime($get_year . $get_month . $get_day . $get_hour . $get_minute) < strtotime('-2 year')) {
						echo '<div class="disclaimer'.($lang['metadata']['language'] == 'en' ? ' translation-notice-above' : '').'">';
							foreach($lang['messages']['disclaimers']['old-post'] AS $notvalid) {
								echo $Parsedown->text($notvalid);
							}
						echo '</div>';
					}
				}



				if(empty($is_nsfw) OR $is_nsfw == false OR $is_nsfw == true AND $removednsfwmark == true) {

					if((isset($post->hundreddaystooffload) AND $post->hundreddaystooffload->enabled == true)) {
						echo '<div class="cover" style="background-image: url('.url($dir_images.'/100DaysToOffload.webp', true).');"></div>';

					} elseif(isset($post->updatelog) AND $post->updatelog == true) {
						echo '<div class="cover" style="background-image: url('.url($dir_images.'/metadata-updatelog.webp', true).');"></div>';
		
					} elseif(!empty($is_nsfw) AND $is_nsfw == true AND $removednsfwmark == true) {
						echo '<div class="cover" style="background-image: url('.url('cover:'.$datetime, true).');"></div>';

					} elseif(file_exists($dir_files.'/uploads/covers/'.$datetime.'.webp')) {
						echo '<div class="cover" style="background-image: url('.url('cover:'.$datetime, true).');"></div>';
					}

					/*if($count_titles > 0) {
						echo '<details>';
							echo '<summary class="no-select">'.$lang['pages']['blog']['read']['table-of-content'].'</summary>';

							echo '<div class="titles">';
								foreach($titles[0] AS $title) {
									echo '<a href="#'.mb_strtolower(seo(strip_tags(trim($title)))).'">';
										echo strip_tags($title);
									echo '</a>';
								}
							echo '</div>';
						echo '</details>';
					}*/

					echo '<div class="post">';
						echo tags($Parsedown->text(($get_lang == 'se' ? $post->content->se : $post->content->en)));
					echo '</div>';

					echo '<div class="side-by-side">';
						echo '<div class="hundreddaystooffload side-by-side">';
							if(isset($post->hundreddaystooffload) AND $post->hundreddaystooffload->enabled == true) {
								echo '<a href="'.url('blog/100daystooffload').'">';
									echo svgicon('hashtag');
									echo $lang['pages']['blog']['read']['hundreddaystooffload'];
								echo '</a>';

								echo '<div class="day">';
									echo '<span class="zero no-select">';
										echo ($post->hundreddaystooffload->day < 10 ? '00' : ($post->hundreddaystooffload->day < 100 ? '0' : ''));
									echo '</span>';
									echo $post->hundreddaystooffload->day.' '.mb_strtolower($lang['pages']['blog']['read']['of']).' 100';
								echo '</div>';
							}
						echo '</div>';

						echo '<div class="no-ai">';
							echo link_('<img src="'.url($dir_images.'/written-by-human-'.($get_theme == 'light' ? 'white' : 'black').'-'.$lang['metadata']['language'].'.svg').'" height="41" width="128">', 'https://notbyai.fyi/');
						echo '</div>';
					echo '</div>';



					if($badvisitor == false) {
						echo '<div class="reactions" id="reactions">';
							if($notchecked == true) {
								echo '<div class="emojis">';
									foreach($arr_reactions AS $reaction) {
										echo '<div>';
											echo svgicon('emoji-'.$reaction['emoji']);
											echo '<div class="count">';
												echo (empty($arr_countreactions[$reaction['emoji']]) ? '0' : $arr_countreactions[$reaction['emoji']]);
											echo '</div>';
										echo '</div>';
									}
								echo '</div>';

								echo '<div class="reason">';
									foreach($lang['pages']['blog']['reactions']['notchecked'] AS $content) {
										echo $Parsedown->text($content);
									}
								echo '</div>';


							} else {
								echo '<div class="emojis">';
									foreach($arr_reactions AS $reaction) {
										echo '<a href="'.url('blog/'.$datetime.'/reaction:'.$reaction['emoji']).'" ';
										echo (($visitor_reacted == true AND $re == $reaction['emoji']) ? 'class="chosen" title="'.$lang['tooltips']['your-reaction'].': '.mb_strtolower($reaction['title']).'"' : 'title="'.$reaction['title'].'"');
										echo '>';
											echo svgicon('emoji-'.$reaction['emoji']);
											echo '<div class="count">';
												echo (empty($arr_countreactions[$reaction['emoji']]) ? '0' : $arr_countreactions[$reaction['emoji']]);
											echo '</div>';
										echo '</a>';
									}
								echo '</div>';
							}
						echo '</div>';



						echo '<div class="handle-read-status" id="read-status">';
							echo '<h2>'.$lang['pages']['blog']['read']['readstatus']['title'].'</h2>';

							echo '<div class="description">';
								foreach($lang['pages']['blog']['read']['readstatus']['desc'] AS $desc) {
									echo $Parsedown->text($desc);
								}
							echo '</div>';

							if(!file_exists($dir_files.'/posts/marked-as-read/'.$datetime.'-'.$ipaddress)) {
								echo '<a href="'.url('blog/'.$datetime.'/mark-as-read#read-status').'">';
									echo svgicon('eye') . $lang['pages']['blog']['read']['readstatus']['options']['read'];
								echo '</a>';

							} else {
								echo '<a href="'.url('blog/'.$datetime.'/mark-as-unread#read-status').'">';
									echo svgicon('eye-closed') . $lang['pages']['blog']['read']['readstatus']['options']['unread'];
								echo '</a>';
							}
						echo '</div>';



						echo '<div class="share">';
							echo '<h2>'.$lang['pages']['blog']['read']['share']['title'].'</h2>';

							echo '<div class="side-by-side">';
								echo '<div class="mastodon">';
									echo '<div>';
										echo '<a href="https://mst.airikr.me/share?text='.$share_string.'">';
											echo svgicon('brand-mastodon').'mst.airikr.me';
										echo '</a>';
									echo '</div>';

									echo '<div>';
										echo '<a href="https://fosstodon.org/share?text='.$share_string.'">';
											echo svgicon('brand-mastodon').'fosstodon.org';
										echo '</a>';
									echo '</div>';

									echo '<div>';
										echo '<a href="https://mastodon.nu/share?text='.$share_string.'">';
											echo svgicon('brand-mastodon').'mastodon.nu';
										echo '</a>';
									echo '</div>';

									echo '<div class="other-instance">';
										echo svgicon('brand-mastodon');
										echo '<form action="'.url('share-to-custom-instance').'" method="POST" autocomplete="off" novalidate>';
											echo '<input type="hidden" name="hidden-string" value="'.$share_string.'">';

											echo '<input type="text" name="protocol" value="https://" readonly>';
											echo '<input type="url" name="instance">';
											echo '<input type="submit" name="share" value="OK">';
										echo '</form>';
									echo '</div>';
								echo '</div>';


								echo '<div class="other">';
									echo '<div class="bluesky">';
										echo '<a href="https://bsky.app/intent/compose?text='.$share_string.'">';
											echo svgicon('brand-bluesky').'bsky.app';
										echo '</a>';
									echo '</div>';

									echo '<div class="threads">';
										echo '<a href="https://threads.net/intent/post?text='.$share_string.'">';
											echo svgicon('brand-threads').'threads.net';
										echo '</a>';
									echo '</div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					}



					echo '<div class="paging">';
						echo '<div class="previous">';
							if(isset($files[$paging_next])) {
								$next_info = pathinfo($files[$paging_next]);

								echo '<a href="'.url('blog/'.$next_info['filename']).'" class="side-by-side">';
									echo '<div class="desktop">'.$lang['pages']['blog']['read']['paging']['previous'].'</div>';
									echo '<div class="portable">'.svgicon('arrow-left').'</div>';
								echo '</a>';

							} else {
								echo '<div class="empty no-select">';
									echo '<div class="desktop">'.$lang['pages']['blog']['read']['paging']['previous'].'</div>';
									echo '<div class="portable">'.svgicon('arrow-left').'</div>';
								echo '</div>';
							}
						echo '</div>';

						echo '<div class="show-all">';
							echo '<a href="'.url('blog').'" class="side-by-side">';
								echo '<div class="desktop">'.$lang['pages']['blog']['read']['paging']['showall'].'</div>';
								echo '<div class="portable">'.svgicon('list').'</div>';
							echo '</a>';
						echo '</div>';

						echo '<div class="random">';
							echo '<a href="'.url('blog/'.$datetime.'/random-post').'" class="side-by-side">';
								echo '<div class="desktop">'.$lang['pages']['blog']['read']['paging']['random'].'</div>';
								echo '<div class="portable">'.svgicon('random').'</div>';
							echo '</a>';
						echo '</div>';

						echo '<div class="next">';
							if(isset($files[$paging_previous])) {
								$previous_info = pathinfo($files[$paging_previous]);

								echo '<a href="'.url('blog/'.$previous_info['filename']).'" class="side-by-side">';
									echo '<div class="desktop">'.$lang['pages']['blog']['read']['paging']['next'].'</div>';
									echo '<div class="portable">'.svgicon('arrow-right').'</div>';
								echo '</a>';

							} else {
								echo '<div class="empty no-select">';
									echo '<div class="desktop">'.$lang['pages']['blog']['read']['paging']['next'].'</div>';
									echo '<div class="portable">'.svgicon('arrow-right').'</div>';
								echo '</div>';
							}
						echo '</div>';
					echo '</div>';



					#require_once 'page-blog-post-comments.php';



					/*echo '<div class="webmentions">';
						echo '<h2>Webmentions</h2>';

						foreach($lang['pages']['blog']['read']['webmentions'] AS $content) {
							echo $Parsedown->text($content);
						}


						if(!file_exists($dir_files.'/posts/webmentions/'.$datetime)) {
							echo '<div class="message">';
								echo $lang['messages']['no-webmentions'];
							echo '</div>';

						} else {
							$webmentions = json_decode(file_get_contents($dir_files.'/posts/webmentions/'.$datetime.'/webmention.json'), false);

							echo '<div class="favourites">';
								echo svgicon('favourite');

								$c_favourites = 0;
								foreach($webmentions AS $favourite) {
									if($favourite->type == 'favourite') {
										$c_favourites++;

										echo '<a href="'.$favourite->url.'" title="Gå till '.$favourite->name.' profil"';
										echo ($c_favourites == 1 ? ' class="first"' : '');
										echo '>';
											echo '<img src="'.url('webmention:'.seo($favourite->name), true).'">';
										echo '</a>';
									}
								}
							echo '</div>';

							echo '<div class="boosts">';
								echo svgicon('boost');

								$c_boosts = 0;
								foreach($webmentions AS $boost) {
									if($boost->type == 'boost') {
										$c_boosts++;

										echo '<a href="'.$boost->url.'" title="Gå till '.$boost->name.' profil"';
										echo ($c_boosts == 1 ? ' class="first"' : '');
										echo '>';
											echo '<img src="'.url('webmention:'.seo($boost->name), true).'">';
										echo '</a>';
									}
								}
							echo '</div>';

							echo '<div class="replies">';
								$c_replies = 0;
								foreach($webmentions AS $reply) {
									if($reply->type == 'reply') {
										$c_replies++;

										echo '<div class="reply">';
											echo '<div class="info">';
												echo '<div class="avatar" style="background-image: url('.url('webmention:'.seo($reply->name), true).');"></div>';
												echo '<div class="info">';
													echo '<div class="name">';
														echo $reply->name;
													echo '</div>';

													echo '<div class="published">';
														echo date_($reply->reply_sent, 'datetime');
													echo '</div>';
												echo '</div>';
											echo '</div>';

											echo '<div class="content">';
												echo $reply->reply;
											echo '</div>';
										echo '</div>';
									}
								}
							echo '</div>';
						}
					echo '</div>';*/



				} else {

					echo '<div class="nsfw">';
						echo '<div class="icon">'.svgicon('shield').'</div>';
						foreach($lang['pages']['blog']['read']['nsfw']['content'] AS $content) {
							echo $Parsedown->text($content);
						}
					echo '</div>';

				}
			echo '</section>';
		}







		require_once 'site-footer.php';

	}

?>
