<?php

	require_once 'site-header.php';







	echo '<section id="sitemap">';
		echo '<h1>'.$lang['pages']['sitemap']['title'].'</h1>';

		foreach($lang['pages']['sitemap']['content'] AS $content) {
			echo $Parsedown->text($content);
		}


		echo '<hr>';


		echo '<div class="sitemap">';
			echo '<div class="level-1">';
				echo '<a href="'.url('').'">';
					echo $lang['nav']['about'];
				echo '</a>';

				echo '<div class="level-2">';
					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('slogan').'">';
							echo $lang['pages']['slogan']['title'];
						echo '</a>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('blog').'">';
					echo $lang['nav']['blog'];
				echo '</a>';

				echo '<div class="level-2">';
					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('blog/reactions').'">';
							echo $lang['pages']['blog']['subtitles']['filter']['reactions'];
						echo '</a>';
					echo '</div>';

					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('blog/100daystooffload').'">';
							echo $lang['pages']['blog']['subtitles']['filter']['hundreddaystooffload'];
						echo '</a>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('gallery').'">';
					echo $lang['nav']['gallery'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('biking').'">';
					echo $lang['nav']['biking'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('links').'">';
					echo $lang['nav']['links'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('guestbook').'">';
					echo $lang['nav']['guestbook'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('rss').'">';
					echo $lang['footer']['rss'];
				echo '</a>';

				echo '<div class="level-2">';
					foreach($lang['pages']['rss']['list'] AS $list) {
						echo '<div>';
							echo '<span class="level no-select">-</span>';
							echo '<a href="'.$list['url'].'">';
								echo $list['name'];
							echo '</a>';
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('contact').'">';
					echo $lang['footer']['contact'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('colophone').'">';
					echo $lang['footer']['colophone'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('ai').'">';
					echo $lang['footer']['ai'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('privacy').'">';
					echo $lang['footer']['privacy'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('copyright').'">';
					echo $lang['footer']['creative-commons'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('chat').'">';
					echo $lang['footer']['chat'];
				echo '</a>';

				echo '<div class="level-2">';
					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('chat/rules').'">';
							echo $lang['pages']['chat']['rules']['title'];
						echo '</a>';
					echo '</div>';

					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('chat/join').'">';
							echo $lang['pages']['chat']['guide']['title'];
						echo '</a>';
					echo '</div>';

					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('chat/faq').'">';
							echo $lang['pages']['chat']['faq']['title'];
						echo '</a>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('admin').'">';
					echo $lang['footer']['login'];
				echo '</a>';
			echo '</div>';



			echo '<h2>Gömda</h2>';

			echo '<div class="level-1">';
				echo '<a href="'.url('metadata').'">';
					echo $lang['pages']['sitemap']['metadata'];
				echo '</a>';
			echo '</div>';

			echo '<div class="level-1">';
				echo '<a href="'.url('uses').'">';
					echo $lang['pages']['sitemap']['uses'];
				echo '</a>';

				echo '<div class="level-2">';
					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('uses/desktop').'">';
							echo $lang['pages']['uses']['pc']['desktop'];
						echo '</a>';
					echo '</div>';

					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('uses/laptop').'">';
							echo $lang['pages']['uses']['pc']['laptop'];
						echo '</a>';
					echo '</div>';

					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('uses/softwares_pc').'">';
							echo $lang['pages']['uses']['softwares']['pc'];
						echo '</a>';
					echo '</div>';

					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('uses/softwares_phone').'">';
							echo $lang['pages']['uses']['softwares']['phone'];
						echo '</a>';
					echo '</div>';

					echo '<div>';
						echo '<span class="level no-select">-</span>';
						echo '<a href="'.url('uses/selfhosting').'">';
							echo $lang['pages']['uses']['softwares']['selfhosting'];
						echo '</a>';
					echo '</div>';
				echo '</div>';
			echo '</div>';
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
