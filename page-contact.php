<?php

	use chillerlan\QRCode\{QRCode, QROptions};
	use chillerlan\QRCode\Common\EccLevel;

	require_once 'site-header.php';

	$options = new QROptions([
		'version' => 7,
		'eccLevel' => EccLevel::M,
		'outputBase64' => false,
		'scale' => 10
	]);







	echo '<section id="contact">';
		echo '<div class="pagelogo">'.svgicon('contact').'</div>';

		echo '<h1>'.$lang['pages']['contact']['title'].'</h1>';

		foreach($lang['pages']['contact']['content'] AS $content) {
			echo $Parsedown->text($content);
		}

		echo '<div class="finding-me">';
			echo '<div>';
				echo '<a href="https://mst-gts.airikr.me/@edgren" class="side-by-side">';
					echo svgicon('brand-mastodon');
					echo 'Mastodon';
				echo '</a>';
			echo '</div>';

			echo '<div>';
				echo '<a href="https://koroth.se/connect" class="side-by-side">';
					echo svgicon('brand-mumble');
					echo 'Mumble';
				echo '</a>';
			echo '</div>';

			echo '<div>';
				echo '<a href="xmpp:airikr@chat.airikr.me" class="side-by-side">';
					echo svgicon('brand-xmpp');
					echo 'airikr@chat.airikr.me';
				echo '</a>';

				echo '<details class="devices">';
					echo '<summary class="no-select">';
						echo '<div class="open">'.svgicon('details-open').'</div>';
						echo '<div class="close">'.svgicon('details-close').'</div>';
						echo $lang['pages']['me']['xmpp']['my-devices'];
					echo '</summary>';

					echo '<div class="content">';
						echo '<div class="info">';
							foreach($lang['pages']['me']['xmpp']['devices'] AS $content) {
								echo $Parsedown->text($content);
							}
						echo '</div>';

						echo '<div class="devices">';
							foreach($arr_devices AS $device) {
								$qrdata = 'xmpp:airikr@chat.airikr.me?message;omemo-sid-'.$device['sid'].'='.mb_strtolower(str_replace(' ', '', $device['code']));

								echo '<details title="'.$device['tooltip'].'">';
									echo '<summary>';
										echo '<div class="icon">'.svgicon($device['icon']).'</div>';
										echo '<div class="code">'.wordwrap($device['code'], 35, '<br>').'</div>';
									echo '</summary>';

									echo '<div class="qr">'.(new QRCode($options))->render($qrdata).'</div>';
									echo '<div class="address">'.$qrdata.'</div>';
								echo '</details>';
							}
						echo '</div>';
					echo '</div>';
				echo '</details>';
			echo '</div>';
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
