<?php

	if(isset($_POST['send'])) {

		require_once 'site-settings.php';

		$post_name = safetag($_POST['name']);
		$post_website = safetag($_POST['website']);
		$post_message = safetag($_POST['message']);


		if(empty($post_name) OR empty($post_message)) {
			if(!empty($post_name)) { $_SESSION['name'] = $post_name; }
			if(!empty($post_website)) { $_SESSION['website'] = $post_website; }
			if(!empty($post_message)) { $_SESSION['message'] = $post_message; }

			$error = null;
			foreach($lang['messages']['errors']['empty-fields'] AS $content) {
				$error .= $Parsedown->text($content);
			}

			$arr_requiredfields = [
				$lang['pages']['guestbook']['form']['name'],
				$lang['pages']['guestbook']['form']['message']
			];

			die(simplepage('<div class="error">'.$error.'<ul><li>'.implode('</li><li>', $arr_requiredfields).'</li></ul></div><div class="go-back"><a href="'.url('guestbook').'">'.$lang['goback'].'</a></div>'));


		} elseif(!empty($post_website) AND !filter_var($post_website, FILTER_VALIDATE_URL)) {
			if(!empty($post_name)) { $_SESSION['name'] = $post_name; }
			if(!empty($post_website)) { $_SESSION['website'] = $post_website; }
			if(!empty($post_message)) { $_SESSION['message'] = $post_message; }

			$error = null;
			foreach($lang['messages']['errors']['notvalid-url'] AS $content) {
				$error .= $Parsedown->text($content);
			}

			die(simplepage('<div class="error">'.$error.'</div><div class="go-back"><a href="'.url('guestbook').'">'.$lang['goback'].'</a></div>'));


		} else {
			$timestamp = time();
			$arr_post = [
				'name' => $post_name,
				'website' => (empty($post_website) ? null : $post_website),
				'ip' => endecrypt(getip()),
				'message' => $post_message,
				'is_accepted' => false
			];

			send_email(
				$config_email,
				'Nytt gästboksinlägg',

				'"'.$post_name.'" har skickat ett inlägg i din gästbok'.(empty($post_website) ? '.' : ', med följande länk: '.$post_website).'
				<div class="message">'.$post_message.'</div>
				<a href="'.$site_url.'/guestbook/review:'.$timestamp.'/accept">Acceptera</a> - <a href="'.$site_url.'/guestbook/review:'.$timestamp.'/deny">Neka</a>',

				'Öppna meddelandet i e-postklienten för att läsa det.'
			);

			file_put_contents($dir_files.'/guestbook/'.$timestamp.'.json', json_encode($arr_post));

			unset($_SESSION['name']);
			unset($_SESSION['website']);
			unset($_SESSION['message']);

			header("Location: ".url('guestbook/thanks'));
			exit;
		}



	} elseif(isset($_GET['cnl'])) {

		require_once 'site-settings.php';

		unset($_SESSION['name']);
		unset($_SESSION['website']);
		unset($_SESSION['message']);

		header("Location: ".url('guestbook'));
		exit;



	} else {

		require_once 'site-header.php';



		$dir_empty = glob($dir_files.'/guestbook/*');

		if(!empty($dir_empty)) {
			$posts = glob($dir_files.'/guestbook/*.json');
			natsort($posts);
		}







		echo '<section id="guestbook">';
			echo '<h1>'.$lang['pages']['guestbook']['title'].'</h1>';

			foreach($lang['pages']['guestbook']['content'] AS $content) {
				echo $Parsedown->text($content);
			}


			echo '<nav>';
				echo '<div class="rss side-by-side">';
					echo '<a href="'.url('rss/guestbook?lang='.$get_lang, true).'">';
						echo svgicon('rss') . $lang['pages']['guestbook']['nav']['rss'];
					echo '</a>';
				echo '</div>';
			echo '</nav>';


			echo '<details';
			echo (((isset($_SESSION['name']) OR isset($_SESSION['website']) OR isset($_SESSION['message']))) ? ' open' : '');
			echo (isset($_GET['tnk']) ? ' class="thanks"' : '');
			echo ($notchecked == true ? ' class="notchecked"' : '');
			echo ($badvisitor == true ? ' class="badvisitor"' : '');
			echo '>';
				echo '<summary class="no-select';
				echo ($notchecked == true ? ' notchecked' : '');
				echo ($badvisitor == true ? ' badvisitor' : '');
				echo '">';
					echo '<div class="open">'.svgicon('details-open').'</div>';
					echo '<div class="close">'.svgicon('details-close').'</div>';
					echo svgicon('write') . $lang['pages']['guestbook']['sign'];
				echo '</summary>';


				if($notchecked == false AND $badvisitor == false) {
					echo '<div class="terms">';
						foreach($lang['pages']['guestbook']['terms'] AS $content) {
							echo $Parsedown->text($content);
						}
					echo '</div>';


					echo '<form action="'.url('guestbook').'" method="POST" autocomplete="off" novalidate>';

						echo '<div class="side-by-side">';
							echo '<div class="item name">';
								echo '<div class="label">';
									echo $lang['pages']['guestbook']['form']['name'];
								echo '</div>';

								echo '<div class="field">';
									echo '<input type="text" name="name"';
									echo (isset($_SESSION['name']) ? ' value="'.$_SESSION['name'].'"' : '');
									echo '>';
								echo '</div>';
							echo '</div>';

							echo '<div class="item website">';
								echo '<div class="label">';
									echo $lang['pages']['guestbook']['form']['website'];
								echo '</div>';

								echo '<div class="field">';
									echo '<input type="url" name="website"';
									echo (isset($_SESSION['website']) ? ' value="'.$_SESSION['website'].'"' : '');
									echo '>';
								echo '</div>';
							echo '</div>';
						echo '</div>';



						echo '<div class="item">';
							echo '<div class="label">';
								echo $lang['pages']['guestbook']['form']['message'];
							echo '</div>';

							echo '<div class="field">';
								echo '<textarea name="message">';
									echo (isset($_SESSION['message']) ? $_SESSION['message'] : '');
								echo '</textarea>';
							echo '</div>';
						echo '</div>';

						echo '<div class="button">';
							echo '<input type="submit" name="send" value="'.$lang['pages']['guestbook']['form']['button'].'">';

							if(isset($_SESSION['name']) OR isset($_SESSION['website']) OR isset($_SESSION['message'])) {
								echo '<a href="'.url('guestbook/cancel').'" class="cancel">';
									echo $lang['pages']['guestbook']['form']['cancel'];
								echo '</a>';
							}
						echo '</div>';

					echo '</form>';



				} elseif($notchecked == true) {
					echo '<div class="content">';
						foreach($lang['pages']['guestbook']['notchecked'] AS $content) {
							echo $Parsedown->text($content);
						}
					echo '</div>';



				} elseif($notchecked == false AND $badvisitor == true) {
					echo '<div class="content">';
						foreach($lang['pages']['guestbook']['badvisitor'] AS $content) {
							echo $Parsedown->text($content);
						}
					echo '</div>';
				}
			echo '</details>';



			if(isset($_GET['tnk'])) {
				echo '<div class="thanks message color-green">';
					echo $lang['messages']['guestbook-thanks'];
				echo '</div>';
			}



			if(count($dir_empty) == 0) {
				echo '<div class="message">';
					echo $lang['messages']['no-posts-guestbook'];
				echo '</div>';

			} else {
				echo '<div class="posts">';
					foreach(array_reverse($posts) AS $post) {
						$fileinfo = pathinfo($post);
						$post = json_decode(file_get_contents($post), false);

						if($post->is_accepted == true) {
							echo '<div class="post">';
								echo '<div class="name">';
									echo $post->name;
								echo '</div>';

								echo '<div class="info">';
									echo '<div class="published">';
										echo date_($fileinfo['filename'], 'datetime');
									echo '</div>';

									if(!empty($post->website)) {
										echo '<div class="website">';
											echo link_('Webbsida', $post->website);
										echo '</div>';
									}
								echo '</div>';

								echo '<div class="content">';
									echo $Parsedown->text($post->message);
								echo '</div>';
							echo '</div>';
						}
					}
				echo '</div>';
			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
