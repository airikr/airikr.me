<?php

	use ParagonIE\Halite\File;
	use ParagonIE\Halite\KeyFactory;
	use ParagonIE\Halite\Symmetric\Crypto AS Symmetric;
	use ParagonIE\HiddenString\HiddenString;

	function endecrypt($string, $encrypt = true, $file = false) {
		global $dir_files;

		$enckey = KeyFactory::loadEncryptionKey($dir_files.'/encryption.key');

		if($encrypt == true) {
			if($file == false) {
				return Symmetric::encrypt(new HiddenString($string), $enckey);
			} else {
				File::encrypt($string, $string.'.enc', $enckey);
			}



		} else {
			if($file == false) {
				return Symmetric::decrypt($string, $enckey)->getString();
			} else {
				File::decrypt($string.'.enc', $string, $enckey);
			}
		}
	}

?>