<?php

	# Visa total sträcka per månad i en graf
	# Visa total tid för varje månad också
	# Lägg till en paging
	# Visa vilka mål som har uppnåtts och hur lång tid det tog att genomföra dom
	# Skapa en 3D-ritning av cykeln
	# Visa alla tillbehör jag har köpt till cykeln

	if(isset($_POST['recalculate'])) {

		require_once 'site-settings.php';

		$url_parsed = parse_url($uri);
		$url_strip = (empty($config_folder) ? ltrim($url_parsed['path'], '/') : ltrim($url_parsed['path'], '/airikr/'));

		$get_option = (is_numeric($_POST['averagespeed']) ? safetag($_POST['averagespeed']) : null);


		if(empty($get_option)) {
			die(simplepage('Average speed can only be numeric. <a href="'.url('biking').'">Go back</a>'));

		} else {
			header("Location: ".url($url_strip, false, null, null, null, 'avs:'.ltrim($get_option, 0)));
			exit;
		}



	} else {

		$arr_plans = [
			[
				'when' => [
					'season' => 'spring',
					'year' => 2025
				],
				'destination' => 'Forshaga',
				'distance' => 29
			],
			[
				'when' => [
					'season' => 'autumn',
					'year' => 2025
				],
				'destination' => 'Grums',
				'distance' => 36
			],
			[
				'when' => [
					'season' => 'spring',
					'year' => 2026
				],
				'destination' => 'Ransäter',
				'distance' => 59
			],
			[
				'when' => [
					'season' => 'summer',
					'year' => 2026
				],
				'destination' => 'Vänernleden',
				'distance' => 640
			],
			[
				'when' => [
					'season' => 'summer',
					'year' => 2027
				],
				'destination' => 'summercabin',
				'distance' => 221
			],
			[
				'when' => [
					'season' => 'spring',
					'year' => 2028
				],
				'destination' => 'Eskilstuna',
				'distance' => 238
			],
			[
				'when' => [
					'season' => 'spring',
					'year' => 2028
				],
				'destination' => 'Huddinge',
				'distance' => 340
			]
		];



		if(isset($_GET['json'])) {
			header('Content-Type: application/json;charset=utf-8');

			echo json_encode($arr_plans);



		} else {

			require_once 'site-header.php';



			$get_averagespeed = (!isset($_GET['avs']) ? 19 : safetag($_GET['avs']));

			$arr_seasons = [
				'winter' => $lang['pages']['biking']['seasons']['winter'],
				'spring' => $lang['pages']['biking']['seasons']['spring'],
				'summer' => $lang['pages']['biking']['seasons']['summer'],
				'autumn' => $lang['pages']['biking']['seasons']['autumn']
			];

			$arr_destinations = [
				'westcoast' => $lang['pages']['biking']['destinations']['westcoast'],
				'summercabin' => $lang['pages']['biking']['destinations']['summercabin']
			];







			echo '<section id="biking">';
				echo '<h1>'.$lang['pages']['biking']['title'].'</h1>';
				echo '<div class="cover" style="background-image: url('.url('metadata:biking', true).');"></div>';

				foreach($lang['pages']['biking']['intro'] AS $content) {
					echo $Parsedown->text($content);
				}

				/*echo '<details class="specifications">';
					echo '<summary class="no-select">';
						echo '<div class="open">'.svgicon('details-open').'</div>';
						echo '<div class="close">'.svgicon('details-close').'</div>';
						echo svgicon('plans') . $lang['pages']['biking']['plans']['title'];
					echo '</summary>';

					echo '<div class="content">';
						foreach($lang['pages']['biking']['specifications'] AS $content) {
							echo $Parsedown->text($content);
						}
					echo '</div>';
				echo '</details>';*/


				echo '<nav>';
					echo '<div class="rss side-by-side">';
						echo '<a href="'.url('rss/biking?lang='.$get_lang, true).'">';
							echo svgicon('rss') . $lang['pages']['biking']['nav']['rss'];
						echo '</a>';
					echo '</div>';
				echo '</nav>';


				echo '<div class="tracks">';
					echo '<div class="item head side-by-side">';
						echo '<div class="when">'.$lang['pages']['biking']['list']['when'].'</div>';
						echo '<div class="distance">'.$lang['pages']['biking']['list']['distance'].'</div>';
						echo '<div class="movingtime">'.$lang['pages']['biking']['list']['movingtime'].'</div>';
					echo '</div>';

					$tracks = glob($dir_files.'/tracks/json/*.json');
					$total_distance = 0;
					$total_hours = 0;
					$total_minutes = 0;
					$arr_distances = [];
					$arr_durations = [];
					$arr_speeds = [];
					$arr_elevations = [];

					natsort($tracks);
					foreach(array_reverse($tracks) AS $track) {
						$fileinfo = pathinfo($track);
						$data = json_decode(file_get_contents($track), true);
						$total_distance += $data['distance']['km'];
						$total_hours += $data['movingtime']['hours'];
						$total_minutes += $data['movingtime']['minutes'];

						$arr_distances[] = [
							'file' => $fileinfo['filename'],
							'km' => (float)$data['distance']['km']
						];

						$arr_durations[] = [
							'file' => $fileinfo['filename'],
							'time' => (int)(($data['movingtime']['hours'] * 3600) + ($data['movingtime']['minutes'] * 60))
						];

						$arr_speeds[] = [
							'file' => $fileinfo['filename'],
							'kmh' => (float)$data['speed']['top']['kmh']
						];


						if(isset($data['elevation'])) {
							$arr_elevations_max[] = [
								'file' => $fileinfo['filename'],
								'm' => (float)$data['elevation']['max']['m']
							];

							$arr_elevations_min[] = [
								'file' => $fileinfo['filename'],
								'm' => (float)$data['elevation']['min']['m']
							];
						}


						echo '<div class="item side-by-side">';
							echo '<div class="when">';
								echo '<a href="'.url('biking-track:'.$fileinfo['filename']).'">';
									#echo '<div class="image" style="background-image: url('.url('track-thumb:'.$data['occurred']['filename']).');"></div>';
									echo $data['occurred']['date'].', '.$data['occurred']['time'];
								echo '</a>';
							echo '</div>';

							echo '<div class="distance">';
								echo format_number($data['distance']['km']).' km';
							echo '</div>';

							echo '<div class="movingtime">';
								echo $data['movingtime']['hours'] . $lang['pages']['biking']['details']['hour'].' '.$data['movingtime']['minutes'] . $lang['pages']['biking']['details']['minute'];
							echo '</div>';
						echo '</div>';
					}

					echo '<div class="item stats side-by-side">';
						echo '<div class="when"></div>';
						echo '<div class="distance">'.format_number($total_distance).' km</div>';
						echo '<div class="movingtime">'.($total_hours + floor($total_minutes / 60)) . $lang['pages']['biking']['details']['hour'].' '.($total_minutes % 60) . $lang['pages']['biking']['details']['minute'].'</div>';
					echo '</div>';
				echo '</div>';



				foreach($arr_distances AS $dik => $div) {
					$arr_distance[$dik] = $div['km'].'-'.$div['file'];
				}

				foreach($arr_speeds AS $spk => $spv) {
					$arr_speed[$spk] = $spv['kmh'].'-'.$spv['file'];
				}

				foreach($arr_durations AS $duk => $duv) {
					$arr_duration[$duk] = $duv['time'].'-'.$duv['file'];
				}

				if(isset($arr_elevations_min) AND isset($arr_elevations_max)) {
					foreach($arr_elevations_max AS $eak => $eav) {
						$arr_elevations_max[$eak] = $eav['m'].'-'.$eav['file'];
					}

					foreach($arr_elevations_min AS $eik => $eiv) {
						$arr_elevations_min[$eik] = $eiv['m'].'-'.$eiv['file'];
					}
				}

				list($midi_km, $midi_file) = explode('-', min($arr_distance));
				list($madi_km, $madi_file) = explode('-', max($arr_distance));
				list($misp_kmh, $misp_file) = explode('-', min($arr_speed));
				list($masp_kmh, $masp_file) = explode('-', max($arr_speed));
				list($midu_time, $midu_file) = explode('-', min($arr_duration));
				list($madu_time, $madu_file) = explode('-', max($arr_duration));

				if(isset($arr_elevations_min) AND isset($arr_elevations_max)) {
					list($miei_m, $miei_file) = explode('-', min($arr_elevations_min));
					list($maea_m, $maea_file) = explode('-', max($arr_elevations_max));
				}

				$time_longest_hours = floor($madu_time / 3600);
				$madu_time -= $time_longest_hours * 3600;
				$time_longest_minutes = floor($madu_time / 60);
				$madu_time -= $time_longest_minutes * 60;

				$time_shortest_hours = floor($midu_time / 3600);
				$midu_time -= $time_shortest_hours * 3600;
				$time_shortest_minutes = floor($midu_time / 60);
				$midu_time -= $time_shortest_minutes * 60;



				echo '<details class="statistics" open>';
					echo '<summary class="no-select">';
						echo '<div class="open">'.svgicon('details-open').'</div>';
						echo '<div class="close">'.svgicon('details-close').'</div>';
						echo svgicon('statistics') . $lang['pages']['biking']['statistics']['title'];
					echo '</summary>';

					echo '<div class="content">';
						echo '<div class="item">';
							echo '<div class="value">';
								echo count($tracks);
							echo '</div>';

							echo '<div class="label">';
								echo $lang['pages']['biking']['statistics']['number-of'];
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="value">';
								echo '<a href="'.url('biking-track:'.$madi_file).'">';
									echo format_number($madi_km, 1).' km';
								echo '</a>';
							echo '</div>';

							echo '<div class="label">';
								echo $lang['pages']['biking']['statistics']['duration-min'];
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="value">';
								echo '<a href="'.url('biking-track:'.$midi_file).'">';
									echo format_number($midi_km, 1).' km';
								echo '</a>';
							echo '</div>';

							echo '<div class="label">';
								echo $lang['pages']['biking']['statistics']['duration-max'];
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="value">';
								echo '<a href="'.url('biking-track:'.$masp_file).'">';
									echo format_number($masp_kmh, 1).' km';
								echo '</a>';
							echo '</div>';

							echo '<div class="label">';
								echo $lang['pages']['biking']['statistics']['speed-min'];
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="value">';
								echo '<a href="'.url('biking-track:'.$misp_file).'">';
									echo format_number($misp_kmh, 1).' km';
								echo '</a>';
							echo '</div>';

							echo '<div class="label">';
								echo $lang['pages']['biking']['statistics']['speed-max'];
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="value">';
								echo '<a href="'.url('biking-track:'.$madu_file).'">';
									echo $time_longest_hours . $lang['pages']['biking']['details']['hour'].' '.$time_longest_minutes . $lang['pages']['biking']['details']['minute'];
								echo '</a>';
							echo '</div>';

							echo '<div class="label">';
								echo $lang['pages']['biking']['statistics']['time-min'];
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="value">';
								echo '<a href="'.url('biking-track:'.$midu_file).'">';
									echo $time_shortest_hours . $lang['pages']['biking']['details']['hour'].' '.$time_shortest_minutes . $lang['pages']['biking']['details']['minute'];
								echo '</a>';
							echo '</div>';

							echo '<div class="label">';
								echo $lang['pages']['biking']['statistics']['time-max'];
							echo '</div>';
						echo '</div>';


						if(isset($arr_elevations_min) AND isset($arr_elevations_max)) {
							echo '<div class="item">';
								echo '<div class="value">';
									echo '<a href="'.url('biking-track:'.$maea_file).'">';
										echo format_number($maea_m);
									echo '</a>';
								echo '</div>';

								echo '<div class="label">';
									echo $lang['pages']['biking']['statistics']['elevation-max'];
								echo '</div>';
							echo '</div>';

							echo '<div class="item">';
								echo '<div class="value">';
									echo '<a href="'.url('biking-track:'.$miei_file).'">';
										echo format_number($miei_m);
									echo '</a>';
								echo '</div>';

								echo '<div class="label">';
									echo $lang['pages']['biking']['statistics']['elevation-min'];
								echo '</div>';
							echo '</div>';
						}
					echo '</div>';
				echo '</details>';



				if(count($arr_plans) != 0) {
					echo '<details class="plans">';
						echo '<summary class="no-select">';
							echo '<div class="open">'.svgicon('details-open').'</div>';
							echo '<div class="close">'.svgicon('details-close').'</div>';
							echo svgicon('plans') . $lang['pages']['biking']['plans']['title'];
						echo '</summary>';

						echo '<div class="content">';
							foreach($lang['pages']['biking']['plans']['content'] AS $content) {
								echo $Parsedown->text($content);
							}

							echo '<div class="item head side-by-side">';
								echo '<div class="when">'.$lang['pages']['biking']['list']['when'].'</div>';
								echo '<div class="destination">'.$lang['pages']['biking']['list']['destination'].'</div>';
								echo '<div class="distance">'.$lang['pages']['biking']['list']['distance'].'</div>';
								echo '<div class="expected-traveltime">'.$lang['pages']['biking']['list']['expected-traveltime'].'</div>';
							echo '</div>';

							foreach($arr_plans AS $plan) {
								$traveltime = format_number(($plan['distance'] / $get_averagespeed) * 60, 0, '', '');

								echo '<div class="item side-by-side">';
									echo '<div class="when">';
										echo '<div class="label">'.$lang['pages']['biking']['list']['when'].':</div>';
										echo $arr_seasons[$plan['when']['season']].' '.$plan['when']['year'];
									echo '</div>';

									echo '<div class="destination">';
										echo '<div class="label">'.$lang['pages']['biking']['list']['destination'].':</div>';
										echo (array_key_exists($plan['destination'], $arr_destinations) ? $arr_destinations[$plan['destination']] : $plan['destination']);
									echo '</div>';

									echo '<div class="distance">';
										echo '<div class="label">'.$lang['pages']['biking']['list']['distance'].':</div>';
										echo $plan['distance'].' km';
									echo '</div>';

									echo '<div class="expected-traveltime">';
										echo '<div class="label">'.$lang['pages']['biking']['list']['expected-traveltime'].':</div>';
										echo floor($traveltime / 60).'h '.($traveltime - floor($traveltime / 60) * 60).'m';
									echo '</div>';
								echo '</div>';
							}
						echo '</div>';
					echo '</details>';
				}
			echo '</section>';







			require_once 'site-footer.php';

		}

	}

?>
