<?php

	require_once 'site-settings.php';

	require_once $dir_functions.'/age.php';
	require_once $dir_functions.'/calc-loading-time.php';
	require_once $dir_functions.'/check-ip.php';
	require_once $dir_functions.'/checkbox.php';
	require_once $dir_functions.'/create-folder.php';
	require_once $dir_functions.'/date.php';
	require_once $dir_functions.'/dirsize.php';
	require_once $dir_functions.'/endecrypt.php';
	require_once $dir_functions.'/external-link.php';
	require_once $dir_functions.'/format-filesize.php';
	require_once $dir_functions.'/format-number.php';
	require_once $dir_functions.'/get-ip.php';
	require_once $dir_functions.'/keycodes.php';
	require_once $dir_functions.'/pw-random.php';
	require_once $dir_functions.'/readingtime.php';
	require_once $dir_functions.'/render-calendar.php';
	require_once $dir_functions.'/safe-tag.php';
	require_once $dir_functions.'/send-email.php';
	require_once $dir_functions.'/seo.php';
	require_once $dir_functions.'/simple-page.php';
	require_once $dir_functions.'/svg-icon.php';
	require_once $dir_functions.'/tags.php';
	require_once $dir_functions.'/truncate.php';
	require_once $dir_functions.'/url.php';
	require_once $dir_functions.'/validate.php';

?>
