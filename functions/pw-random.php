<?php

	function pw_random($digit = 20) {
		$comb = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*';
		$shfl = str_shuffle($comb);
		$pwd = substr($shfl, 0, $digit);

		return $pwd;
	}

?>