<?php

	require_once 'site-settings.php';

	echo '<!DOCTYPE html>';
	echo '<html lang="'.$lang['metadata']['iso6391'].'">';

		echo '<head>';
			echo '<title>'.($is_reading == false ? (empty($metadata_title) ? '' : $metadata_title.' - ') : $metadata_title.' - ') . $site_title.'</title>';

			echo '<meta charset="UTF-8">';
			echo '<meta name="theme-color" content="#1a1b26">';

			echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, maximum-scale=3">';
			echo (count($robots) == 0 ? null : '<meta name="robots" content="'.implode(',', $robots).'">');
			echo '<meta name="description" content="'.strip_tags(($is_reading == false ? $Parsedown->text($metadata_description) : truncate($Parsedown->text($metadata_description)))).'">';

			echo '<meta http-equiv="cache-control" content="cache">';
			echo '<meta http-equiv="expires" content="'.(60*60*24).'">';
			echo '<meta name="robots" content="noarchive">';

			echo '<meta property="og:locale" content="'.$lang['metadata']['locale'].'">';
			echo '<meta property="og:title" content="'.($is_reading == false ? $site_title : $metadata_title).'">';
			echo '<meta property="og:site_name" content="'.$site_title.'">';
			echo '<meta property="og:type" content="website">';
			echo '<meta property="og:url" content="'.$site_url_current.'">';
			echo '<meta property="og:description" content="'.strip_tags(($is_reading == false ? $Parsedown->text($metadata_description) : truncate($Parsedown->text($metadata_description)))).'">';

			if(!empty($metadata_image)) {
				echo '<meta property="og:image" content="'.$metadata_image.'">';
				echo '<meta property="og:image:secure_url" content="'.$metadata_image.'">';
				echo '<meta property="og:image:height" content="'.$metadata_image_height.'">';
				echo '<meta property="og:image:width" content="'.$metadata_image_width.'">';
				echo '<meta property="og:image:type" content="image/jpeg">';
			}

			echo '<meta property="twitter:card" content="summary_large_image">';
			echo '<meta property="twitter:url" content="'.$site_url_current.'">';
			echo '<meta property="twitter:title" content="'.($is_reading == false ? $site_title : $metadata_title).'">';
			echo '<meta property="twitter:description" content="'.strip_tags(($is_reading == false ? $Parsedown->text($metadata_description) : truncate($Parsedown->text($metadata_description)))).'">';
			echo ($is_reading == false ? null : (empty($metadata_image) ? null : '<meta property="twitter:image" content="'.$metadata_image.'">'));

			echo '<meta name="fediverse:creator" content="@edgren@mst-gts.airikr.me">';

			echo '<link rel="canonical" href="'.$site_url_current.'">';

			foreach($arr_relme AS $relme) {
				echo '<link href="'.$relme.'" rel="me">';
			}

			echo (file_exists('manifest.json') ? '<link rel="manifest" href="'.url('manifest.json').'" crossorigin="use-credentials">' : null);

			echo '<link type="image/x-icon" rel="icon" href="'.$site_favicon.'">';
			echo (empty($site_url_cdn) ? null : '<link rel="dns-prefetch" href="'.$site_url_cdn.'" crossorigin="">');

			$desktop = file_get_contents((($minified == true AND file_exists($dir_css.'/desktop.min.css')) ? $dir_css.'/desktop.min.css' : $dir_css.'/desktop.css'));
			$page = ((!file_exists($dir_css_pages.'/'.str_replace('.php', '.css', $filename)) ? null : file_get_contents((($minified == true AND file_exists($dir_css_pages_minified.'/'.str_replace('.php', '.css', $filename))) ? $dir_css_pages_minified.'/'.str_replace('.php', '.css', $filename) : $dir_css_pages.'/'.str_replace('.php', '.css', $filename)))));
			$portable = file_get_contents((($minified == true AND file_exists($dir_css.'/portable.min.css')) ? $dir_css.'/portable.min.css' : $dir_css.'/portable.css'));
			$theme = file_get_contents((($minified == true AND file_exists($dir_css.'/theme-'.$get_theme.'.min.css')) ? $dir_css.'/theme-'.$get_theme.'.min.css' : $dir_css.'/theme-'.$get_theme.'.css'));

			echo '<link href="'.url($dir_fonts.'/iosevka-300.woff2', true).'" as="font" type="font/woff2" crossorigin="anonymous">';
			echo '<link href="'.url($dir_fonts.'/iosevka-700.woff2', true).'" as="font" type="font/woff2" crossorigin="anonymous">';

			echo '<link rel="preload" href="'.url($dir_images.'/selfie.webp', true).'" as="image" type="image/webp">';

			echo '<style>';
				echo '@font-face{font-family:"Iosevka";font-style:normal;font-weight:300;font-display:swap;src:url('.url($dir_fonts.'/iosevka-300.woff2', true).');unicode-range:U+000-5FF}@font-face{font-family:"Iosevka";font-style:normal;font-weight:700;font-display:swap;src:url('.url($dir_fonts.'/iosevka-700.woff2', true).');unicode-range:U+000-5FF}';
				echo $desktop . $page . $portable . $theme;
			echo '</style>';

			echo '<link rel="dns-prefetch" href="https://analytics.'.$site_domain.'/">';
			echo '<link rel="webmention" href="https://webmention.io/'.$site_domain.'/webmention">';
			echo '<link rel="pingback" href="https://webmention.io/'.$site_domain.'/xmlrpc">';
		echo '</head>';

		echo '<body';
		echo ' data-folder="'.(empty($config_folder) ? '/' : '/'.$config_folder.'/').'"';
		echo (!isset($parse_url_current) ? '' : ' data-page="'.$parse_url_current['path'].'"');
		echo '>';



			echo '<section id="website">';
				if($filename != 'page-me.php') {
					if($notchecked == true) {
						echo '<details class="ipintel">';
							echo '<summary class="no-select">';
								echo '<div class="open">'.svgicon('details-open').'</div>';
								echo '<div class="close">'.svgicon('details-close').'</div>';
								echo svgicon('privacy') . $lang['ipintel']['title'];
							echo '</summary>';

							echo '<div class="content">';
								foreach($lang['ipintel']['content'] AS $ipintel) {
									echo $Parsedown->text($ipintel);
								}

								echo '<div class="agree">';
									echo '<a href="'.url('allow-ipintel'.(empty(ltrim($uri, (empty($config_folder) ? '/' : '/airikr/'))) ? '' : '/previous-page:'.ltrim($uri, (empty($config_folder) ? '/' : '/airikr/'))).'?lang='.$get_lang.'&thm='.$get_theme, true).'">';
										echo $lang['ipintel']['agree'];
									echo '</a>';
								echo '</div>';
							echo '</div>';
						echo '</details>';
					}


					echo '<header>';
						echo '<a href="'.url('').'">';
							echo $admin_name;
						echo '</a>';
					echo '</header>';

					echo '<nav>';
						echo '<div class="small">';
							echo '<details>';
								echo '<summary class="no-select">';
									echo '<div class="open">'.svgicon('hamburger') . $lang['nav']['open'].'</div>';
									echo '<div class="close">'.svgicon('hamburger') . $lang['nav']['close'].'</div>';
								echo '</summary>';

								echo '<a href="'.url('').'">';
									echo $lang['nav']['about'];
								echo '</a>';

								if($filename == 'page-verify.php') {
									echo '<div class="inactive no-select">'.$lang['nav']['blog'].'</div>';

								} else {
									echo '<a href="'.url('blog').'">';
										echo $lang['nav']['blog'];
									echo '</a>';
								}

								echo '<a href="'.url('gallery').'">';
									echo $lang['nav']['gallery'];
								echo '</a>';

								echo '<a href="'.url('biking').'">';
									echo $lang['nav']['biking'];
								echo '</a>';

								if($badvisitor == false) {
									echo '<a href="'.url('links').'">';
										echo $lang['nav']['links'];
									echo '</a>';
								}

								echo '<a href="'.url('guestbook').'">';
									echo $lang['nav']['guestbook'];
								echo '</a>';
							echo '</details>';
						echo '</div>';



						echo '<div class="wide">';
							echo '<a href="'.url('').'">';
								echo $lang['nav']['about'];
							echo '</a>';

							if($filename == 'page-verify.php') {
								echo '<div class="inactive no-select">'.$lang['nav']['blog'].'</div>';

							} else {
								echo '<a href="'.url('blog').'">';
									echo $lang['nav']['blog'];
								echo '</a>';
							}

							echo '<a href="'.url('gallery').'">';
								echo $lang['nav']['gallery'];
							echo '</a>';

							echo '<a href="'.url('biking').'">';
								echo $lang['nav']['biking'];
							echo '</a>';

							if($badvisitor == false) {
								echo '<a href="'.url('links').'">';
									echo $lang['nav']['links'];
								echo '</a>';
							}

							echo '<a href="'.url('guestbook').'">';
								echo $lang['nav']['guestbook'];
							echo '</a>';
						echo '</div>';
					echo '</nav>';
				}

				$lt_page_start = getrusage();
				echo '<main>';

?>
