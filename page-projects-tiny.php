<?php

	if(isset($_GET['calculate-steps'])) {

		require_once 'site-settings.php';

		$get_steplength = safetag($_GET['stp']);
		$get_kilometers = safetag($_GET['km']);

		if(empty($get_steplength) OR empty($get_kilometers)) {
			header("Location: ".url('tiny-projects'));
			exit;

		} else {
			header("Location: ".url('tiny-projects/step-length:'.$get_steplength.'/km:'.$get_kilometers));
			exit;
		}



	} else {

		require_once 'site-header.php';



		$get_steplength = (isset($_GET['stp']) ? '0.'.safetag($_GET['stp']) : null);
		$get_kilometers = (isset($_GET['km']) ? safetag($_GET['km']) : null);

		$km_in_m = 1000;







		echo '<section id="tiny-projects">';
			echo '<h1>Tiny projects</h1>';

			echo '<div class="blocks">';
				echo '<div>';
					echo '<h2>Step counter</h2>';

					echo '<form action="'.url('tiny-projects').'" method="GET" id="stepcounter">';
						echo '<div class="item">';
							echo '<div class="label">';
								echo 'Step length';
							echo '</div>';

							echo '<div class="field">';
								echo '<input type="text" name="stp"';
								echo (empty($get_steplength) ? '' : ' value="'.ltrim($get_steplength, '0.').'"');
								echo '>';
							echo '</div>';
						echo '</div>';

						echo '<div class="item">';
							echo '<div class="label">';
								echo 'Walked kilometers';
							echo '</div>';

							echo '<div class="field">';
								echo '<input type="text" name="km"';
								echo (empty($get_kilometers) ? '' : ' value="'.ltrim($get_kilometers, '0.').'"');
								echo '>';
							echo '</div>';
						echo '</div>';

						echo '<div class="button">';
							echo '<input type="submit" name="calculate-steps" value="Calculate">';
						echo '</div>';
					echo '</form>';

					if(!empty($get_steplength) AND !empty($get_kilometers)) {
						echo 'Approx. '.format_number(((float)$get_kilometers * (int)$km_in_m) / (float)$get_steplength, 0).' steps';
					}
				echo '</div>';
			echo '</div>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>