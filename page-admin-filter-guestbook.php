<?php

	if(isset($_GET['del'])) {

		require_once 'site-settings.php';

		$get_timestamp = safetag($_GET['del']);
		unlink($dir_files.'/guestbook/'.$get_timestamp.'.json');

		header("Location: ".url('admin/filter:guestbook'));
		exit;



	} elseif(isset($_GET['acc'])) {

		require_once 'site-settings.php';

		$get_timestamp = safetag($_GET['tim']);
		$get_accept = safetag($_GET['acc']);

		$post = json_decode(file_get_contents($dir_files.'/guestbook/'.$get_timestamp.'.json'), false);

		if($post->is_accepted == false) {
			$arr_post = [
				'name' => $post->name,
				'website' => (empty($post->website) ? null : $post->website),
				'ip' => $post->ip,
				'message' => $post->message,
				'is_accepted' => true,
				'accepted' => time()
			];

			file_put_contents($dir_files.'/guestbook/'.$get_timestamp.'.json', json_encode($arr_post));
		}

		header("Location: ".url('admin/filter:guestbook'));
		exit;



	} else {

		require_once 'site-header.php';



		$c_posts = 0;
		$posts = glob($dir_files.'/guestbook/*.json');
		natsort($posts);







		echo '<section id="filter-guestbook">';
			echo '<h1>';
				echo '<a href="'.url('admin/overview').'">';
					echo 'Översikt';
				echo '</a>';

				echo svgicon('chevron-right');

				echo 'Gästboksinlägg';
			echo '</h1>';


			echo '<div class="head">';
				echo '<div class="options"></div>';

				echo '<div class="when">';
					echo 'Skickades';
				echo '</div>';

				echo '<div class="name">';
					echo 'Namn';
				echo '</div>';
			echo '</div>';



			foreach(array_reverse($posts) AS $post) {
				$fileinfo = pathinfo($post);
				$data = json_decode(file_get_contents($post), true);
				$c_posts++;

				echo '<details'.($c_posts == 1 ? ' class="first"' : '').'>';
					echo '<summary>';
						echo '<div class="options">';
							if($data['is_accepted'] == false) {
								echo '<a href="'.url('admin/filter:guestbook/accept:'.$fileinfo['filename']).'" class="accept" title="Acceptera och offentliggör inlägget">';
									echo svgicon('accept');
								echo '</a>';
							} else {
								echo '<div class="accepted">'.svgicon('accept').'</div>';
							}

							echo '<a href="'.url('admin/filter:guestbook/delete:'.$fileinfo['filename']).'" class="delete" title="Permanent ta bort inlägget">';
								echo svgicon('trash');
							echo '</a>';
						echo '</div>';

						echo '<div class="when">';
							echo date_($fileinfo['filename'], 'datetime');
						echo '</div>';

						echo '<div class="name">';
							echo $data['name'];
						echo '</div>';
					echo '</summary>';

					echo '<div class="content">';
						echo $data['message'];
					echo '</div>';
				echo '</details>';
			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
