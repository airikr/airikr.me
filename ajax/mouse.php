<?php

	use Spatie\Valuestore\Valuestore;

	require_once '../site-settings.php';
	require_once '../vendor/autoload.php';

	$date = DateTime::createFromFormat('U.u', microtime(TRUE));
	$datetime = $date->format('YmdHis-u');
	$dir = $dir_files.'/logs/'.hash('sha256', getip()).'/mouse';
	$mouse = Valuestore::make($dir.'/'.$datetime.'.json');

	#$get_event = safetag($_GET['eve']);
	$get_client_x = safetag($_GET['clx']);
	$get_client_y = safetag($_GET['cly']);
	$get_screen_x = safetag($_GET['scx']);
	$get_screen_y = safetag($_GET['scy']);
	$get_button = safetag($_GET['but']);
	$get_page = safetag($_GET['pag']);

	$arr_buttons = [
		0 => 'left',
		1 => 'middle',
		2 => 'right',
		3 => 'x1 (back)',
		4 => 'x2 (forward)'
	];

	$arr_datetime = [
		'date' => [
			'year' => (int)$date->format('Y'),
			'month' => (int)$date->format('m'),
			'day' => (int)$date->format('d')
		],
		'time' => [
			'hour' => (int)$date->format('H'),
			'minute' => (int)$date->format('i'),
			'second' => (int)$date->format('s'),
			'microsecond' => (int)$date->format('u')
		]
	];

	$mouse->put([
		'page' => $get_page,
		'button' => [
			'button' => (int)$get_button,
			'string' => $arr_buttons[$get_button],
		],
		'positions' => [
			'client-x' => (int)$get_client_x,
			'client-y' => (int)$get_client_y,
			'screen-x' => (int)$get_screen_x,
			'screen-y' => (int)$get_screen_y
		]
	]);



	if(file_exists($dir.'/'.$datetime.'.json')) {
		echo 'mouse event logged';

	} else {
		echo 'mouse event not logged';
	}

?>
