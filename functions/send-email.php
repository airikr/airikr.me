<?php

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;



	function send_email($to_email, $subject, $content_html, $content_nohtml) {
		global $smtp_host, $smtp_email, $smtp_password, $smtp_port, $config_encoding, $config_email, $site_title;
		$mail = new PHPMailer(true);

		try {
			$mail->SMTPDebug = SMTP::DEBUG_OFF;
			$mail->isSMTP();
			$mail->Host = $smtp_host;
			$mail->SMTPAuth = true;
			$mail->Username = $smtp_email;
			$mail->Password = $smtp_password;
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
			$mail->Port = $smtp_port;

			$mail->setFrom($config_email, mb_convert_encoding($site_title, $config_encoding));
			$mail->addAddress($to_email, null);
			#$mail->addReplyTo('info@example.com', 'Information');
			#$mail->addCC('cc@example.com');
			#$mail->addBCC('bcc@example.com');

			#$mail->addAttachment('/tmp/image.jpg', 'new.jpg');

			$mail->isHTML(true);
			$mail->Subject = mb_convert_encoding($subject, $config_encoding);
			$mail->Body = simplepage(mb_convert_encoding($content_html, $config_encoding));
			$mail->AltBody = mb_convert_encoding($content_nohtml, $config_encoding);

			$mail->send();


		} catch (Exception $e) {
			echo 'mail-error';
		}
	}



	function mailcontent($string) {
		return simplepage($string);
	}

?>
