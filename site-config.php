<?php

	$site_title = null;
	$site_protocol = 'https';
	$site_domain = null;
	$site_subdomain = null;
	$site_url = $site_protocol.'://'.(empty($site_subdomain) ? null : $site_subdomain.'.') . $site_domain;
	$site_url_current = sprintf(
		'%s://%s/%s',
		isset($_SERVER['HTTPS']) ? 'https' : 'http',
		$_SERVER['HTTP_HOST'],
		trim($_SERVER['REQUEST_URI'], '/\\')
	);
	$site_url_cdn = null;
	$site_favicon = 'data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=';

	$metadata_image = null;

	$minified = false;
	$debugging = true;

	$config_folder = null;
	$config_root = '/'.(($_SERVER['HTTP_HOST'] == 'localhost' OR strpos($_SERVER['HTTP_HOST'], '192.168') !== false) ? 'srv/http' : 'var/www');
	$config_email = null;
	$config_email_noreply = 'no-reply@'.$site_domain;
	$config_webmaster = null;
	$config_encoding = 'ISO-8859-1';
	$config_timezone = 'Europe/Stockholm';

	$admin_name = null;
	$admin_email = null;
	$admin_website = null;

	$api_deepl_authkey = null;

	$arr_relme = [];

?>
