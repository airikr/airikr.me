<?php

	$uniqueid = uniqid();
	$multiavatar = new Multiavatar();
	$comments = glob($dir_files.'/posts/comments/'.$datetime.'-*.json');
	$is_editing = false;
	natsort($comments);

	if(isset($_GET['cmt'])) {
		$get_uniqueid = safetag($_GET['cmt']);
		$is_editing = true;
		$edit = json_decode(file_get_contents($dir_files.'/posts/comments/'.$datetime.'-'.$get_uniqueid.'.json'), false);
	}



	echo '<div class="comments" id="comments">';
		echo '<details'.((isset($_SESSION['name']) OR isset($_SESSION['email']) OR isset($_SESSION['website']) OR isset($_SESSION['comment']) OR !empty($edit)) ? ' open' : '').'>';
			echo '<summary>';
				echo '<div class="open">'.svgicon('details-open').'</div>';
				echo '<div class="close">'.svgicon('details-close').'</div>';
				echo svgicon(($is_editing == false ? 'new' : 'manage').'-comment');
				echo $lang['pages']['blog']['comment'][($is_editing == false ? 'new' : 'manage')];
			echo '</summary>';

			echo '<div class="terms">';
				foreach($lang['pages']['blog']['comment']['terms'] AS $content) {
					echo $Parsedown->text($content);
				}
			echo '</div>';

			echo '<div class="delete-comment">';
				foreach($lang['pages']['blog']['comment']['delete'] AS $content) {
					echo $Parsedown->text($content);
				}
			echo '</div>';



			echo '<form action="'.url('blog/'.$datetime . ($is_editing == false ? '' : '/comment/'.$get_uniqueid.'/edit')).'" method="POST" autocomplete="off" novalidate>';
				echo '<input type="hidden" name="postid" value="'.$datetime.'">';

				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['pages']['blog']['comment']['form']['name'];
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="text" name="name"';
						echo (!isset($_SESSION['name']) ? null : ' value="'.$_SESSION['name'].'"');
						echo ($is_loggedin == false ? null : ($is_editing == true ? null : ' value="'.$admin_name.'"'));
						echo ($is_editing == false ? '' : ' value="'.$edit->about->name.'"');
						echo ' placeholder="'.$lang['pages']['blog']['comment']['form']['placeholders']['name'][array_rand($lang['pages']['blog']['comment']['form']['placeholders']['name'])].'" tabindex="1">';
					echo '</div>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['pages']['blog']['comment']['form']['email'];
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="email" name="email"';
						echo (!isset($_SESSION['email']) ? null : ' value="'.$_SESSION['email'].'"');
						echo ($is_loggedin == false ? null : ($is_editing == true ? null : ' value="'.$admin_email.'"'));
						echo ($is_editing == false ? ' placeholder="'.$lang['pages']['blog']['comment']['form']['placeholders']['email'].'"' : ' placeholder="'.$lang['pages']['blog']['comment']['form']['placeholders']['email-leave-empty'].'"');
						echo ' tabindex="2">';
					echo '</div>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['pages']['blog']['comment']['form']['website'];
					echo '</div>';

					echo '<div class="value">';
						echo '<input type="url" name="website"';
						echo (!isset($_SESSION['website']) ? null : ' value="'.$_SESSION['website'].'"');
						echo ($is_loggedin == false ? null : ($is_editing == true ? null : ' value="'.$admin_website.'"'));
						echo (($is_editing == false OR $is_editing == true AND empty($edit->about->website)) ? '' : ' value="'.$edit->about->website.'"');
						echo ' placeholder="'.$lang['pages']['blog']['comment']['form']['placeholders']['website'].'" tabindex="3">';
					echo '</div>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo $lang['pages']['blog']['comment']['form']['comment'];
					echo '</div>';

					echo '<div class="value">';
						echo '<textarea name="comment" tabindex="4">';
							echo (!isset($_SESSION['comment']) ? ($is_editing == false ? '' : $edit->comment) : $_SESSION['comment']);
						echo '</textarea>';
					echo '</div>';
				echo '</div>';


				if($is_editing == false) {
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['pages']['blog']['comment']['form']['password'];
						echo '</div>';

						echo '<div class="value">';
							echo '<input type="text" name="password" value="'.$uniqueid.'" readonly>';
						echo '</div>';

						echo '<div class="description">';
							foreach($lang['pages']['blog']['comment']['form']['descriptions']['password'] AS $content) {
								echo $Parsedown->text($content);
							}
						echo '</div>';
					echo '</div>';

				} else {
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['pages']['blog']['comment']['form']['password'];
						echo '</div>';

						echo '<div class="value">';
							echo '<input type="password" name="password">';
						echo '</div>';

						echo '<div class="description">';
							foreach($lang['pages']['blog']['comment']['form']['descriptions']['password-editing'] AS $content) {
								echo $Parsedown->text($content);
							}
						echo '</div>';
					echo '</div>';
				}

				echo '<div class="button">';
					echo '<input type="submit" name="';
					echo ($is_editing == false ? 'send-comment' : 'save-comment');
					echo '" value="';
					echo ($is_editing == false ? $lang['pages']['blog']['comment']['form']['send'] : $lang['pages']['blog']['comment']['form']['save']);
					echo '" tabindex="5">';

					echo '<div class="options">';
						echo '<div class="left">';
							if(isset($_SESSION['name']) OR isset($_SESSION['email']) OR isset($_SESSION['website']) OR isset($_SESSION['comment']) OR $is_editing == true) {
								echo '<a href="'.url('blog/'.$datetime.'/comment/cancel').'" class="cancel">';
									echo svgicon('cancel');
									echo $lang['pages']['blog']['comment']['form']['cancel'];
								echo '</a>';
							}
						echo '</div>';

						/*echo '<div class="right">';
							if($is_editing == true) {
								echo '<a href="'.url('blog/'.$datetime.'/delete-comment').'" class="delete">';
									echo svgicon('trash');
									echo $lang['pages']['blog']['comment']['form']['delete'];
								echo '</a>';
							}
						echo '</div>';*/
					echo '</div>';
				echo '</div>';
			echo '</form>';
		echo '</details>';



		if(count($comments) == 0) {
			echo '<div class="message">'.$lang['messages']['no-comments'].'</div>';

		} else {
			echo '<h2>';
				echo $lang['pages']['blog']['comment']['subtitles']['comments'].' ('.count($comments).')';
				echo '<a href="'.url('rss/comments').'" title="'.$lang['tooltips']['rss-comments'].'">'.svgicon('rss').'</a>';
			echo '</h2>';

			foreach($comments AS $comment) {
				$fileinfo = pathinfo($comment);
				$comment = json_decode(file_get_contents($comment), false);
				$avatar = $multiavatar($comment->about->name . $comment->about->website . (empty($comment->about->website) ? null : $comment->about->website), null, null);

				list($dt, $ts) = explode('-', $fileinfo['filename']);

				echo '<div class="comment" id="comment-'.$comment->id.'">';
					echo '<div class="avatar">';
						echo $avatar;
					echo '</div>';

					echo '<div class="content">';
						echo '<div class="info">';
							echo '<div class="name">';
								echo $comment->about->name;
							echo '</div>';

							echo '<div class="published">';
								echo '<a href="'.url('blog/'.$dt.'#comment-'.$comment->id).'" title="'.$lang['tooltips']['directlink-comment'].'">';
									echo date_($comment->published, 'datetime');
								echo '</a>';
							echo '</div>';

							if($is_loggedin == true) {
								echo '<div class="admin">';
									echo svgicon('email') . endecrypt($comment->about->email, false);
								echo '</div>';
							}
						echo '</div>';

						echo '<div class="content">';
							echo $Parsedown->text($comment->comment);
						echo '</div>';

						echo '<div class="options">';
							if(substr_count($fileinfo['filename'], '-') == 2) {
								echo '<div class="old-system no-select" title="'.$lang['tooltips']['old-comment-system'].'">';
									echo svgicon('manage') . $lang['pages']['blog']['comment']['options']['manage'];
								echo '</div>';

							} else {
								echo '<a href="'.url('blog/'.$datetime.'/comment/'.$comment->id.'/edit#comments').'">';
									echo svgicon('manage') . $lang['pages']['blog']['comment']['options']['manage'];
								echo '</a>';
							}
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}
		}
	echo '</div>';

?>
