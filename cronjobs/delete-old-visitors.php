<?php

	require_once '/path/to/folder/site-settings.php';

	foreach(glob($dir_files.'/visitors/*.json') AS $file) {
		$exif = json_decode(file_get_contents($file), false);

		if((time() - $exif->occurred) > 21600) {
			unlink($file);
		}
	}

?>
