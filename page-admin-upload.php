<?php

	# Kolla om bilden redan finns på servern, genom att kolla innehållet på bilden

	use Intervention\Image\ImageManager;
	use Intervention\Image\Drivers\Imagick\Driver;



	if(isset($_POST['upload'])) {

		require_once 'site-settings.php';

		$uniqueid = uniqid();

		$file_tmp = $_FILES['file']['tmp_name'];
		$file_name = $_FILES['file']['name'];
		$file_info = pathinfo($file_name);

		$file_name_old = $dir_files.'/uploads/'.$file_name;
		$file_name_new = $dir_files.'/uploads/'.$uniqueid.'.webp';
		$file_name_thumb = $dir_files.'/uploads/'.$uniqueid.'-thumb.webp';
		$file_name_thumb_list = $dir_files.'/uploads/'.$uniqueid.'-thumb-list.webp';

		$file_name_gpx = $dir_files.'/tracks/'.$file_name;

		$file_name_mp4 = $dir_files.'/videos/'.$file_name;


		if(empty($file_tmp)) {
			die(simplepage('Please select a file first. <a href="'.url('upload').'">Go back</a>'));

		} elseif($file_info['extension'] == 'jpg' AND !move_uploaded_file($file_tmp, $file_name_old) OR
				 $file_info['extension'] == 'gpx' AND !move_uploaded_file($file_tmp, $file_name_gpx) OR
				 $file_info['extension'] == 'mp4' AND !move_uploaded_file($file_tmp, $file_name_mp4)) {
			die(simplepage('The file has not been uploaded. Please try again. <a href="'.url('upload').'">Go back</a>'));

		} else {
			if($file_info['extension'] == 'jpg') {
				$manager = new ImageManager(['driver' => 'imagick']);

				$manager->make($file_name_old)->resize(2000, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->encode('webp', 70)->save($file_name_new);

				$manager->make($file_name_new)->resize(800, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->save($file_name_thumb);

				$manager->make($file_name_new)->resize(50, null, function($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->save($file_name_thumb_list);

				$img = new Imagick($file_name_new);
				$img->stripImage();
				$img->writeImage($file_name_new);
				/*$img->setImageProperty('keywords', 'airikr');
				die($img->getImageProperty('keywords'));*/
				$img->clear();
				$img->destroy();

				unlink($file_name_old);
			}


			header("Location: ".url('admin/uploads'));
			exit;
		}



	} else {

		require_once 'site-header.php';







		if($is_loggedin == false) {
			header("Location: ".url('admin?prev=upload'));
			exit;



		} else {
			echo '<section id="upload">';
				echo '<h1>';
					echo '<a href="'.url('uploads').'">';
						echo svgicon('goback');
					echo '</a>';
					
					echo $lang['pages']['upload']['title'];
				echo '</h1>';

				echo '<form action="'.url('admin/upload').'" method="POST" enctype="multipart/form-data">';
					echo '<div class="item">';
						echo '<div class="label">';
							echo $lang['pages']['upload']['choose-image'];
						echo '</div>';

						echo '<div class="value">';
							echo '<input type="file" name="file" accept=".webp,.jpg,.gpx,.mp4">';
						echo '</div>';
					echo '</div>';



					echo '<div class="button">';
						echo '<input type="submit" name="upload" value="'.$lang['pages']['upload']['button'].'">';
					echo '</div>';
				echo '</form>';
			echo '</section>';
		}







		require_once 'site-footer.php';

	}

?>
