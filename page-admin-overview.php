<?php

	require_once 'site-header.php';


	$data = json_decode(file_get_contents($dir_files.'/stats.json'), true);







	echo '<section id="overview">';
		echo '<h1>Översikt</h1>';


		echo '<div class="quick">';

			echo '<div class="item">';
				echo '<div class="value">';
					echo $data['blog']['posts']['total'];
				echo '</div>';

				echo '<div class="label">';
					echo 'Blogginlägg';
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="value">';
					echo $data['gallery']['photos'];
				echo '</div>';

				echo '<div class="label">';
					echo 'Bilder';
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="value">';
					echo '<a href="'.url('admin/filter:guestbook').'">';
						echo $data['guestbook']['total'];
					echo '</a>';

					echo ($data['guestbook']['waiting_for_review'] == 0 ? '' : ' *');
				echo '</div>';

				echo '<div class="label">';
					echo 'Gästboksinlägg';
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="value">';
					echo $data['bicycle']['count'];
				echo '</div>';

				echo '<div class="label">';
					echo 'GPX-spår';
				echo '</div>';
			echo '</div>';

		echo '</div>';



		echo '<div class="details">';

			echo '<h2>Blogg</h2>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Reaktioner';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo '<a href="'.url('admin/filter:reactions').'">';
						echo format_number($data['blog']['reactions']);
					echo '</a>';
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="sub no-select">';
					echo 'Ͱ';
				echo '</div>';

				echo '<div class="label">';
					echo 'Genomsnittligt per dag';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['blog']['average']['reactions']['per_day']);
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="sub no-select">';
					echo 'Ͱ';
				echo '</div>';

				echo '<div class="label">';
					echo 'Genomsnittligt per vecka';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['blog']['average']['reactions']['per_week']);
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="sub no-select">';
					echo 'Ͱ';
				echo '</div>';

				echo '<div class="label">';
					echo 'Genomsnittligt per månad';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['blog']['average']['reactions']['per_month']);
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="sub end no-select">';
					echo '˪';
				echo '</div>';

				echo '<div class="label">';
					echo 'Genomsnittligt per år';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['blog']['average']['reactions']['per_year']);
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo '100 Days To Offload';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo '<a href="'.url('blog/100daystooffload').'">';
						echo format_number($data['blog']['posts']['hundreddaystooffload']['yes']);
					echo '</a>';
				echo '</div>';
			echo '</div>';



			echo '<h2>Besökare</h2>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Förbigången';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['visitors']['bypassed']);
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Verifierade';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['visitors']['verified']);
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Mest sannolika proxy';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['visitors']['most-likely-proxy']);
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Låg risk';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['visitors']['low-risk']);
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo '50/50';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['visitors']['fifty-fifty']);
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Ingen risk';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['visitors']['no-risk']);
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Behöver kollas';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['visitors']['needs-checking']);
				echo '</div>';
			echo '</div>';

			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Dålig IP';
				echo '</div>';

				echo '<div class="line"></div>';

				echo '<div class="value">';
					echo format_number($data['visitors']['bad-ip']);
				echo '</div>';
			echo '</div>';

		echo '</div>';



		echo '<div class="api">';
			echo date_(filemtime($dir_files.'/stats.json'), 'datetime');
			echo '<a href="'.url('api/update').'">Uppdatera</a>';
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
